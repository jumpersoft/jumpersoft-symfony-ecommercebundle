<?php

namespace Jumpersoft\EcommerceBundle\Controller\Store;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\EcommerceBundle\Validators;
use Jumpersoft\EcommerceBundle\DependencyInjection\StoreActions;

class StorePublicCategoryController extends BaseController
{

    /**
     * DESC: Servira para traer la info de cualquier categoría, de productos, empresa, ayuda, etc
     *
     * TODO: Falta depurar que debe traer de acuerdo al tipo de categoría,
     * por ejemplo solo para la de productos debe traer la info de items, attributes, bestSeller, para las otras no debería
     *
     * @Route("/store/cat/{type}/{id}", requirements={"id"="^(.{20})$"})
     */
    public function getStoreCategoryAction($type, $id, Request $request)
    {
        $filters = $request->get("filters");
        $data = [
            "breadCrumbs" => ($parents = $this->em->getRepository("E:Store")->getBreadCrumbs($this->defaultStoreId, $id)),
            "category" => $this->em->getRepository("E:Store")->getCategoryDetail($this->defaultStoreId, $id, $parents)
            ];

        if ($type == "item") {
            // CATEGORIES
            $cats = $this->em->getRepository('E:StoreView')->getStoreViewCategories(null, $id);
            // PRODUCTS IN CATEGORIES
            $items = $this->em->getRepository('E:StoreView')->getStoreViewCategoriesItems(null, $id);
            // FINAL ARRAY
            $data = array_merge($data, [
                "showCase" => array_merge($cats, $items),
                "items" => [],
                "attributes" => [
                    "price" => [],
                    "sizing" => [],
                    "color" => [],
                    "brand" => []
                ],
                "images" => [],
                "bestSeller" => []
            ]);
        }

        $this->jsReturn["data"] = $data;
        return $this->responseJson();
    }
}
