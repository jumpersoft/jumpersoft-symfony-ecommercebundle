<?php

namespace Jumpersoft\EcommerceBundle\Controller\Store;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\EcommerceBundle\Validators;
use Jumpersoft\EcommerceBundle\DependencyInjection\StoreActions;
use Symfony\Component\HttpFoundation\IpUtils;

class StorePublicItemController extends BaseController
{

    /**
     * @Route("/store/i/comments/{id}", name="apiGetStoreItem", condition="request.isXmlHttpRequest()", requirements={"id"="^(.{20})$"})
     * TODO DEJAR ANTES QUE LA RUTA PARA TRAER EL PRODUCTO PUES SE DEBE ARREGLAR LA RUTA PARA EL PRODUCTO PUES ENTRA ANTES QUE ESTA RUTA
     */
    public function getItemCommentsAction($id)
    {
        //TODO
        // 1 - Revisar tablas para almacenar comentarios
        // 2 - Consultas para traer comentarios y editar
        // 2.1 Se debe solo traer paginados los comentarios, al ver las respuetas estas deben traerse dinámicamente o en su caso ver la forma de que traiga un número
        //     limitado de hijos y si cuenta con más traerlos a petición con un botón que diga mostrar más
        // 2.2 Controles y validadores para crearlos
        // 3 - En el panel de admin mostrarlos y aprobarlos
        $this->jsReturn["data"] = [
            ["name" => "Jhon Doe", "date" => "Fab 11, 2016", "comment" => "Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac.", "children" => [
                    ["name" => "Jhon Doe Child", "date" => "Fab 12, 2016", "comment" => "Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac. 2"],
                    ["name" => "Jhon Doe Child Son", "date" => "Fab 12, 2016", "comment" => "Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac. 3", "children" => [
                            ["name" => "Jhon Doe Child Son of Son", "date" => "Fab 13, 2016", "comment" => "Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac. 4"]
                        ]]
                ]]
        ];

        return $this->responseJson();
    }

    /**
     * @Route("/store/i/{urlKey}", condition="request.isXmlHttpRequest()", requirements={"urlKey"="^\S+\w{1,255}\S{1,}$"})
     */
    public function getItemAction(Request $request, $urlKey, StoreActions $storeActions)
    {
        if ($urlKey) {
            $filters = ['order' => 'asc', 'start' => '0', 'length' => '20'];
            //Set item data
            $this->jsReturn["data"] = [
                "item" => $item = $this->em->getRepository('E:Store')->getItem($urlKey),
                "relatedProducts" => $this->em->getRepository('E:Store')->getItems($filters, ['crossSell' => true, 'id' => $item["id"], 'storeId' => $this->defaultStoreId]),
                "breadCrumbs" => !empty($item["categoryId"]) ? $this->em->getRepository("E:Store")->getBreadCrumbs($this->defaultStoreId, $item["categoryId"]) : ''
            ];
            //Store click
            $storeActions->setWebHit($item, $request);
        } else {
            throw new \Exception($result->msg ? $result->msg : "El producto no existe");
        }
        return $this->responseJson();
    }

    /**
     * TODO: FALTA REVISAR Y ADECUAR CON VUE
     * @Route("/api/store/search", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function storeSearch(Request $request)
    {
        $this->getFilters($request);
        $params = [
            'categoryRef' => $request->get('categoryRef'),
            'q' => $request->get('q'),
            "storeId" => $this->getDefaultStoreId($this->getUser()),
            "getItemsInChildren" => true
        ];
        $this->jsReturn["data"] = $this->em->getRepository('E:Store')->getItems($this->jsReturn, $params);

        //Search for text-input-autocomplete
        $this->jsReturn["status"] = true;
        $this->jsReturn["error"] = null;
        return $this->responseJson();
    }

    /**
     * TODO: FALTA REVISAR Y ADECUAR CON VUE
     * @Route("/api/store/items", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getItems(Request $request)
    {
        $this->getFilters($request);
        $params = [
            'categoryRef' => $request->get('categoryRef'),
            'q' => $request->get('q'), //-> Work for search functions
            'getItemsInChildren' => $request->get('getItemsInChildren') ? $request->get('getItemsInChildren') : false,
            'storeId' => $this->getDefaultStoreId($this->getUser())
        ];
        $this->jsReturn["data"] = $this->em->getRepository('E:Store')->getItems($this->jsReturn, $params);
        return $this->responseJson();
    }
}
