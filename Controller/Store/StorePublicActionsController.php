<?php
namespace Jumpersoft\EcommerceBundle\Controller\Store;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\DependencyInjection\PaymentGateway;
use Jumpersoft\EcommerceBundle\Entity\OrderRecord;
use Jumpersoft\EcommerceBundle\Entity\OrderRecordItem;
use Jumpersoft\EcommerceBundle\Entity\Transaction;
use Jumpersoft\EcommerceBundle\Entity\TransactionException;
use Jumpersoft\EcommerceBundle\Entity\UserCard;
use Jumpersoft\EcommerceBundle\DependencyInjection\ShoppingCartEngine;
use Jumpersoft\EcommerceBundle\Validators;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Route("/store", condition="request.isXmlHttpRequest()")
 */
class StorePublicActionsController extends BaseController
{

    /**
     * @Route("/initData", methods={"GET"})
     */
    public function storeInitData(ShoppingCartEngine $objCart, PaymentGateway $objGateway)
    {
        // USER DATA
        $user = is_object($usr = $this->getUser()) ? ["name" => $usr->getName(), "roles" => $usr->getRoles()] : [];
        // CART DATA
        $cart = $objCart->getCartInfo(is_object($usr) ? $usr->getId() : null);
        // STORE DATA
        $store = $this->em->getRepository('E:Store')->getStoreData($this->defaultStoreId);
        $store["payments"] = ['merchantId' => $objGateway->getMerchantId(), 'publicKey' => $objGateway->getPublicKey()];
        // MENUS
        $menus = $this->em->getRepository('E:StoreView')->getStoreViewMenus($this->defaultStoreId);
        // CATEGORIES
        $cats = $this->em->getRepository('E:StoreView')->getStoreViewCategories($this->defaultStoreId);
        // PRODUCTS IN CATEGORIES
        $items = $this->em->getRepository('E:StoreView')->getStoreViewCategoriesItems($this->defaultStoreId);
        //TESTIMONIALS
        $misc = $this->em->getRepository("E:StoreView")->getMiscellaneous($this->defaultStoreId);
        //ALL TYPES HERE
        $types = $this->em->createQuery("select t.id as value, t.name as text from E:Type t where t.active = 1 order by t.sequence")->getResult();

        $this->jsReturn["data"] = [
            "user" => $user,
            "cart" => $cart,
            "menu" => $menus,
            "cats" => $cats,
            "items" => $items,
            "misc" => $misc,
            "store" => $store,
            "catalog" => [
                "paymentMethods" => $this->em->getRepository('E:OrderRecord')->getPaymentMethodsToCombo(),
                "shippingMethods" => array_values(array_filter($types, fn($d) => strrpos($d["value"], "SHI_MET") === 0))
            ],
            "validators" => array_merge(
                Validators\AddressValidator::getValidators("address"),
                Validators\UserValidator::getValidators("login"),
                Validators\StoreUserValidator::getValidators("storeUserProfile", "storeUserSignUp", "storeRecoverAccount", "storeResetPassword", "creditCard"),
                Validators\ContactMessageValidator::getValidators("contactMessageValidator"),
            ),
            "system" => [
                "serverStatus" => $this->em->getRepository("E:Parameters")->findOneById("SERVER_STATUS")->getValue()
            ]
        ];
        return $this->responseJson();
    }

    /**
     * @Route("/userData", methods={"GET"})
     */
    public function storeUserData(ShoppingCartEngine $objCart)
    {
        $user = is_object($usr = $this->getUser()) ? ["fullName" => $usr->getFullName(), "role" => $usr->getRole()] : [];
        $cart = $objCart->getCartInfo(is_object($usr) ? $usr->getId() : null);
        $this->jsReturn["data"] = ["user" => $user, "cart" => $cart];
        return $this->responseJson();
    }

    /**
     * @Route("/cart", methods={"POST"})
     * @JsResponse(transaction="true")
     */
    public function cartUpdateAction(Request $request, ShoppingCartEngine $objCart)
    {
        $cookies = null;
        @['action' => $action, 'id' => $id, 'quantity' => $quantity, 'couponCode' => $promotionCode] = $request->request->all();
        $user = $this->getUser();
        $result = $objCart->updateCart($id, $action, $quantity, $promotionCode, is_object($user) ? $user->getId() : null);
        if ($result->success) {
            $cartInfoJs = $result->cartJSON;
            $cookies = $result->createNewCookie ? [['id' => $result->cookieName, 'val' => $result->cookieId, 'exp' => $result->cookieExpiration]] : null;
            switch ($action) {
                case "add":
                    $this->jsReturn["msg"] = "Producto añadido al carrito";
                    break;
                case "remove":
                    $this->jsReturn["msg"] = "Producto eliminado del carrito";
                    break;
                case "empty":
                    $this->jsReturn["msg"] = "Carrito vaciado";
                    break;
                case "applyCoupon":
                    $this->jsReturn["msg"] = "Cupón aplicado";
                    break;
                case "removeCoupon":
                    $this->jsReturn["msg"] = "Cupón retirado del carrito";
                    break;
            }
            $this->jsReturn["cartInfo"] = $cartInfoJs;
            $this->jsReturn["id"] = $id;
        } else {
            throw new \Exception($result->msg ? $result->msg : "Error al actualizar el carrito");
        }
        return $this->responseJson(null, $cookies, $result->removeCookie ? ['shopcartId'] : null);
    }

    /**
     * @Route("/contactMessage", methods={"PUT"})
     * @JsResponse(transaction="true")
     */
    public function contactMessageAction(Request $request)
    {
        // Build POST request:
        $data = $request->request->all();
        $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
        $recaptcha_secret = '6LfkdbsZAAAAAEsiHU8nDSYaobqzlwDjDn7pcLyd'; /* LLAVE PRIVADA */
        // Make and decode POST request:
        $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $data["token"]);
        $recaptcha = json_decode($recaptcha);
        // Take action based on the score returned:
        if ($recaptcha->score >= 0.5) {
            $values = Validators\ContactMessageValidator::proccessInputValues($data, Validators\ContactMessageValidator::$contactMessageValidator);
            if (Validators\ContactMessageValidator::validate(Validators\ContactMessageValidator::$contactMessageValidator, $values)) {
                $message = new Entity\ContactMessage();
                $this->jsUtil->setValuesToEntity($message, array_merge($values, [
                    "id" => $this->jsUtil->getUid(),
                    "registerDate" => $this->jsUtil->getLocalDateTime(),
                ]));
                $this->em->persist($message);
            } else {
                throw new \Exception($this->msg["errorForm"]);
            }
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
        $this->jsReturn["msg"] = "El mensaje ha sido recibido, te contactaremos a la brevedad";
        return $this->responseJson();
    }

    /**
     * @Route("/lastViews", methods={"GET"})
     * TODO: Agregar tablas nuevas para guardar el historial del usuario, actualmente solo se guardan en cookies, pero falta guardar esa info por cada usuario logeado
     */
    public function lastViewsAction(Request $request)
    {
        $filters = ['order' => 'asc', 'start' => '0', 'length' => '18'];
        $this->jsReturn["data"] = $this->em->getRepository('E:Store')->getLastViewedItems($filters, ["storeId" => $this->defaultStoreId, "itemIds" => $request->get("ids")]);
        return $this->responseJson();
    }

    /**
     * @Route("/search/{q}", methods={"GET"})
     */
    public function searchAction($q)
    {
        $filters = ['order' => 'asc', 'start' => '0', 'length' => '18'];
        if (!empty($q)) {
            $this->jsReturn["data"] = $this->em->getRepository('E:Store')->getSearchResult($filters, $q);
        }
        return $this->responseJson();
    }
}
