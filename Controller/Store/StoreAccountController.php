<?php
namespace Jumpersoft\EcommerceBundle\Controller\Store;

use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\EcommerceBundle\Validators;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class StoreAccountController extends BaseController
{

    /**
     * @Route("/api/store/account/resume", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function storeAccountResumeAction(Request $request)
    {
        $user = $this->em->getRepository('E:Store')->getCustomerResume($this->getUser()->getId());
        $this->jsReturn["user"] = $user;
        return $this->responseJson();
    }

    /**
     * @Route("/api/store/account/order", name="getStoreAccountOrders", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getStoreOrdersAction(Request $request)
    {
        $this->getFilters($request);
        $user = $this->getUser();
        $this->jsReturn["data"] = $this->em->getRepository('E:OrderRecord')->getStoreOrders($this->jsReturn, $user->getId(), $this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/store/account/order/{id}", name="getStoreAccountOrder", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getStoreOrderAction($id)
    {
        $this->jsReturn["order"] = $this->em->getRepository('E:OrderRecord')->getStoreOrderRecord($id, $this->defaultStoreId, $this->getUser()->getId());
        return $this->responseJson();
    }

    /**
     * @Route("/api/store/account/suscriptions", name="getStoreAccountSuscriptions", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getStoreAccountSuscriptions()
    {
        $userId = $this->getUser()->getId();
        $row = $this->em->getRepository('E:Store')->getCustomerSuscriptions($userId);
        $this->jsReturn["data"] = $row;
        return $this->responseJson();
    }

    /**
     * @Route("/api/store/account/suscriptions", name ="storeAccountSuscriptionsEdit", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function storeAccountSuscriptionsEdit(Request $request)
    {
        $valInput = $request->request->get('suscriptions');
        $values = Validators\StoreUserValidator::proccessInputValues($valInput, Validators\StoreUserValidator::$suscriptionsValidators);

        if (Validators\StoreUserValidator::validate(Validators\StoreUserValidator::$suscriptionsValidators, $values)) {
            $user = $this->getUser();
            $suscription = $this->em->getRepository('E:UserSuscription')->find($user->getId());
            if ($suscription == null) {
                $suscription = new UserSuscription();
                $suscription->setUser($user);
            }
            $this->jsUtil->setValuesToEntity($suscription, $values);
            $this->em->persist($suscription);
            $this->jsReturn["msg"] = "Suscripciones guardadas con éxito";
            return $this->responseJson();
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
    }

    /**
     * @Route("/api/store/account/downloads", name="getStoreAccountDownloads", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getStoreAccountDownloads(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Store')->getCustomerDownloads($this->jsReturn, $this->getUser());
        return $this->responseJson();
    }

    /**
     * @Route("/api/store/wishlist", name="getStoreWishlistItems", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getStoreWishlistItems(Request $request)
    {
        $this->getFilters($request);
        $user = $this->getUser();
        $id = $request->cookies->get("wishlistId");
        $this->jsReturn["data"] = $this->em->getRepository('E:Wishlist')->getStoreWishlistItems($this->jsReturn, $id, $user);
        return $this->responseJson();
    }

    /**
     * @Route("/api/store/wishlist", name ="putStoreWishlistUpdate", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putStoreWishlistUpdate(Request $request)
    {
        $user = $this->getUser();
        $wishlistId = $request->cookies->get("wishlistId");
        $itemId = $request->get('id');
        $action = $request->get('action');
        $cookies = $removeCookies = null;

        //$wl = $this->em->getRepository('E:Wishlist')->findOneBy(["user" => $user, "item" => $this->em->getReference('E:Item', $id)]);
        $wl = $this->em->getRepository('E:Wishlist')->getStoreWishlist($wishlistId, $user);
        if ($action == "add") {
            if ($wl == null) {
                $wl = new Entity\Wishlist();
                $wl->setId($this->jsUtil->getUid());
                $cookies = [['id' => "wishlistId", 'val' => $wl->getId(), 'exp' => (new \DateTime('+30 days'))->getTimestamp()]];
                $wl->setUser($user);
                $wl->setRegisterDate($this->jsUtil->getLocalDateTime());
                $this->em->persist($wl);
            }
            if (count(current($wl->getItems()->filter(function ($i) use ($itemId) {
                            return $i->getItem()->getId() == $itemId;
                        }))) == 0) {
                $wlItem = new Entity\WishlistItem();
                $wlItem->setId($this->jsUtil->getUid());
                $wlItem->setWishlist($this->em->getReference('E:Wishlist', $wl->getId()));
                $wlItem->setItem($this->em->getReference('E:Item', $itemId));
                $wl->addItem($wlItem);
                $this->em->persist($wlItem);
                $this->jsReturn["msg"] = "Producto añadido con éxito";
            } else {
                throw new \Exception("El producto ya está en su lista de deseados");
            }
        } elseif ($action == "remove") {
            if (count($item = current($wl->getItems()->filter(function ($i) use ($itemId) {
                        return $i->getItem()->getId() == $itemId;
                    }))) > 0) {
                $item = reset($item);
                $this->em->remove($item);
                $this->em->flush();
                if ($wl->getItems()->count() == 0 && !is_object($user)) {
                    $removeCookies = ["wishlistId"];
                }
            }
            $this->jsReturn["msg"] = "Producto eliminado de la lista";
        }
        return $this->responseJson(null, $cookies, $removeCookies);
    }

    /**
     * @Route("/api/store/comparison", name="getStoreComparisonItems", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getStoreComparisonItems(Request $request)
    {
        $this->getFilters($request);
        $user = $this->getUser();
        $id = $request->cookies->get("comparisonId");
        $this->jsReturn["data"] = $this->em->getRepository('E:Comparison')->getStoreComparisonItems($this->jsReturn, $id, $user);
        return $this->responseJson();
    }

    /**
     * @Route("/api/store/comparison", name ="putStoreComparisonUpdate", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putStoreComparisonUpdate(Request $request)
    {
        $user = $this->getUser();
        $comparisonId = $request->cookies->get("comparisonId");
        $itemId = $request->get('id');
        $action = $request->get('action');
        $cookies = $removeCookies = null;

        //$wl = $this->em->getRepository('E:Comparison')->findOneBy(["user" => $user, "item" => $this->em->getReference('E:Item', $id)]);
        $wl = $this->em->getRepository('E:Comparison')->getStoreComparison($comparisonId, $user);
        if ($action == "add") {
            if ($wl == null) {
                $wl = new Entity\Comparison();
                $wl->setId($this->jsUtil->getUid());
                $cookies = [['id' => "comparisonId", 'val' => $wl->getId(), 'exp' => (new \DateTime('+30 days'))->getTimestamp()]];
                $wl->setUser($user);
                $wl->setRegisterDate($this->jsUtil->getLocalDateTime());
                $this->em->persist($wl);
                $this->jsReturn["msg"] = "Producto añadido con éxito a la comparación";
            }
            if (count($wl->getItems()->filter(function ($i) use ($itemId) {
                        return $i->getItem()->getId() == $itemId;
                    })) == 0) {
                $wlItem = new Entity\ComparisonItem();
                $wlItem->setId($this->jsUtil->getUid());
                $wlItem->setComparison($this->em->getReference('E:Comparison', $wl->getId()));
                $wlItem->setItem($this->em->getReference('E:Item', $itemId));
                $wl->addItem($wlItem);
                $this->em->persist($wlItem);
                $this->jsReturn["msg"] = "Producto añadido con éxito";
            } else {
                throw new \Exception("El producto ya está en su lista de comparaciones");
            }
        } elseif ($action == "remove") {
            if (count($item = current($wl->getItems()->filter(function ($i) use ($itemId) {
                        return $i->getItem()->getId() == $itemId;
                    }))) > 0) {
                $item = reset($item);
                $this->em->remove($item);
                $this->em->flush();
                if ($wl->getItems()->count() == 0 && !is_object($user)) {
                    $removeCookies = ["comparisonId"];
                }
            }
            $this->jsReturn["msg"] = "Producto eliminado de la lista de comparaciones";
        }
        return $this->responseJson(null, $cookies, $removeCookies);
    }
}
