<?php

namespace Jumpersoft\EcommerceBundle\Controller\Store;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\DependencyInjection\StoreActions;
use Jumpersoft\EcommerceBundle\DependencyInjection\PaymentGateway;
use Jumpersoft\EcommerceBundle\Entity\OrderRecord;
use Jumpersoft\EcommerceBundle\Entity\OrderRecordItem;
use Jumpersoft\EcommerceBundle\Entity\Transaction;
use Jumpersoft\EcommerceBundle\Entity\TransactionException;
use Jumpersoft\EcommerceBundle\Entity\UserCard;
use Jumpersoft\EcommerceBundle\DependencyInjection\ShoppingCartEngine;
use Jumpersoft\EcommerceBundle\Validators;
use Jumpersoft\EcommerceBundle\Entity;

class StoreCheckoutController extends BaseController
{

    /**
     * @Route("/api/store/checkout/save-order", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function checkoutSaveOrderAction(Request $request, ShoppingCartEngine $objCart, PaymentGateway $objGateway)
    {
        $user = $this->getUser();
        $gatewayUserId = $user->getGatewayUserId();
        $cart = $objCart->getCartInfo($user->getId());
        $eventDate = $this->jsUtil->getLocalDateTime();
        $data = $request->request->all();

        //Get Payment Method
        $paymentMethodId = $objGateway->getPaymentMethodId($paymentMethod = $data['paymentMethodId']);
        $paymentMethodDesc = $objGateway->getPaymentMethodDesc($paymentMethod);
        $tokenId = $data["token_id"] ?? "";

        if (!empty($data["cardId"]) && ($card = $user->getCards()->filter(fn ($c) => $c->getId() === $data["cardId"])->first())) {
            $cardId = $card->getGatewayCardId();
        }

        /* START - STORE ORDERRECORD AND ITEMS */
        $orderRecord = new OrderRecord($orderRecordId = $this->jsUtil->getUid(), $eventDate);
        $this->jsUtil->setValuesToEntity($orderRecord, [
            "store" => $this->getDefaultStoreId($user),
            "customer" => $user,
            "date" => $eventDate,
            "paymentForm" => PaymentGateway::$PM_FORM_ONE_EXIBITION,
            "paymentMethod" => $paymentMethod,
            "shippingMethod" => $data["shippingMethodId"],
            //"shipmentDate" => null, // TODO STORE - FALTA SABER COMO SETEARSE AQUI LA FECHA APROXIMADA O SI NO HACER QUE SEA NULL Y PONER EL TIPO DE ENVIO EN OTRO CAMPO
            "shippingAddressType" => "ADR_OWN",
            "billingAddressType" => "ADR_OWN",
            "status" => "ORD_PEN",
            "subTotal" => $cart["subTotal"],
            "discount" => $cart["discount"],
            "total" => $cart["total"],
        ]);

        if (!empty($addressId = $data["addressId"])) {
            $orderRecord->setShippingAddress($this->em->getRepository('E:Address')->getAddress($addressId, $user->getId(), "view"));
        }
        if ($billingAddress = $user->getDefaultBillingAddress()) {
            $orderRecord->setBillingAddress($this->em->getRepository('E:Address')->getAddress($billingAddress, $user->getId(), "view"));
        }

        $orderRecord->SetFolio($this->em->getRepository('E:OrderRecord')->findNextFolio($orderRecord->getStore()->getId(), $orderRecord->getDate()->format('Ym')));
        $this->em->persist($orderRecord);
        // - SAVE ITEMS BY EACH 20 ITEMS IN ORDER TO NOT OVERFLOW BUFFER MEMORY
        $batchSize = 20;
        $max = sizeof($cart["items"]);
        $i = 0;
        foreach ($cart["items"] as $key => $ci) {
            $oItem = new OrderRecordItem();
            $this->jsUtil->setValuesToEntity($oItem, [
                "id" => $this->jsUtil->getUid(),
                "registerDate" => $eventDate,
                "orderRecord" => $orderRecord,
                "status" => 'ORI_FUL_SUC',
                "item" => $ci['id'],
                "regularPrice" => $ci['regularPrice'],
                "salePrice" => $ci['salePrice'],
                "discount" => $ci['discount'],
                //"discountType" => $ci['discountType'],
                "quantity" => $ci['quantity'],
                "subTotal" => ($total = $ci['quantity'] * $ci['salePrice']),
                "total" => $total,
                "currency" => "mxn",
                "priceType" => "PRI_BAS"
            ]);

            $this->em->persist($oItem);
            if (($i % $batchSize) === 0) {
                $this->em->flush();
            }
            $i++;
        }
        // - UPDATE CART STATUS
        $cartRow = $this->em->getRepository('E:ShoppingCart')->find($cart['id']);
        if ($cartRow) {
            $cartRow->setStatus($this->em->getReference('E:Status', 'SHC_IN_ORD'));
            $cartRow->setUpdateDate($orderRecord->getDate());
        }
        $this->em->flush(); // Flush orderrecord and items before complete transactión - if there is an error then the transaction does not complete
        /* END - STORE ORDERRECORD AND ITEMS */

        /*         * *        * */
        /* START CHARGE PROCESS IN GETAWAY - DEFAULT OPENPAY */

        // - TRANSACTION DATA TO GATEWAY
        $transactionData = [
            'method' => $paymentMethod,
            'amount' => (float) $cart["total"],
            'currency' => 'MXN',
            'description' => $paymentMethodDesc,
            'order_id' => $orderRecordId];

        // - TRANSACTION DATA - SET CUSTOMER DATA IF THIS HAS NOT A GATEWAYUSERID
        if (!$user->getGatewayUserId()) {
            $transactionData['customer'] = [
                'name' => $user->getName(),
                'last_name' => $user->getLastName(),
                'phone_number' => $user->getPhone(),
                'email' => $user->getEmail()];
        }

        // - TRANSACTION DATA - CARD PAYMENT COMPLEMENT - For card payments in one step or saved card
        if ($paymentMethod === PaymentGateway::$PM_CARD_ONE_STEP || $paymentMethod === PaymentGateway::$PM_CARD) {
            $transactionData = array_merge($transactionData, [
                'source_id' => $tokenId ? $tokenId : $cardId, /* Can be a token_id or a processorCardId stored */
                //'use_card_points' => false,
                'device_session_id' => $request->get("device_session_id")]);
        }

        // - MAKE CHARGE - PROCESS ORDER IN GATEWAY
        $paymentProcessResult = $objGateway->makeCharge($transactionData, $gatewayUserId);

        // - STORE TRANSACTION RESULTS
        if ($paymentProcessResult["success"]) {
            $tran = new Transaction($this->jsUtil->getUid(), $eventDate, $ip = $request->getClientIp(), $user);
            $this->jsUtil->setValuesToEntity($tran, [
                "operationType" => 'OPE_IN', // in default
                "transactionType" => 'TRA_CHARGE', // charge default
                "paymentMethod" => $paymentMethod,
                "orderRecord" => $orderRecordId,
                "gatewayTransactionId" => ($transactionId = $paymentProcessResult["result"]["id"]),
                "gatewayStatusId" => $paymentProcessResult["result"]["status"],
                "creationDate" => $paymentProcessResult["result"]["creation_date"],
                "operationDate" => $paymentProcessResult["result"]["operation_date"],
                "errorMessage" => $paymentProcessResult["result"]["error_message"],
                "gatewayResult" => $paymentProcessResult["result"],
                "event" => (PaymentGateway::$TR_PAYMENT_STATUS_IN_PROGRESS),
                    //"gatewayTransactionId" => $this->jsUtil->getUid(),
                    //"gatewayResult" => array('TEST'),
            ]);

            if ($paymentMethod === PaymentGateway::$PM_STORE) {
                $tran->setPaynetReferenceId($paymentReference = $paymentProcessResult["result"]["serializableData"]["payment_method"]["reference"]);
            } elseif ($paymentMethod === PaymentGateway::$PM_CARD && $paymentProcessResult["result"]["serializableData"]["card"]) {
                $tran->setCard($paymentProcessResult["result"]["serializableData"]["card"]);
            }
        } else {
            // TODO ESTO NUNCA SE EJECUTA PUES ESTA DENTRO DE UNA SOLA TRANSACCIÓN, VER COMO MANDAR A UNA TRANSACCIÓN APARTE
//            $tlog = new TransactionException($this->jsUtil->getUid(), $eventDate, $ip, $user);
//            $this->jsUtil->setValuesToEntity($tlog, [
//                "operationType" => $operationTypeId,
//                "paymentMethod" => $paymentMethodId,
//                "transactionType" => $transactionTypeId,
//                "event" => PaymentGateway::$ERROR_PAYMENTTRANSACTION,
//                "gatewayResult" => ($paymentProcessResult["message"] . " - " . $paymentProcessResult["jsonResult"]),
//            ]);
//            $this->em->persist($tlog);
//            $this->em->flush();
            throw new \Exception("Hubo un error en la transacción del pago");
        }
        $this->em->persist($tran);
        /* END CHARGE PROCESS IN GETAWAY - DEFAULT OPENPAY */
        /*         * *        * */

        /* UPDATE ORDERRECORD AND TRANSACTION RELATIONS */
        $this->jsUtil->setValuesToEntity($orderRecord, [
            "status" => $objGateway->getOrderStatusEquivalence($tran->getGatewayStatusId()), //Set order status acord to gateway status transaction
        ]);
        $this->em->persist($orderRecord);
        $this->em->flush();

        // RETURN RESULT
        $this->jsReturn["data"]["orderRecord"] = ["transactionId" => $transactionId, "id" => $orderRecord->getId(), "folio" => $orderRecord->getFolio(), "paymentReference" => ($paymentReference ?? "")];
        $this->jsReturn["data"]["cartInfo"] = $objCart->getCartInfo($user->getId());
        $this->jsReturn["msg"] = "Pedido procesado con éxito";

        //TODO SEND CONFIRMATION DATA TO CUSTOMER AND VEDOR EMAILS.


        return $this->responseJson();
    }

    /**
     * @Route("/api/store/checkout/card", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getCards(Request $request)
    {
        $user = $this->getUser();
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:User')->getCards($this->jsReturn, $user->getId());
        return $this->responseJson();
    }

    /**
     * @Route("/api/store/checkout/card", name="storeCardSave", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true")
     */
    public function cardSaveAction(Request $request, PaymentGateway $objGateway)
    {
        $user = $this->getUser();
        $errMsg = "Error al agregar la tarjeta, consulte con el administrador";
        $token = $request->get('token');
        $id = null;

        //IF CUSTOMER GATEWAY DOESN'T EXIST WE CREATE IT AND SAVE ID IN DB
        if (!$gatewayUserId = $user->getGatewayUserId()) {
            $result = $objGateway->add("customer", ["name" => $user->getFullName(), "email" => $user->getEmail()]);
            if ($result["result"]->id) {
                $user->setGatewayUserId($gatewayUserId = $result["result"]->id);
                $this->em->persist($user);
                $this->em->flush();
            } else {
                throw new \Exception($errMsg);
            }
        }

        //CREATE TC IN GATEWAY FOR CUSTOMER AND SAVE IN DB
        $result = $objGateway->add("card", $token, $gatewayUserId);
        if ($result["result"] && $result["success"] && $result["result"]->id && $result["result"]->brand) {
            $card = $result["result"]->serializableData;
            $objCard = new UserCard(
                $id = $this->jsUtil->getUid(),
                $result["result"]->brand,
                $result["result"]->type,
                $card["card_number"],
                $card["holder_name"],
                $card["expiration_year"],
                $card["expiration_month"],
                $result["result"]->creation_date,
                $user,
                $result["result"]->id,
                false
            );
            $this->em->persist($objCard);
            $this->em->flush();
            $this->jsReturn["msg"] = "Tarjeta agregada correctamente";
            $this->jsReturn["success"] = true;
            $this->jsReturn["id"] = $id;
        } else {
            throw new \Exception($result["message"] ?? $errMsg);
        }
        return $this->responseJson();
    }

    /**
     * @Route("/api/store/checkout/card/{id}", name="storeCardRemove", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function cardRemoveAction($id, Request $request, PaymentGateway $objGateway)
    {
        $user = $this->getUser();
        if ($card = $user->getCards()->filter(fn ($c) => $c->getId() === $id)->first()) {
            if ($user->getGatewayUserId() && $card->getGatewayCardId()) {
                $result = $objGateway->delete("card", $user->getGatewayUserId(), $card->getGatewayCardId());
            } else {
                throw new \Exception("Error al eliminar la tarjeta, consulte con el administrador");
            }
            $obj = $this->em->getRepository('E:User')->getCard($user->getId(), $card->getId(), "delete");
            $this->em->remove($obj);
            $this->jsReturn["msg"] = "Tarjeta eliminada correctamente";
            $this->jsReturn["success"] = true;
        } else {
            throw new \Exception("Error al tratar de borrar la tarjeta");
        }

        return $this->responseJson();
    }

    /**
     * @Route("/store/checkout/receipt", name="storeCheckoutReceiptPdf")
     */
    public function checkoutReceiptPdfAction()
    {
        return $this->redirect('https://sandbox-dashboard.openpay.mx/paynet-pdf/mzdtln0bmtms6o3kck8f/000020TRT3PGJAWHQHPERTJOQJ0005006');
    }
}
