<?php
namespace Jumpersoft\EcommerceBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftMailerExtension;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\EcommerceBundle\DependencyInjection\ShoppingCartEngine;
use Jumpersoft\EcommerceBundle\DependencyInjection\StoreActions;
use Jumpersoft\EcommerceBundle\Validators\StoreUserValidator;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;

/**
 * Description of AuthController
 *
 * @author Angel
 */
class AuthController extends BaseController
{

    /**
     * @Route("/login/check", condition="request.isXmlHttpRequest()")
     * @Route("/store/login_check", condition="request.isXmlHttpRequest()", methods={"POST"})
     */
    public function loginCheck(Request $request, ShoppingCartEngine $objCart, StoreActions $objStore, CsrfTokenManagerInterface $csrfProvider, UserPasswordEncoderInterface $passwordEncoder, JWTEncoderInterface $jwtEncoder)
    {
        $this->jsReturn["msg"] = 'Cuenta invalida';
        $response = new JsonResponse($this->jsReturn, 401);

        $username = $request->get('username');
        $password = $request->get('password');

        if (empty($username)) {
            $parameters = json_decode($request->getContent(), true);
            $username = $parameters['username'] ?? "";
            $password = $parameters['password'] ?? "";
        }

        $user = $this->getDoctrine()->getRepository('E:User')->findOneBy(['username' => $username]);

        if (!$user) {
            return $response;
        }

        // password check
        if (!$passwordEncoder->isPasswordValid($user, $password)) {
            return $response;
        }

        //Set user name
        $payload = ['username' => $user->getUsername()];
        //Set expiration time data
        $token_ttl = $this->params->get('jumpersoft.security.jwt_token_ttl');
        $payload['exp'] = ($tokenExpiration = new \DateTime('+' . $token_ttl . ' second'))->getTimestamp(); //Aqui debe traer el tiempo de token_ttl
        //Set ip data
        $payload['ip'] = $request->getClientIp();

        ///Make JWT token that hold only information about user name
        $token = $jwtEncoder->encode($payload);

        /* START EXTRA FUNCTIONS AFTER LOGIN */
        //
        //Merge Shopping Carts (Cart in loggof and current user cart if exist)
        if ($request->cookies->get("shopcartId")) {
            $resCart = $objCart->updateCart(null, "mergeCarts", null, null, $user->getId());
            if ($resCart->removeCookie) {
                $response->headers->clearCookie('shopcartId');
            }
        }
        //Merge wishlist and comparison list
        if ($ckWishlistId = $request->cookies->get("wishlistId")) {
            $res = $objStore->mergeWishlist($ckWishlistId, $user);
            if ($res->success) {
                $response->headers->clearCookie('wishlistId');
            }
        }
        if ($ckComparisonId = $request->cookies->get("comparisonId")) {
            $res = $objStore->mergeComparisonlist($ckComparisonId, $user);
            if ($res->success) {
                $response->headers->clearCookie('comparisonId');
            }
        }
        //
        /* END EXTRA FUNCTIONS AFTER LOGIN */

        $this->jsReturn["msg"] = "Acceso exitoso";
        if ($request->getPathInfo() == "/store/login_check") {
            $this->jsReturn["data"] = [
                "user" => ["username" => $user->getUserName(), "fullName" => $user->getFullName(), "role" => $user->getRole()],
                "cart" => $objCart->getCartInfo($user->getId())
            ];
        } else {
            $this->jsReturn["data"] = [
                "user" => ["username" => $user->getUserName(), "name" => $user->getFullName(), "role" => $user->getRole()]
            ];
        }

        $response->setStatusCode(200);
        $response->setData($this->jsReturn);
        //Set here if cookies is sent whit: secure, http_only flags or others
        $response->headers->setCookie(new Cookie('bearer', $token, $tokenExpiration, '/', null, true));
        $response->headers->setCookie(new Cookie('csrf', $csrfProvider->refreshToken($this->params->get('jumpersoft.security.token_key')), 0, '/', null, true));
        $response->headers->set('ses-exp', $payload["exp"]);
        $response->headers->set('serv-date', $this->jsUtil->getLocalDateTimeString());
        return $response;
    }

    /**
     * @Route("/api/panel/logout", condition="request.isXmlHttpRequest()")
     * @Route("/api/store/logout", condition="request.isXmlHttpRequest()")
     * @Route("/logout", name="logout")
     * @JsResponse(renewJwtToken="false")
     */
    public function logoutAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $url = $request->get("from");
            $url = $url ? $url : "/";
            $response = new RedirectResponse($url);
        } else {
            $response = new JsonResponse(['success' => 'true']);
        }

        $response->headers->clearCookie('bearer'); //We delete cookies here  because cookies work in http_only and can not delete in javascript.
        $response->headers->clearCookie('csrf');

        return $response;
    }

    /**
     * @Route("/login/resetRequest", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(transaction="true")
     */
    public function resetRequestAction(Request $request, JumpersoftMailerExtension $objMail)
    {
        $user = $this->em->getRepository('E:User')->findOneByUsername($email = $request->request->get("email"));
        if ($user) {
            $eventDate = $this->jsUtil->getLocalDateTime();
            $passwordRecoveryCode = md5(uniqid(rand(), true));
            $user->setPasswordRecoveryCode($passwordRecoveryCode);
            $user->setLastRequestPasswordRecoveryDate($eventDate = $this->jsUtil->getLocalDateTime());
//            $data = ["-link-" => $request->headers->get("origin") . "/login/reset/" . $passwordRecoveryCode, "-fecha-" => $eventDate->format("Y-m-d H:i:s")];
//            $srngEmail->sendNotification("", $email, "Sunergy - Información de recuperación de cuenta", null, $this->getParameter("snrg.auth.templateResetPasswordId"), $data, null, null, null, false);
            $message = $objMail->sendNotificationFromNoReply(
                $user->getEmail(),
                "Información de recuperacion de cuenta",
                '@notificationViews/resetPasswordRequest.html.twig',
                ['requestDate' => $eventDate->format("Y-m-d H:i:s"), 'link' => $request->headers->get("origin") . "/reiniciar-acceso/$passwordRecoveryCode"],
                true
            );
        } else {
            throw new \Exception("La cuenta de correo no esta registrada");
        }

        $this->jsReturn["msg"] = "Recuperación enviada con éxito, revisa tu correo y sigue las instrucciones";
        return $this->responseJson();
    }

    /**
     * @Route("/login/reset/{token}", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(transaction="true")
     */
    public function resetAction($token, Request $request, UserPasswordEncoderInterface $encoderPwd)
    {
        $eventDate = $this->jsUtil->getLocalDateTime();
        $data = $request->request->all();
        $values = StoreUserValidator::proccessInputValues($data, StoreUserValidator::$storeResetPassword);
        $user = $this->em->getRepository('E:User')->findOneByPasswordRecoveryCode($token);
        if ($user) {
            if (StoreUserValidator::validate(StoreUserValidator::$storeResetPassword, $values) && isset($token) && $user) {
                $password = $encoderPwd->encodePassword($user, $data['password']);
                $this->jsUtil->setValuesToEntity($user, [
                    "password" => $password,
                    "isAccountNonLocked" => true,
                    "isAccountNonExpired" => true,
                    "passwordRecoveryCode" => null,
                    "lastPasswordRecoveryDate" => $eventDate
                ]);
                $this->em->persist($user);
                $this->jsReturn["msg"] = 'Se ha reestablecido correctamente tu contraseña';
            } else {
                throw new \Exception($this->msg["errorForm"]);
            }
        } else {
            throw new \Exception("La liga de recuperación ya no es válida");
        }

        return $this->responseJson();
    }
}
