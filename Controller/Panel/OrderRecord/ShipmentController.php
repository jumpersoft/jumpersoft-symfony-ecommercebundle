<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\OrderRecord;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\EcommerceBundle\Validators;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class ShipmentController extends BaseController
{

    /**
     * @Route("/api/panel/shipment", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Shipment')->getShipments($this->jsReturn, $this->getUser()->getId(), $this->defaultStoreId, $this->isUser('ROLE_SELLER'));
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/shipment/{id}", condition="request.isXmlHttpRequest()", methods={"GET"}, requirements={"id"="^(.{20})$"})
     */
    public function getAction($id, Request $request)
    {
        $row = $this->em->getRepository('E:OrderRecord')->getOrderRecord($id, $this->getUser(), $this->defaultStoreId, "view");
        $this->jsReturn["data"] = $row;
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/shipment", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction(Request $request)
    {
        $customers = $request->request->get('customers');
        $scheduleCat = $this->em->getRepository("E:Shipment")->getSchedule();
        $eventDate = $this->jsUtil->getLocalDateTime();
        $user = $this->getUser();
        foreach ($customers as $ckey => $cData) {
            $i = 0;
            $shipmentDate = null;
            foreach ($cData["orders"] as $okey => $oData) {
                //GET AND CHECK ORDER STATUS
                if (($order = $this->em->getRepository("E:OrderRecord")->getOrderRecord($oData["id"], $user, $this->defaultStoreId, "update"))->getStatus()->getId() == "ORD_FUL_SUC") {
                    $customer = $order->getCustomer();
                    $shippingAddress = $order->getShippingAddress();
                    $shipmentDate = $shipmentDate ?? $order->getShipmentDate();

                    //SEARCH SCHEDULE TO ASSIGN SHIPMENT TYPE
                    if (!isset($typeId)) {
                        $key = array_search($shipmentDate->format("H-i"), array_column($scheduleCat, "time"));
                        $schedule = $key === 0 || $key > 0 ? $scheduleCat[$key] : null;
                    }

                    //ASIGN SHIPMENT TYPE AND METHOD
                    $typeId = $typeId ?? ($schedule ? "SHI_TYP_PRO" : "SHI_TYP_EXP");
                    $methodId = $typeId == "SHI_TYP_PRO" ? "SHI_MET_FRE" : "SHI_MET_FLA";

                    //GET SHIPMENT
                    if (!($shipment = $this->em->getRepository("E:Shipment")->getCurrentShipment($customer->getId(), $shipmentDate->format('Y-m-d H:i:s'), $shippingAddress["id"]))) {
                        //CREATE SHIPMENT
                        $shipment = new Entity\Shipment();
                        $this->jsUtil->setValuesToEntity($shipment, [
                            "id" => $this->jsUtil->getUid(),
                            "folio" => $this->em->getRepository("E:Shipment")->getNextFolio($shipmentDate),
                            "type" => $typeId,
                            "method" => $methodId,
                            "status" => "SHI_PEN",
                            "customer" => $customer,
                            "shippingAddress" => $shippingAddress,
                            "date" => $shipmentDate,
                            "schedule" => $schedule["id"] ?? null,
                            "notes" => $cData["notes"] ?? "",
                            "registerDate" => $eventDate
                        ]);
                        $this->em->persist($shipment);
                        $this->em->flush();
                    } else {
                        if ($shipment->getStatus()->getId() != "SHI_PEN") {
                            throw new \Exception("Ya hay un envío en curso para este cliente, fecha de envío y dirección: {$customer->getFullName()}, {$shipmentDate->format('Y-m-d H:i:s')}, 
                                    Calle: {$shippingAddress['streetAddress']},  Num Ext: {$shippingAddress['numExt']}, Ciudad: {$shippingAddress['city']}");
                        }
                    }

                    //CHECK ORDER DATETIME COINCIDENCE
                    if ($i == 0 || ($i > 0 && $order)) {
                        //UPDATE ORDER STATUS
                        $this->jsUtil->setValuesToEntity($order, ["status" => $statusId = "ORD_SHI_PEN"]);
                        $this->em->persist($order);

                        foreach ($oData["items"] as $ikey => $iData) {
                            //ADD SHIPMENT ITEM
                            if (($orItem = $this->em->getRepository("E:OrderRecordItem")->findOneBy(["id" => $iData["id"]]))->getStatus()->getId() == "ORI_FUL_SUC") {
                                $shipmentItem = new Entity\ShipmentItem();
                                $this->jsUtil->setValuesToEntity($shipmentItem, [
                                    "id" => $this->jsUtil->getUid(),
                                    "shipment" => $shipment,
                                    "orderRecordItem" => $orItem,
                                    "quantity" => $orItem->getQuantityFulFilled(),
                                    "status" => "SHI_PEN",
                                    "registerDate" => $eventDate
                                ]);
                                $this->em->persist($shipmentItem);

                                //UPDATE ORDER ITEM STATUS
                                $this->jsUtil->setValuesToEntity($orItem, ["status" => $orStatusId = "ORI_SHI_PEN"]);
                                $this->em->persist($orItem);

                                $customers[$ckey]["orders"][$okey]["items"][$ikey]["statusId"] = $orStatusId;
                                $iData["status"] = "Envío pendiente";
                            } else {
                                throw new \Exception("El producto << {$orItem->getItem()->getName()} >> no esta surtido y no puede prepararse para envío, pedido #{$order->getFolio()}, del cliente {$order->getCustomer()->getFullName()}, no tiene el estatus correcto para asignarse a un envío o ya esta asignado a uno");
                            }
                        }
                        //RETURN DATA - ORDER RECORD
                        $customers[$ckey]["orders"][$okey]["statusId"] = $statusId;
                        $customers[$ckey]["orders"][$okey]["status"] = "Envío pendiente";

                        $this->em->flush();
                    }

                    //Proccess Update
                    $proc = new Entity\ProccessStep($this->jsUtil->getUid(), $order, $this->em->getReference("E:Step", $statusId), null, $eventDate, $user);
                    $this->em->persist($proc);
                } else {
                    throw new \Exception("El pedido {$order->getFolio()}, del cliente {$order->getCustomer()->getFullName()}, no tiene el estatus correcto para asignarse a un envío o ya esta asignado a uno");
                }
            }
            //RETURN DATA - CUSTOMER
            $customers[$ckey]["shipmentFolio"] = $shipment->getFolio();
            $customers[$ckey]["shipmentId"] = $shipment->getId();
        }

        $this->jsReturn["data"] = $customers;
        $this->jsReturn["msg"] = "Envío creado con éxito";
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/shipment/{action}", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function postAction($action, Request $request)
    {
        $orderIds = $request->request->get('orderIds');
        $eventDate = $this->jsUtil->getLocalDateTime();
        $user = $this->getUser();

        switch ($action) {
            case "cancelShipmentPending":
            case "cancelShipmentPendingSelected":
                $currentStatusId = "ORD_SHI_PEN";
                $statusTarget = $this->em->getReference("E:Status", "ORD_FUL_SUC");
                $statusORITarget = $this->em->getReference("E:Status", "ORI_FUL_SUC");
                $shipmentsToDelete = [];
                $stepId = "ORD_SHI_PEN_CAN";
                break;
            case "shipmentInProgress":
            case "shipmentInProgressSelected":
                $currentStatusId = "ORD_SHI_PEN";
                $statusTarget = $this->em->getReference("E:Status", $stepId = "ORD_SHI_INP");
                $statusORITarget = $this->em->getReference("E:Status", "ORI_SHI_INP");
                break;
            case "cancelShipmentInProgress":
            case "cancelShipmentInProgressSelected":
                $currentStatusId = "ORD_SHI_INP";
                $statusTarget = $this->em->getReference("E:Status", "ORD_SHI_PEN");
                $statusORITarget = $this->em->getReference("E:Status", "ORI_SHI_PEN");
                $stepId = "ORD_SHI_INP_CAN";
                break;
        }
        foreach ($orderIds as $orderId) {
            $order = $this->em->getRepository("E:OrderRecord")->getOrderRecord($orderId, $user, $this->defaultStoreId, "update");
            //CHECK STATUS CHANGES RULES
            if ($order->getStatus()->getId() == $currentStatusId && $this->validateChangeStatus(Validators\OrderRecordValidator::$stepProcess, $currentStatusId, $statusTarget->getId())) {
                $oItems = $order->getItems();
                //REMOVE SHIPMENT ITEMS
                if (strpos($action, "cancelShipmentPending") === 0) {
                    foreach ($oItems as $oItem) {
                        $sItem = $this->em->getRepository("E:ShipmentItem")->findOneByOrderRecordItem($oItem);
                        !in_array($sItem->getShipment()->getId(), $shipmentsToDelete) ? array_push($shipmentsToDelete, $sItem->getShipment()->getId()) : null;
                        $this->em->remove($sItem);
                        $this->em->flush();
                    }
                }

                //CHANGE ORDERRECORD ITEM STATUS
                foreach ($oItems as $oItem) {
                    $oItem->setStatus($statusORITarget);
                    $this->em->persist($oItem);
                }

                //CHANGE ORDERRECORD STATUS
                $this->jsUtil->setValuesToEntity($order, ["status" => $statusTarget->getId()]);
                $this->em->persist($order);

                //Proccess Update
                $proc = new Entity\ProccessStep($this->jsUtil->getUid(), $order, $this->em->getReference("E:Step", $stepId), null, $eventDate, $user);
                $this->em->persist($proc);
            } else {
                throw new \Exception($this->msg["invalidStatus"]);
            }
        }
        //REMOVE SHIPMENT IF IT HAVEN'T ANY ITEMS
        if (isset($shipmentsToDelete)) {
            foreach ($shipmentsToDelete as $shipmentId) {
                if (($shipment = $this->em->getReference("E:Shipment", $shipmentId))->getItems()->count() == 0) {
                    $this->em->remove($shipment);
                }
            }
        }

        $this->jsReturn["msg"] = "Estatus cambiado con éxito";
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/shipment/print/{document}/{type}", methods={"POST"})
     */
    public function printHtml($document, $type, Request $request)
    {
        $user = $this->getUser();
        if ($data = $this->em->getRepository('E:OrderRecord')->getOrderRecord($orders = $request->request->get("orders"), $user, $this->defaultStoreId, "view")) {
            $company = $this->em->getRepository('E:Store')->getStore($this->defaultStoreId, $user->getId(), "view", $this->isUser('ROLE_SUPER_ADMIN'));
            $data = ["data" => is_array($orders) ? $data : [$data], "company" => $company];
            if ($request->getPathInfo() == "/api/panel/shipment/print/$document/pdf") {
                return $this->getReceiptPDF($data); //TODO PENDIENTE TRAERSE FUNCIONALIDAD PARA CREACIÓN DE PDFS
            } else {
                return $this->render("@documentViews/$document/index.html.twig", $data);
            }
        }
        throw new \Exception("No se encontraron los pedidos indicados");
    }

    /**
     * @Route("/api/panel/shipment/export/{format}", methods={"POST", "GET"}, requirements={"format"="csv|xls|xlsx"})
     */
    public function export($format = "csv", Request $request, SerializerInterface $serializer)
    {
        $this->getFilters($request);
        $data = $this->em->getRepository('E:OrderRecord')->getOrders($this->jsReturn, $this->getUser(), $this->defaultStoreId, "report", $request->request->get("orders"));
        if ($format == "xls" || $format == "xlsx") {
            $response = $this->responseXls($this->getOrdersXls($data), 'orders' . $this->jsUtil->getLocalDateTimeString("YmdHis") . '.xls');
            return $response;
        } else {
            return $this->responseFile($serializer->encode($data, 'csv'), 'text/csv', 'file' . $this->jsUtil->getLocalDateTimeString("YmdHis") . '.csv');
        }
    }

    public function getOrdersXls($data)
    {
        //TODO Falta agregar la variable para acceder a estos documentos de forma dinámica y se puedan usar los personalizados por el cliente
        $spreadsheet = IOFactory::load($this->params->get('kernel.project_dir') . "/src/Jumpersoft/EcommerceBundle/Resources/views/document/report/orders.xlsx");
        $worksheet = $spreadsheet->getActiveSheet();
        $count = count($data);
        $row = 9; // la fila de la templeta donde empieza la tabla
        for ($i = 0; $i < $count; $i++) {
            $worksheet->getCell("A$row")->setValue($i + 1);
            $worksheet->getCell("B$row")->setValue($data[$i]["folio"]);
            $worksheet->getCell("C$row")->setValue($data[$i]["date"]);
            $worksheet->getCell("D$row")->setValue($data[$i]["name"]);
            $worksheet->getCell("E$row")->setValue($data[$i]["status"]);
            $worksheet->getCell("F$row")->setValue($data[$i]["subTotal"]);
            $worksheet->getCell("G$row")->setValue($data[$i]["discount"]);
            $worksheet->getCell("H$row")->setValue($data[$i]["total"]);
            $row++;
        }

        return new Xls($spreadsheet);
    }
}
