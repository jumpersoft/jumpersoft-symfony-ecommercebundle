<?php
namespace Jumpersoft\EcommerceBundle\Controller\Panel\OrderRecord;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Profiler\Profiler;
use Symfony\Component\Serializer\SerializerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\BaseBundle\DependencyInjection\ExchangeRate;
use Jumpersoft\EcommerceBundle\Validators\OrderRecordValidator;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\EcommerceBundle\DependencyInjection;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class OrderRecordController extends BaseController
{

    /**
     * @Route("/api/panel/orderRecord", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:OrderRecord')->getOrders($this->jsReturn, $this->getUser(), $this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/{id}", condition="request.isXmlHttpRequest()", methods={"GET"}, requirements={"id"="^(.{20})$"})
     */
    public function getAction($id, Request $request)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:OrderRecord')->getOrderRecord($id, $this->getUser(), $this->defaultStoreId, "view");
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/timeline/{id}", condition="request.isXmlHttpRequest()", methods={"GET"}, requirements={"id"="^(.{20})$"})
     */
    public function getTimeLineAction($id, Request $request)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:OrderRecord')->getOrderRecordTimeLine($id, $this->defaultStoreId, $this->getUser());
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/{id}", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAction($id, Request $request, DependencyInjection\OrderRecordActions $orActions)
    {
        //NOTE: DOCTRINE CAN'T DELETE WITH JOINS, SO FIRST WE GET THE OBJECT AND THEN DELETE IT, ON THIS CASE ORDER ITEMS ARE DELETS BY ONCASCADE DELETE CLAUSULE.
        $orderRecord = $this->em->getRepository('E:OrderRecord')->getOrderRecord($id, $this->getUser(), $this->defaultStoreId, 'update');
        if ($orderRecord->getStatus()->getId() != "ORD_PEN") {
            throw new \Exception("Solo se pueden borrar pedidos pendientes");
        }
        //UPDATE STOCK
        //TODO FALTA PROBAR
        $orActions->returnStock("fromOrder", null, 0, null, $orderRecord->getId());

        $this->em->remove($orderRecord);
        $this->jsReturn["msg"] = "Pedido eliminado";
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/{id}", name="orderNewEdit", condition="request.isXmlHttpRequest()", methods={"PUT"}, requirements={"id"="^(.{20}|0)$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction($id, Request $request, ExchangeRate $exchangeRate, DependencyInjection\RadesaActions $radesa)
    {
        $eventDate = $this->jsUtil->getLocalDateTime();
        $edit = $request->request->get('edit') == "true";
        $inputValues = $request->request->get('order');
        $values = OrderRecordValidator::proccessInputValues($inputValues, OrderRecordValidator::$orderRecord);

        $shippingAddress = !empty($values["shippingAddressId"]) ? $this->em->getRepository('E:Address')->getAddress($values["shippingAddressId"], $values["shippingAddressTypeId"] == 'ADR_OWN' ? $values["customerId"] : null, "view") : null;
        $billingAddress = !empty($values["billingUserId"]) ? $this->em->getRepository("E:Address")->getBillingCustomers($this->jsFilter, "", $values["billingUserId"]) : null;

        //CUSTOM VALIDATIONS
        $radesa->validateActivityCustomerShipping($shippingAddress["userId"], $id);

        if (OrderRecordValidator::validate(OrderRecordValidator::$orderRecord, $values, [], !$edit ? ["subTotal", "total"] : [])) {
            $user = $this->getUser();
            if ($edit) {
                $orderRecord = $this->em->getRepository('E:OrderRecord')->getOrderRecord($id, $user, $this->defaultStoreId, "update");
                $orderRecord->setUpdateDate($eventDate);
                if ($orderRecord->getDate()->format('Ym') != ($newYearMonth = $this->jsUtil->getDateTime($values['date'])->format('Ym'))) {
                    $values["folio"] = $this->em->getRepository('E:OrderRecord')->findNextFolio($this->defaultStoreId, $newYearMonth);
                }
            } else {
                $orderRecord = new Entity\OrderRecord($id = $this->jsUtil->getUid());
                $values = array_merge($values, [
                    "customer" => $values["customerId"],
                    "seller" => $user,
                    "folio" => $this->em->getRepository('E:OrderRecord')->findNextFolio($this->defaultStoreId, $this->jsUtil->getDateTime($values['date'])->format('Ym')),
                    "store" => $this->defaultStoreId,
                    "status" => 'ORD_PEN',
                    "exchangeRate" => $exchangeRate->getExchangeRate()["value"],
                ]);
            }

            $values = array_merge($values, [
                "date" => $values['date'],
                "paymentForm" => $values["paymentFormId"],
                "paymentMethod" => $values["paymentMethodId"],
                "shippingAddressType" => $values["shippingAddressTypeId"],
                "billingAddressType" => $values["billingAddressTypeId"],
                "shippingAddress" => $shippingAddress,
                "billingAddress" => $billingAddress
            ]);

            $this->jsUtil->setValuesToEntity($orderRecord, $values);
            $this->em->persist($orderRecord);

            //Proccess Update
            $proc = new Entity\ProccessStep($this->jsUtil->getUid(), $orderRecord, $this->em->getReference("E:Step", $edit ? "ORD_UPD" : "ORD_INS"), null, $eventDate, $user);
            $this->em->persist($proc);
            $this->em->flush();

            $this->jsReturn["msg"] = $edit ? "Pedido modificado con éxito" : "Pedido creado con éxito";
            $this->jsReturn["data"] = $this->em->getRepository('E:OrderRecord')->getOrderRecord($id, $this->getUser(), $this->defaultStoreId, "view");
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/{id}/cancel", condition="request.isXmlHttpRequest()", methods={"POST"}, requirements={"id"="^(.{20}|0)$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function cancelAction($id, Request $request, DependencyInjection\OrderRecordActions $orActions)
    {
        $user = $this->getUser();
        $eventDate = $this->jsUtil->getLocalDateTime();
        $data = $request->request->all();
        $statusId = $data["statusId"];
        $oriStatusId = $statusId == "ORD_CAN_RET" ? 'ORI_CAN_RET' : 'ORI_CAN_NOR';
        $data = OrderRecordValidator::proccessInputValues($data, OrderRecordValidator::$cancelOrder);
        if (OrderRecordValidator::validate(OrderRecordValidator::$cancelOrder, $data)) {
            $orderRecord = $this->em->getRepository('E:OrderRecord')->getOrderRecord($id, $user, $this->defaultStoreId, 'update');
            //CHECK STATUS CHANGES RULES
            if ($this->validateChangeStatus(OrderRecordValidator::$stepProcess, $orderRecord->getStatus()->getId(), $statusId)) {

                //RETURN STOCK IF ANY ITEM WAS FULFILLED
                $data["returnFulFill"] == 1 ? $orActions->returnStock("fromOrder", $ori, 0, null, $orderRecord->getId()) : null;

                //UPDATE ORDERRECORDITEM STATUS
                $orItems = $orderRecord->getItems();
                foreach ($orItems as $orItem) {
                    $orItem->setStatus($this->em->getReference('E:Status', $oriStatusId));
                    $this->em->persist($orItem);
                }

                //UPDATE ORDERRECORD STATUS
                $data["date"] = $eventDate->format("Y-m-d H:i:s");
                $this->jsUtil->setValuesToEntity($orderRecord, ["status" => $statusId, "cancelation" => $data]);
                $this->em->persist($orderRecord);
            } else {
                throw new \Exception($this->msg["invalidStatus"]);
            }

            //Proccess Update
            $proc = new Entity\ProccessStep($this->jsUtil->getUid(), $orderRecord, $this->em->getReference("E:Step", $statusId), null, $eventDate, $user);
            $this->em->persist($proc);
            $this->em->flush();

            $this->jsReturn["msg"] = "Pedido cancelado";
            $this->jsReturn["data"] = $this->em->getRepository('E:OrderRecord')->getOrderRecord($id, $user, $this->defaultStoreId, 'view');
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/{id}/changeToStatus/{status}", condition="request.isXmlHttpRequest()", methods={"PUT"}, requirements={"id"="^(.{20}|0)$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function statusAction($id, $status, Request $request)
    {
        $id = $request->get('id');
        $user = $this->getUser();
        $eventDate = $this->jsUtil->getLocalDateTime();

        switch ($status) {
            case 'delivered':
                $statusId = 'ORD_DLV';
                $oriStatusId = 'ORI_DLV';
                $status = "Entregado";
                $this->msg["updateOK"] = "Pedido entregado";
                break;
            case 'pay':
                $statusId = 'ORD_PAI';
                $oriStatusId = 'ORI_PAI';
                $status = "Pagado";
                $this->msg["updateOK"] = "Pedido pagado";
                break;
            case 'inv':
                $statusId = 'ORD_INV';
                $oriStatusId = 'ORI_INV';
                $status = "Facturado";
                $this->msg["updateOK"] = "Pedido facturado";
                break;
            default:
                $statusId = '';
                break;
        }

        $orderRecord = $this->em->getRepository('E:OrderRecord')->getOrderRecord($id, $user, $this->defaultStoreId, 'update');
        //CHECK STATUS CHANGES RULES
        if ($this->validateChangeStatus(OrderRecordValidator::$stepProcess, $orderRecord->getStatus()->getId(), $statusId)) {

            //UPDATE ORDERRECORDITEM STATUS
            $orItems = $orderRecord->getItems();
            foreach ($orItems as $orItem) {
                $orItem->setStatus($this->em->getReference('E:Status', $oriStatusId));
                $this->em->persist($orItem);
            }

            //UPDATE ORDERRECORD STATUS
            $orderRecord->setStatus($this->em->getReference('E:Status', $statusId));
            $this->em->persist($orderRecord);
        } else {
            throw new \Exception($this->msg["invalidStatus"]);
        }

        //Proccess Update
        $proc = new Entity\ProccessStep($this->jsUtil->getUid(), $orderRecord, $this->em->getReference("E:Step", $statusId), null, $eventDate, $user);
        $this->em->persist($proc);

        $this->jsReturn["msg"] = $this->msg["updateOK"];
        $this->jsReturn["data"]["statusId"] = $statusId;
        $this->jsReturn["data"]["status"] = $status;

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/customers", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function geCustomersAction(Request $request)
    {
        $this->getFilters($request);
        $q = $request->get('q');
        $this->jsReturn["data"] = $this->em->getRepository('E:OrderRecord')->getCustomers($this->jsReturn, $q);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/{typeCustomer}/customers", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getShippingCustomers($typeCustomer, Request $request)
    {
        $this->getFilters($request);
        $q = $request->get('q');
        $this->jsReturn["data"] = $typeCustomer == "shipping" ? $this->em->getRepository('E:Address')->getShippingCustomers($this->jsReturn, $q) :
            $this->em->getRepository('E:Address')->getBillingCustomers($this->jsReturn, $q);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/getTransactionLog", name="getOrderRecordTransactionLog", condition="request.isXmlHttpRequest()", methods={"GET"})
     * @Configuration\Security("is_granted('ROLE_CUSTOMER')")
     */
    public function getTransactionLog(Request $request)
    {
        $id = $request->get('transactionId');
        $userId = $this->getUser()->getId();
        $row = $this->em->getRepository('E:OrderRecord')->getTransactionLog($id, $userId, $this->isUser('ROLE_SELLER'));
        $this->jsReturn["data"] = $row;
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/print/{document}/{type}", methods={"POST", "GET"})
     */
    public function print($document, $type, Request $request, ?Profiler $profiler)
    {
        $user = $this->getUser();
        $orders = $request->getMethod() == "POST" ? $request->request->get("orders") : explode(",", $request->get("orders"));
        $document = str_replace("Selected", "", $document);
        if ($document == "receipt") {
            $data = $this->em->getRepository('E:OrderRecord')->getReceipts($orders, $user, $this->defaultStoreId);
        } elseif ($document == "label") {
            $data = $this->em->getRepository('E:OrderRecord')->getLabels($orders, $user, $this->defaultStoreId);
        } elseif ($document == "remission") {
            $data = $this->em->getRepository('E:OrderRecord')->getRemissions($orders, $user, $this->defaultStoreId);
        }

        if ($data) {
            $data = [
                "data" => $data,
                "company" => $this->em->getRepository('E:Store')->getStore($this->defaultStoreId, $user->getId(), "view", $this->isUser('ROLE_SUPER_ADMIN'))
            ];
            $this->debug && null !== $profiler ? $profiler->disable() : null; //Deshabilitamos symfony toolbar debuger
            return $this->render("@documentViews/receipt/$document.html.twig", $data);
        }
        throw new \Exception("No se encontró información para imprimir");
    }

    /**
     * @Route("/api/panel/orderRecord/export/{format}", methods={"POST", "GET"}, requirements={"format"="csv|xls|xlsx"})
     */
    public function export($format = "csv", Request $request, SerializerInterface $serializer)
    {
        $this->getFilters($request);
        $data = $this->em->getRepository('E:OrderRecord')->getOrders($this->jsReturn, $this->getUser(), $this->defaultStoreId, "report", $request->request->get("orders"));
        if ($format == "xls" || $format == "xlsx") {
            $response = $this->responseXls($this->getOrdersXls($data), 'orders' . $this->jsUtil->getLocalDateTimeString("YmdHis") . '.xls');
            return $response;
        } else {
            return $this->responseFile($serializer->encode($data, 'csv'), 'text/csv', 'orders' . $this->jsUtil->getLocalDateTimeString("YmdHis") . '.csv');
        }
    }

    public function getOrdersXls($data)
    {
        $spreadsheet = IOFactory::load($this->params->get('views.document') . "/report/orders.xlsx");
        $worksheet = $spreadsheet->getActiveSheet();
        $count = count($data);
        $row = 9; // la fila de la templeta donde empieza la tabla
        for ($i = 0; $i < $count; $i++) {
            $worksheet->getCell("A$row")->setValue($i + 1);
            $worksheet->getCell("B$row")->setValue($data[$i]["folio"]);
            $worksheet->getCell("C$row")->setValue($data[$i]["date"]);
            $worksheet->getCell("D$row")->setValue($data[$i]["name"]);
            $worksheet->getCell("E$row")->setValue($data[$i]["status"]);
            $worksheet->getCell("F$row")->setValue($data[$i]["subTotal"]);
            $worksheet->getCell("G$row")->setValue($data[$i]["discount"]);
            $worksheet->getCell("H$row")->setValue($data[$i]["total"]);
            $row++;
        }

        return new Xls($spreadsheet);
    }

    /**
     * @Route("/api/panel/orderRecord/container/return", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function containerReturn(Request $request)
    {
        $user = $this->getUser();
        $data = $request->request->get("data");
        if (count($data) > 0) {
            //$folios = array_map($data, fn($d) => ["folio" => explode("/", $d)[0] ?? "", "sequence" => explode("/", $d)[1] ?? ""]);
            $orItems = $this->em->createQuery("select oi
                                                from E:OrderRecordItem oi 
                                                join oi.item i
                                                join oi.orderRecord o 
                                               where concat(o.folio, '/', oi.sequence) in (:folios)
                                                 and i.useContainer = 1
                                                 and identity(oi.containerStatus) = 'CON_USE' ")->setParameters(["folios" => $data])->getResult();
            if (($total = count($orItems)) > 0) {
                foreach ($orItems as $oi) {
                    $oi->setContainerStatus($this->em->getReference("E:Status", "CON_RET"));
                    $this->em->persist($oi);
                    $total--;
                }
                $this->jsReturn["msg"] = ($total > 0 ? "Algunos contenedores fueron devueltos otros no, probablemente ya habian sido devueltos" : "Contenedores devueltos");
                return $this->responseJson();
            }
            throw new \Exception("No se encontraron pedidos relacionados a contenedores para procesar con la información proporcionada");
        }
        throw new \Exception("No hay datos para procesar");
    }
}
