<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\OrderRecord;

use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\EcommerceBundle\Entity\UserLog;
use Jumpersoft\EcommerceBundle\DependencyInjection\PaymentGateway;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class TransactionController extends BaseController
{

    /**
     * @Route("/api/panel/transaction/getAll", name="getTransactions", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $userId = $this->getUser()->getId();
        //$this->defaultStoreId = $request->get('currentStoreId');
        $this->jsReturn["data"] = $this->em->getRepository('E:Transaction')->getTransactions($this->jsReturn);
        return $this->responseJson();
    }
}
