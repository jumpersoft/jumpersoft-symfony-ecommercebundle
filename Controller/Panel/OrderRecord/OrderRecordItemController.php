<?php
namespace Jumpersoft\EcommerceBundle\Controller\Panel\OrderRecord;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\DependencyInjection;
use Jumpersoft\EcommerceBundle\Validators\OrderRecordValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class OrderRecordItemController extends BaseController
{

    /**
     * @Route("/api/panel/orderRecord/item/getAll", name="getOrderRecordItems", condition="request.isXmlHttpRequest()")
     * @Route("/api/store/account/order/item/getAll", name="getStoreAccountOrderItems", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:OrderRecordItem')->getOrderRecordItems($this->jsReturn, $this->getUser(), $request->get('orderRecordId'), $this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/item/{id}", condition="request.isXmlHttpRequest()", methods={"GET"}, requirements={"id"="^(.{20})$"})
     */
    public function getAction(Request $request)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:OrderRecordItem')->getOrderRecordItem($this->getUser(), $request->get('orderRecordId'), $request->get('id'), $this->defaultStoreId);
        $this->jsReturn["item"] = $this->em->getRepository('E:OrderRecordItem')->getItemForOrderRecordItem($this->jsReturn["data"]["itemId"], $this->jsReturn["data"]["customerId"]);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/item/{id}", condition="request.isXmlHttpRequest()", methods={"PUT"}, requirements={"id"="^(.{20}|0)$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function put($id, Request $request, DependencyInjection\OrderRecordActions $orActions, DependencyInjection\RadesaActions $radesa)
    {
        ["orItem" => $data, "itemId" => $itemId, "edit" => $edit] = $request->request->all();
        $edit = $edit == "true";
        $values = OrderRecordValidator::proccessInputValues($data, OrderRecordValidator::$orderRecordItem);

        $eventDate = $this->jsUtil->getLocalDateTime();
        $order = $this->em->getRepository('E:OrderRecord')->getOrderRecord($orderRecordId = $values['orderRecordId'], $user = $this->getUser(), $this->defaultStoreId, "update");
        $item = $this->em->getRepository('E:OrderRecordItem')->getItemForOrderRecordItem($itemId, $order->getCustomer()->getId());

        //VARIABLE VALIDATIONS
        $variableValidation = ["quantity" => ($item["fractionable"] == true ? "quantityFractionable" . $item["decimals"] : "quantity")];

        //AVOID FIELDS
        $avoidVal = ["quantity" => ($item["trackingTypeId"] != "INV_TYP_SIM" ? "quantity" : "")];
        !$item["radioactive"] ? $avoidVal = array_merge($avoidVal, ["volume", "calibrationDate"]) : null;

        //CUSTOM SELLERS VALIDATIONS
        $radesa->validateActivityByDay($order->getShippingAddress()["userId"], $item["id"], $values["quantity"], $id ?? null);

        if (OrderRecordValidator::validate(OrderRecordValidator::$orderRecordItem, $values, $variableValidation, $avoidVal)) {
            if (!$edit) {
                $orItem = new Entity\OrderRecordItem();
                $values = array_merge($values, [
                    "id" => $this->jsUtil->getUid(),
                    "orderRecord" => $order,
                    "registerDate" => $eventDate,
                    "item" => $itemId,
                    "status" => 'ORI_FUL_PEN',
                    "containerStatus" => "CON_USE",
                    "sequence" => $this->em->createQuery("select IFNULL(max(oi.sequence),0) + 1 from E:OrderRecordItem oi where oi.orderRecord = :order ")->setParameter('order', $order)->getSingleScalarResult()
                ]);
            } else {
                $orItem = $this->em->getRepository('E:OrderRecordItem')->getOrderRecordItem($user, $orderRecordId, $id, $this->defaultStoreId, "update");
            }

            //GET THE LOWEST PRICE
            $orActions->setLowestPriceAndTotals($orItem, $item, $values, $order->getExchangeRate());

            //SET GENERAL VALUES
            $this->jsUtil->setValuesToEntity($orItem, $values);
            $this->em->persist($orItem);

            //FULFILL
            if (($autoFulFill = ($data["autoFulFill"] ?? "false") == "true")) {
                $res = $orActions->pullStock("fromOrderItemAutoFill", $orItem, $values["quantity"]);
            }

            //UPDATE TOTALS
            $this->em->flush();
            $orderRecord = $this->em->getRepository('E:OrderRecord')->getOrderRecord($orderRecordId, $user, $this->defaultStoreId, "update");
            $orActions->setTotals($orderRecord);

            //UPDATE ORDER STATUS
            $orActions->updateOrderStatus("itemEdited", $orderRecord);
            $this->em->persist($orderRecord);

            //PROCCESS UPDATE
            $proc = new Entity\ProccessStep($this->jsUtil->getUid(), $orderRecord, $this->em->getReference("E:Step", "ORD_UPD"), ["msg" => ($edit ? "Producto actualizado" : "Producto agregado") . (" (" . $item["name"] . ")"),], $eventDate, $user);
            $this->em->persist($proc);

            $this->jsReturn["msg"] = "Producto/Servicio " . ($edit ? "modificado con éxito" : "añadido con éxito") . (isset($res) ? ". " . $res["msg"] : "");
            $this->em->flush();
            $this->jsReturn["data"]["orderRecordItem"] = $this->em->getRepository('E:OrderRecordItem')->getOrderRecordItem($user, $orderRecordId, $orItem->getId(), $this->defaultStoreId);
            $this->jsReturn["data"]["orderRecord"] = $this->em->getRepository('E:OrderRecord')->getOrderRecord($orderRecordId, $user, $this->defaultStoreId, "view");
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/item/{id}", condition="request.isXmlHttpRequest()", methods={"DELETE"}, requirements={"id"="^(.{20})$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAction($id, Request $request, DependencyInjection\OrderRecordActions $orActions)
    {
        $orderRecordId = $request->get('orderRecordId');
        $user = $this->getUser();
        $orderRecord = $this->em->getRepository('E:OrderRecord')->getOrderRecord($orderRecordId, $user, $this->defaultStoreId, "update", true);
        if ($orderRecord) {
            //NOTE: DOCTRINE CAN'T DELETE WITH JOINS, SO FIRST WE GET THE OBJECT AND THEN DELETE IT
            $oitem = $this->em->getRepository('E:OrderRecordItem')->getOrderRecordItem($user, $orderRecordId, $id, $this->defaultStoreId, "update");

            //UPDATE STOCK
            $res = $orActions->returnStock("fromOrderItem", $oitem, $oitem->getQuantity());
            $this->em->remove($oitem);
            $this->em->flush();

            //UPDATE ORDER STATUS
            $orActions->updateOrderStatus("itemEdited", $orderRecord);
            $this->em->persist($orderRecord);

            //UPDATE TOTALS
            $orActions->setTotals($orderRecord);
            $this->em->persist($orderRecord);

            //Proccess Update
            $proc = new Entity\ProccessStep($this->jsUtil->getUid(), $orderRecord, $this->em->getReference("E:Step", "ORD_UPD"), ["msg" => "Producto eliminado" . (" (" . $oitem->getItem()->getName() . ")")], $this->jsUtil->getLocalDateTime(), $user);
            $this->em->persist($proc);
        } else {
            throw new \Exception("El estatus del pedido no permite cambios");
        }
        $this->jsReturn["msg"] = "Producto/Servicio eliminado" . ($res["msg"] ? ". " . $res["msg"] : "");
        $this->em->flush();
        $this->jsReturn["data"]["orderRecord"] = $this->em->getRepository('E:OrderRecord')->getOrderRecord($orderRecordId, $user, $this->defaultStoreId, "view");
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/item/fulFill/{id}", condition="request.isXmlHttpRequest()", methods={"PUT"}, requirements={"id"="^(.{20}|0)$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function fulFill($id, Request $request, DependencyInjection\OrderRecordActions $orActions)
    {
        $orderRecordId = $request->request->get('orderRecordId');
        $quantity = $request->request->get('quantity');
        $inventoryId = $request->request->get('inventoryId');
        $user = $this->getUser();
        $orItem = $this->em->getRepository('E:OrderRecordItem')->getOrderRecordItem($user, $orderRecordId, $id, $this->defaultStoreId, "update");

        //UPDATE ORDERRECORDITEM, INVENTORY AND STOCK
        $res = $orActions->pullStock("fromOrderItemInventory", $orItem, $quantity, $inventoryId);

        if ($res["success"]) {
            //UPDATE ORDER STATUS
            $order = $orItem->getOrderRecord();
            $orActions->updateOrderStatus("itemEdited", $order);
            $this->em->persist($order);

            //PROCCESS UPDATE
            $proc = new Entity\ProccessStep($this->jsUtil->getUid(), $order, $this->em->getReference("E:Step", "ORD_FUL_SUC"), ["msg" => "Se surtio: " . $orItem->getItem()->getName()], $this->jsUtil->getLocalDateTime(), $user);
            $this->em->persist($proc);

            $this->em->flush();

            $this->jsReturn["msg"] = $res["msg"];
            $this->jsReturn["data"]["orderRecordItem"] = $this->em->getRepository('E:OrderRecordItem')->getOrderRecordItem($user, $request->get('orderRecordId'), $request->get('id'), $this->defaultStoreId);
            $this->jsReturn["data"]["orderRecord"] = $this->em->getRepository('E:OrderRecord')->getOrderRecord($orderRecordId, $user, $this->defaultStoreId, "view");
        } else {
            throw new \Exception($res["msg"]);
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/item/autoFulFill", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function autoFulFill(Request $request, DependencyInjection\OrderRecordActions $orActions)
    {
        $user = $this->getUser();
        $orderRecordId = $request->request->get('orderRecordId');
        $orderRecordItemId = $request->request->get('orderRecordItemId') ?? "";
        $type = $request->request->get("type") ?? "";
        $orItem = $type == "fromOrderItemAutoFill" ? $this->em->getRepository('E:OrderRecordItem')->getOrderRecordItem($user, $orderRecordId, $orderRecordItemId, $this->defaultStoreId, "update") : null;

        //UPDATE STOCK
        $res = $orActions->pullStock($type, $orItem, $orItem ? $orItem->getQuantity() : null, null, $orderRecordId);

        if ($res["success"]) {
            //UPDATE ORDER STATUS
            $order = $orItem->getOrderRecord();
            $orActions->updateOrderStatus("itemEdited", $order);
            $this->em->persist($order);

            //PROCCESS UPDATE
            $this->em->persist(new Entity\ProccessStep($this->jsUtil->getUid(), $order, $this->em->getReference("E:Step", "ORD_FUL_SUC"), null, $this->jsUtil->getLocalDateTime(), $user));

            $this->em->flush();
            $this->jsReturn["msg"] = $res["msg"];
            $this->jsReturn["data"]["orderRecordItem"] = $type == "fromOrderItemAutoFill" ? $this->em->getRepository('E:OrderRecordItem')->getOrderRecordItem($user, $orderRecordId, $orderRecordItemId, $this->defaultStoreId) : null;
            $this->jsReturn["data"]["orderRecord"] = $this->em->getRepository('E:OrderRecord')->getOrderRecord($orderRecordId, $user, $this->defaultStoreId, "view");
        } else {
            throw new \Exception($res["msg"]);
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/item/returnFulFill", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function returnFulFill(Request $request, DependencyInjection\OrderRecordActions $orActions)
    {
        $user = $this->getUser();
        $orderRecordId = $request->request->get('orderRecordId');
        $orderRecordItemId = $request->request->get('orderRecordItemId') ?? "";
        $orderRecordItemInventoryId = $request->request->get('orderRecordItemInventoryId') ?? null;
        $type = $request->request->get("type") ?? "";
        $orItem = $type != "fromOrder" ? $this->em->getRepository('E:OrderRecordItem')->getOrderRecordItem($user, $orderRecordId, $orderRecordItemId, $this->defaultStoreId, "update") : null;

        //RETURN STOCK
        $res = $orActions->returnStock($type, $orItem, $orItem ? $orItem->getQuantity() : null, $orderRecordItemInventoryId, $orderRecordId);

        if ($res["success"]) {
            //UPDATE ORDER STATUS
            $order = $orItem->getOrderRecord();
            $orActions->updateOrderStatus("itemEdited", $order);
            $this->em->persist($order);

            //PROCCESS UPDATE
            $proc = new Entity\ProccessStep($this->jsUtil->getUid(), $order, $this->em->getReference("E:Step", "ORD_FUL_SUC_CAN"), ["msg" => "Surtido cancelado: " . $orItem->getItem()->getName()], $this->jsUtil->getLocalDateTime(), $user);
            $this->em->persist($proc);

            $this->jsReturn["msg"] = $res["msg"];
            $this->jsReturn["data"]["orderRecordItem"] = $type == "fromOrderItem" || $type == "fromOrderItemInventory" ? $this->em->getRepository('E:OrderRecordItem')->getOrderRecordItem($user, $orderRecordId, $orderRecordItemId, $this->defaultStoreId) : null;
            $this->jsReturn["data"]["orderRecord"] = $this->em->getRepository('E:OrderRecord')->getOrderRecord($orderRecordId, $user, $this->defaultStoreId, "view");
        } else {
            throw new \Exception($res["msg"]);
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/catalog/items", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getCatalogItemsAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:OrderRecordItem')->getItemsToCombo($this->jsReturn, $request->get('q'), $this->defaultStoreId, $request->get('orderRecordId'), $request->get('customerId'));
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/orderRecord/item/inventory/{action}/{id}", condition="request.isXmlHttpRequest()", methods={"GET"}, requirements={"id"="^(.{20})$"})
     */
    public function getOrderRecordItemInventoryAction($action, $id, Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:OrderRecordItem')->getOrderRecordItemInventory($this->jsReturn, $action, $id);
        return $this->responseJson();
    }

    /**
     * //TODO Esta pendiente de atender hasta que se continue el surtido de productos compuestos desde los pedidos.
     * @Route("/api/panel/orderRecord/item/inventory/compositeItems/{itemId}", condition="request.isXmlHttpRequest()", methods={"GET"}, requirements={"itemId"="^(.{20})$"})
     */
    public function getOrderRecordItemCompositeItems($itemId)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:OrderRecordItem')->getOrderRecordItemCompositeItems($itemId);
        return $this->responseJson();
    }
}
