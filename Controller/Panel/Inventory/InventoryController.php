<?php
namespace Jumpersoft\EcommerceBundle\Controller\Panel\Inventory;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\DependencyInjection;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\EcommerceBundle\Validators\InventoryValidator;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class InventoryController extends BaseController
{

    /**
     * @Route("/api/panel/inventory", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Inventory')->getInventory($this->jsReturn, $this->defaultStoreId, $this->getUser());
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/inventory/INV_OPE_IN/{itemId}/{useNoEco}", condition="request.isXmlHttpRequest()", methods={"PUT"}, requirements={"itemId"="^(.{20}|0)$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function operationIn($itemId, $useNoEco, Request $request)
    {
        $values = $this->operation("INV_IN_NEW", $itemId, $useNoEco, $request->request->get('data'), null, $request->request->get('compositeItems'));
        $this->jsReturn["msg"] = count($values) > 0 ? "Entradas registradas" : "Entrada registrada";
        $this->jsReturn["data"] = $values;
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/inventory/INV_OPE_OUT/{inventoryId}", condition="request.isXmlHttpRequest()", methods={"PUT"}, requirements={"inventoryId"="^(.{20}|0)$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function operationOut($inventoryId, Request $request)
    {
        $this->operation("INV_OUT_", null, null, $request->request->get('data'), $inventoryId);
        $this->jsReturn["msg"] = "Salida registrada";
        return $this->responseJson();
    }

    public function operation($typeId, $itemId, $useNoEco, $data, $inventoryId = null, $compositeItems = null)
    {
        $user = $this->getUser();
        $eventDate = $this->jsUtil->getLocalDateTime();
        if (strpos($typeId, "INV_IN_") === 0) {
            //OPERATION IN
            $item = $this->em->getRepository('E:Item')->getItem($itemId, $user->getId(), $this->defaultStoreId, "update");
            $useNoEco = $useNoEco == "true";
            $serialized = $item->getSerialized() == 1;
            $pi = 0;

            //VALIDATIONS
            if ($item->getType()->getId() == "ITE_COM" && $compositeItems && count($compositeItems) == 0) {
                throw new \Exception("El producto compuesto no tiene productos relacionados, ingrese al menos un producto");
            }
            foreach ($data as $row) {
                $values[$pi] = InventoryValidator::proccessInputValues($row, InventoryValidator::$inventoryIn);
                //Filed Validations
                $avoidField = $serialized ? ["lot"] : ["serial"];
                !$item->getPerishable() && !$item->getRadioactive() ? array_push($avoidField, "expirationDate") : null;
                !$item->getRadioactive() ? array_push($avoidField, "calibrationDate", "volumeReceived") : null;
                !$useNoEco ? array_push($avoidField, "noEco") : null;
                $variableValidation = ["quantityReceived" => ($item->getUnitMeasure()->getFractionable() == true ? "quantityReceivedFractionable" . $item->getDecimals() : "quantityReceived")];

                if (InventoryValidator::validate(InventoryValidator::$inventoryIn, $values[$pi], $variableValidation, $avoidField)) {
                    $pi++;
                } else {
                    throw new \Exception($this->msg["errorForm"]);
                }
            }

            //INSERTS
            $batchSize = 20;
            for ($i = 0; $i < $pi; $i++) {
                $inventory = new Entity\Inventory();
                $values[$i] = array_merge($values[$i], [
                    "id" => $this->jsUtil->getUid(),
                    "registerDate" => $eventDate,
                    "eventDate" => $values[$i]['eventDate'],
                    "operation" => $item->getType()->getId() == "ITE_COM" ? "INV_OPE_IN_PEN" : "INV_OPE_IN",
                    "type" => $typeId,
                    "expirationDate" => $item->getPerishable() || $item->getRadioactive() ? $values[$i]['expirationDate'] : null,
                    "stock" => $item->getType()->getId() == "ITE_COM" ? 0 : $values[$i]["quantityReceived"],
                    "item" => $item]);
                $this->jsUtil->setValuesToEntity($inventory, $values[$i]);
                $this->em->persist($inventory);

                //Insert composite item if it is a composite item
                if ($compositeItems) {
                    foreach ($compositeItems as $ci) {
                        $inventoryCompositeItem = new Entity\InventoryCompositeItem(
                            $this->jsUtil->getUid(),
                            $inventory,
                            $this->em->getReference("E:ItemCompositeItem", $ci["id"]),
                            $ci["quantity"] * $inventory->getQuantityReceived(),
                            0,
                            $this->em->getReference("E:Status", "INV_IN_COM_FUL_PEN"),
                            $eventDate
                        );
                        $this->em->persist($inventoryCompositeItem);
                    }
                }

                if (($i % $batchSize) === 0) {
                    $this->em->flush();
                }
            }
            return $values;
        } elseif (strpos($typeId, "INV_OUT_") === 0) {
            //OPERATION OUT
            $inventory = $this->em->getRepository('E:Inventory')->findOneBy(["id" => $inventoryId]);
            $item = $this->em->getRepository('E:Item')->getItem($inventory->getItem()->getId(), $user->getId(), $this->defaultStoreId, "update");

            if ($inventory->getStock() >= $data["quantityOutput"]) {
                $values = InventoryValidator::proccessInputValues($data, InventoryValidator::$inventoryOut);
                $variableValidations = ["quantityOutput" => ($item->getUnitMeasure()->getFractionable() == true ? ("quantityOutput" . $item->getDecimals()) : "quantityOutput")];
                $values["typeId"] == "INV_OUT_DIS" ? $variableValidations["quantityOutput"] = "quantityOutput4" : null;
                if (InventoryValidator::validate(InventoryValidator::$inventoryOut, $values, $variableValidations)) {
                    $out = new Entity\Inventory();
                    $values = array_merge($values, [
                        "id" => $this->jsUtil->getUid(),
                        "registerDate" => $eventDate,
                        "eventDate" => $values['eventDate'],
                        "operation" => "INV_OPE_OUT",
                        "type" => $values["typeId"],
                        "item" => $item,
                        "parent" => $inventory,
                        "serial" => $item->getSerialized() ? $inventory->getSerial() : null,
                        "lot" => $item->getSerialized() ? null : $inventory->getLot()
                    ]);
                    $this->jsUtil->setValuesToEntity($out, $values);
                    $this->em->persist($out);
                    //Update stock
                    $inventory->setStock($inventory->getStock() - $values["quantityOutput"]);
                    $this->em->persist($inventory);
                    return [];
                } else {
                    throw new \Exception($this->msg["errorForm"]);
                }
            } else {
                throw new \Exception("La cantidad de salida es mayor a la cantidad en stock");
            }
        }
    }

    /**
     * @Route("/api/panel/inventory/catalog/items", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getCatalogItemsAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Inventory')->getCatalogItems($this->jsReturn, $request->get('q'), $this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/inventory/export/{format}", methods={"POST", "GET"}, requirements={"format"="csv|xls|xlsx"})
     */
    public function export($format = "csv", Request $request, SerializerInterface $serializer)
    {
        $this->getFilters($request);
        $data = $this->em->getRepository('E:Inventory')->getInventory($this->jsReturn, $this->defaultStoreId, $this->getUser(), "report", $request->request->get("items"));
        if ($format == "xls" || $format == "xlsx") {
            $response = $this->responseXls($this->getOrdersXls($data), 'inventory' . $this->jsUtil->getLocalDateTimeString("YmdHis") . '.xls');
            return $response;
        } else {
            return $this->responseFile($serializer->encode($data, 'csv'), 'text/csv', 'inventory' . $this->jsUtil->getLocalDateTimeString("YmdHis") . '.csv');
        }
    }

    public function getOrdersXls($data)
    {
        $spreadsheet = IOFactory::load($this->params->get('views.document') . "/report/inventory.xlsx");
        $worksheet = $spreadsheet->getActiveSheet();
        $count = count($data);
        $row = 9; // la fila de la templeta donde empieza la tabla
        for ($i = 0; $i < $count; $i++) {
            $worksheet->getCell("A$row")->setValue($i + 1);
            $worksheet->getCell("B$row")->setValue($data[$i]["lot"]);
            $worksheet->getCell("C$row")->setValue($data[$i]["serial"]);
            $worksheet->getCell("D$row")->setValue($data[$i]["name"]);
            $worksheet->getCell("E$row")->setValue($data[$i]["sku"]);
            $worksheet->getCell("F$row")->setValue($data[$i]["quantityReceived"]);
            $worksheet->getCell("G$row")->setValue($data[$i]["stock"]);
            $worksheet->getCell("H$row")->setValue($data[$i]["quantityOutput"]);
            $worksheet->getCell("I$row")->setValue($data[$i]["eventDate"]);
            $worksheet->getCell("J$row")->setValue($data[$i]["operation"]);
            $worksheet->getCell("K$row")->setValue($data[$i]["type"]);
            $worksheet->getCell("L$row")->setValue($data[$i]["expirationDate"]);
            $row++;
        }
        return new Xls($spreadsheet);
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route("/api/panel/inventory/{id}", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function delete($id, Request $request)
    {
        $user = $this->getUser();
        $inv = $this->em->createQuery('select inv from E:Inventory inv join inv.item i join i.store s where inv.id = :id and s.id = :storeId')->setParameters(["id" => $id, "storeId" => $this->defaultStoreId])->getOneOrNullResult();
        if ($inv && $inv->getOperation()->getId() == "INV_OPE_IN" && $inv->getStock() > 0 && $inv->getStock() == $inv->getQuantityReceived()) {
            $this->em->remove($inv);
            $this->jsReturn["msg"] = "Entrada eliminada";
            return $this->responseJson();
        } elseif ($inv && $inv->getOperation()->getId() == "INV_OPE_IN_PEN") {
            $fulfilledCounts = $this->em->getRepository("E:Inventory")->getInventoryCompositeItemInventorySums($id);
            if ($fulfilledCounts["quantityFulfilled"] == 0) {
                $this->em->remove($inv);
                $this->jsReturn["msg"] = "Entrada eliminada";
                return $this->responseJson();
            }
        }

        throw new \Exception("La entrada no puede eliminarse ya que puede tener producto surtidos");
    }

    /**
     * @Route("/api/panel/inventory/itemRelatedDispersion/{itemId}", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getItemRelatedDispersion($itemId, Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Inventory')->getItemRelatedDispersion($this->jsReturn, $request->get('q'), $this->defaultStoreId, $itemId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/inventory/INV_OPE_DIS/{itemRelatedId}/{inventoryId}/{useNoEco}", condition="request.isXmlHttpRequest()", methods={"PUT"}, requirements={"inventoryId"="^(.{20}|0)$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function dispersion($itemRelatedId, $inventoryId, $useNoEco, Request $request)
    {
        $inventory = $this->em->getRepository('E:Inventory')->findOneBy(["id" => $inventoryId, "operation" => $this->em->getReference("E:Type", "INV_OPE_IN")]);
        $item = $inventory->getItem();
        $itemRelated = $this->em->getRepository("E:Item")->findOneById($itemRelatedId);

        //OPERATION IN
        $dataIn = $request->request->get('data');
        $values = $this->operation("INV_IN_DIS", $itemRelatedId, $useNoEco, $dataIn);

        //OPERATION OUT
        $quantityRequired = array_sum(array_column($dataIn, "quantityReceived"));
        // - Unitmeasure validations
        if ($item->getUnitMeasureBulkSale()->getId() == $itemRelated->getUnitMeasureBulkSale()->getId()) {
            $quantityOutput = round(($quantityRequired * $itemRelated->getQuantityByUnitBulkSale()) / $item->getQuantityByUnitBulkSale(), 4);
        } elseif ($item->getUnitMeasureBulkSale()->getId() == $itemRelated->getUnitMeasure()->getId()) {
            $quantityOutput = round(($quantityRequired / $item->getQuantityByUnitBulkSale()), 4);
        } else {
            throw new Exception("Las unidades de medida no coinciden, consulte con el administrador");
        }

        $dataOut = ["itemId" => $item->getId(), "operationId" => "INV_OPE_OUT", "typeId" => "INV_OUT_DIS", "eventDate" => $request->request->get('eventDate'), "quantityOutput" => $quantityOutput];
        $this->operation("INV_OUT_", null, null, $dataOut, $inventoryId);

        $this->jsReturn["msg"] = "Producto dispersado";
        $this->jsReturn["data"] = $values;

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/inventory/compositeItem/{inventoryId}", condition="request.isXmlHttpRequest()", methods={"GET"}, requirements={"inventoryId"="^(.{20})$"})
     */
    public function getInventoryCompositeItems($inventoryId)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:Inventory')->getInventoryCompositeItems($inventoryId, $this->defaultStoreId, $this->getUser());
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/inventory/compositeItem/{action}/{id}", condition="request.isXmlHttpRequest()", methods={"GET"}, requirements={"id"="^(.{20})$"})
     */
    public function getCompositeItemAction($action, $id, Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Inventory')->getCompositeItem($this->jsReturn, $action, $id);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/inventory/compositeItem/{action}", condition="request.isXmlHttpRequest()", methods={"PUT"}, requirements={"action"="fulFill|autoFulFill"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function fulFill($action, Request $request, DependencyInjection\InventoryActions $invActions)
    {
        $user = $this->getUser();
        if ($action == "fulFill") {
            ["inventoryCompositeItemId" => $inventoryCompositeItemId, "inventoryId" => $inventoryId, "quantity" => $quantity] = $request->request->all();
            $res = $invActions->pullStock($action, $inventoryCompositeItemId, $inventoryId, $quantity);
        } else {
            $res = $invActions->pullStock($action, null, null, null, $request->request->get("ids"));
        }

        if ($res["success"]) {
            $this->jsReturn["msg"] = $res["msg"];
            if (isset($res["inventoryComposite"])) {
                $this->jsReturn["data"]["inventoryCompositeItem"] = $this->em->getRepository('E:Inventory')->getInventoryCompositeItems($res["inventoryComposite"]->getId(), $this->defaultStoreId, $user, $inventoryCompositeItemId);
                $this->jsReturn["data"]["inventory"] = $this->em->getRepository('E:Inventory')->getInventory($this->jsReturn, $this->defaultStoreId, $user, "oneResult", [$res["inventoryComposite"]->getId()]);
            }
        } else {
            throw new \Exception($res["msg"]);
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/inventory/compositeItem/returnFulFill", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function returnFulFill(Request $request, DependencyInjection\InventoryActions $invActions)
    {
        $user = $this->getUser();
        ["ids" => $ids, "type" => $type] = $request->request->all();
        $res = $invActions->returnStock($type, $ids);
        if ($res["success"]) {
            $this->jsReturn["msg"] = $res["msg"];
            if ($res["inventoryCompositeItem"]) {
                $this->jsReturn["data"]["inventoryCompositeItem"] = $this->em->getRepository('E:Inventory')->getInventoryCompositeItems($res["inventoryComposite"]->getId(), $this->defaultStoreId, $user, $res["inventoryCompositeItem"]->getId());
                $this->jsReturn["data"]["inventory"] = $this->em->getRepository('E:Inventory')->getInventory($this->jsReturn, $this->defaultStoreId, $user, "oneResult", [$res["inventoryComposite"]->getId()]);
            }
        } else {
            throw new \Exception($res["msg"]);
        }

        return $this->responseJson();
    }
}
