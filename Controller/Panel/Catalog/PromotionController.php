<?php
namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\PromotionValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class PromotionController extends BaseController
{

    /**
     * @Route("/api/panel/promotion/getAll", name="getPromotions", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Promotion')->getPromotions($this->jsReturn, $this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/promotion/get", name="getPromotion", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAction(Request $request)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:Promotion')->getPromotion($request->get('id'), $this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/promotion/delete", name="deletePromotion", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true")
     */
    public function deleteAction(Request $request)
    {
        $row = $this->em->getRepository('E:Promotion')->getPromotion($request->request->get('id'), $this->defaultStoreId, "delete");
        $this->em->remove($row);
        $this->em->flush();
        $this->jsReturn["msg"] = "Promoción eliminada";
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/promotion/put", name="promotionNewEdit", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction(Request $request)
    {
        $this->msg["insertOK"] = "Promoción creada con éxito";
        $this->msg["updateOK"] = "Promoción modificada con éxito";

        $edit = $request->request->get('edit');
        $promotionInput = $request->request->get('promotion');
        $values = PromotionValidator::proccessInputValues($promotionInput, PromotionValidator::$promotion);
        $returnData["id"] = $request->request->get('id');

        if (PromotionValidator::validate(PromotionValidator::$promotion, $values)) {
            $promotion = new Entity\Promotion();
            if ($edit == "false") {
                $values = array_merge($values, [
                    "id" => $returnData["id"] = $this->jsUtil->getUid(),
                    "store" => $this->defaultStoreId,
                    "registerDate" => $returnData["registerDate"] = $this->jsUtil->getLocalDateTime(),
                    "status" => $returnData["statusId"] = "PRO_ACT",
                    "code" => $code = strtoupper(!empty($values["code"]) ? $values["code"] : implode("-", str_split($this->getUid(false, 15), 5))),
                    "type" => $values["typeId"]
                ]);
                $returnData["status"] = "Vigente";
            } else {
                $promotion = $this->em->getRepository('E:Promotion')->getPromotion($returnData["id"], $this->defaultStoreId, "update");
                $promotion->SetUpdateDate($this->jsUtil->getLocalDateTime());
                $avoid = ["code", "typeId", "discount", "discountType", "itemId"];
            }

            $this->jsUtil->setValuesToEntity($promotion, $values, $avoid ?? []);
            $this->em->persist($promotion);

            $this->jsReturn["msg"] = $edit == "false" ? $this->msg["insertOK"] : $this->msg["updateOK"];
            $this->jsReturn["data"] = $returnData;
            $this->jsReturn["code"] = isset($code) ? $code : "";
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/promotion/item/getAll", name="getPromotionItems", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getPromotionItemsAction(Request $request)
    {
        $this->getFilters($request);
        $userId = $this->getUser()->getId();
        $promotionId = $request->get('promotionId');
        $this->jsReturn["data"] = $this->em->getRepository('E:Promotion')->getPromotionItems($this->jsReturn, $userId, $this->defaultStoreId, $promotionId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/promotion/item/available", name="getPromotionItemsAvailable", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getPromotionItemsAvailable(Request $request)
    {
        $this->getFilters($request);
        $userId = $this->getUser()->getId();
        $promotionId = $request->get('promotionId');
        $q = $request->get('q');
        $this->jsReturn["data"] = $this->em->getRepository('E:Promotion')->getPromotionItemsAvailable($this->jsReturn, $promotionId, $userId, $this->defaultStoreId, $q);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/promotion/item/add", name="promotionItemAdd", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function promotionItemAddAction(Request $request)
    {
        $user = $this->getUser();
        $promotionId = $request->request->get('promotionId');
        $items = $request->request->get('items');
        $returnData = [];
        if (!empty($promotionId) && !empty($items)) {
            foreach ($items as $item) {
                $promotionItem = new Entity\PromotionItem();
                $promotionItem->setId($returnData["id"] = $this->jsUtil->getUid());
                $promotionItem->setPromotion($this->em->getReference('E:Promotion', $promotionId));
                $promotionItem->setItem($this->em->getReference('E:Item', $item));
                $promotionItem->setRegisterDate($returnData["registerDate"] = $this->jsUtil->getLocalDateTime());
                $this->em->persist($promotionItem);
            }
            $this->jsReturn["msg"] = count($items) > 1 ? "Productos añadidos con éxito" : "Producto añadido con éxito";
            $this->jsReturn["data"] = $returnData;
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/promotion/item/delete", name="promotionItemDelete", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function promotionItemDeleteAction(Request $request)
    {
        $user = $this->getUser();
        $promotionId = $request->request->get('promotionId');
        $items = $request->get('items');
        foreach ($items as $item) {
            $promotionItem = $this->em->getRepository('E:Promotion')->getPromotionItem($promotionId, $item, $user->getId(), $this->defaultStoreId, "delete");
            if ($promotionItem) {
                $this->em->remove($promotionItem);
            }
        }
        $this->jsReturn["msg"] = count($items) > 1 ? "Productos desasociados" : "Producto desasociado";
        return $this->responseJson();
    }
}
