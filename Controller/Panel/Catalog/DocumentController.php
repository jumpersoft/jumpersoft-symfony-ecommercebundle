<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftImageExtension;
use Jumpersoft\EcommerceBundle\Validators\DocumentValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class DocumentController extends BaseController
{

    /**
     * @Route("/api/panel/document", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getImagesAction(Request $request)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:Document')->getImages(
            $this->defaultStoreId,
            $request->get("itemId"),
            $request->get("itemAttributeId"),
            $request->get("categoryId"),
            $this->getUser()->getId()
        );
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/document", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAction(Request $request, Filesystem $fs)
    {
        $images = $request->get('filesToDelete');
        $filesToDelete = [];
        $dirProject = $this->params->get('kernel.project_dir') . '/public/';
        if (count($images) > 0) {
            foreach ($images as $img) {
                $image = $this->em->getRepository('E:Document')->getImage($img["id"]);
                if ($iao = $image->getItemAttributeOption()) {
                    $ia = $iao->getItemAttribute();
                    $dir = $dirProject . $ia->getUrlFiles();
                } else {
                    $dir = $dirProject . ($image->getItem() ? $image->getItem()->getUrlFiles() : ($image->getCategory() ? $image->getCategory()->getUrlFiles() : $image->getStore()->getUrlFiles()));
                }
                array_push($filesToDelete, ["fileName" => $dir . "/" . $image->getFileName(), "thumbnail" => $dir . "/" . $image->getThumbnail()]);
                $this->em->remove($image);
            }
            //Delete fisical files
            foreach ($filesToDelete as $file) {
                if ($fs->exists($file["fileName"]) || $fs->exists($file["thumbnail"])) {
                    $fs->remove($file["fileName"]);
                    $fs->remove($file["thumbnail"]);
                }
            }
            $this->jsReturn["msg"] = count($images) > 1 ? "Imágenes eliminadas" : "Imagen eliminada";
            return $this->responseJson();
        }
    }

    /**
     * @Route("/api/panel/document", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function postAction(Request $request, JumpersoftImageExtension $imageExt, Filesystem $fs)
    {
        $files = $request->files->get('files');
        $data = $request->request->all();
        $user = $this->getUser();
        $dirProject = $this->params->get('kernel.project_dir') . '/public/';
        $userOriginalFileName = empty($data["userOriginalFileName"]) ? true : false;
        if (count($files) > 0) {
            $imageThumbSize = $this->params->get('jscommerce.images')["size.thumbnail"];
            if (isset($data["itemAttributeOptionId"])) {
                $item = $this->em->getRepository('E:Item')->getItem($data["itemId"], $user->getId(), $this->defaultStoreId, "update");
                $object = $item->getItemAttribute();
                $iao = $this->em->getRepository('E:ItemAttributeOption')->findOneById($data["itemAttributeOptionId"]);
                $countImages = $iao->getImages()->count();
            } elseif (isset($data["itemId"])) {
                $object = $item = $this->em->getRepository('E:Item')->getItem($data["itemId"], $user->getId(), $this->defaultStoreId, "update");
                $countImages = $item->getImages()->count();
            } elseif (isset($data["categoryId"])) {
                $object = $category = $this->em->getRepository('E:Category')->getCategory($data["categoryId"], $user->getId(), $this->defaultStoreId, "update");
                $countImages = $category->getImages()->count();
            } else {
                $object = $store = $this->em->getRepository('E:Store')->getStore($this->defaultStoreId, $user->getId(), "update");
                $countImages = $store->getImages()->count();
            }

            if (($countImages + count($files)) > $this->params->get('jscommerce.images')["maxFiles"]) {
                throw new \Exception("El máximo de imágenes permitido es " . $this->params->get('jscommerce.images')["maxFiles"]);
            }

            foreach ($files as $key => $file) {
                //Set data
                $id = $this->jsReturn["files"][$key]["id"] = $this->jsUtil->getUid();
                if ($userOriginalFileName) {
                    $fileName = $this->jsUtil->sanitizeFilename($file->getClientOriginalName(), true);
                    $thumbnail = pathinfo($fileName, PATHINFO_FILENAME) . '_' . $imageThumbSize . '.' . $file->getClientOriginalExtension();
                } else {
                    $fileName = $this->jsReturn["files"][$key]["id"] . '.' . $file->getClientOriginalExtension();
                    $thumbnail = $this->jsReturn["files"][$key]["id"] . '_' . $imageThumbSize . '.' . $file->getClientOriginalExtension();
                }
                $this->jsReturn["files"][$key]["active"] = true;
                $this->jsReturn["files"][$key]["fileName"] = $object->getUrlFiles() . "/" . $fileName;
                $this->jsReturn["files"][$key]["thumbnail"] = $object->getUrlFiles() . "/" . $thumbnail;
                $fileSize = $this->jsReturn["files"][$key]["fileSize"] = $file->getSize();
                if (!$fs->exists($imageDir = $dirProject . $object->getUrlFiles())) {
                    $fs->mkdir($imageDir);
                }
                $file->move($imageDir . '/', $fileName);
                list($width, $height) = getimagesize($imageDir . '/' . $fileName);
                $size = $this->jsReturn["files"][$key]["size"] = $width . 'x' . $height . 'px';
                //Save
                $document = new Entity\Document();
                $this->jsUtil->setValuesToEntity($document, [
                    "id" => $id,
                    "status" => 'IMG_ACT',
                    "registerDate" => $this->jsUtil->getLocalDateTime(),
                    "active" => 1,
                    "fileName" => $fileName,
                    "thumbnail" => $thumbnail,
                    "fileSize" => $fileSize,
                    "size" => $size,
                    "sequence" => ($countImages + ($key + 1)),
                ]);

                isset($iao) ? $document->setItemAttributeOption($iao) : (isset($item) ? $document->setItem($item) : (isset($category) ? $document->setCategory($category) : $document->setStore($store)));
                $this->em->persist($document);

                $imageExt->resizeImages($fileName, $imageDir, ['thumbSize' => $imageThumbSize], ['thumbSize']);
            }
            $this->jsReturn["msg"] = count($files) > 1 ? "Imágenes salvadas con éxito" : "Imagen salvada con éxito";
            return $this->responseJson();
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
    }

    /**
     * @Route("/api/panel/document/reorder", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function reorderAction(Request $request)
    {
        $images = $request->get('images');
        if (count($images) > 0) {
            foreach ($images as $key => $image) {
                if ($image = $this->em->getRepository('E:Document')->getImage($image["id"])) {
                    $image->setSequence($key);
                }
            }
            $this->jsReturn["msg"] = "Imágenes reordenadas";
            return $this->responseJson();
        }
    }

    /**
     * @Route("/api/panel/document", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction(Request $request)
    {
        $data = $request->request->get('image');
        $values = DocumentValidator::proccessInputValues($data, DocumentValidator::$document);
        if (DocumentValidator::validate(DocumentValidator::$document, $values)) {
            $image = $this->em->getRepository('E:Document')->getImage($data["id"]);
            //We do unique "IMG_ITEM_PRIMARY" for items
            if (!empty($values["typeId"]) && stristr("IMG_ITEM_PRIMARY|IMG_ITEM_SECONDARY|IMG_STORE_LOGO_HEADER|IMG_STORE_LOGO_SHORT_HEADER|IMG_STORE_LOGO_FOOTER", $values["typeId"]) && $values["typeId"] != ($image->getType() ? $image->getType()->getId() : null)) {
                $parameters = ["id" => $data["id"], "typeId" => $data["typeId"]];
                if ($image->getItemAttributeOption()) {
                    $and = " identity(d.itemAttributeOption) = :itemAttributeOptionId";
                    $parameters["itemAttributeOptionId"] = $image->getItemAttributeOption()->getId();
                } elseif ($image->getItem()) {
                    $and = " identity(d.item) = :itemId ";
                    $parameters["itemId"] = $image->getItem()->getId();
                } elseif ($image->getCategory()) {
                    $and = " identity(d.category) = :categoryId ";
                    $parameters["categoryId"] = $image->getCategory()->getId();
                } else {
                    $and = " identity(d.store) = :storeId ";
                    $parameters["storeId"] = $image->getStore()->getId();
                }
                $images = $this->em->createQuery("select d from E:Document d 
                                                   where d.id <> :id 
                                                     and identity(d.type) = :typeId 
                                                     and $and ")->setParameters($parameters)->getResult();
                foreach ($images as $iImage) {
                    $iImage->setType(null);
                    $this->em->persist($iImage);
                    $reloadForChangues = true;
                }
            }
            $this->jsUtil->setValuesToEntity($image, array_merge($values, ["type" => $data["typeId"] ?: null]));
            $this->em->persist($image);

            $this->jsReturn["msg"] = $this->msg["updateOK"];
            $this->jsReturn["reloadForChangues"] = $reloadForChangues ?? null;
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }
}
