<?php
namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\TagValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class TagController extends BaseController
{

    /**
     * @Route("/api/panel/catalog/tag", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAll(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Tag')->getTags($this->jsReturn, $request->get('q'), $this->getUser()->getId(), $request->get('currentStoreId') ?? $this->defaultStoreId, $request->get('for'));
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/catalog/tag/{id}", condition="request.isXmlHttpRequest()", methods={"GET"}, requirements={"id"="^(.{20})$"})
     */
    public function getAction($id, Request $request)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:Tag')->getTag($id, $this->getUser()->getId(), $request->get('currentStoreId') ?? $this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/catalog/tag", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function delete(Request $request)
    {
        $user = $this->getUser();
        $rows = $request->get('rows');
        foreach ($rows as $r) {
            $row = $this->em->getRepository('E:Tag')->getTag($r["id"], $user->getId(), $this->defaultStoreId, "delete");
            $this->em->remove($row);
        }
        $this->jsReturn["msg"] = count($rows) > 1 ? "Etiquetas eliminadas" : "Etiqueta eliminada";
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/catalog/tag/{id}", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction($id, Request $request)
    {
        //Messages
        $this->msg["insertOK"] = "Etiqueta creada con éxito";
        $this->msg["updateOK"] = "Etiqueta modificada con éxito";
        $this->msg["unitExist"] = "Yá existe una etiqueta con ese nombre";
        $this->msg["urlNameKeyExist"] = "Yá existe un url key con este nombre";

        $edit = $request->request->get('edit') == "true";
        $tagInput = $request->request->get('tag');
        $fieldValue = TagValidator::proccessInputValues($tagInput, TagValidator::$tag);

        $user = $this->getUser();

        $name = $fieldValue['name'];
        $urlKey = $fieldValue['urlKey'];

        //Check if unit measure name exist
        if ($this->em->getRepository('E:Tag')->checkTag($name, $id, $this->defaultStoreId)) {
            throw new \Exception($this->msg["unitExist"]);
        }
        //Check url key name exist
        if ($this->em->getRepository('E:Tag')->checkUrlKeyAction($urlKey, $id, $this->defaultStoreId)) {
            throw new \Exception($this->msg["urlNameKeyExist"]);
        }

        if (TagValidator::validate(TagValidator::$tag, $fieldValue)) {
            if (!$edit) {
                $tag = new Entity\Tag();
                $fieldValue = array_merge($fieldValue, [
                    "id" => $id = $this->jsUtil->getUid(),
                    "store" => $this->defaultStoreId,
                    "registerDate" => $this->jsUtil->getLocalDateTime()
                ]);
            } else {
                $tag = $this->em->getRepository('E:Tag')->getTag($id, $user->getId(), $this->defaultStoreId, "update");
                $tag->SetUpdateDate($this->jsUtil->getLocalDateTime());
            }

            $this->jsUtil->setValuesToEntity($tag, $fieldValue);
            $this->em->persist($tag);

            $this->jsReturn["msg"] = $this->msg["insertOK"];
            $this->jsReturn["id"] = !$edit ? $id : null;
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/catalog/tag/check/{name}/{id}", name="checkTag", methods={"POST"})
     */
    public function checkTagAction($name, $id, Request $request)
    {
        $result = $this->em->getRepository('E:Tag')->checkTag($name, $id, $this->defaultStoreId);
        return new Response($result ? "false" : "true");
    }

    /**
     * @Route("/api/panel/catalog/tag/checkUrlKey/{urlKey}/{id}", name ="checkTagUrlKey", methods={"POST"})
     */
    public function checkUrlKeyAction($urlKey, $id, Request $request)
    {
        $result = $this->em->getRepository('E:Tag')->checkUrlKeyAction($urlKey, $id, $this->defaultStoreId);
        return new Response(!$result ? "true" : "false");
    }
}
