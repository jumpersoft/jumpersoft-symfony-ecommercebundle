<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftImageExtension;
use Jumpersoft\EcommerceBundle\Validators\CategoryValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 * @Route("/api/panel/category", condition="request.isXmlHttpRequest()")
 */
class CategoryController extends BaseController
{

    /**
     * @Route("/tree", methods={"GET"})
     */
    public function getTreeAction(Request $request)
    {
        $this->getFilters($request);
        $rows = $this->em->getRepository('E:Category')->getCategoriesTree($this->jsReturn, $this->defaultStoreId, $node = $request->get('node'));
        if ($node != "root") {
            return $this->responseJson(null, null, null, $rows);
        }
        $this->jsReturn["children"] = $rows;
        return $this->responseJson();
    }

    /**
     * @Route("/{id}", methods={"GET"}, requirements={"id"="^(.{20})$"})
     */
    public function getAction($id)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:Category')->getCategory($id, $this->getUser()->getId(), $this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Route("/search/{typeId}", methods={"GET"})
     * Desc: Se utiliza para buscar una categoría por el tipo, ej. en combo para marcas en catálogo de productos.
     */
    public function getCategoryByType(Request $request, $typeId)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:Category')->getCategoryByType($this->defaultStoreId, $typeId, $request->get("q"));
        return $this->responseJson();
    }

    /**
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="^(.{20})$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAction($id, Filesystem $fs)
    {
        $user = $this->getUser();
        $row = $this->em->getRepository('E:Category')->getCategory($id, $user->getId(), $this->defaultStoreId, "delete");
        $this->em->remove($row);
        $catDir = $this->getFileStoreCategoryDir($this->defaultStoreId, $id);
        if ($fs->exists(array($catDir))) {
            $fs->remove(array($catDir));
        }
        $this->jsReturn["msg"] = "Categoría eliminada";
        return $this->responseJson();
    }

    /**
     * @Route("/{id}", methods={"PUT"}, requirements={"id"="^(.{20}|0)$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction($id, Request $request, JumpersoftImageExtension $imageExt)
    {
        //Messages
        $this->msg["insertOK"] = "Categoría creada con éxito";
        $this->msg["updateOK"] = "Categoría modificada con éxito";
        $this->msg["unitExist"] = "Yá existe una categoría con ese nombre";
        $this->msg["urlNameKeyExist"] = "El url key ya existe, elija otro por favor";
        $eventDate = $this->jsUtil->getLocalDateTime();
        $ipSource = $request->getClientIp();

        $edit = $request->request->get('edit') == "true";
        $inputValues = $request->request->get('category');
        $fieldValue = CategoryValidator::proccessInputValues($inputValues, CategoryValidator::$category);
        $itemImagesForm = $request->request->get('categoryImagesForm') ?? [];

        $user = $this->getUser();
        $name = $fieldValue['name'];

        if (CategoryValidator::validate(CategoryValidator::$category, $fieldValue)) {
            if ($edit) {
                $category = $this->em->getRepository('E:Category')->getCategory($id, $user->getId(), $this->defaultStoreId, "update");
                if ($category) {
                    $category->SetUpdateDate($this->jsUtil->getLocalDateTime());
                } else {
                    throw new \Exception($this->msg["updateError"]);
                }
            } else {
                $category = new Entity\Category();
                $fieldValue = array_merge($fieldValue, [
                    "id" => $id = $this->jsUtil->getUid(),
                    "ref" => $this->jsUtil->getUid(),
                    "store" => $this->defaultStoreId,
                    "urlFiles" => "/" . $this->getFileStoreCategoryDir($this->defaultStoreId, $id),
                    "registerDate" => $this->jsUtil->getLocalDateTime(),
                    "folder" => false,
                    "viewType" => "STR_VIEW_CAT_DEFAULT"
                ]);
            }

            $fieldValue["type"] = $fieldValue["typeId"];
            $this->jsUtil->setValuesToEntity($category, $fieldValue, $fieldValue["typeId"] == "MNU" && $category->getCategoryRelated() ? ["title", "subtitle", "label", "text"] : []);

            //Set parent
            if (!$edit) {
                if (!empty($fieldValue['parentId'])) {
                    $category->setParent($newParent = $this->em->getReference("E:Category", $fieldValue['parentId']));
                    $category->setSequence($newParent->getChildren()->Count() + 1);
                } else {
                    $category->setParent(null);
                    $category->setSequence($this->em->getRepository('E:Category')->getCategoryOnRootCount($this->defaultStoreId, $category->getType()->getId()) + 1);
                }
            }
            
            $this->em->persist($category);

            $this->jsReturn["msg"] = !$edit ? $this->msg["insertOK"] : $this->msg["updateOK"];
            $this->jsReturn["id"] = $id;
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }

    /**
     * @Route("/{hitMode}", methods={"POST"}, requirements={"hitMode"="changeIndex|changeLocation"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function changeIndexOrLocationAction($hitMode, Request $request)
    {
        $user = $this->getUser();
        $category = $this->em->getRepository('E:Category')->getCategory($categoryId = $request->request->get('categoryId'), $user->getId(), $this->defaultStoreId, "update");
        $parent = $category->getParent();
        $data = $request->request->all();

        if ($hitMode == "changeIndex") {
            $newIndex = $data["newIndex"];
            $catSiblings = $this->em->createQuery("select c from E:Category c
                                                    where c.parent = ?1 
                                                      and (c.sequence >= ?2 or c.sequence is null)
                                                      and c.id != ?3 ")->setParameters([1 => $parent, 2 => $newIndex, 3 => $category->getId()])->getResult();
            $category->setSequence($newIndex);
            foreach ($catSiblings as $cat) {
                $cat->setSequence(++$newIndex);
                $this->em->persist($cat);
            }
            $this->msg["updateOK"] = "Categoría reordenada";
        } elseif ($hitMode == "changeLocation") {
            $targetCategoryId = $data['targetCategoryId'];
            $targetCategoryId != "root" ? $targetCategory = $this->em->getRepository('E:Category')->getCategory($targetCategoryId, $user->getId(), $this->defaultStoreId, "update") : null;
            if ($targetCategoryId == "root" && $data["addToMenu"] == "false") {
                $category->setParent(null);
                $count = $this->em->createQuery("select count(c.id) from E:Category c where identity(c.type) = ?1 and identity(c.parent) is null ")->setParameters([1 => $data["typeId"]])->getSingleScalarResult();
                $category->setSequence($count + 1);
                $this->msg["updateOK"] = "Categoría reubicada";
            } elseif ($category->getType()->getId() == "MNU" || (isset($targetCategory) && $targetCategory->getType()->getId() != "MNU")) {
                // Relocate to other node
                $category->setParent($targetCategory);
                $category->setSequence($targetCategory->getChildren()->count() + 1);
                $this->msg["updateOK"] = "Categoría reubicada";
            } else {
                // Add item to menu
                $childMenu = $this->em->getRepository('E:Category')->getCategoryMenuChildFromCategoryRelated($category->getId(), $targetCategoryId != "root" ? $targetCategory->getId() : null);
                if (!$childMenu) {
                    $childMenu = new Entity\Category();
                    $id = $this->jsUtil->getUid();
                    $childMenu->SetId($id);
                    //$childMenu->SetRef($this->jsUtil->getUid());
                    $childMenu->setType($this->em->getReference('E:CategoryType', "MNU"));
                    $childMenu->setName($category->getName());
                    $childMenu->SetStore($this->em->getReference('E:Store', $this->defaultStoreId));
                    $childMenu->setUrlFiles("/" . $this->getFileStoreCategoryDir($this->defaultStoreId, $id));
                    $childMenu->SetRegisterDate($this->jsUtil->getLocalDateTime());
                    $childMenu->setCategoryRelated($category);
                    $childMenu->setParent($targetCategoryId != "root" ? $targetCategory : null);
                    $this->em->persist($childMenu);
                    $this->em->flush();
                    $this->jsReturn["id"] = $id;
                    $this->msg["updateOK"] = "Menú asignado correctamente";
                } else {
                    throw new \Exception("La opcion ya esta asignada al menu seleccionado");
                }
            }
        }
        $this->em->persist($category);
        $this->jsReturn["msg"] = $this->msg["updateOK"];
        return $this->responseJson();
    }

    /**
     * @Route("/{id}/item/{action}", methods={"GET"}, requirements={"id"="^(.{20})$", "action"="notAssigned|assigned"})
     */
    public function getCategoryItemAction($id, $action, Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Category')->getCategoryItems($this->jsReturn, $id, $this->defaultStoreId, $action);
        return $this->responseJson();
    }

    /**
     * @Route("/{id}/item", methods={"POST"}, requirements={"id"="^(.{20})$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function postCategoryItemAction($id, Request $request)
    {
        $ids = $request->request->get("ids");
        $countItems = $this->em->getRepository("E:Category")->getCategoryItemsCount($id);
        $event = $this->jsUtil->getLocalDateTime();
        foreach ($ids as $itemId) {
            $ic = new Entity\ItemCategory();
            $this->jsUtil->setValuesToEntity($ic, [
                "id" => $this->jsUtil->getUid(), "sequence" => $countItems++, "registerDate" => $event, "category" => $this->em->getReference("E:Category", $id), "item" => $this->em->getReference("E:Item", $itemId)]);
            $this->em->persist($ic);
        }
        return $this->responseJson();
    }

    /**
     * @Route("/{id}/item", methods={"DELETE"}, requirements={"id"="^(.{20})$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteCategoryItemAction($id, Request $request)
    {
        $sql = $this->em->createQuery("delete from E:ItemCategory a
                                        where identity(a.category) = :id 
                                          and identity(a.item) in (:ids) ")->setParameters(["id" => $id, "ids" => $request->get("ids")]);
        $sql->execute();
        $this->em->flush();
        $statement = $this->em->getConnection()->prepare("UPDATE ItemCategory AS t
                                                                JOIN
                                                                ( SELECT @rownum:=@rownum+1 as rownum, categoryId
                                                                  FROM ItemCategory    
                                                                  CROSS JOIN (select @rownum := 0) rn
                                                                  WHERE categoryId = :id
                                                                  order by sequence asc
                                                                ) AS r ON t.categoryId = r.categoryId
                                                                SET t.sequence = r.rownum");
        $statement->bindValue('id', $id);
        $statement->execute();
        $this->jsReturn["msg"] = "Productos desasignados";
        return $this->responseJson();
    }

    /**
     * @Route("/{categoryId}/item/reorder/{id}/{oldIndex}/{newIndex}", methods={"POST"}, requirements={"categoryId"="^(.{20})$", "id"="^(.{20})$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function reorderCategoryItemAction($categoryId, $id, $oldIndex, $newIndex)
    {
        $dir = $oldIndex < $newIndex ? "down" : "up";
        $subCondition = $dir == "down" ? "and ((a.sequence >= :oldIndex and a.sequence <= :newIndex) or a.sequence is null)" : "and ((a.sequence >= :newIndex and a.sequence <= :oldIndex) or a.sequence is null)";
        $itemCategory = $this->em->getReference("E:ItemCategory", $id);
        $itemsSiblings = $this->em->createQuery("select a from E:ItemCategory a
                                                  where a.category = :category
                                                     $subCondition
                                                     and a.id <> :id
                                               order by a.sequence asc")
                        ->setParameters(["category" => $this->em->getReference("E:Category", $categoryId), "oldIndex" => $oldIndex, "newIndex" => $newIndex, "id" => $id])
                        ->setFirstResult(0)->setMaxResults(50)->getResult();
        foreach ($itemsSiblings as $key => $a) {
            if ($dir == "down") {
                $a->setSequence($oldIndex++);
                $this->em->persist($a);
                if ($key + 1 == count($itemsSiblings)) {
                    $itemCategory->setSequence($oldIndex++);
                    $this->em->persist($itemCategory);
                }
            } else {
                if ($key == 0) {
                    $itemCategory->setSequence($newIndex++);
                    $this->em->persist($itemCategory);
                }
                $a->setSequence($newIndex++);
                $this->em->persist($a);
            }
        }
        $this->jsReturn["msg"] = "Producto reordenado";
        return $this->responseJson();
    }
}
