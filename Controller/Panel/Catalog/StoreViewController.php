<?php
namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\StoreCatalogValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class StoreViewController extends BaseController
{

    /**
     * @Route("/api/panel/store/view", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:Store')->getStoreViewConf($this->getUser()->getId(), $this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/store/view/general", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putViewGeneralAction(Request $request)
    {
        $storeInput = $request->request->get('store');
        $id = $request->request->get('id');
        $fieldValue = StoreCatalogValidator::proccessInputValues($storeInput, StoreCatalogValidator::$storeView);
        $user = $this->getUser();
        if (StoreCatalogValidator::validate(StoreCatalogValidator::$storeView, $fieldValue)) {
            $store = $this->em->getRepository('E:Store')->getStore($id, $user->getId(), "update", $this->isUser('ROLE_SUPER_ADMIN'));
            if (!($viewGeneral = $store->getViewGeneral())) {
                $viewGeneral = new Entity\StoreViewGeneral();
                $viewGeneral->setStore($store);
            }
            $this->jsUtil->setValuesToEntity($viewGeneral, $fieldValue);
            $this->em->persist($viewGeneral);
            $this->jsReturn["msg"] = $this->msg["updateOK"];
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/store/view/social", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getSocialNetworksAction()
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:StoreSocialNetwork')->getSocialNetworks($this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/store/view/social/{id}", condition="request.isXmlHttpRequest()", methods={"PUT"}, requirements={"id"="^(.{20}|0)$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putSocialAction($id, Request $request)
    {
        $user = $this->getUser();
        $edit = !empty($id);
        $data = $request->request->get('socialNetwork');
        $values = StoreCatalogValidator::proccessInputValues($data, StoreCatalogValidator::$socialNetwork);

        if (StoreCatalogValidator::validate(StoreCatalogValidator::$socialNetwork, $values)) {
            $eventDate = $this->jsUtil->getLocalDateTime();
            if (!$edit) {
                $social = new Entity\StoreSocialNetwork();
                $this->jsUtil->setValuesToEntity($social, [
                    "id" => $id = $this->jsUtil->getUid(),
                    "store" => $this->defaultStoreId,
                    "registerDate" => $eventDate
                ]);
            } else {
                $social = $this->em->getRepository("E:StoreSocialNetwork")->findOneById($id);
                $social->setUpdateDate($eventDate);
            }

            $this->jsUtil->setValuesToEntity($social, $values);
            $this->em->persist($social);
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        $this->jsReturn["msg"] = $edit ? $this->msg["updateOK"] : $this->msg["insertOK"];
        $this->jsReturn["id"] = $id;
        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route("/api/panel/store/view/social", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function delete(Request $request)
    {
        $user = $this->getUser();
        $ids = $request->get("ids");
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $socialNetwork = $this->em->getRepository('E:StoreSocialNetwork')->findOneById($id);
                $this->em->remove($socialNetwork);
            }

            $this->jsReturn["msg"] = count($ids) == 1 ? "Item eliminado" : "Items eliminados";
            return $this->responseJson();
        }
        throw new \Exception($this->msg["deleteError"]);
    }
}
