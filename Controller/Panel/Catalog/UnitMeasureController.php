<?php
namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\ItemValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SUPER_ADMIN')")
 */
class UnitMeasureController extends BaseController
{

    /**
     * @Route("/api/panel/catalog/unitMeasure/getAll", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:UnitMeasure')->getUnitsMeasures($this->jsReturn);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/catalog/unitMeasure/get", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAction(Request $request)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:UnitMeasure')->getUnitMeasure($request->get('id'), $this->getUser()->getId());
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/catalog/unitMeasure/delete", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAction(Request $request)
    {
        $user = $this->getUser();
        $rows = $request->request->get('rows');
        if (count($rows) > 0) {
            foreach ($rows as $r) {
                $row = $this->em->getRepository('E:UnitMeasure')->getUnitMeasure($r["id"], "delete");
                $this->em->remove($row);
            }
            $this->jsReturn["msg"] = count($rows) > 1 ? $this->msg["deletedOKn"] : $this->msg["deletedOK"];
        }
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/catalog/unitMeasure/put", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction(Request $request)
    {
        //Messages
        $this->msg["insertOK"] = "Unidad de medida creada con éxito";
        $this->msg["updateOK"] = "Unidad de medida modificada con éxito";
        $this->msg["unitExist"] = "Yá existe una unidad de medida con ese nombre";

        $eventDate = $this->jsUtil->getLocalDateTime();
        $ipSource = $request->getClientIp();

        $edit = $request->request->get('edit') == "true";
        $unitMeasureInput = $request->request->get('unitMeasure');
        $values = ItemValidator::proccessInputValues($unitMeasureInput, ItemValidator::$unitMeasure);
        $user = $this->getUser();

        //Check if unit measure name exist
        if ($this->em->getRepository('E:UnitMeasure')->checkUnitMeasure($values['name'], $values['id'])) {
            throw new \Exception($this->msg["unitExist"]);
        }

        //Check if unit id
        if (!$edit && $this->em->getRepository('E:UnitMeasure')->checkId($values['id'])) {
            throw new \Exception($this->msg["unitExist"]);
        }

        if (ItemValidator::validate(ItemValidator::$unitMeasure, $values)) {
            if (!$edit) {
                $unitMeasure = new Entity\UnitMeasure();
                $unitMeasure->SetRegisterDate($eventDate);
            } else {
                $unitMeasure = $this->em->getRepository('E:UnitMeasure')->getUnitMeasure($values['id'], "update");
                $unitMeasure->SetUpdateDate($eventDate);
            }

            $this->jsUtil->setValuesToEntity($unitMeasure, $values);
            $this->em->persist($unitMeasure);

            $this->jsReturn["msg"] = !$edit ? $this->msg["insertOK"] : $this->msg["updateOK"];
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/catalog/unitMeasure/check", methods={"POST"})
     */
    public function checkUnitMeasureAction(Request $request)
    {
        $result = $this->em->getRepository('E:UnitMeasure')->checkUnitMeasure($request->request->get('name'), $request->request->get('id'), $this->defaultStoreId);
        return new Response($result ? "false" : "true");
    }
}
