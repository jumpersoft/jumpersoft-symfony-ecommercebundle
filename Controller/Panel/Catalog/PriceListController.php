<?php
namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\PriceListValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class PriceListController extends BaseController
{

    /**
     * @Route("/api/panel/priceList", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:PriceList')->getPriceLists($this->jsReturn, $this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/priceList/{id}", condition="request.isXmlHttpRequest()", methods={"GET"}, requirements={"id"="^(.{20})$"})
     */
    public function getAction($id)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:PriceList')->getPriceList($id, $this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route("/api/panel/priceList/{id}", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @JsResponse(csrf="true")
     */
    public function deleteAction($id)
    {
        $row = $this->em->getRepository('E:PriceList')->getPriceList($id, $this->defaultStoreId, "delete");
        $this->em->remove($row);
        $this->em->flush();
        $this->jsReturn["msg"] = "Lista de precios eliminada";
        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route("/api/panel/priceList/{id}", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction($id, Request $request)
    {
        $edit = $request->request->get('edit') == "true";
        $priceListInput = $request->request->get('priceList');
        $values = PriceListValidator::proccessInputValues($priceListInput, PriceListValidator::$priceList);
        $returnData["id"] = $request->request->get('id');

        if (PriceListValidator::validate(PriceListValidator::$priceList, $values)) {
            $priceList = new Entity\PriceList();
            if (!$edit) {
                $values = array_merge($values, [
                    "id" => $returnData["id"] = $this->jsUtil->getUid(),
                    "store" => $this->defaultStoreId,
                    "registerDate" => $returnData["registerDate"] = $this->jsUtil->getLocalDateTime(),
                    "status" => $returnData["statusId"] = "PRL_ACT",
                ]);
                $returnData["status"] = "Activa";
            } else {
                $priceList = $this->em->getRepository('E:PriceList')->getPriceList($id, $this->defaultStoreId, "update");
                $priceList->SetUpdateDate($returnData["updateDate"] = $this->jsUtil->getLocalDateTime());
            }
            $this->jsUtil->setValuesToEntity($priceList, $values);
            $this->em->persist($priceList);
            $this->jsReturn["msg"] = !$edit ? "Lista de precios creada con éxito" : "Lista de precios modificada con éxito";
            $this->jsReturn["data"] = $returnData;
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/priceList/item", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getPriceListItemsAction(Request $request)
    {
        $this->getFilters($request);
        $userId = $this->getUser()->getId();
        $priceListId = $request->get('priceListId');
        $this->jsReturn["data"] = $this->em->getRepository('E:PriceList')->getPriceListItems($this->jsReturn, $userId, $this->defaultStoreId, $priceListId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/priceList/item/available", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getPriceListItemsAvailable(Request $request)
    {
        $this->getFilters($request);
        $userId = $this->getUser()->getId();
        $priceListId = $request->get('priceListId');
        $q = $request->get('q');
        $this->jsReturn["data"] = $this->em->getRepository('E:PriceList')->getPriceListItemsAvailable($this->jsReturn, $priceListId, $userId, $this->defaultStoreId, $q);
        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route("/api/panel/priceList/item/{priceListId}", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function priceListItemAddAction($priceListId, Request $request)
    {
        $user = $this->getUser();
        $items = $request->request->get('items');
        $returnData = [];
        if (!empty($priceListId) && !empty($items)) {
            foreach ($items as $it) {
                $item = $this->em->getRepository("E:Item")->findOneById($it);
                $newItem = new Entity\PriceListItem();
                $newItem->setId($this->jsUtil->getUid());
                $newItem->setPriceList($this->em->getReference('E:PriceList', $priceListId));
                $newItem->setItem($item);
                $newItem->setCurrency($item->getCurrency());
                $newItem->setRegularPrice($item->getSalePrice());
                $newItem->setSalePrice($item->getSalePrice());
                $newItem->setRegisterDate($this->jsUtil->getLocalDateTime());
                $this->em->persist($newItem);
            }
            $this->jsReturn["msg"] = count($items) > 1 ? "Productos añadidos con éxito" : "Producto añadido con éxito";
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route("/api/panel/priceList/item/{id}", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function priceListItemUpdateAction($id, Request $request)
    {
        $user = $this->getUser();
        $data = $request->request->get('item');
        $data = PriceListValidator::proccessInputValues($data, PriceListValidator::$priceListItem);
        if (PriceListValidator::validate(PriceListValidator::$priceListItem, $data)) {
            $item = $this->em->getRepository("E:PriceListItem")->findOneById($id);
            $item->setUpdateDate($this->jsUtil->getLocalDateTime());
            $this->jsUtil->setValuesToEntity($item, $data);
            $this->em->persist($item);
            $this->jsReturn["msg"] = "Precio modificado con éxito";
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route("/api/panel/priceList/item/{priceListId}", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function priceListItemDeleteAction($priceListId, Request $request)
    {
        $user = $this->getUser();
        $items = $request->get('items');
        foreach ($items as $item) {
            $priceListItem = $this->em->getRepository('E:PriceList')->getPriceListItem($priceListId, $item, $user->getId(), $this->defaultStoreId, "delete");
            if ($priceListItem) {
                $this->em->remove($priceListItem);
            }
        }
        $this->jsReturn["msg"] = count($items) > 1 ? "Productos desasociados" : "Producto desasociado";
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/priceList/customer", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getPriceListCustomerAction(Request $request)
    {
        $this->getFilters($request);
        $priceListId = $request->get('priceListId');
        $this->jsReturn["data"] = $this->em->getRepository('E:PriceList')->getPriceListCustomers($this->jsReturn, $this->defaultStoreId, $priceListId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/priceList/customer/available", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getPriceListCustomersAvailable(Request $request)
    {
        $this->getFilters($request);
        $userId = $this->getUser()->getId();
        $priceListId = $request->get('priceListId');
        $q = $request->get('q');
        $this->jsReturn["data"] = $this->em->getRepository('E:PriceList')->getPriceListCustomersAvailable($this->jsReturn, $priceListId, $q);
        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route("/api/panel/priceList/customer/{priceListId}", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function priceListCustomerAddAction($priceListId, Request $request)
    {
        $customers = $request->request->get('customers');
        $returnData = [];
        if (!empty($priceListId) && !empty($customers)) {
            foreach ($customers as $it) {
                $customer = $this->em->getRepository("E:User")->findOneById($it);
                $newItem = new Entity\PriceListCustomer();
                $newItem->setId($this->jsUtil->getUid());
                $newItem->setPriceList($this->em->getReference('E:PriceList', $priceListId));
                $newItem->setCustomer($customer);
                $newItem->setRegisterDate($this->jsUtil->getLocalDateTime());
                $this->em->persist($newItem);
            }
            $this->jsReturn["msg"] = count($customers) > 1 ? "Clientes añadidos con éxito" : "Cliente añadido con éxito";
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route("/api/panel/priceList/customer/{priceListId}", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function priceListCustomerDeleteAction($priceListId, Request $request)
    {
        $customers = $request->get('customers');
        foreach ($customers as $customer) {
            $priceListCustomer = $this->em->getRepository('E:PriceListCustomer')->findOneById($customer);
            if ($priceListCustomer) {
                $this->em->remove($priceListCustomer);
            }
        }
        $this->jsReturn["msg"] = count($customers) > 1 ? "Clientes desasociados" : "Cliente desasociado";
        return $this->responseJson();
    }
}
