<?php
namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\TermsValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class TermsController extends BaseController
{

    /**
     * @Route("/api/panel/terms/getAll", name="getAllTerms", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $userId = $this->getUser()->getId();
        $this->jsReturn["data"] = $this->em->getRepository('E:Terms')->getTerms($this->jsReturn, $userId, $this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/terms/get", name="getTerms", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAction(Request $request)
    {
        $row = $this->em->getRepository('E:Terms')->getTerm($request->get('id'), $this->getUser()->getId(), $this->defaultStoreId);
        $this->jsReturn["data"] = $row;
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/terms/delete", name="deleteTerms", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true")
     */
    public function deleteAction(Request $request)
    {
        $this->msg["deletedOK"] = "Términos eliminados";
        $user = $this->getUser();
        $id = $request->request->get('id');
        $row = $this->em->getRepository('E:Terms')->getTerm($id, $user->getId(), $this->defaultStoreId, "delete");
        $this->em->remove($row);
        $this->em->flush();
        $this->jsReturn["msg"] = $this->msg["deletedOK"];
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/terms/put", name="termsNewEdit", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction(Request $request)
    {
        //Messages
        $this->msg["insertOK"] = "Términos creados con éxito";
        $this->msg["updateOK"] = "Términos modificados con éxito";

        $eventDate = $this->jsUtil->getLocalDateTime();
        $ipSource = $request->getClientIp();

        $edit = $request->request->get('edit');
        $inputValues = $request->request->get('terms');
        $fieldValue = TermsValidator::proccessInputValues($inputValues, TermsValidator::$terms);

        $user = $this->getUser();
        $returnData["id"] = $request->request->get('id');

        if (TermsValidator::validate(TermsValidator::$terms, $fieldValue)) {
            $userLog = new Entity\UserLog($this->jsUtil->getUid(), $user, $ipSource, $eventDate, $this->em->getReference('E:Status', $edit == 'false' ? 'TRM_INS' : 'TRM_UPD'));

            if ($edit == "true") {
                $terms = $this->em->getRepository('E:Terms')->getTerm($returnData["id"], $user->getId(), $this->defaultStoreId, "update");
                $terms->SetUpdateDate($returnData["updateDate"] = $userLog->getEventDate());
            } else {
                $terms = new Entity\Terms();
                $fieldValue = array_merge($fieldValue, [
                    "id" => $returnData["id"] = $this->jsUtil->getUid(),
                    "registerDate" => $returnData["registerDate"] = $userLog->getEventDate(),
                    "store" => $this->defaultStoreId
                ]);
            }

            $fieldValue["type"] = $fieldValue["typeId"];
            $this->jsUtil->setValuesToEntity($terms, $fieldValue);
            $this->em->persist($terms);
            $this->em->persist($userLog);
            $this->jsReturn["msg"] = ($edit == "true" ? $this->msg["updateOK"] : $this->msg["insertOK"]);
            $this->jsReturn["data"] = $returnData;
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
        return $this->responseJson();
    }
}
