<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\AnnouncementValidator;
use Jumpersoft\EcommerceBundle\Entity;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 */
class AnnouncementController extends BaseController
{

    /**
     * @Configuration\Security("is_granted('ROLE_CUSTOMER')")
     * @Route("/api/panel/catalog/announcement/getAll", name="getAnnouncements", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $rows = $this->em->getRepository('E:Announcement')->getAnnouncements($this->jsReturn, $this->getUser()->getId(), $this->defaultStoreId);
        $this->jsReturn["data"] = $rows;
        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_CUSTOMER')")
     * @Route("/api/panel/catalog/announcement/get", name="getAnnouncement", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAction(Request $request)
    {
        $row = $this->em->getRepository('E:Announcement')->getAnnouncement($request->get('id'), $this->getUser()->getId(), $this->defaultStoreId);
        $this->jsReturn["data"] = $row;
        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_SELLER')")
     * @Route("/api/panel/catalog/announcement/delete", name="deleteAnnouncement", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true")
     */
    public function deleteAction(Request $request)
    {
        $row = $this->em->getRepository('E:Announcement')->getAnnouncement($request->request->get('id'), $this->getUser()->getId(), $this->defaultStoreId, "delete");
        $this->em->remove($row);
        $this->em->flush();
        $this->jsReturn["msg"] = "Aviso eliminado";
        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_SELLER')")
     * @Route("/api/panel/catalog/announcement/put", name="announcementsNewEdit", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction(Request $request)
    {
        $edit = $request->request->get('edit');
        $inputValues = $request->request->get('announcement');
        $fieldValue = AnnouncementValidator::proccessInputValues($inputValues, AnnouncementValidator::$announcement);
        $user = $this->getUser();
        $returnData["id"] = $request->request->get('id');

        if (AnnouncementValidator::validate(AnnouncementValidator::$announcement, $fieldValue)) {
            if ($edit == "true") {
                $announcement = $this->em->getRepository('E:Module')->getAnnouncement($returnData["id"], $user->getId(), $this->defaultStoreId, "update");
                $announcement->setUpdateDate($this->jsUtil->getLocalDateTime());
                //Delete modules
                $modules = $announcement->getModules();
                if (count($modules) > 0) {
                    foreach ($modules as $module) {
                        $announcement->removeModule($module);
                    }
                    $this->em->persist($announcement);
                    $this->em->flush();
                }
                //Delete roles
                $roles = $announcement->getRoles();
                if (count($roles) > 0) {
                    foreach ($roles as $role) {
                        $announcement->removeRole($role);
                    }
                    $this->em->persist($announcement);
                    $this->em->flush();
                }
            } else {
                $announcement = new Entity\Announcement();
                $fieldValue = array_merge($fieldValue, [
                    "id" => $returnData["id"] = $this->jsUtil->getUid(),
                    "registerDate" => $returnData["registerDate"] = $this->jsUtil->getLocalDateTime(),
                    "store" => $this->defaultStoreId,
                    "status" => $returnData["statusId"] = 'ANC_ACT'
                ]);
                $returnData["status"] = "Activo";
            }

            //Set general values
            $this->jsUtil->setValuesToEntity($announcement, $fieldValue);
            
            //Add modules
            $modules = $fieldValue["moduleIds"];
            if (count($modules) > 0) {
                foreach ($modules as $module) {
                    $announcement->addModule($this->em->getReference('E:Module', $module));
                }
            }
            //Add roles
            $roles = $fieldValue["roleIds"];
            if (count($roles) > 0) {
                foreach ($roles as $role) {
                    $announcement->addRole($this->em->getReference('E:Role', $role));
                }
            }
            $this->em->persist($announcement);
            $this->jsReturn["msg"] = ($edit == "true" ? "Aviso modificado con éxito" : "Aviso creado con éxito");
            $this->jsReturn["data"] = $returnData;
            return $this->responseJson();
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
    }

    /**
     * @Route("/announcement/getAnnouncementsToShow", name="getAnnouncementsToShow", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAnnouncementsToShow(Request $request)
    {
        $user = $this->getUser();
        $roles = $user ? $user->getRole() : "ROLE_ANON";
        $moduleId = $request->get('moduleId');
        $this->jsReturn["data"] = $this->em->getRepository('E:Announcement')->getAnnouncementsToShow($moduleId, $roles, $this->defaultStoreId);
        return $this->responseJson();
    }
}
