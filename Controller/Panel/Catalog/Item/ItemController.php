<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog\Item;

use Symfony\Component\Filesystem\Filesystem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\ItemValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 * @Route("/api/panel/item")
 */
class ItemController extends BaseController
{

    /**
     * @Route("/", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAll(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Item')->getItems($this->jsReturn, $this->getUser()->getId(), $this->defaultStoreId, $request->get('q'), $request->get('for'));
        return $this->responseJson();
    }

    /**
     * @Route("/{id}", condition="request.isXmlHttpRequest()", methods={"GET"}, requirements={"id"="^(.{20})$"})
     */
    public function getAction(Request $request)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:Item')->getItem($request->get('id'), $this->getUser()->getId(), $this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route(condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function delete(Request $request, Filesystem $fs)
    {
        $user = $this->getUser();
        $itemIds = $request->get("itemIds");
        if (count($itemIds) > 0) {
            foreach ($itemIds as $id) {
                $item = $this->em->getRepository('E:Item')->getItem($id, $user->getId(), $this->defaultStoreId, "delete");
                $this->em->remove($item);
                $itemDir = $this->getFileStoreItemDir($this->defaultStoreId, $id);
                if ($fs->exists(array($itemDir))) {
                    $fs->remove(array($itemDir));
                }
            }

            $this->jsReturn["msg"] = count($itemIds) == 1 ? "Producto eliminado" : "Productos eliminados";
            return $this->responseJson();
        }
        throw new \Exception($this->msg["deleteError"]);
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route("/{id}", condition="request.isXmlHttpRequest()", methods={"PUT"}, requirements={"id"="^(.{20}|0)$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function put($id, Request $request)
    {
        $edit = $request->request->get('edit') === "true";
        $data = $request->request->get('item');
        $values = ItemValidator::proccessInputValues($data, ItemValidator::$item);
        $user = $this->getUser();
        $response["id"] = $id;
        $unitMeasure = $this->em->getRepository("E:UnitMeasure")->findOneById($values["unitMeasureId"] ?? "UNI_NAN");

        //CHECK NAME
        if ($this->em->getRepository('E:Item')->checkName($values["name"], $response["id"], $this->defaultStoreId)) {
            throw new \Exception("El nombre del producto ya existe, elija otro por favor");
        }

        //Check urlKey, if extis add a random number
        if ($this->em->getRepository('E:Item')->checkUrlKey($values["urlKey"], $response["id"], $this->defaultStoreId)) {
            $values["urlKey"] = $values["urlKey"] + $this->getUid(false, 5);
        }

        $values["fractionable"] = isset($values["fractionable"]) ? $values["fractionable"] : false;

        //FRACTIONABLE QUANTITY RULES
        $specialFractionableFields = [
            "stock" => ($values["fractionable"] == true ? "stockFractionable" . $values["decimals"] : "stock"),
            "minQtyAllowedInCart" => ($values["fractionable"] == true ? "stockFractionable" . $values["decimals"] : "stock"),
            "maxQtyAllowedInCart" => ($values["fractionable"] == true ? "stockFractionable" . $values["decimals"] : "stock"),
            "maxQtyAllowedInCart" => ($values["fractionable"] == true ? "stockFractionable" . $values["decimals"] : "stock"),
            "notifyForQtyBelow" => ($values["fractionable"] == true ? "stockFractionable" . $values["decimals"] : "stock"),
            "qtyToBecomeOutOfStock" => ($values["fractionable"] == true ? "stockFractionable" . $values["decimals"] : "stock"),
            "quantityByUnitBulkSale" => ($values["bulkSale"] ? "stockFractionable" . $values["decimalsBulkSale"] : "stock")];

        //AVOIDED FIELDS RULES
        $avoidFields = [];
        $values["typeId"] == "ITE_CON" ? array_push($avoidFields, "decimals", "weight", "unitMeasureId", "minQtyAllowedInCart", "maxQtyAllowedInCart") : null;
        $values["trackingTypeId"] == "INV_TYP_NAN" || $values["trackingTypeId"] == "INV_TYP_DET" ? array_push($avoidFields, "stock", "notifyForQtyBelow", "qtyToBecomeOutOfStock") : null;
        !$values["fractionable"] ? array_push($avoidFields, "decimals") : null;
        $values["typeId"] != "ITE_CON" && $data["workWithPricesParent"] == "true" && isset($data["parent"]) ? array_push($avoidFields, "currency", "basePrice", "salePrice") : null;
        !$values["radioactive"] ? array_push($avoidFields, "isotopeId", "useVolumeToFulFill") : null;
        !$values["bulkSale"] && !$unitMeasure->getGrouper() ? array_push($avoidFields, "unitMeasureBulkSaleId", "quantityByUnitBulkSale", "decimalsBulkSale") : null;

        if (ItemValidator::validate(ItemValidator::$item, $values, $specialFractionableFields, $avoidFields)) {
            if (!$edit) {
                $item = new Entity\Item();
                $values = array_merge($values, [
                    "id" => $response["id"] = $this->jsUtil->getUid(),
                    "registerDate" => $response["registerDate"] = $this->jsUtil->getLocalDateTime(),
                    "store" => $this->defaultStoreId,
                    "urlFiles" => "/" . $this->getFileStoreItemDir($this->defaultStoreId, $response["id"]),
                    "type" => $values["typeId"],
                ]);
            } else {
                $item = $this->em->getRepository('E:Item')->getItem($response["id"], $user->getId(), $this->defaultStoreId, "update");
                $item->setUpdateDate($response["updateDate"] = $this->jsUtil->getLocalDateTime());
            }

            $values = array_merge($values, [
                "status" => $values["statusId"],
                "currency" => $values["currency"],
                "unitMeasure" => $values["typeId"] == "ITE_CON" ? "UNI_NAN" : $values["unitMeasureId"],
                "brand" => $values["brandId"],
                "unitMeasureBulkSale" => $values["bulkSale"] || $values["radioactive"] || $unitMeasure->getGrouper() ? $values["unitMeasureBulkSaleId"] : null,
                //"trackingType" => $values["radioactive"] ? 'INV_TYP_DET' : $values["trackingTypeId"], //Utilizar esta función cuando los productos radioactivos se surtan correctamente
                "trackingType" => $values["trackingTypeId"],
                "isotope" => $values["radioactive"] ? $values["isotopeId"] : null,
                "newUntil" => $values['newUntil'] ?? null,
                "discountUntil" => $values['discountUntil'] ?? null
            ]);

            $this->jsUtil->setValuesToEntity($item, $values, $edit ? ["type"] : []);

            //CHANGE GLOBAL PARENT PROPERTIES IN CHILDS
            if ($values["typeId"] == "ITE_CON" && $edit) {
                $childs = $item->getVariants();
                foreach ($childs as $child) {
                    $this->jsUtil->setValuesToEntity($child, ["perishable" => $item->getPerishable(), "dangerous" => $item->getDangerous(), "radioactive" => $item->getRadioactive(), "isotope" => $item->getIsotope(), "useVolumeToFulFill" => $item->getUseVolumeToFulFill()]);
                }
            }

            $this->em->persist($item);

            !$edit ? $this->jsReturn["data"]["id"] = $response["id"] : "";
            $this->jsReturn["msg"] = !$edit ? "Producto creado con éxito" : "Producto modificado con éxito";
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route("/extra/{action}/{id}",condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putExtra($action, $id, Request $request)
    {
        $user = $this->getUser();
        switch ($action) {
            case "meta":
                $itemValidators = ItemValidator::$meta;
                $this->msg["updateOK"] = "Meta data modificada con éxito";
                break;
            case "socialMedia":
                $itemValidators = ItemValidator::$socialMedia;
                $this->msg["updateOK"] = "Valores modificados con éxito";
                break;
        }

        $values = ItemValidator::proccessInputValues($request->request->get('item'), $itemValidators);

        switch ($action) {
            case "meta":
                //Check urlKey
                if ($this->em->getRepository('E:Item')->checkUrlKey($values["urlKey"], $id, $this->defaultStoreId)) {
                    throw new \Exception($this->msg["urlNameExist"]);
                }
                break;
        }

        if (ItemValidator::validate($itemValidators, $values)) {
            $item = $this->em->getRepository('E:Item')->getItem($id, $user->getId(), $this->defaultStoreId, "update");
            $item->setUpdateDate($this->jsUtil->getLocalDateTime());
            $this->jsUtil->setValuesToEntity($item, $values);

            switch ($action) {
                case "socialMedia":
                    $item->setSocialLinks($values);
                    break;
            }

            $this->em->persist($item);
            $this->jsReturn["msg"] = $this->msg["updateOK"];
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
        return $this->responseJson();
    }

    /**
     * @Route("/checkName", name="checkItemName", methods={"POST"})
     */
    public function checkNameAction(Request $request)
    {
        $name = $request->request->get('name');
        $id = $request->request->get('id');
        $result = $this->em->getRepository('E:Item')->checkName($name, $id, $this->defaultStoreId);
        return new Response($result ? "false" : "true");
    }

    /**
     * @Route("/checkUrlKey", name ="checkItemUrlKey", methods={"GET"})
     */
    public function checkUrlKeyAction(Request $request)
    {
        $urlKey = $request->request->get('urlKey');
        $id = $request->request->get('id');
        $result = $this->em->getRepository('E:Item')->checkUrlKey($urlKey, $id, $this->defaultStoreId);
        return new Response(!$result ? "true" : "false");
    }

    /**
     * @Route("/export/{format}", methods={"POST", "GET"}, requirements={"format"="csv|xls|xlsx"})
     */
    public function export($format = "csv", Request $request, SerializerInterface $serializer)
    {
        $this->getFilters($request);
        $data = $this->em->getRepository('E:Item')->getItems($this->jsReturn, $this->getUser()->getId(), $this->defaultStoreId, $request->get('q'), "report", $request->request->get("orders"));
        if ($format == "xls" || $format == "xlsx") {
            $response = $this->responseXls($this->getItemsXls($data), 'items' . $this->jsUtil->getLocalDateTimeString("YmdHis") . '.xls');
            return $response;
        } else {
            return $this->responseFile($serializer->encode($data, 'csv'), 'text/csv', 'items' . $this->jsUtil->getLocalDateTimeString("YmdHis") . '.csv');
        }
    }

    public function getItemsXls($data)
    {
        $spreadsheet = IOFactory::load($this->params->get('views.document') . "/report/items.xlsx");
        $worksheet = $spreadsheet->getActiveSheet();
        $count = count($data);
        $row = 9; // la fila de la templeta donde empieza la tabla
        for ($i = 0; $i < $count; $i++) {
            $worksheet->getCell("A$row")->setValue($i + 1);
            $worksheet->getCell("B$row")->setValue($data[$i]["name"]);
            $worksheet->getCell("C$row")->setValue($data[$i]["sku"]);
            $worksheet->getCell("D$row")->setValue($data[$i]["regularPrice"]);
            $worksheet->getCell("E$row")->setValue($data[$i]["discount"] . ($data[$i]["discountType"] == "%" ? "%" : ""));
            $worksheet->getCell("F$row")->setValue($data[$i]["salePrice"]);
            $worksheet->getCell("G$row")->setValue($data[$i]["currency"]);
            $worksheet->getCell("H$row")->setValue($data[$i]["stock"]);
            $worksheet->getCell("I$row")->setValue($data[$i]["unitMeasure"]);
            $worksheet->getCell("J$row")->setValue($data[$i]["type"]);
            $worksheet->getCell("K$row")->setValue($data[$i]["status"]);
            $worksheet->getCell("L$row")->setValue($data[$i]["updateDate"]);
            $worksheet->getCell("M$row")->setValue($data[$i]["registerDate"]);
            $row++;
        }

        return new Xls($spreadsheet);
    }
}
