<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog\Item;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\AttributeValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Route("/api/panel/item/attribute", condition="request.isXmlHttpRequest()")
 * @Configuration\Security("is_granted('ROLE_ADMIN')")
 */
class ItemAttributeController extends BaseController
{

    /**
     * @Route("/{itemId}/{attributeId}/{id}", condition="request.isXmlHttpRequest()", methods={"PUT"}, requirements={"itemId"="^(.{20})$", "attributeId"="^(.{20})$", "id"="^(.{20}|0)$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction($itemId, $attributeId, $id, Request $request)
    {
        $data = $request->request->all();
        $options = $data['options'] ?? [];
        $edit = !empty($id);
        $user = $this->getUser();
        $attribute = $this->em->getReference("E:Attribute", $attributeId);
        $item = $this->em->getRepository('E:Item')->getItem($itemId, $user->getId(), $this->defaultStoreId, "update");
        $attributesCount = $item->getAttributes()->count();

        if ($item->getVariants()->count() > 0 && strrpos($attribute->getType()->getId(), "INP_DRP") === true) {
            throw new \Exception("El producto ya tiene variantes configuradas por lo que no puede agregarse otra variante, debe eliminar las variantes antes de editar o agregar una nueva");
        }

        if (!$edit) {
            $itemAttribute = new Entity\ItemAttribute();
            $this->jsUtil->setValuesToEntity($itemAttribute, [
                "id" => $itemAttributeId = $this->jsUtil->getUid(),
                "item" => $item,
                "attribute" => $attribute,
                "isVariant" => $data["isVariant"] == "1",
                "urlFiles" => $item->getUrlFiles() . "/" . $itemAttributeId,
                "sequence" => $attributesCount + 1
            ]);
        } else {
            $itemAttribute = $this->em->getRepository('E:ItemAttribute')->findOneById($id);
        }
        $this->em->persist($itemAttribute);

        //WORK WITH OPTIONS
        if (count($options ?? []) > 0 && !$edit) {
            //REMOVE PREVIOUS OPTIONS
            if ($edit && $itemAttribute->getOptions()->count() > 0) {
                $iaos = $itemAttribute->getOptions();
                foreach ($iaos as $iaoption) {
                    $this->em->remove($iaoption);
                }
                $this->em->flush();
            }
            //ADD NEW OPTIONS
            foreach ($options as $key => $optionId) {
                $option = new Entity\ItemAttributeOption();
                $this->jsUtil->setValuesToEntity($option, [
                    "id" => $iaoId = $this->jsUtil->getUid(),
                    "itemAttribute" => $itemAttribute,
                    "attributeOption" => $this->em->getReference("E:AttributeOption", $lastOptionId = $optionId),
                    "sequence" => $key + 1
                ]);
                $this->jsReturn["itemAttributeOptionsIds"][] = ["attributeOptionId" => $optionId, "itemAttributeOptionId" => $iaoId];
                $this->em->persist($option);
            }
        }
        $itemAttribute->setDefaultOption($data["defaultOption"] ?? $lastOptionId ?? null);
        $this->jsReturn["msg"] = $edit ? "Atributo modificado con éxito" : "Atributo añadido con éxito";
        $this->jsReturn["id"] = !$edit ? $itemAttributeId : null;

        return $this->responseJson();
    }

    /**
     * @Route("/{itemId}/{attibuteId}", condition="request.isXmlHttpRequest()", methods={"DELETE"}, requirements={"id"="^(.{20})$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAction($itemId, $attibuteId)
    {
        $item = $this->em->getRepository('E:Item')->getItem($itemId, $user = $this->getUser(), $this->defaultStoreId, "update");
        if ($itemAttribute = $this->em->getRepository("E:ItemAttribute")->findOneBy(["item" => $item, "attribute" => $this->em->getReference("E:Attribute", $attibuteId)])) {
            $itemId = $itemAttribute->getItem()->getId();
            $this->em->remove($itemAttribute);
            $this->em->flush();
            $statement = $this->em->getConnection()->prepare("UPDATE ItemAttribute AS t
                                                                JOIN
                                                                ( SELECT @rownum:=@rownum+1 as rownum, id
                                                                  FROM ItemAttribute    
                                                                  CROSS JOIN (select @rownum := 0) rn
                                                                  WHERE itemid = :id
                                                                  order by sequence asc
                                                                ) AS r ON t.id = r.id
                                                                SET t.sequence = r.rownum");
            $statement->bindValue('id', $itemId);
            $statement->execute();
        } else {
            throw new \Exception("Hubo un problema al eliminar el atributo");
        }
        $this->jsReturn["msg"] = "Atributo eliminado";
        return $this->responseJson();
    }

    /**
     * @Route("/handleAttributes/{id}/{value}", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putHandleAttribute($id, $value)
    {
        $user = $this->getUser();
        $item = $this->em->getRepository('E:Item')->getItem($id, $user->getId(), $this->defaultStoreId, "update");
        if ($value == "false" && $item->getVariants()->count() > 0) {
            throw new \Exception("No es posible deshabilitar esta opción pues el producto cuenta con variantes de productos, elimine los productos y podrá desactivar esta opción");
        }
        $item->setHandleAttributes($value == "true");
        $this->em->persist($item);
        return $this->responseJson();
    }

    /**
     * @Route("/reorder/{id}/{oldIndex}/{newIndex}", methods={"PUT"}, requirements={"id"="^(.{20})$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function reorderAction($id, $oldIndex, $newIndex)
    {
        $dir = $oldIndex < $newIndex ? "down" : "up";
        $subQuery = $dir == "down" ? "and ((a.sequence >= :oldIndex and a.sequence <= :newIndex) or a.sequence is null)" : "and ((a.sequence >= :newIndex and a.sequence <= :oldIndex) or a.sequence is null)";
        $itemAttribute = $this->em->getRepository('E:ItemAttribute')->findOneById($id);
        $itemAttributeSiblings = $this->em->createQuery("select a from E:ItemAttribute a
                                                          where identity(a.item) = :itemId
                                                      $subQuery
                                                            and a.id <> :id
                                                       order by a.sequence asc")
                        ->setParameters(["itemId" => $itemAttribute->getItem()->getId(), "oldIndex" => $oldIndex, "newIndex" => $newIndex, "id" => $id])->getResult();
        foreach ($itemAttributeSiblings as $key => $a) {
            if ($dir == "down") {
                $a->setSequence($oldIndex++);
                $this->em->persist($a);
                if ($key + 1 == count($itemAttributeSiblings)) {
                    $itemAttribute->setSequence($oldIndex++);
                    $this->em->persist($itemAttribute);
                }
            } else {
                if ($key == 0) {
                    $itemAttribute->setSequence($newIndex++);
                    $this->em->persist($itemAttribute);
                }
                $a->setSequence($newIndex++);
                $this->em->persist($a);
            }
        }
        $this->jsReturn["msg"] = "Atributo reordenado";
        return $this->responseJson();
    }

    /**
     * @Route("/reorder/option/{id}/{oldIndex}/{newIndex}", methods={"PUT"}, requirements={"id"="^(.{20})$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function reorderOptionAction($id, $oldIndex, $newIndex)
    {
        $dir = $oldIndex < $newIndex ? "down" : "up";
        $subQuery = $dir == "down" ? "and ((a.sequence >= :oldIndex and a.sequence <= :newIndex) or a.sequence is null)" : "and ((a.sequence >= :newIndex and a.sequence <= :oldIndex) or a.sequence is null)";
        $itemAttributeOption = $this->em->getRepository('E:ItemAttributeOption')->findOneById($id);
        $itemAttributeOptionSiblings = $this->em->createQuery("select a from E:ItemAttributeOption a
                                                          where identity(a.itemAttribute) = :itemAttributeId
                                                      $subQuery
                                                            and a.id <> :id
                                                       order by a.sequence asc")
                        ->setParameters(["itemAttributeId" => $itemAttributeOption->getItemAttribute()->getId(), "oldIndex" => $oldIndex, "newIndex" => $newIndex, "id" => $id])->getResult();
        foreach ($itemAttributeOptionSiblings as $key => $a) {
            if ($dir == "down") {
                $a->setSequence($oldIndex++);
                $this->em->persist($a);
                if ($key + 1 == count($itemAttributeOptionSiblings)) {
                    $itemAttributeOption->setSequence($oldIndex++);
                    $this->em->persist($itemAttributeOption);
                }
            } else {
                if ($key == 0) {
                    $itemAttributeOption->setSequence($newIndex++);
                    $this->em->persist($itemAttributeOption);
                }
                $a->setSequence($newIndex++);
                $this->em->persist($a);
            }
        }
        $this->jsReturn["msg"] = "Opción reordenada";
        return $this->responseJson();
    }

    /**
     * @Route("/itemAttribute/{itemId}/{itemAttributeId}/{prevItemAttributeId}", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     * Desc: store itemAttributeId default to work with images for each option
     */
    public function putItemAttribute($itemId, $itemAttributeId, $prevItemAttributeId, Filesystem $fs)
    {
        $user = $this->getUser();
        if (!($item = $this->em->getRepository('E:Item')->getItem($itemId, $user->getId(), $this->defaultStoreId, "update"))) {
            throw new \Exception("Problemas al guardar la opción, consulte con el administrador");
        }
        // GET AND DELETE PREVIOUS DOCUMENTS
        if ($prevItemAttributeId) {
            $itemAttributeOptionIds = array_map(fn ($d) => $d["id"], $this->em->createQuery("select iao.id from E:ItemAttributeOption iao where identity(iao.itemAttribute) = :itemAttributeId")
                            ->setParameters(["itemAttributeId" => $prevItemAttributeId])->getResult());
            if (count($documents = $this->em->createQuery("select d from E:Document d where identity(d.itemAttributeOption) in (:ids) ")->setParameters(["ids" => $itemAttributeOptionIds])->getResult()) > 0) {
                $filesToDelete = [];
                $dir = $this->params->get('kernel.project_dir') . '/public/' . $documents[0]->getItemAttributeOption()->getItemAttribute()->getUrlFiles();
                foreach ($documents as $document) {
                    array_push($filesToDelete, ["fileName" => $dir . "/" . $document->getFileName(), "thumbnail" => $dir . "/" . $document->getThumbnail()]);
                    $this->em->remove($item);
                }
                // - delete fisical files
                foreach ($filesToDelete as $file) {
                    if ($fs->exists($file["fileName"]) || $fs->exists($file["thumbnail"])) {
                        $fs->remove($file["fileName"]);
                        $fs->remove($file["thumbnail"]);
                    }
                }
            }
        }
        // SET NEW ITEMATTRIBUTE
        $item->setItemAttribute($this->em->getReference("E:ItemAttribute", $itemAttributeId));
        $this->em->persist($item);
        return $this->responseJson();
    }
}
