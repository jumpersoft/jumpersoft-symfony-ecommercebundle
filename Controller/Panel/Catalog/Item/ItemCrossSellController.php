<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog\Item;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\EcommerceBundle\Validators;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class ItemCrossSellController extends BaseController
{

    /**
     * @Route("/api/panel/item/crossSell/getAll", name="getItemCrossSells", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Item')->getCrossSells($this->jsReturn, $this->getUser()->getId(), $this->defaultStoreId, $request->get('itemId'));
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/item/crossSell/delete", name="deleteItemCrossSell", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAction(Request $request)
    {
        $user = $this->getUser();
        $itemId = $request->request->get('itemId');
        $items = $request->get('items');
        if (!empty($itemId) && !empty($items)) {
            foreach ($items as $crossSell) {
                $itemCrossSell = $this->em->getRepository('E:Item')->getCrossSell($crossSell["id"], $itemId, $user->getId(), $this->defaultStoreId, "delete");
                $this->em->remove($itemCrossSell);
            }
            $this->jsReturn["msg"] = count($items) > 1 ? "Productos desasociados" : "Producto desasociado";
            return $this->responseJson();
        }
        throw new \Exception($this->msg["deleteError"]);
    }

    /*
     * @Route("/api/panel/item/crossSell/put", name="itemCrossSellEdit", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    //public function putAction(Request $request, Validators\ItemValidator $validator) {
//        $itemId = $request->request->get('itemId');
//        $itemCrossSellForm = $request->request->get('itemCrossSellForm');
//
//        if (count($itemCrossSellForm) > 0) {
//            foreach ($itemCrossSellForm as $crossSell) {
//                $itemValue = $validator->proccessInputValues($crossSell, $validator->crossSell);
//                if ($validator->validate($validator->crossSell)) {
//                    $itemCrossSell = $this->em->getRepository('E:Item')->getCrossSell($crossSell['id'], $itemId, $this->getUser()->getId(), $this->defaultStoreId, "update");
//                    $itemCrossSell->SetValues($itemValue);
//                    $this->em->persist($itemCrossSell);
//                }
//            }
//        }
//
//        $this->jsReturn["msg"] = count($itemCrossSellForm) > 1 ? "Productos modificados con éxito" : "Producto modificado con éxito";
//        return $this->responseJson();
    //}

    /**
     * @Route("/api/panel/item/crossSell/add", name="itemCrossSellNew", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function addAction(Request $request)
    {
        $user = $this->getUser();
        $itemId = $request->request->get('itemId');
        $items = $request->request->get('items');
        $returnIds = [];
        if (!empty($itemId) && !empty($items)) {
            foreach ($items as $itemRelatedId) {
                $itemCrossSell = new Entity\ItemCrossSell();
                $returnIds[$itemRelatedId] = $this->jsUtil->getUid();
                $itemCrossSell->setId($returnIds[$itemRelatedId]);
                $itemCrossSell->setItem($this->em->getReference('E:Item', $itemId));
                $itemCrossSell->setItemRelated($this->em->getReference('E:Item', $itemRelatedId));
                $this->em->persist($itemCrossSell);
            }


            $this->jsReturn["msg"] = count($items) > 1 ? "Productos añadidos con éxito" : "Producto añadido con éxito";
            $this->jsReturn["data"] = $returnIds;
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/item/crossSell/itemsAvailable", name="getItemsCrossSellAvailable", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getItemsCrossSellAvailable(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Item')->getItemsCrossSellAvailable($this->jsReturn, $request->get('itemId'), $this->getUser()->getId(), $this->defaultStoreId, $request->get('q'));
        return $this->responseJson();
    }
}
