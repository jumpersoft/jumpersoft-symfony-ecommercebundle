<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog\Item;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class ItemUpSellController extends BaseController
{

    /**
     * @Route("/api/panel/item/upSell/getAll", name="getItemUpSells", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Item')->getUpSells($this->jsReturn, $this->getUser()->getId(), $this->defaultStoreId, $request->get('itemId'));
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/item/upSell/delete", name="deleteItemUpSell", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAction(Request $request)
    {
        $user = $this->getUser();
        $itemId = $request->request->get('itemId');
        $items = $request->get('items');
        if (!empty($itemId) && !empty($items)) {
            foreach ($items as $upSell) {
                $itemUpSell = $this->em->getRepository('E:Item')->getUpSell($upSell["id"], $itemId, $user->getId(), $this->defaultStoreId, "delete");
                $this->em->remove($itemUpSell);
            }
            $this->jsReturn["msg"] = count($items) > 1 ? "Productos desasociados" : "Producto desasociado";
            return $this->responseJson();
        }
    }

    /*
     * @Route("/api/panel/item/upSell/put", name="itemUpSellEdit", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    //public function putAction(Request $request, Validators\ItemValidator $validator) {
//        $user = $this->getUser();
//        $itemId = $request->request->get('itemId');
//        $itemUpSellForm = $request->request->get('itemUpSellForm');
//        if (count($itemUpSellForm) > 0) {
//            foreach ($itemUpSellForm as $upSell) {
//                $itemValue = $validator->proccessInputValues($upSell, $validator->crossSell);
//                if ($validator->validate($validator->crossSell)) {
//                    $itemUpSell = $this->em->getRepository('E:Item')->getUpSell($upSell['id'], $itemId, $user->getId(), $this->defaultStoreId, "update");
//                    $itemUpSell->SetValues($itemValue);
//                    $this->em->persist($itemUpSell);
//                }
//            }
//        }
//        $this->jsReturn["msg"] = count($itemUpSellForm) > 1 ? "Productos modificados con éxito" : "Producto modificado con éxito";
//        return $this->responseJson();
    //}

    /**
     * @Route("/api/panel/item/upSell", name="itemUpSellNew", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction(Request $request)
    {
        $user = $this->getUser();
        $itemId = $request->request->get('itemId');
        $items = $request->request->get('items');
        $returnIds = [];
        if (!empty($itemId) && !empty($items)) {
            foreach ($items as $itemRelatedId) {
                $itemUpSell = new Entity\ItemUpSell();
                $returnIds[$itemRelatedId] = $this->jsUtil->getUid();
                $itemUpSell->setId($returnIds[$itemRelatedId]);
                $itemUpSell->setItem($this->em->getReference('E:Item', $itemId));
                $itemUpSell->setItemRelated($this->em->getReference('E:Item', $itemRelatedId));
                $this->em->persist($itemUpSell);
            }
            $this->jsReturn["msg"] = count($items) > 1 ? "Productos añadidos con éxito" : "Producto añadido con éxito";
            $this->jsReturn["data"] = $returnIds;
            return $this->responseJson();
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
    }

    /**
     * @Route("/api/panel/item/upSell/itemsAvailable", name="getItemsUpSellAvailable", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getItemsUpSellAvailable(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Item')->getItemsUpSellAvailable($this->jsReturn, $request->get('itemId'), $this->getUser()->getId(), $this->defaultStoreId, $request->get('q'));
        return $this->responseJson();
    }
}
