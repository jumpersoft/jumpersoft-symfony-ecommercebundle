<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog\Item;

use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Entity\Item;
use Jumpersoft\EcommerceBundle\Entity\UserLog;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class ItemPriceController extends BaseController
{

    /**
     * @Route("/api/panel/item/bulk/base/{id}", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction($id, Request $request)
    {
        $user = $this->getUser();
        if ($request->request->get("type") == "list") {
            $item = $this->em->getRepository('E:PriceListItem')->findOneById($id);
        } else {
            $item = $this->em->getRepository('E:Item')->getItem($id, $user->getId(), $this->defaultStoreId, "update");
        }
        $item->setBulk($request->request->get("bulk"));
        $this->em->persist($item);
        $this->jsReturn["msg"] = "Precios modificados con éxito";
        return $this->responseJson();
    }
}
