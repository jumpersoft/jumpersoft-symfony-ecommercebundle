<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog\Item;

use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\ItemValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class ItemInventoryController extends BaseController
{

    /**
     * @Route("/api/panel/item/inventory/sku/{parentItemId}", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putSkus($parentItemId, Request $request)
    {
        $skus = $request->request->get('skus');
        $user = $this->getUser();
        $parent = $this->em->getRepository('E:Item')->getItem($parentItemId, $user->getId(), $this->defaultStoreId, "update");
        $resp = [];
        $edit = false;
        foreach ($skus as $i => $sku) {
            $data = ItemValidator::proccessInputValues($sku, ItemValidator::$sku);
            if (ItemValidator::validate(ItemValidator::$sku, $data)) {
                if (!$edit) {
                    $resp[] = ["prevId" => $sku["id"], "id" => $id = $this->jsUtil->getUid(), "registerDate" => $registerDate = $this->jsUtil->getLocalDateTime()];
                    $item = new Entity\Item();
                    $sku = array_merge($sku, [
                        "registerDate" => $registerDate,
                        "id" => $id,
                        "trackingType" => "INV_TYP_SIM",
                        "currency" => "mxn",
                        "unitMeasure" => $sku["unitMeasureId"],
                        "sequence" => $i,
                        "parent" => $parent,
                        "store" => $this->defaultStoreId,
                        "urlFiles" => ("/" . $this->getFileStoreItemDir($this->defaultStoreId, $id)),
                        "type" => 'ITE_SIM',
                        "status" => "ITE_AVA",
                        "stock" => 0,
                        "minQtyAllowedInCart" => 1,
                        "maxQtyAllowedInCart" => 5,
                        "variantsGroup" => $sku["variantsGroup"],
                        "perishable" => $parent->getPerishable(),
                        "dangerous" => $parent->getDangerous(),
                        "radioactive" => $parent->getRadioactive(),
                        "isotope" => $parent->getIsotope(),
                        "useVolumeToFulFill" => $parent->getUseVolumeToFulFill(),
                        "trackingType" => $parent->getTrackingType(),
                        "brand" => $parent->getBrand(),
                    ]);
                    $this->jsUtil->setValuesToEntity($item, $sku);
                }

                $this->em->persist($item);

                $this->jsReturn["msg"] = $edit ? "Variantes modificadas con éxito" : "Variantes creadas con éxito";
                $this->jsReturn["data"] = !$edit ? $resp : null;
            } else {
                throw new \Exception($this->msg["errorForm"]);
            }
        }
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/item/inventory/sku/reorder/{parentId}/{itemId}/{currentSequence}/{newSequence}", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function reorderAction($parentId, $itemId, $currentSequence, $newSequence)
    {
        $item = $this->em->getRepository('E:Item')->getItem($itemId, $this->getUser()->getId(), $this->defaultStoreId, "update");
        $conditon = $currentSequence > $newSequence ? " >= " : " <= ";
        $operator = $currentSequence > $newSequence ? " + " : " - ";
        $query = $this->em->createQuery("update E:Item i set i.sequence = i.sequence $operator 1 
                                          where IDENTITY(i.parent) = :parentId
                                            and i.sequence $conditon :newSequence ")->setParameters(["parentId" => $parentId, "newSequence" => $newSequence]);
        $query->execute();
        $item->setSequence($newSequence);
        $this->em->persist($item);
        $this->jsReturn["msg"] = "Productos reordenados";
        return $this->responseJson();
    }
}
