<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog\Item;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\EcommerceBundle\Validators\ItemValidator;

/**
 * @Configuration\Security("is_granted('ROLE_ADMIN')")
 */
class ItemDispersionController extends BaseController
{

    /**
     * @Route("/api/panel/item/dispersion/getAll/{itemId}", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction($itemId)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:Item')->getDispersions($this->getUser()->getId(), $this->defaultStoreId, $itemId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/item/dispersion/delete/{itemId}/{id}",  condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAction($itemId, $id)
    {
        $user = $this->getUser();
        $item = $this->em->getRepository('E:Item')->getItem($itemId, $user->getId(), $this->defaultStoreId, "update");
        if (!empty($itemId) && !empty($id) && $item) {
            $itemDispersion = $this->em->getRepository('E:ItemDispersion')->findOneById($id);
            $this->em->remove($itemDispersion);
            $this->jsReturn["msg"] = "Producto desasociado";
            return $this->responseJson();
        }
        throw new \Exception($this->msg["deleteError"]);
    }

    /**
     * @Route("/api/panel/item/dispersion/add", name="itemDispersionNew", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function addAction(Request $request)
    {
        $user = $this->getUser();
        $items = $request->request->get('items');
        $returnIds = [];
        $item = $this->em->getRepository('E:Item')->getItem($request->request->get('itemId'), $user->getId(), $this->defaultStoreId, "update");
        $edit = false;
        if (!empty($items) && $item) {
            foreach ($items as $itemRelated) {
                $values = ItemValidator::proccessInputValues($itemRelated, ItemValidator::$dispersion);
                if (ItemValidator::validate(ItemValidator::$dispersion, $values)) {
                    if (strlen($itemRelated["id"]) == 8) {
                        //NEW
                        $itemDispersion = new Entity\ItemDispersion();
                        $itemDispersion->setId($itemRelated["id"] = $this->jsUtil->getUid());
                        $itemDispersion->setItem($item);
                        $itemDispersion->setItemRelated($this->em->getReference('E:Item', $itemRelated["itemRelatedId"]));
                        $returnIds[] = $itemRelated;
                    } else {
                        //EDIT
                        $itemDispersion = $this->em->getRepository("E:ItemDispersion")->findOneById($itemRelated["id"]);
                        $edit = true;
                    }
                    $this->jsUtil->setValuesToEntity($itemDispersion, $values);
                    $this->em->persist($itemDispersion);
                } else {
                    throw new \Exception($this->msg["errorForm"]);
                }
            }

            $this->jsReturn["msg"] = $edit ? "Producto modificado" : (count($items) > 1 ? "Productos añadidos con éxito" : "Producto añadido con éxito");
            $this->jsReturn["data"] = $returnIds;
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/item/dispersion/itemsAvailable", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getItemsDispersionAvailable(Request $request)
    {
        $this->getFilters($request);
        $item = $this->em->getRepository("E:Item")->findOneById($request->get('itemId'));
        $this->jsReturn["data"] = $this->em->getRepository('E:Item')->getItemsDispersionAvailable($this->jsReturn, $item, $this->getUser()->getId(), $this->defaultStoreId, $request->get('q'));
        return $this->responseJson();
    }
}
