<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog\Item;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class ItemCategoryController extends BaseController
{

    /**
     * @Route("/api/panel/item/category/getAll", name="getItemCategories", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Item')->getItemCategories($this->jsReturn, $this->getUser()->getId(), $this->defaultStoreId, $request->get('itemId'));
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/item/category/delete", name="deleteItemCategory", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAction(Request $request)
    {
        $user = $this->getUser();
        $itemId = $request->request->get('itemId');
        $categories = $request->get('categories');
        if (!empty($itemId) && !empty($categories)) {
            $item = $this->em->getRepository('E:Item')->getItem($itemId, $user->getId(), $this->defaultStoreId, "update");
            foreach ($categories as $category) {
                $item->removeCategory($this->em->getReference('E:Category', $category["id"]));
            }
            $this->em->persist($item);
            $this->jsReturn["msg"] = count($categories) > 1 ? "Categorías desasociadas" : "Categoría desasociada";
            return $this->responseJson();
        }
        throw new \Exception($this->msg["deleteError"]);
    }

    /**
     * @Route("/api/panel/item/category/put", name="itemCategoryNewEdit", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction(Request $request)
    {
        $user = $this->getUser();
        $itemId = $request->request->get('itemId');
        $categories = $request->request->get('categories');
        if (!empty($itemId) && !empty($categories)) {
            $item = $this->em->getRepository('E:Item')->getItem($itemId, $user->getId(), $this->defaultStoreId, "update");
            foreach ($categories as $categoryId) {
                $item->addCategory($this->em->getReference('E:Category', $categoryId));
                $this->em->persist($item);
            }
            $this->jsReturn["msg"] = count($categories) > 1 ? "Categorías añadidas con éxito" : "Categoría añadida con éxito";
            return $this->responseJson();
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
    }

    /**
     * @Route("/api/panel/item/category/update", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function updateAction(Request $request)
    {
        $user = $this->getUser();
        $itemId = $request->request->get('itemId');
        $categories = $request->request->get('categories');
        $mainCategoryId = $request->request->get('categoryId');
        if (!empty($itemId) && !empty($categories)) {
            $item = $this->em->getRepository('E:Item')->getItem($itemId, $user->getId(), $this->defaultStoreId, "update");
            $item->setCategory($mainCategoryId ? $this->em->getReference("E:Category", $mainCategoryId) : null);
            $this->em->persist($item);
            //Remove all items
            $remCategories = $item->getCategories();
            foreach ($remCategories as $category) {
                $this->em->remove($category);
            }
            $this->em->flush();
            //Add new items
            $eventDate = $this->jsUtil->getLocalDateTime();
            foreach ($categories as $i => $categoryId) {
                $itemCategory = new Entity\ItemCategory();
                $this->jsUtil->setValuesToEntity($itemCategory, ["id" => $this->jsUtil->getUid(), "item" => $item, "category" => $categoryId, "sequence" => $i + 1, "registerDate" => $eventDate]);
                $this->em->persist($itemCategory);
            }
        }

        $this->jsReturn["msg"] = "Categorías modificadas";
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/item/category/categoryAvailable", name="getItemCategoryAvailable", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getItemCategoryAvailable(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Item')->getItemCategoryAvailable($this->jsReturn, $request->get('itemId'), $this->getUser()->getId(), $this->defaultStoreId, $request->get('q'));
        return $this->responseJson();
    }
}
