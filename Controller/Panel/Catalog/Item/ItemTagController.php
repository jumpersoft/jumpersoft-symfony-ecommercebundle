<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog\Item;

use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Entity;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class ItemTagController extends BaseController
{

    /**
     * @Route("/api/panel/item/tag/getAll", name="getItemTags", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Item')->getTags($this->jsReturn, $this->getUser()->getId(), $this->defaultStoreId, $request->get('itemId'));
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/item/tag/delete", name="deleteItemTag", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAction(Request $request)
    {
        $user = $this->getUser();
        $itemId = $request->request->get('itemId');
        $tags = $request->get('tags');
        if (!empty($itemId) && !empty($tags)) {
            $item = $this->em->getRepository('E:Item')->getItem($itemId, $user->getId(), $this->defaultStoreId, "update");
            foreach ($tags as $tag) {
                $item->removeTag($this->em->getReference('E:Tag', $tag["id"]));
            }
            $this->em->persist($item);
            $this->jsReturn["msg"] = count($tags) > 1 ? "Etiquetas desasociadas" : "Etiqueta desasociada";
            return $this->responseJson();
        }
    }

    /**
     * @Route("/api/panel/item/tag/put", name="itemTagNewEdit", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction(Request $request)
    {
        $user = $this->getUser();
        $itemId = $request->request->get('itemId');
        $tags = $request->request->get('tags');
        if (!empty($itemId) && !empty($tags)) {
            $item = $this->em->getRepository('E:Item')->getItem($itemId, $user->getId(), $this->defaultStoreId, "update");
            foreach ($tags as $tagId) {
                $item->addTag($this->em->getReference('E:Tag', $tagId));
                $this->em->persist($item);
            }
            $this->jsReturn["msg"] = count($tags) > 1 ? "Etiquetas añadidas con éxito" : "Etiqueta añadida con éxito";
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/item/tag/tagAvailable", name="getItemTagAvailable", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getItemTagAvailable(Request $request)
    {
        $this->getFilters($request);
        $userId = $this->getUser()->getId();
        $itemId = $request->get('itemId');
        $q = $request->get('q');
        $this->jsReturn["data"] = $this->em->getRepository('E:Item')->getItemTagAvailable($this->jsReturn, $itemId, $userId, $this->defaultStoreId, $q);
        return $this->responseJson();
    }
}
