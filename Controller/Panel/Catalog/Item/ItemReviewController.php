<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog\Item;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\ItemValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class ItemReviewController extends BaseController
{

    /**
     * @Route("/api/panel/item/review/getAll", name="getItemReviews", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $userId = $this->getUser()->getId();
        $this->defaultStoreId = $request->get('currentStoreId');
        $itemId = $request->get('itemId');
        $this->jsReturn["data"] = $this->em->getRepository('E:ItemReview')->getReviews($this->jsReturn, $userId, $this->defaultStoreId, $itemId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/item/review/delete", name="deleteItemReview", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAction(Request $request)
    {
        $user = $this->getUser();
        $itemId = $request->request->get('itemId');
        $items = $request->get('items');
        if (!empty($itemId) && !empty($items)) {
            foreach ($items as $review) {
                $itemReview = $this->em->getRepository('E:ItemReview')->getReview($review["id"], $itemId, $user->getId(), $this->defaultStoreId, "delete");
                $this->em->remove($itemReview);
            }
            $this->jsReturn["msg"] = count($items) > 1 ? "Productos desasociados" : "Producto desasociado";
            return $this->responseJson();
        }
    }

    /**
     * @Route("/api/panel/item/review/put", name="itemReviewEdit", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction(Request $request)
    {
        $reviewValidators = ItemValidator::$review;
        $user = $this->getUser();
        $itemId = $request->request->get('itemId');
        $this->defaultStoreId = $request->get('currentStoreId');
        $itemReviewForm = $request->request->get('itemReviewForm');
        if (count($itemReviewForm) > 0) {
            foreach ($itemReviewForm as $review) {
                $itemValue = ItemValidator::proccessInputValues($review, $reviewValidators);
                if (ItemValidator::validate($reviewValidators, $itemValue)) {
                    $itemReview = $this->em->getRepository('E:ItemReview')->getReview($review['id'], $itemId, $user->getId(), $this->defaultStoreId, "update");
                    $itemReview->setStatus($this->em->getReference('E:Status', $review["statusId"]));
                    $this->em->persist($itemReview);
                }
            }
        }
        $this->jsReturn["msg"] = count($itemReviewForm) > 1 ? "Productos modificados con éxito" : "Producto modificado con éxito";
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/item/review/add", name="itemReviewNew", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function addAction(Request $request)
    {
        $user = $this->getUser();
        $this->defaultStoreId = $request->get('currentStoreId');
        $itemId = $request->request->get('itemId');
        $items = $request->request->get('items');

        if (!empty($itemId) && !empty($items)) {
            foreach ($items as $itemRelatedId) {
                $itemReview = new Entity\ItemReview();
                $itemReview->setId($this->jsUtil->getUid());
                $itemReview->setItem($this->em->getReference('E:Item', $itemId));
                $itemReview->setItemRelated($this->em->getReference('E:Item', $itemRelatedId));
                $this->em->persist($itemReview);
            }
            $this->jsReturn["msg"] = count($items) > 1 ? "Productos añadidos con éxito" : "Producto añadido con éxito";
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }
}
