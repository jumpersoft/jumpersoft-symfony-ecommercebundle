<?php
namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\RadesaUserLicenceValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_ADMIN')")
 */
class RadesaUserLicenceController extends BaseController
{

    /**
     * @Route("/api/panel/user/{userId}/licence", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction($userId, Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:UserLicence')->getLicences($this->jsReturn, $userId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/user/{userId}/licence", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function postAction($userId, Request $request)
    {
        $eventDate = $this->jsUtil->getLocalDateTime();
        $user = $this->getUser();
        $data = $request->request->all();
        $edit = !empty($id = $data["id"]);
        $data = RadesaUserLicenceValidator::proccessInputValues($data, RadesaUserLicenceValidator::$userLicence);

        //Check if folio exist
        if ($this->em->getRepository('E:UserLicence')->checkFolio($data["folio"], $id, $this->defaultStoreId)) {
            throw new \Exception("El número de licencia ya está registrada");
        }

        if (RadesaUserLicenceValidator::validate(RadesaUserLicenceValidator::$userLicence, $data)) {
            if (!$edit) {
                $licence = new Entity\UserLicence();
                $data = array_merge($data, [
                    "id" => $id = $this->jsUtil->getUid(),
                    "user" => $userId,
                    "registerDate" => $eventDate
                ]);
            } else {
                $licence = $this->em->getRepository('E:UserLicence')->getLicences($this->jsReturn, $userId, "update", null, $id);
            }

            $this->jsUtil->setValuesToEntity($licence, array_merge($data, [
                "status" => $data["statusId"]
            ]));
            $this->em->persist($licence);

            $this->jsReturn["id"] = !$edit ? $id : null;
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        $this->jsReturn["data"] = $licence->getId();
        $this->jsReturn["msg"] = $edit ? "Licencia modificada" : "Licencia creada";
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/user/{userId}/licence/{id}", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function delete($userId, $id)
    {
        $licence = $this->em->getRepository('E:UserLicence')->getLicences($this->jsReturn, $userId, "delete", null, $id);
        $this->em->remove($licence);
        $this->jsReturn["msg"] = "Licencia eliminada";
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/user/{userId}/licence/{licenceId}/isotope", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllIsotopeAction($userId, $licenceId, Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:UserLicence')->getIsotopes($this->jsReturn, $request->get("userId"));
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/user/{userId}/licence/{licenceId}/isotope", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putIsotopeAction($userId, $licenceId, Request $request)
    {
        $eventDate = $this->jsUtil->getLocalDateTime();
        $data = $request->request->all();
        $data = RadesaUserLicenceValidator::proccessInputValues($data, RadesaUserLicenceValidator::$userLicenceIsotope);
        if (RadesaUserLicenceValidator::validate(RadesaUserLicenceValidator::$userLicenceIsotope, $data)) {
            $licence = $this->em->getRepository('E:UserLicence')->getLicences($this->jsReturn, $userId, "update", null, $licenceId);
            $isotope = new Entity\UserLicenceIsotope();
            $data = array_merge($data, [
                "id" => $id = $this->jsUtil->getUid(),
                "licence" => $licence,
                "isotope" => $data["isotopeId"],
                "unitMeasure" => $data["unitMeasureId"],
            ]);

            $this->jsUtil->setValuesToEntity($isotope, $data);
            $this->em->persist($isotope);

            $this->jsReturn["id"] = $id;
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        $this->jsReturn["data"] = $isotope->getId();
        $this->jsReturn["msg"] = "Isotopo agregado";
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/user/{userId}/licence/{licenceId}/isotope/{id}", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteIsotope($userId, $licenceId, $id)
    {
        $licence = $this->em->getRepository('E:UserLicence')->getLicences($this->jsReturn, $userId, "delete", null, $licenceId);
        $isotope = $this->em->getRepository("E:UserLicenceIsotope")->findOneBy(["id" => $id, "licence" => $licence]);
        $this->em->remove($isotope);
        $this->jsReturn["msg"] = "Isotopo eliminado";
        return $this->responseJson();
    }
}
