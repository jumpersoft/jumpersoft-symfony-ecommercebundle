<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\CustomerValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class CustomerController extends BaseController
{

    /**
     * @Route("/api/panel/catalog/customer/getAll", name="getCustomers", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        try {
            $this->getFilters($request);
            $userId = $this->getUser()->getId();
            $this->defaultStoreId = $request->get('currentStoreId');
            $customers = $this->em->getRepository('E:Customer')->getCustomersSuppliers($this->jsReturn, $userId, $this->defaultStoreId, $this->isUser('ROLE_ADMIN'));
            $this->jsReturn["data"] = $customers;
        } catch (\Exception $e) {
            if (!isset($this->jsReturn["msg"])) {
                $this->jsReturn["msg"] = $this->msg["errorGeneral"];
            }
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/catalog/customer/get", name="getCustomer", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAction(Request $request)
    {
        try {
            $user = $this->getUser();
            $id = $request->get('id');
            $this->defaultStoreId = $request->get('currentStoreId');
            $item = $this->em->getRepository('E:Customer')->getCustomer($id, $user->getId(), $this->defaultStoreId, $this->isUser('ROLE_ADMIN'));
            $usersRelated = $this->em->getRepository('E:Customer')->getCustomerUsers($id, $user->getId(), $this->defaultStoreId, $this->isUser('ROLE_ADMIN'));
            $item["users"] = $usersRelated;
            if ($item) {
                $this->jsReturn["data"] = $item;
            }
        } catch (\Exception $e) {
            if (!isset($this->jsReturn["msg"])) {
                $this->jsReturn["msg"] = $this->msg["errorGeneral"];
            }
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/catalog/customer/delete", name="deleteCustomer", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true")
     */
    public function deleteAction(Request $request)
    {
        $this->msg["deletedOK"] = "Cliente eliminado";
        try {
            $user = $this->getUser();
            $id = $request->get('id');
            $this->defaultStoreId = $request->get('currentStoreId');
            if (isset($id)) {
                $item = $this->em->getRepository('E:Customer')->getCustomer($id, $user->getId(), $this->defaultStoreId, $this->isUser('ROLE_ADMIN'), "delete");
                if ($item) {
                    $this->em->remove($item);
                    $this->em->flush();

                    $this->jsReturn["msg"] = $this->msg["deletedOK"];
                } else {
                    throw new \Exception();
                }
            } else {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            if (!isset($this->jsReturn["msg"])) {
                $this->jsReturn["msg"] = $this->msg["errorGeneral"];
            }
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/catalog/customer/put", name="newEdit", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction(Request $request, LoggerInterface $logger)
    {
        try {
            $this->msg["insertOK"] = "Cliente creado con éxito";
            $this->msg["updateOK"] = "Cliente modificado con éxito";
            $eventDate = $this->jsUtil->getLocalDateTime();
            $ipSource = $request->getClientIp();

            $edit = $request->request->get('edit');
            $customerInput = json_decode($request->request->get('customer'), true);
            $cvCustomer = CustomerValidator::proccessInputValues($customerInput, CustomerValidator::$customerValidators);


            $user = $this->getUser();
            $id = $request->request->get('id');
            $this->defaultStoreId = $request->get('currentStoreId');

            if (CustomerValidator::validate(CustomerValidator::$customerValidators, $cvCustomer)) {
                $userLog = new Entity\UserLog($this->jsUtil->getUid(), $user, $ipSource, $eventDate, $this->em->getReference('E:Status', $edit == 'false' ? 'CUS_INS' : 'CUS_UPD'));

                if ($edit == "false") {
                    $id = $this->jsUtil->getUid();
                    $customer = new Entity\Customer();
                    $customer->SetId($id);
                    $customer->setRegisterDate($userLog->getEventDate());
                } else {
                    $customer = $this->em->getRepository('E:Customer')->getCustomer($id, $user->getId(), $this->defaultStoreId, $this->isUser('ROLE_ADMIN'), "update");
                    if ($customer) {
                        $customer->setUpdateDate($userLog->getEventDate());
                        //Delete currently users related
                        $usersRelated = $customer->getUsers();
                        if (count($usersRelated) > 0) {
                            foreach ($usersRelated as $userRelated) {
                                $customer->removeUser($userRelated);
                            }
                            $this->em->persist($customer);
                            $this->em->flush();
                        }
                    } else {
                        throw new \Exception();
                    }
                }
                
                $this->jsUtil->setValuesToEntity($customer, $cvCustomer);

                //Add users related
                $nusersRelated = json_decode($cvCustomer["users"]);
                if (count($nusersRelated) > 0) {
                    foreach ($nusersRelated as $nuserRelated) {
                        $customer->addUser($this->em->getReference('E:User', $nuserRelated));
                    }
                }
                $this->em->persist($customer);
                $this->em->persist($userLog);

                $this->jsReturn["msg"] = $edit == "false" ? $this->msg["insertOK"] : $this->msg["updateOK"];
                $this->jsReturn["id"] = $id;
            } else {
                $this->jsReturn["msg"] = $this->msg["errorForm"];
                throw new \Exception();
            }
        } catch (\Exception $e) {
            if (!isset($this->jsReturn["msg"])) {
                $this->jsReturn["msg"] = $this->msg["errorGeneral"];
            }
            $logger->error("Query failed: " . $this->jsReturn["msg"] . (isset($e) ? $e : ""));
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/catalog/customer/getCustomerUsersNotAsigned", name="getCustomerUsersNotAsigned", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getCustomerUsersNotAsigned(Request $request)
    {
        try {
            $this->getFilters($request);
            $q = $request->get('q');
            $userId = $this->getUser()->getId();
            $rows = $this->em->getRepository('E:Customer')->getCustomerUsersNotAsigned($this->jsReturn, $q, $userId);
            $this->jsReturn["data"] = $rows;
        } catch (\Exception $e) {
            if (!isset($this->jsReturn["msg"])) {
                $this->jsReturn["msg"] = $this->msg["errorGeneral"];
            }
        }

        return $this->responseJson();
    }
}
