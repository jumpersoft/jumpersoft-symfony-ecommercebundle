<?php
namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\UserValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class UserController extends BaseController
{

    /**
     * @Route("/api/panel/user", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAll(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:User')->getUsers($this->jsReturn, $this->getUser()->getId(), $this->defaultStoreId, $this->isUser('ROLE_ADMIN'), $this->isUser('ROLE_SUPER_ADMIN'));
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/user/{id}", condition="request.isXmlHttpRequest()", methods={"GET"}, requirements={"id"="^(.{20})$"})
     */
    public function getUserAction($id, Request $request)
    {
        $row = $this->em->getRepository('E:User')->getUser($id, $this->getUser()->getId(), $this->defaultStoreId, $this->isUser('ROLE_ADMIN'));
        $this->jsReturn["data"] = $row;
        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route("/api/panel/user/{id}", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function delete($id, Request $request)
    {
        $user = $this->getUser();
        $orders = $this->em->getRepository('E:Store')->getStoreOrdersCountByUserId($this->defaultStoreId, $id);

        if ($orders == 0) {
            $eventDate = $this->jsUtil->getLocalDateTime();
            $ipSource = $request->getClientIp();
            $userLog = new Entity\UserLog($this->jsUtil->getUid(), $user, $ipSource, $eventDate, $this->em->getReference('E:Status', 'USR_DEL'));

            $userRow = $this->em->getRepository('E:User')->getUser($id, $user->getId(), $this->defaultStoreId, $this->isUser('ROLE_ADMIN'), "delete");
            $userLog->setComment("username: " . $userRow->getUsername());
            $this->em->remove($userRow);
            $this->em->persist($userLog);

            $this->jsReturn["msg"] = "Usuario eliminado";
            return $this->responseJson();
        } else {
            throw new \Exception("El usuario tiene pedidos relacionos y no puede eliminarse");
        }
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route("/api/panel/user", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putUserAction(Request $request, UserPasswordEncoderInterface $encoderPwd)
    {
        $edit = $request->request->get('edit') == "true";
        $returnData["id"] = $request->request->get('id');
        $data = $request->request->get('user');

        //Llenamos valores de campos de acuerdo a arreglo de campos permitidos para User y Empresa
        $values = UserValidator::proccessInputValues($data, UserValidator::$user);

        //Variables generales
        $eventDate = $this->jsUtil->getLocalDateTime();
        $ipSource = $request->getClientIp();
        $userId = $this->getUser()->getId();

        //Not validate rules
        $notValidate = ["email", "agree", "currentPassword"];
        empty($values["password"]) ? array_push($notValidate, "password", "confirmPassword") : null;
        if ($values["roleId"] == 'ROLE_CUSTOMER') {
            $notValidate[] = "username";
            $values["entityTypeId"] == 'CUS_MOR' ? array_push($notValidate, "lastName", "mothersLastName") : array_push($notValidate, "tradeName", "rfc");
        } else {
            array_push($notValidate, "tradeName", "rfc");
        }

        //Handle username with ROLE_CUSTOMER
        if (!$edit && $values["roleId"] == 'ROLE_CUSTOMER') {
            $returnData["username"] = $values["username"] = empty($values["username"]) ? $this->jsUtil->getUid() . "@" . ($this->params->get("company.domain") ?? "jumpersoft.com") : $values["username"];
            $values["password"] = $this->jsUtil->getUid();
        }

        if (UserValidator::validate(UserValidator::$user, $values, [], $notValidate)) {
            $userLog = new Entity\UserLog($this->jsUtil->getUid(), $this->getUser(), $ipSource, $eventDate, $this->em->getReference('E:Status', !$edit ? 'USR_INS' : 'USR_UPD'));
            if (!$edit) {
                $user = new Entity\User();
                $this->jsUtil->setValuesToEntity($user, $values = array_merge($values, [
                    "id" => $returnData["id"] = $this->jsUtil->getUid(),
                    "registerDate" => $returnData["registerDate"] = $userLog->getEventDate(),
                    "signupCode" => $this->jsUtil->getUid(),
                    "isActive" => $values["roleId"] != 'ROLE_CUSTOMER' ? 1 : 0,
                    "defaultStore" => $this->defaultStoreId
                ]));
                $this->em->persist($user);
                if ($values["roleId"] != "ROLE_CUSTOMER") {
                    $store = $this->em->getRepository("E:Store")->findOneById($this->defaultStoreId);
                    $store->addUser($user);
                    $this->em->persist($store);
                }
            } else {
                $user = $this->em->getRepository('E:User')->getUser($returnData["id"], $userId, $this->defaultStoreId, $this->isUser('ROLE_ADMIN'), "update");
                $values["username"] = $values["roleId"] == 'ROLE_CUSTOMER' && empty($values["username"]) ? $user->getUsername() : $values["username"];
            }

            //Check username exists
            if ($this->em->getRepository("E:User")->checkUsername($values["username"], $returnData["id"])) {
                throw new \Exception("El correo ya existe, elija otro");
            }

            $this->jsUtil->setValuesToEntity($user, array_merge($values, [
                "roles" => [$values["roleId"]],
                "email" => $values["username"],
                "entityType" => $values["entityTypeId"]
                ]), ["password"]);

            if (!empty($values['password'])) {
                $password = $encoderPwd->encodePassword($user, $values['password']);
                $user->setPassword($password);
            }
            $this->em->persist($user);
            $this->em->persist($userLog);

            $this->jsReturn["msg"] = ($edit ? "Usuario modificado con éxito" : "Usuario creado con éxito");
            $this->jsReturn["data"] = $returnData;
            return $this->responseJson();
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
    }

    /**
     * @Configuration\Security("is_granted('ROLE_ADMIN')")
     * @Route("/api/panel/user/{id}/rules", condition="request.isXmlHttpRequest()", methods={"POST"}, requirements={"id"="^(.{20})$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function postActions($id, Request $request)
    {
        $eventDate = $this->jsUtil->getLocalDateTime();
        $data = $request->request->all();
        $userLogged = $this->getUser();
        $data = UserValidator::proccessInputValues($data, UserValidator::$userRules);
        $avoid = $data["allowsReturns"] != "1" ? ["returnPercentage"] : [];
        if (UserValidator::validate(UserValidator::$userRules, $data, [], $avoid)) {
            $user = $this->em->getRepository('E:User')->getUser($id, $userLogged->getId(), $this->defaultStoreId, $this->isUser('ROLE_ADMIN'), "update");
            $this->jsUtil->setValuesToEntity($user, [
                "rules" => $data
            ]);
            $this->em->persist($user);
            $this->jsReturn["msg"] = "Reglas modificadas";
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }
}
