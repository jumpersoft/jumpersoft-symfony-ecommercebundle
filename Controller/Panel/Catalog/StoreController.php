<?php
namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Entity\Store;
use Jumpersoft\EcommerceBundle\Entity\UserLog;
use Jumpersoft\EcommerceBundle\Validators\StoreCatalogValidator;
use Jumpersoft\EcommerceBundle\Entity;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class StoreController extends BaseController
{

    /**
     * @Route("/api/panel/store", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function listing(Request $request)
    {
        $this->getFilters($request);
        $userId = $this->getUser()->getId();
        $source = $request->get('source');
        $queryStr = $request->get('q');
        $this->jsReturn["data"] = $this->em->getRepository('E:Store')->getStores($this->jsReturn, $queryStr, $userId, $source, $this->isUser('ROLE_SUPER_ADMIN'));
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/store/{id}", condition="request.isXmlHttpRequest()", methods={"GET"}, requirements={"id"="^(.{20}|default)$"})
     */
    public function getAction($id, Request $request)
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:Store')->getStore($id == "default" ? $this->defaultStoreId : $id, $this->getUser()->getId(), "view", $this->isUser('ROLE_SUPER_ADMIN'));
        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/api/panel/store/{id}", condition="request.isXmlHttpRequest()", methods={"DELETE"}, requirements={"id"="^(.{20}|default)$"})
     * @JsResponse(csrf="true")
     */
    public function delete($id, Request $request)
    {
        $ordersCount = $this->em->getRepository('E:Store')->getStoreOrdersCount($id);
        if ($ordersCount == 0) {
            $store = $this->em->getRepository('E:Store')->getStore($id, $this->getUser()->getId(), "delete", $this->isUser('ROLE_SUPER_ADMIN'));
            $this->em->remove($store);
            $this->em->flush();
            $this->jsReturn["msg"] = "Tienda eliminada";
        } else {
            throw new \Exception("La tienda tiene pedidos relacionados y no puede eliminarse");
        }
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/store/{id}", condition="request.isXmlHttpRequest()", methods={"PUT"}, requirements={"id"="^(.{20}|default)$"})
     * @JsResponse(csrf="true")
     */
    public function put($id, Request $request)
    {
        $this->msg["insertOK"] = "Tienda creada con éxito";
        $this->msg["updateOK"] = "Tienda modificada con éxito";
        $this->msg["urlNameKeyExist"] = "El url key de la tienda ya existe, elija otro por favor";

        $eventDate = $this->jsUtil->getLocalDateTime();
        $ipSource = $request->getClientIp();

        $edit = $request->request->get('edit');
        $storeInput = $request->request->get('store');
        $fieldValue = StoreCatalogValidator::proccessInputValues($storeInput, StoreCatalogValidator::$store);

        $user = $this->getUser();
        $returnData["id"] = $id;

        //TODO: Descomentar cuando sea neceario
        //Check if urlKey exist
//        if ($this->em->getRepository('E:Store')->checkUrlKeyAction($fieldValue["urlKey"], $returnData["id"])) {
//            throw new \Exception($this->msg["urlNameKeyExist"]);
//        }

        if (StoreCatalogValidator::validate(StoreCatalogValidator::$store, $fieldValue)) {
            $userLog = new Entity\UserLog($this->jsUtil->getUid(), $user, $ipSource, $eventDate, $this->em->getReference('E:Status', $edit == 'false' ? 'STR_INS' : 'STR_UPD'));
            if ($this->em->transactional(function ($em) use ($edit, &$returnData, &$user, &$userLog, &$fieldValue) {
                    if ($edit == "false") {
                        $store = new Entity\Store();
                        $fieldValue = array_merge($fieldValue, [
                            "registerDate" => $userLog->getEventDate(),
                            "id" => $this->jsUtil->getUid()
                        ]);
                    } else {
                        $store = $this->em->getRepository('E:Store')->getStore($returnData["id"], $user, "update");
                        $store->setUpdateDate($userLog->getEventDate());
                    }

                    $this->jsUtil->setValuesToEntity($store, $fieldValue);

                    $nusersRelated = $fieldValue["userIds"] ?? [];
                    if (count($nusersRelated) > 0) {
                        foreach ($nusersRelated as $nuserId) {
                            if (!($userRelated = $this->em->createQuery('select u from E:User u join u.stores s where s.id = :id and u.id = :userId ')->setParameters(["id" => $store->getId(), "userId" => $nuserId])->getOneOrNullResult())) {
                                $store->addUser($userRelated = $this->em->getReference("E:User", $nuserId));
                            }
                            if (!$userRelated->getDefaultStore() && $defaultStore = $this->em->getRepository('E:Store')->findOneByCompany($store)) {
                                $userRelated->setDefaultStore($defaultStore);
                            }
                        }
                    }

                    $this->em->persist($store);
                    $this->em->persist($userLog);
                })) {
                $this->jsReturn["msg"] = $edit == "false" ? $this->msg["insertOK"] : $this->msg["updateOK"];
                $this->jsReturn["data"] = $returnData;
            } else {
                throw new \Exception($this->msg["insertError"]);
            }
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }

    /**
     * Control para revisar si el url key ya existe
     * @Route("/api/panel/store/checkUrlKey", name ="checkStoreUrlKey", methods={"POST"})
     */
    public function checkUrlKeyAction(Request $request)
    {
        $urlKey = $request->request->get('urlKey');
        $id = $request->request->get('id');
        $result = $this->em->getRepository('E:Store')->checkUrlKeyAction($urlKey, $id);
        return new Response(!$result ? "true" : "false");
    }

    /**
     * @Route("/api/panel/store/catalog/user", condition="request.isXmlHttpRequest()", methods={"GET"})
     * @Configuration\Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function getCatalogUser(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Store')->getStoreUsersNotAsigned($this->jsReturn, $request->get('q'), $request->get('storeId'));
        return $this->responseJson();
    }
}
