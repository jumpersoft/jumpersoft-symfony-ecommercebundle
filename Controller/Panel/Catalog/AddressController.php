<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\AddressValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @Configuration\Security("is_granted('ROLE_CUSTOMER')")
 *
 * NOTAS: Esta clase es compartida por el módulo de direcciones en el catálogo del panel y por el módulo de direcciones en la página de tienda por los clientes
 */
class AddressController extends BaseController
{

    /**
     * @Route("/api/panel/user/address", condition="request.isXmlHttpRequest()", methods={"GET"})
     * @Route("/api/store/account/address", name="getStoreAccountAddresses", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        if ($this->isUser("ROLE_SELLER") && $request->getPathInfo() == "/api/panel/user/address") {
            $userId = $request->get('userId');
        } else {
            $userId = $this->getUser()->getId();
        }
        $rows = $this->em->getRepository('E:Address')->getAddresses($this->jsReturn, $userId);
        $this->jsReturn["data"] = $rows;
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/user/address/{id}", name="getAddress", condition="request.isXmlHttpRequest()", methods={"GET"})
     * @Route("/api/store/account/address/{id}", name="getStoreAccountAddress", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAction($id, Request $request)
    {
        if ($this->isUser("ROLE_SELLER") && $request->getPathInfo() == ("/api/panel/user/address/get/" . $id)) {
            $userId = $request->get('userId');
        } else {
            $userId = $this->getUser()->getId();
        }
        $row = $this->em->getRepository('E:Address')->getAddress($id, $userId);
        $this->jsReturn["data"] = $row;
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/user/address/{id}", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @Route("/api/store/account/address/{id}", condition="request.isXmlHttpRequest()", methods={"DELETE"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAction($id, Request $request)
    {
        if ($this->isUser("ROLE_SELLER") && $request->getPathInfo() == ("/api/panel/user/address/" . $id)) {
            $userId = $request->get('userId');
        } else {
            $userId = $this->getUser()->getId();
        }
        if (!empty($id) && !empty($userId)) {
            $row = $this->em->getRepository('E:Address')->getAddress($id, $userId, "delete");
            $this->em->remove($row);
            $this->jsReturn["msg"] = "Dirección eliminada";
            return $this->responseJson();
        }
        throw new \Exception($this->msg["deletedError"]);
    }

    /**
     * @Route("/api/panel/user/address/{id}", name="addressNewEdit", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @Route("/api/store/account/address/{id}", name="storeAccountAddressNewEdit", condition="request.isXmlHttpRequest()", methods={"PUT"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction($id, Request $request)
    {
        $values = AddressValidator::proccessInputValues($request->request->get('address'), AddressValidator::$address);
        if (AddressValidator::validate(AddressValidator::$address, $values)) {
            $edit = $request->request->get('edit') == "true";
            $returnData["id"] = $id;
            $eventDate = $this->jsUtil->getLocalDateTime();
            if ($this->isUser("ROLE_SELLER") && $request->getPathInfo() == ("/api/panel/user/address/" . $id)) {
                $userId = $request->request->get('userId');
            } else {
                $userId = $this->getUser()->getId();
            }

            $user = $this->em->getRepository('E:User')->find($userId);
            if (!$edit) {
                $address = new Entity\Address();
                $values = array_merge($values, ["id" => ($returnData["id"] = $this->jsUtil->getUid()), "user" => $user, "registerDate" => $eventDate]);
            } else {
                $address = $this->em->getRepository('E:Address')->getAddress($returnData["id"], $userId, "update");
                $values = array_merge($values, ["updateDate" => $eventDate]);
            }
            $this->jsUtil->setValuesToEntity($address, $values);
            
            if (isset($values["defaultShippingAddress"]) && $values["defaultShippingAddress"] == 1 || $user->getAddresses()->count() == 0) {
                $user->setDefaultShippingAddress($address);
            }
            if (isset($values["defaultBillingAddress"]) && $values["defaultBillingAddress"] == 1) {
                $user->setDefaultBillingAddress($address);
            }
            $this->em->persist($address);

            $this->jsReturn["msg"] = $edit ? "Dirección modificada con éxito" : "Dirección creada con éxito";
            $this->jsReturn["data"] = $returnData;
            return $this->responseJson();
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
    }
}
