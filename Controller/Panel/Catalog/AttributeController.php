<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\Catalog;

use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\AttributeValidator;
use Jumpersoft\EcommerceBundle\Entity;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/api/panel/attribute", condition="request.isXmlHttpRequest()")
 * @Configuration\Security("is_granted('ROLE_ADMIN')")
 */
class AttributeController extends BaseController
{

    /**
     * @Route("/set", methods={"GET"})
     */
    public function getSetsAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:AttributeSet')->getAttributeSets($this->jsReturn, $request->get('q'), $this->getUser()->getId(), $this->defaultStoreId, $request->get('for'), $request->get('itemId'));
        return $this->responseJson();
    }

    /**
     * @Route("/set/{id}", methods={"GET"}, requirements={"id"="^(.{20})$"})
     */
    public function getSetAction($id, Request $request)
    {
        $this->jsReturn["attributeSet"] = $this->em->getRepository('E:AttributeSet')->getAttributeSet($id, $this->getUser()->getId(), $request->get('currentStoreId') ?? $this->defaultStoreId);
        return $this->responseJson();
    }

    /**
     * @Route("/set", methods={"DELETE"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteSet(Request $request)
    {
        $user = $this->getUser();
        $rows = $request->get('rows');
        foreach ($rows as $r) {
            $row = $this->em->getRepository('E:AttributeSet')->getAttributeSet($r, $user->getId(), $this->defaultStoreId, "delete");
            $this->em->remove($row);
        }
        $this->jsReturn["msg"] = count($rows) > 1 ? "Grupos de atributos eliminados" : "Grupo de atributos eliminado";
        return $this->responseJson();
    }

    /**
     * @Route("/set/{id}", methods={"PUT"}, requirements={"id"="^(.{20}|0)$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putSet($id, Request $request)
    {
        $edit = $request->request->get('edit') == "true";
        $input = $request->request->get('attributeSet');
        $fieldValue = AttributeValidator::proccessInputValues($input, AttributeValidator::$attributeSet);
        $user = $this->getUser();

        //Check if attributeset name exist
        if ($this->em->getRepository('E:AttributeSet')->checkSetName($fieldValue['name'], $id, $this->defaultStoreId)) {
            throw new \Exception("Yá existe una etiqueta con ese nombre");
        }

        if (AttributeValidator::validate(AttributeValidator::$attributeSet, $fieldValue)) {
            if (!$edit) {
                $attributeSet = new Entity\AttributeSet();
                $fieldValue = array_merge($fieldValue, [
                    "id" => ($id = $this->jsUtil->getUid()),
                    "store" => $this->defaultStoreId,
                    "registerDate" => $this->jsUtil->getLocalDateTime()
                ]);
            } else {
                $attributeSet = $this->em->getRepository('E:AttributeSet')->getAttributeSet($id, $user->getId(), $this->defaultStoreId, "update");
                $attributeSet->SetUpdateDate($this->jsUtil->getLocalDateTime());
            }

            $this->jsUtil->setValuesToEntity($attributeSet, $fieldValue);
            $this->em->persist($attributeSet);

            $this->jsReturn["msg"] = $edit ? "Etiqueta modificada con éxito" : "Etiqueta creada con éxito";
            $this->jsReturn["id"] = !$edit ? $id : null;
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }
        return $this->responseJson();
    }

    /**
     * @Route("/set/check/{name}/{id}", methods={"GET"})
     */
    public function checkSetName($name, $id)
    {
        $result = $this->em->getRepository('E:AttributeSet')->checkAttributeSet($name, $id, $this->defaultStoreId);
        return new Response($result ? "false" : "true");
    }

    /**
     * @Route("/{attributeSetId}/{id}", methods={"PUT"}, requirements={"id"="^(.{20}|0)$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAttribute($attributeSetId, $id, Request $request)
    {
        $edit = !empty($id) && strlen($id) == 20;
        $eventDate = $this->jsUtil->getLocalDateTime();
        $data = AttributeValidator::proccessInputValues($data = $request->request->get('attribute'), AttributeValidator::$attribute);
        $attributeSet = $this->em->getReference("E:AttributeSet", $attributeSetId);
        $countAttribute = $attributeSet->getAttributes()->count();
        if (AttributeValidator::validate(AttributeValidator::$attribute, $data)) {
            //SET ATTRIBUTE
            if (!$edit) {
                $attribute = new Entity\Attribute();
                $this->jsUtil->setValuesToEntity($attribute, [
                    "id" => ($this->jsReturn["id"] = $id = $this->jsUtil->getUid()),
                    "attributeSet" => $attributeSetId,
                    "registerDate" => $eventDate,
                    "type" => $data["typeId"],
                    "sequence" => $countAttribute + 1
                ]);
            } else {
                $attribute = $this->em->getRepository('E:Attribute')->findOneById($id);
            }
            $this->jsUtil->setValuesToEntity($attribute, $data, ["options"]);

            //SET ATTRIBUTE OPTIONS
            $this->jsReturn["ids"] = [];
            if (isset($data["options"])) {
                $countOptions = $attribute->getOptions()->count();
                foreach ($data["options"] as $optionData) {
                    $values = AttributeValidator::proccessInputValues($optionData, AttributeValidator::$attributeOption);
                    if (AttributeValidator::validate(AttributeValidator::$attributeOption, $values)) {
                        if (strlen($optionData["id"]) < 20) {
                            $option = new Entity\AttributeOption();
                            $this->jsUtil->setValuesToEntity($option, [
                                "id" => ($nid = $this->jsUtil->getUid()),
                                "attribute" => $id,
                                "registerDate" => $eventDate,
                                "sequence" => ++$countOptions
                            ]);
                            $this->jsReturn["ids"][] = ["oldId" => $optionData["id"], "id" => $nid];
                        } else {
                            $option = $this->em->getRepository('E:AttributeOption')->findOneById($optionData["id"]);
                        }
                        $this->jsUtil->setValuesToEntity($option, $optionData, ["id"]);
                        $this->em->persist($option);
                    } else {
                        throw new \Exception($this->msg["errorForm"]);
                    }
                }
            }

            $this->em->persist($attribute);
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        $user = $this->getUser();
        $this->jsReturn["msg"] = "Atributos modificados";
        return $this->responseJson();
    }

    /**
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="^(.{20})$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAttribute($id)
    {
        if (($attribute = $this->em->getRepository("E:Attribute")->findOneById($id))) {
            $attributeSetId = $attribute->getAttributeSet()->getId();
            $this->em->remove($attribute);
            $this->em->flush();
            $statement = $this->em->getConnection()->prepare("UPDATE attribute AS t
                                                                JOIN
                                                                ( SELECT @rownum:=@rownum+1 as rownum, id
                                                                  FROM attribute    
                                                                  CROSS JOIN (select @rownum := 0) rn
                                                                  WHERE attributesetid = :id
                                                                  order by sequence asc
                                                                ) AS r ON t.id = r.id
                                                                SET t.sequence = r.rownum");
            $statement->bindValue('id', $attributeSetId);
            $statement->execute();
        } else {
            throw new \Exception("Hubo problemas para borrar el atributo");
        }
        $this->jsReturn["msg"] = "Atributo eliminado";
        return $this->responseJson();
    }

    /**
     * @Route("/reorder/{id}/{oldIndex}/{newIndex}", methods={"PUT"}, requirements={"id"="^(.{20})$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function reorderAttribute($id, $oldIndex, $newIndex)
    {
        $dir = $oldIndex < $newIndex ? "down" : "up";
        $subQuery = $dir == "down" ? "and ((a.sequence >= :oldIndex and a.sequence <= :newIndex) or a.sequence is null)" : "and ((a.sequence >= :newIndex and a.sequence <= :oldIndex) or a.sequence is null)";
        $attribute = $this->em->getRepository('E:Attribute')->findOneById($id);
        $attributeSiblings = $this->em->createQuery("select a from E:Attribute a
                                                      where identity(a.attributeSet) = :attributeSetId 
                                                        $subQuery
                                                        and a.id <> :id
                                                   order by a.sequence asc")
                        ->setParameters(["attributeSetId" => $attribute->getAttributeSet()->getId(), "oldIndex" => $oldIndex, "newIndex" => $newIndex, "id" => $id])->getResult();
        foreach ($attributeSiblings as $key => $a) {
            if ($dir == "down") {
                $a->setSequence($oldIndex++);
                $this->em->persist($a);
                if ($key + 1 == count($attributeSiblings)) {
                    $attribute->setSequence($oldIndex++);
                    $this->em->persist($attribute);
                }
            } else {
                if ($key == 0) {
                    $attribute->setSequence($newIndex++);
                    $this->em->persist($attribute);
                }
                $a->setSequence($newIndex++);
                $this->em->persist($a);
            }
        }
        $this->jsReturn["msg"] = "Atributo reordenado";
        return $this->responseJson();
    }

    /**
     * @Route("/option/{id}", methods={"DELETE"}, requirements={"id"="^(.{20})$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function deleteAttributeOption($id)
    {
        if (($option = $this->em->getRepository("E:AttributeOption")->findOneById($id))) {
            $attributeId = $option->getAttribute()->getId();
            $this->em->remove($option);
            $this->em->flush();
            $statement = $this->em->getConnection()->prepare("UPDATE attributeoption AS t
                                                                JOIN
                                                                ( SELECT @rownum:=@rownum+1 as rownum, id
                                                                  FROM attributeoption    
                                                                  CROSS JOIN (select @rownum := 0) rn
                                                                  WHERE attributeId = :id
                                                                  order by sequence asc
                                                                ) AS r ON t.id = r.id
                                                                SET t.sequence = r.rownum");
            $statement->bindValue('id', $attributeId);
            $statement->execute();
        } else {
            throw new \Exception("Hubo problemas para borrar la opción");
        }
        $this->jsReturn["msg"] = "Opción eliminada";
        return $this->responseJson();
    }

    /**
     * @Route("/option/reorder/{id}/{oldIndex}/{newIndex}", methods={"PUT"}, requirements={"id"="^(.{20})$"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function reorderAttributeOption($id, $oldIndex, $newIndex)
    {
        $dir = $oldIndex < $newIndex ? "down" : "up";
        $subQuery = $dir == "down" ? "and a.sequence >= :oldIndex and a.sequence <= :newIndex" : "and a.sequence >= :newIndex and a.sequence <= :oldIndex ";
        $attributeOption = $this->em->getRepository('E:AttributeOption')->findOneById($id);
        $attributeOptionSiblings = $this->em->createQuery("select a from E:AttributeOption a
                                                      where identity(a.attribute) = :attributeId                                                      
                                                        $subQuery                               
                                                       and a.id <> :id
                                                   order by a.sequence asc")
                        ->setParameters(["attributeId" => $attributeOption->getAttribute()->getId(), "oldIndex" => $oldIndex, "newIndex" => $newIndex, "id" => $id])->getResult();
        foreach ($attributeOptionSiblings as $key => $a) {
            if ($dir == "down") {
                $a->setSequence($oldIndex++);
                $this->em->persist($a);
                if ($key + 1 == count($attributeOptionSiblings)) {
                    $attributeOption->setSequence($oldIndex++);
                    $this->em->persist($attributeOption);
                }
            } else {
                if ($key == 0) {
                    $attributeOption->setSequence($newIndex++);
                    $this->em->persist($attributeOption);
                }
                $a->setSequence($newIndex++);
                $this->em->persist($a);
            }
        }

        $this->jsReturn["msg"] = "Opcion reordenada";
        return $this->responseJson();
    }
}
