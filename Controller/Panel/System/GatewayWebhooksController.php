<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\System;

use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\EcommerceBundle\Entity\TransactionNotification;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;

/**
 * Description of PanelController
 *
 * @author Angel
 *
 * @Configuration\Security("is_granted('ROLE_WEBHOOK')")
 *
 */
class GatewayWebhooksController extends BaseController
{

    /**
     * @Route("/api/openpay/webhooks_a2b6457aeb5", name="openpay_webhooks")
     * Notas:
     *     No olvidar de agregar el IP o DOMINIO del Gateway(openpay u otro) en app_dev.php de otra forma no entrará la petición del gateway.
     */
    public function openpay_webhook_Action(Request $request, LoggerInterface $logger)
    {
        $jsResult = ["success" => false];
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json; charset=UTF-8')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
            $post = $request->request;
        }
        try {
            
            //Messages
            $this->msg["insertOK"] = "Gateway notificacion registered";
            $registerDate = $this->jsUtil->getLocalDateTime();
            $ipSource = $request->getClientIp();

            $gwNotification = new TransactionNotification();
            $gwNotification->setId($this->jsUtil->getUid());
            $gwNotification->setGateway('openpay');
            $gwNotification->setRegisterDate($registerDate);
            $gwNotification->setIpSource($ipSource);
            $gwNotification->setType($post->get('type'));
            $gwNotification->setEventDate($post->get('event_date'));
            $gwNotification->setVerificationCode($post->get('verification_code'));
            $transaction = $post->get('transaction');
            if (isset($transaction)) {
                $gwNotification->setGatewayTransactionId($data["transaction"]["id"]);
                $gwNotification->setTransaction(json_encode($transaction));
            }

            //Agregamos notificación de Gateway y modificamos estatus de la entidad que haya sido modificada (pedido, transaccion etc.)
            if ($this->em->transactional(function ($em) use (&$logger, &$gwNotification) {
                $this->em->persist($gwNotification);
                switch ($gwNotification->getType()) {
                            case 'charge.succeeded':
                                $order = $this->em->getRepository('E:OrderRecord')->getOrderFromGatewayNotification($gwNotification->getGatewayTransactionId());
                                if ($order) {
                                    $order->setStatus($this->em->getReference('E:Status', 'ORD_PAI'));
                                }
                                break;
                            case 'chargeback.created':
                            case 'chargeback.rejected':
                            case 'chargeback.accepted':
                                //TODO GET data to send notification in order to attend the ISSUE!
                                break;
                        }
            })) {
                $jsResult["success"] = true;
            } else {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            $data = substr($request->getContent(), 0, 500);
            $logger->error("Error to save gateway notification: " . (isset($e) ? $e : "") . ". Raw data:" . $data);
            $jsResult["success"] = false;
        }

        return $this->responseJson(null, null, null, $jsResult);
    }

    /**
     * @Route("/api/facebook/webhooks_a2b6457aeb5", name="facebook_webhooks")
     */
    public function facebook_webhook_Action(Request $request, LoggerInterface $logger)
    {
        try {
            if ($request->get("hub_mode") == "subscribe" && $request->get("hub_verify_token") == "tryofwftxz84r9ruznxf") {
                return new Response($request->get("hub_challenge"));
            } else {
                return new Response('failed', 403);
            }
        } catch (\Exception $e) {
            $logger->error("Error to save gateway notification: " . json_encode($post->all()) . (isset($e) ? $e : ""));
        }
    }
}
