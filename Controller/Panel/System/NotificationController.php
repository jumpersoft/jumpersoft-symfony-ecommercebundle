<?php
namespace Jumpersoft\EcommerceBundle\Controller\Panel\System;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftMailerExtension;
use Jumpersoft\EcommerceBundle\Validators\NotificationValidator;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\EcommerceBundle\Entity\Notification;
use Jumpersoft\EcommerceBundle\Entity\UserLog;

/**
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class NotificationController extends BaseController
{

    /**
     * @Route("/api/panel/system/notification/getAll", name="getNotifications", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAllAction(Request $request)
    {
        $this->getFilters($request);
        $userId = $this->getUser()->getId();
        $this->jsReturn["data"] = $this->em->getRepository('E:Notification')->getNotifications($this->jsReturn, $userId);
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/system/notification/get", name="getNotification", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getAction(Request $request)
    {
        $id = $request->get('id');
        $userId = $this->getUser()->getId();
        $row = $this->em->getRepository('E:Notification')->getNotification($id, $userId, $this->isUser('ROLE_SELLER'));
        $this->jsReturn["data"] = $row;
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/system/notification/delete", name="deleteNotification", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true")
     */
    public function deleteAction(Request $request)
    {
        $id = $request->get('id');
        $userId = $this->getUser()->getId();
        $item = $this->em->getRepository('E:Notification')->getNotification($id, $userId, $this->isUser('ROLE_SELLER'), "delete");
        $this->em->remove($item);
        $this->em->flush();
        $this->jsReturn["msg"] = "Notificacion eliminada";
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/system/notification/put", name="notificationsNewEdit", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true")
     */
    public function putAction(Request $request)
    {
        $eventDate = $this->jsUtil->getLocalDateTime();
        $ipSource = $request->getClientIp();

        $edit = $request->request->get('edit');
        $inputValues = $request->request->get('notification');
        $values = NotificationValidator::proccessInputValues($inputValues, NotificationValidator::$validators);

        $userId = $this->getUser()->getId();
        $user = $this->em->getRepository('E:User')->findOneById($userId);
        $returnData["id"] = $request->request->get('id');

        if (NotificationValidator::validate(NotificationValidator::$validators, $values)) {
            $notification = new Entity\Notification();

            if ($this->em->transactional(function ($em) use ($edit, &$returnData, &$user, &$eventDate, &$values, &$notification) {
                    if ($edit == "true") {
                        $notification = $this->em->getRepository('E:Notification')->getNotification($returnData["id"], $user->getId(), $this->isUser('ROLE_SELLER'), "update");
                        $notification->setUpdateDate($eventDate);
                    } else {
                        $values = array_merge($values, [
                            "id" => $returnData["id"] = $this->jsUtil->getUid(),
                            "user" => $user,
                            "registerDate" => $returnData["registerDate"] = $eventDate
                        ]);
                    }

                    $values = array_merge($values, [
                        "orderRecordItem" => !empty($values["orderRecordItemId"]) ? $this->em->getReference('E:OrderRecordItem', $values["orderRecordItemId"]) : null,
                        "type" => $values["typeId"],
                        "to" => $values["toId"]
                    ]);

                    $this->jsUtil->setValuesToEntity($notification, $values);
                    $this->em->persist($notification);
                })) {
                $this->jsReturn["msg"] = ($edit == "true" ? "Notificacion modificada con éxito" : "Notificacion creada con éxito");
                $this->jsReturn["data"] = $returnData;
            } else {
                throw new \Exception($this->msg["insertError"]);
            }
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/system/notification/users", name="getNotificationsUsers", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getNotificationsUsersAction(Request $request)
    {
        $this->getFilters($request);
        $q = $request->get('q');
        $userId = $this->getUser()->getId();
        $this->jsReturn["data"] = $this->em->getRepository('E:Notification')->getUsers($this->jsReturn, $q, $userId, $this->isUser('ROLE_ADMIN'));
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/system/notification/orders", name="getNotificationUserOrders", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getNotificationUserOrdersAction(Request $request)
    {
        $this->getFilters($request);
        $this->jsReturn["data"] = $this->em->getRepository('E:Notification')->getNotificationUserOrders($this->jsReturn, $request->get('userId'), $this->defaultStoreId, $request->get('q'), $request->get('typeId'));
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/system/notification/send", name="sendtNotificationUserOrderRecordItem", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function sendtNotificationUserOrderRecordItem(Request $request, JumpersoftMailerExtension $objMail)
    {
        $id = $request->get('id');
        $userId = $this->getUser()->getId();
        $notification = $this->em->getRepository('E:Notification')->getNotification($id, $userId, $this->isUser('ROLE_SELLER'), "update");
        $para = $this->em->getRepository('E:User')->find($notification->getTo()->getId());
        if ($message = $objMail->sendNotificationFromNoReply($para->getEmail(), $notification->getSubject(), '@siteViews/templates/notification.html.twig', array('message' => html_entity_decode($notification->getMessage())), true)) {
            $notification->setSendDate($this->jsUtil->getLocalDateTime());
            $this->em->persist($notification);
            $this->em->flush();
            $this->jsReturn["msg"] = "Notificación enviada correctamente";
            $this->jsReturn["sendDate"] = $notification->getSendDate();
        } else {
            $this->jsReturn["success"] = $message ? true : false;
            $this->jsReturn["msg"] = "Hubo un problema al envia su notificación";
        }
        return $this->responseJson();
    }
}
