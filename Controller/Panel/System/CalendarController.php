<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel\System;

use Jumpersoft\BaseBundle\Controller\BaseController;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;

/**
 * Description of PanelController
 *
 * @author Angel
 *
 * @Configuration\Security("is_granted('ROLE_SELLER')")
 */
class CalendarController extends BaseController
{
    private static $calendarRule = array(
        "pastEvent" => 0,
        "pastEventColor" => "gray",
        "dangerEvent" => 15,
        "dangerEventColor" => '#B00000',
        "warningEvent" => 30,
        "warningEventColor" => 'orange',
        "okEvent" => 60,
        "okEventColor" => '#3A87AD'
    );

    /**
     * @Route("/api/panel/system/calendar/getAll", name="calendarEventos")
     *
     */
    public function calendarEventosAction()
    {
        $userId = $this->getUser()->getId();
        $jsEevents = $this->em->getRepository('E:HostingAccount')->getUserEventosToFullCallendar($userId);
        $dNow = $this->jsUtil->getLocalDateTime();
        for ($i = 0; $i < count($jsEevents); $i++) {
            $jsEevents[$i]['contractDate'] = isset($jsEevents[$i]['contractDate']) && !empty($jsEevents[$i]['contractDate']) ? $jsEevents[$i]['contractDate']->format('d/m/Y h:i:s A') : '';
            $jsEevents[$i]['date'] = isset($jsEevents[$i]['date']) && !empty($jsEevents[$i]['date']) ? $jsEevents[$i]['date']->format('d/m/Y h:i:s A') : '';
            if (isset($jsEevents[$i]['expirationDate']) && !empty($jsEevents[$i]['expirationDate'])) {
                $days = $jsEevents[$i]['expirationDate']->diff($dNow)->days;
                if ($days <= self::$calendarRule["pastEvent"]) {
                    $jsEevents[$i]['color'] = self::$calendarRule["pastEventColor"];
                } elseif ($days > self::$calendarRule["pastEvent"] && $days <= self::$calendarRule["dangerEvent"]) {
                    $jsEevents[$i]['color'] = self::$calendarRule["dangerEventColor"];
                } elseif ($days > self::$calendarRule["dangerEvent"] && $days <= self::$calendarRule["warningEvent"]) {
                    $jsEevents[$i]['color'] = self::$calendarRule["warningEventColor"];
                } else {
                    $jsEevents[$i]['color'] = self::$calendarRule["okEventColor"];
                }
                $jsEevents[$i]['start'] = $jsEevents[$i]['expirationDate']->format('Y-m-d\\TH:i:s');
                $jsEevents[$i]['expirationDate'] = $jsEevents[$i]['expirationDate']->format('d/m/Y h:i:s A');
            } else {
                $jsEevents[$i]['start'] = $jsEevents[$i]['expirationDate'] = '';
            }
        }
        return $this->getJson($jsEevents);
    }
}
