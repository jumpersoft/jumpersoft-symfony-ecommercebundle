<?php
namespace Jumpersoft\EcommerceBundle\Controller\Panel\System;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\EcommerceBundle\Validators\UserValidator;
use Jumpersoft\EcommerceBundle\Validators\StoreUserValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * @author Angel
 * @Configuration\Security("is_granted('ROLE_CUSTOMER')")
 */
class UserProfileController extends BaseController
{

    /**
      @Route("/api/panel/system/profile/get", name="getProfile", condition="request.isXmlHttpRequest()", methods={"GET"})
      @Route("/api/store/account/profile", name ="getStoreProfile", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getProfileAction()
    {
        $user = $this->getUser();
        $this->jsReturn["data"] = $this->em->getRepository('E:User')->getUserDetail($user->getId());
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/system/profile/edit", name ="profileEdit", condition="request.isXmlHttpRequest()")
     * @Route("/api/store/account/profile", name ="storeAccountProfileEdit", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(csrf="true", transaction="true")
     */
    public function putAction(Request $request, UserPasswordEncoderInterface $encoderPwd)
    {
        $eventDate = $this->jsUtil->getLocalDateTime();
        $userInput = $request->request->get('profile');

        $avoid = ['username', 'password', 'confirmPassword', 'confirmEmail', "agree", "roleId", "entityTypeId", "rfc"];
        $avoid = !isset($cvUser["currentPassword"]) ? array_merge($avoid, ['currentPassword', 'confirmPassword']) : $avoid;

        if ($request->getPathInfo() == "/api/panel/system/profile/edit") {
            $cvUser = UserValidator::proccessInputValues($userInput, UserValidator::$user);
            $valid = UserValidator::validate(UserValidator::$user, $cvUser, [], $avoid);
        } else {
            $cvUser = StoreUserValidator::proccessInputValues($userInput, StoreUserValidator::$storeUserProfile);
            $valid = StoreUserValidator::validate(StoreUserValidator::$storeUserProfile, $cvUser, [], $avoid);
        }

        if ($valid) {
            $user = $this->getUser();
            $this->jsUtil->setValuesToEntity($user, $cvUser, ["password"]);
            if (isset($cvUser['password']) && !empty($cvUser['password'])) {
                if (!$encoderPwd->isPasswordValid($user, $cvUser['currentPassword'])) {
                    throw new \Exception("La contraseña actual no es correcta.");
                }
                $password = $encoderPwd->encodePassword($user, $cvUser['password']);
                $user->setPassword($password);
            }

            $this->em->persist($user);
            $this->em->flush();

            $this->jsReturn["msg"] = $this->msg["updateOK"];
        } else {
            throw new \Exception($this->msg["errorForm"]);
        }

        return $this->responseJson();
    }
}
