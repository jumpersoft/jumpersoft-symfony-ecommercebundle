<?php

namespace Jumpersoft\EcommerceBundle\Controller\Panel;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\EcommerceBundle\DependencyInjection;

/**
 * Description of PanelController
 *
 * @author Angel
 *
 */
class PanelController extends BaseController
{

    /**
     * @Route("/initData", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function initData(DependencyInjection\InitialDataService $initialData)
    {
        $this->jsReturn["data"] = $initialData->getData($this->getUser());
        return $this->responseJson();
    }

    /**
     * @Route("/api/panel/stats", condition="request.isXmlHttpRequest()", methods={"GET"})
     */
    public function getStats()
    {
        $this->jsReturn["data"] = $this->em->getRepository('E:OrderRecord')->getSales($this->defaultStoreId, $this->getUser());
        return $this->responseJson();
    }

    /**
     * @Configuration\Security("is_granted('ROLE_SELLER')")
     * @Route("/api/panel/changeDefaultStore", name="changeDefaultStore", condition="request.isXmlHttpRequest()", methods={"POST"})
     */
    public function changeDefaultStore(Request $request)
    {
        $user = $this->getUser();
        $storeId = $request->get('storeId');
        $store = $this->em->getRepository('E:Store')->getStore($storeId, $user->getId(), "update");
        $user->setDefaultStore($this->em->getReference('E:Store', $storeId));
        $this->em->persist($user);
        $this->em->flush();
        $this->jsReturn["data"] = [];
        return $this->responseJson();
    }
}
