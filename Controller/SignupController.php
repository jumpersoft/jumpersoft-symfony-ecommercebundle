<?php
namespace Jumpersoft\EcommerceBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Jumpersoft\BaseBundle\Controller\BaseController;
use Jumpersoft\BaseBundle\DependencyInjection\Annotations\JsResponse;
use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftMailerExtension;
use Jumpersoft\EcommerceBundle\Validators\StoreUserValidator;
use Jumpersoft\EcommerceBundle\Entity;

/**
 * Description of UserController
 *
 * @author Angel
 */
class SignupController extends BaseController
{

    /**
     * @Route("/signup", condition="request.isXmlHttpRequest()", methods={"POST"})
     * @JsResponse(transaction="true")
     */
    public function signUpAction(Request $request, UserPasswordEncoderInterface $encoderPwd, JumpersoftMailerExtension $objMail)
    {
        $testEnv = $this->getParameter('kernel.environment') == "test";
        $eventDate = $this->jsUtil->getLocalDateTime();
        $data = $request->request->all();
        //CAPTCHA VALIDATION
        if (!$testEnv) {
            $recaptcha = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$this->getParameter("jumpersoft.recaptcha.secretCaptchaKey")}&response={$data["token"]}"));
            $score = $recaptcha->score;
        }
        if ($testEnv || ($recaptcha->success && $recaptcha->score >= 0.5)) {
            $values = StoreUserValidator::proccessInputValues($data, StoreUserValidator::$storeUserSignUp);
            if (StoreUserValidator::validate(StoreUserValidator::$storeUserSignUp, $values)) {
                if ($this->em->getRepository("E:User")->count(["email" => $data["email"]]) == 0) {
                    // ACCOUNT CREATION, INACTIVE WITH TOKEN FOR LATER ACTIVATION
                    $user = new Entity\User();
                    $this->jsUtil->setValuesToEntity($user, array_merge($values, [
                        "id" => $this->jsUtil->getUid(),
                        "signupCode" => $signupCode = $this->jsUtil->getUid(),
                        "username" => $email = $values["email"],
                        "password" => $encoderPwd->encodePassword($user, $values['password']),
                        "isActive" => 0,
                        "registerDate" => $eventDate,
                        "defaultStore" => $this->defaultStoreId,
                        "entityType" => "CUS_FIS",
                        "roles" => ["ROLE_CUSTOMER"]
                    ]));
                    $this->em->persist($user);
                    // SEND EMAIL NOTIFICATION TO NEW CUSTOMER
                    $message = $objMail->sendNotificationFromNoReply(
                        $user->getEmail(),
                        "Notificación de nuevo registro de usuario",
                        '@notificationViews/accountActivation.html.twig',
                        [
                            'fullName' => $user->getFullName(),
                            'Link' => (!$testEnv ? $request->headers->get("origin") : $this->getParameter("test.origin.url")) . "/confirmacion-acceso/$signupCode"
                        ],
                        true
                    );
                } else {
                    throw new \Exception("El correo electrónico ya está dado de alta");
                }
            } else {
                throw new \Exception($this->msg["errorForm"]);
            }
        } else {
            throw new \Exception("La validación del captcha es incorrecta, reinicia tu página y reintenta, si el problema persiste consulta con el administrador del sitio");
        }

        $this->jsReturn["msg"] = "Cuenta creada con éxito, debes confirmar tu cuenta desde tu correo electrónico para continuar";
        return $this->responseJson();
    }

    /**
     * @Route("/signup/confirmation/{token}", condition="request.isXmlHttpRequest()", methods={"GET"})
     * @JsResponse(transaction="true")
     */
    public function signUpConfirmationAction($token, Request $request)
    {
        if ($user = $this->em->createQuery("select u from E:User u where u.signupCode = :token and u.isActive = 0 ")->setParameters(["token" => $token])->getOneOrNullResult()) {
            $user->setIsActive(1);
            $user->setSignupCode(null);
            $this->em->persist($user);
            $this->jsReturn["success"] = true;
            $this->jsReturn["user"] = ["name" => $user->getName(), "username" => $user->getUsername()];
        } else {
            $this->jsReturn["success"] = false;
        }
        return $this->responseJson();
    }
}
