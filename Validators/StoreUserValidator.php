<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of StoreUserValidator
 *
 * @author Angel
 */
class StoreUserValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $storeRecoverAccount = [
        'email' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$', 'maxlength' => 100],
                'messages' => ['required' => "El correo es requerido", 'regex' => "El correo no tiene un formato correcto, ejemplo: tu@gmail.com", 'maxlength' => "El valor m&aacute;ximo para el correo son 100 caracteres"]],
            'value' => ''],
    ];
    public static $storeResetPassword = [
        'password' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=.\-_*])([a-zA-Z0-9sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=*.\-_]){8,36})$'),
                'messages' => array(
                    'required' => "La contraseña es requerida",
                    'regex' => "La contraseña debe contar con al menos 8 caracteres, una letra mayúscula, una minúscula, un número y un caracter especial @!?#$%^&+=.\-_*"
                )),
            'value' => ''),
        'confirmPassword' => array(
            'validator' => array(
                'rules' => array('required' => true, 'equalTo' => 'password'),
                'messages' => array(
                    'required' => "La contraseña es requerida",
                    'equalTo' => "La contraseña debe ser la misma que la contraseña anterior"
                )),
            'value' => '')
    ];
    public static $storeUserSignUp = array(
        'name' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[\w\W]{2,100}$'),
                'messages' => array(
                    'required' => "El nombre es requerido",
                    'regex' => "Mínimo 2 caracter, max 100"
                )),
            'value' => ''),
        'lastName' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[\w\W]{1,100}$'),
                'messages' => array(
                    'required' => "El apellido paterno es requerido",
                    'regex' => "Mínimo 1 caracter, max 100"
                )),
            'value' => ''),
        'mothersLastName' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[\w\W]{0,100}$'),
                'messages' => array(
                    'required' => "El apellido paterno es requerido",
                    'regex' => "Mínimo 1 caracter, max 100"
                )),
            'value' => ''),
        'email' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$', 'maxlength' => 100],
                'messages' => ['required' => "El correo es requerido", 'regex' => "El correo no tiene un formato correcto, ejemplo: tu@gmail.com", 'maxlength' => "El valor m&aacute;ximo para el correo son 100 caracteres"]],
            'value' => ''],
        'password' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=.\-_*])([a-zA-Z0-9sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=*.\-_]){8,36})$'),
                'messages' => array(
                    'required' => "La contraseña es requerida",
                    'regex' => "La contraseña debe contar con al menos 8 caracteres, una letra mayúscula, una minúscula, un número y un caracter especial @!?#$%^&+=.\-_*"
                )),
            'value' => ''),
        'confirmPassword' => array(
            'validator' => array(
                'rules' => array('required' => true, 'equalTo' => 'password'),
                'messages' => array(
                    'required' => "La contraseña es requerida",
                    'equalTo' => "La contraseña debe ser la misma que la contraseña anterior"
                )),
            'value' => '')
    );
    public static $storeUserProfile = array(
        'name' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[\w\W]{1,100}$'),
                'messages' => array(
                    'required' => "El nombre es requerido",
                    'regex' => "Mínimo 1 caracter, max 100"
                )),
            'value' => ''),
        'lastName' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[\w\W]{1,100}$'),
                'messages' => array(
                    'required' => "El apellido paterno es requerido",
                    'regex' => "Mínimo 1 caracter, max 100"
                )),
            'value' => ''),
        'mothersLastName' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[\w\W]{1,100}$'),
                'messages' => array(
                    'required' => "El apellido paterno es requerido",
                    'regex' => "Mínimo 1 caracter, max 100"
                )),
            'value' => ''),
        'username' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$', 'maxlength' => 100],
                'messages' => ['required' => "El dato es requerido", 'regex' => "El dato no tiene un formato correcto de email, ejemplo: tu@gmail.com", 'maxlength' => "El valor m&aacute;ximo para el correo son 100 caracteres"]],
            'value' => ''],
        'email' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$', 'maxlength' => 100],
                'messages' => ['required' => "El correo es requerido", 'regex' => "El correo no tiene un formato correcto, ejemplo: tu@gmail.com", 'maxlength' => "El valor m&aacute;ximo para el correo son 100 caracteres"]],
            'value' => ''],
        'password' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=.\-_*])([a-zA-Z0-9sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=*.\-_]){8,36})$'),
                'messages' => array(
                    'required' => "La contraseña es requerida",
                    'regex' => "La contraseña debe contar con al menos 8 caracteres, una letra mayúscula, una minúscula, un número y un caracter especial @!?#$%^&+=.\-_*"
                )),
            'value' => ''),
        'confirmPassword' => array(
            'validator' => array(
                'rules' => array('required' => true, 'equalTo' => 'password'),
                'messages' => array(
                    'required' => "La contraseña es requerida",
                    'equalTo' => "La contraseña debe ser la misma que la contraseña anterior"
                )),
            'value' => ''),
        'currentPassword' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=.\-_*])([a-zA-Z0-9sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=*.\-_]){8,36})$'),
                'messages' => array(
                    'required' => "La contraseña es requerida",
                    'regex' => "La contraseña debe contar con al menos 8 caracteres, una letra mayúscula, una minúscula, un número y un caracter especial @!?#$%^&+=.\-_*",
                    'equalTo' => "La contraseña debe ser la misma que la contraseña anterior"
                )),
            'value' => ''),
        'alternateEmail' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$', 'maxlength' => 100),
                'messages' => array(
                    'required' => "El correo es requerido",
                    'maxlength' => "El valor m&aacute;ximo para el correo son 100 caracteres",
                    'regex' => "El correo no tiene un formato correcto, ejemplo: tu@gmail.com"
                )),
            'value' => ''),
        'phone' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z .,:\-\(\)]{0,50})$'),
                'messages' => array(
                    'required' => "El teléfono es requerido",
                    'regex' => "Solo se permiten letras y números"
                )),
            'value' => ''),
        'mobile' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z .,:\-\(\)]{0,50})$'),
                'messages' => array(
                    'required' => "El movil es requerido",
                    'regex' => "Solo se permiten letras y números"
                )),
            'value' => ''),
        'agree' => array('validator' => array('rules' => ["required" => true], 'messages' => "Acepte los términos y condiciones"), 'value' => ''),
        'newsletter' => array('validator' => [], 'value' => ''),
        'roleId' => array('validator' => array('rules' => array('required' => true), 'messages' => array('required' => "El rol es requerido")), 'value' => ''),
        'entityTypeId' => ['validator' => ['rules' => ['required' => true], 'messages' => ['required' => "El tipo es requerido"]], 'value' => ''],
        'tradeName' => ['validator' => ['rules' => ['required' => false, 'regex' => "^([0-9 A-Za-zsáéíóúÁÉÍÓÚñÑ]{2,255})$"], 'messages' => ['required' => 'El campo es requerido', 'regex' => 'Escriba entre 2 y 30 caracteres']], 'value' => ''],
        'rfc' => ['validator' => ['rules' => ['required' => true, 'regex' => '^([A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z])?$'], 'messages' => ['required' => "El rfc es requerido", 'regex' => 'Ingresa un RFC válido']], 'value' => ''],
        'rfcNotRequired' => ['validator' => ['rules' => ['required' => false, 'regex' => '^([A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z])?$'], 'messages' => ['required' => "El rfc es requerido", 'regex' => 'Ingresa un RFC válido']], 'value' => ''],
    );
    public static $suscriptionsValidators = [
        'newsletter' => array('validator' => [], 'value' => ''),
        'smsActive' => array('validator' => [], 'value' => ''),
        'photography' => array('validator' => [], 'value' => ''),
        'electronics' => array('validator' => [], 'value' => ''),
        'cellphones' => array('validator' => [], 'value' => ''),
        'computing' => array('validator' => [], 'value' => ''),
        'fashion' => array('validator' => [], 'value' => ''),
        'homeappliances' => array('validator' => [], 'value' => ''),
        'health' => array('validator' => [], 'value' => ''),
        'home' => array('validator' => [], 'value' => ''),
        'children' => array('validator' => [], 'value' => ''),
        'books' => array('validator' => [], 'value' => ''),
        'videogames' => array('validator' => [], 'value' => ''),
        'sports' => array('validator' => [], 'value' => '')
    ];

    /**
     * Only VISA, MASTERCARD Y AMEX
     */
    public static $creditCard = [
        'holder_name' => ['validator' => ['rules' => ['required' => true, 'regex' => "^([A-Za-z ]{4,30})$"], 'messages' => ['required' => 'El campo es requerido', 'regex' => 'Ingrese el nombre'], 'value' => '']],
        'card_number' => ['validator' => ['rules' => ['required' => true, 'regex' => "^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|3[47][0-9]{13})$"], 'messages' => ['required' => 'El campo es requerido', 'regex' => 'Ingrese un número de tarjeta correcto'], 'value' => '']],
        'expiration_month' => ['validator' => ['rules' => ['required' => true, 'min' => 1, 'max' => 12], 'messages' => ['required' => 'El campo es requerido', 'min' => 'El mínimo es 1', 'max' => 'El máximo es 12'], 'value' => '']],
        'expiration_year' => ['validator' => ['rules' => ['required' => true, 'regex' => "^[1-9][0-9]$"], 'messages' => ['required' => 'El campo es requerido', 'regex' => 'Ingrese un año valido'], 'value' => '']],
        'cvv2' => ['validator' => ['rules' => ['required' => true, 'regex' => "^([0-9][0-9][0-9]|[0-9][0-9][0-9][0-9])$"], 'messages' => ['required' => 'El campo es requerido', 'regex' => 'Solo se permiten de 3 a 4'], 'value' => '']],
    ];

}
