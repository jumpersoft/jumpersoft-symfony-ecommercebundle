<?php

namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of MetaValidator
 *
 * @author Angel
 */
class MetaValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $meta = array(
        'text' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true,
                    'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#,.\'\`\-]{1,255})$'),
                'messages' => array(
                    'required' => "EL dato es requerido",
                    'regex' => "El dato debe contar con letras o números, mínimo 1, y algunos caracteres especiales #,.'`-"
                )),
            'value' => ''),
        'value' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true,
                    'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#,.\'\`\-]{1,255})$'),
                'messages' => array(
                    'required' => "EL valor es requerido",
                    'regex' => "El valor debe contar con letras o números, mínimo 1, y algunos caracteres especiales #,.'`-"
                )),
            'value' => ''),
        'code' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true,
                    'regex' => '^([0-9A-Za-z_\-]{1,255})$'),
                'messages' => array(
                    'required' => "El código es requerido",
                    'regex' => "El código debe contar con letras o números y guiones bajos o medios, mínimo 1 max 255"
                )),
            'value' => ''),
        'description' => array(
            'validator' => array(
                'rules' => array(
                    'required' => false,
                    'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#.,\-\n]{0,500})$'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'regex' => "La descripción debe contar con letras o números, mínimo 1, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
    );
}
