<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of TermsValidator
 *
 * @author Angel
 */
class TermsValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $terms = array(
        'text' => array(
            'validator' => [
                "rules" => ['required' => true, 'max' => '5000'],
                'messages' => ['required' => "El texto es requerido", 'max' => 'El máximo de caracteres es 5000']],
            'value' => ''),
        'description' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ&#,.:;\'\`\-]{1,255})$'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'descriptionVal' => "Las descripción debe contar con letras o números, mínimo 1 max 255, y algunos caracteres especiales &#.,-;:"
                )),
            'value' => ''),
        'paymentDays' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^\d{1,4}$'),
                'messages' => array(
                    'required' => "Los días de pago son requeridos",
                    'paymentDaysVal' => "Los días de pago deben ser solo números, max 4"
                )),
            'value' => ''),
        'typeId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El tipo de termino es requerido"
                )),
            'value' => ''),
        'active' => array('validator' => array(), 'value' => '')
    );

}
