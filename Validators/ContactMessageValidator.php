<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of ContactMessageValidator
 *
 * @author Angel
 */
class ContactMessageValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $contactMessageValidator = array(
        'name' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[\w\W]{2,255}$'),
                'messages' => array(
                    'required' => "El nombre es requerido",
                    'regex' => "El nombre debe contar con 2 caracteres como mínimo, max 255"
                )),
            'value' => ''),
        'lastName' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[\w\W]{1,255}$'),
                'messages' => array(
                    'required' => "El apellido paterno es requerido",
                    'regex' => "El nombre debe contar con 2 caracteres como mínimo, max 255"
                )),
            'value' => ''),
        'mothersLastName' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[\w\W]{1,255}$'),
                'messages' => array(
                    'regex' => "El nombre debe contar con 2 caracteres como mínimo, max 255"
                )),
            'value' => ''),
        'phone' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z .,:\-\(\)]{0,50})$'),
                'messages' => array(
                    'required' => "El teléfono es requerido",
                    'regex' => "Solo se permiten letras y números"
                )),
            'value' => ''),
        'email' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$', 'maxlength' => 100],
                'messages' => ['required' => "El correo es requerido", 'regex' => "El correo no tiene un formato correcto, ejemplo: tu@gmail.com", 'maxlength' => "El valor m&aacute;ximo para el correo son 100 caracteres"]],
            'value' => ''],
        'subject' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[\w\W]{1,255}$'),
                'messages' => array(
                    'required' => "El asunto es requerido",
                    'regex' => "El asunto debe constar de al menos un caracter max 255 (letras, n&uacute;meros y caracteres especiales)"
                )),
            'value' => ''),
        'text' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[\w\W]{2,100000}$'),
                'messages' => array(
                    'required' => "El mensaje es requerido",
                    'regex' => "El mensaje no debe contar con caracteres diferentes a letras o numeros y algunos caracteres especiales",
                )),
            'value' => ''
        ),
    );

}
