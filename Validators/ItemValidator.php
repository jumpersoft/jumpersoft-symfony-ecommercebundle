<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of ItemValidator
 *
 * @author Angel
 */
class ItemValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $item = array(
        'typeId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El tipo de producto es requerido"
                )),
            'value' => ''),
        'brandId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "La marca de producto es requerida"
                )),
            'value' => ''),
        'name' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ+*$%#,.()\/\-]{1,255})$',
                    'remote' => array(
                        'url' => "/api/panel/item/checkName",
                        'type' => "post"
                    )),
                'messages' => array(
                    'required' => "El nombre es requerido",
                    'regex' => "Solo se permiten letras o números, mínimo 1 max 500, y algunos caracteres especiales #.-",
                    'remote' => "El nombre ya está en uso, elija otro por favor."
                )),
            'value' => ''),
        'description' => array(
            'validator' => array(
                'rules' => array('required' => false, 'maxlength' => '1000'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'maxlength' => "La descripción debe contar solo con letras o números, mínimo 1 max 1000"
                )),
            'value' => ''),
        'shortDescription' => array(
            'validator' => array(
                'rules' => array('required' => false, 'maxlength' => '255'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'maxlength' => "La descripción debe contar solo con letras o números, mínimo 1 max 255"
                )),
            'value' => ''),
        'urlKey' => array(
            'validator' => array(
                'rules' => array(
                    'required' => false,
                    'regex' => '^([0-9A-Za-z_\-]{1,255})$'),
                'messages' => array(
                    'required' => "El url key es requerido",
                    'regex' => "El url debe contar con letras o números y guiones bajos o medios y sin espacios, mínimo 1 max 255"
                )),
            'value' => ''),
        'specs' => array(
            'validator' => array(
                'rules' => array('required' => false, 'maxlength' => '2000'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'maxlength' => "La descripción debe contar solo con letras o números, mínimo 1 max 2000"
                )),
            'value' => ''),
        // Price
        'pricingType' => array('validator' => array(), 'value' => ''),
        'handleGlobalPrice' => array('validator' => array(), 'value' => ''),
        'workWithPricesParent' => array('validator' => array(), 'value' => ''),
        'regularPrice' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array(
                    'required' => "El precio es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'discount' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[0-9]{0,16}(\.[0-9]{0,2})?$'),
                'messages' => array(
                    'required' => "El descuento es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'discountType' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array('required' => "El tipo es requerido")),
            'value' => ''),
        'discountUntil' => array(
            'validator' => array(
                'rules' => array(
                    'required' => false),
                'messages' => array(
                    'required' => "La fecha es requerida")),
            'value' => ''),
        'salePrice' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array('required' => "El precio es requerido", 'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'currency' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "La moneda es requerida"
                )),
            'value' => ''),
        // Venta a granel
        'bulkSale' => array('validator' => array(), 'value' => ''),
        'unitMeasureBulkSaleId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array('required' => "La unidad es requerida")),
            'value' => ''),
        'quantityByUnitBulkSale' => array(
            'validator' => array('rules' => array('required' => true), 'messages' => array('required' => "La cantidad por la unidad de venta a granel es requerida")),
            'value' => ''),
        'decimalsBulkSale' => array(
            'validator' => array(
                'rules' => array('required' => true, 'min_value' => 1, 'max_value' => 4),
                'messages' => array('required' => "La cantidad es requerida", 'min_value' => "Mínimo 1", "max_value" => "Máximo 4")),
            'value' => ''),
        // Inventario
        'statusId' => array('validator' => array('rules' => array('required' => true), 'messages' => array('required' => "El estatus es requerido")), 'value' => ''),
        'sku' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ+*$%#,.()_\/\'\`\-]{0,255})$'),
                'messages' => array(
                    'required' => "El SKU es requerido",
                    'regex' => "El SKU debe contar con letras o números, mínimo 1, y algunos caracteres especiales #,._'`-"
                )),
            'value' => ''),
        'stock' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,20}?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números enteros, max 20")),
            'value' => ''),
        'stockFractionable1' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,1})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números, max 16, y un decimal , ej: 653 o 3600.4"),
                'inputMaskRegex' => ['type' => 'integer', 'options' => ['rightAlign' => false, 'integerDigits' => '20', 'min' => '0', 'placeholder' => '']]),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'stockFractionable2' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números, max 16, y dos decimales , ej: 653 o 3600.45"),
                'inputMaskRegex' => ['type' => 'integer', 'options' => ['rightAlign' => false, 'integerDigits' => '20', 'min' => '0', 'placeholder' => '']]),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'stockFractionable3' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,3})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números, max 16, y tres decimales , ej: 653 o 3600.532"),
                'inputMaskRegex' => ['type' => 'integer', 'options' => ['rightAlign' => false, 'integerDigits' => '20', 'min' => '0', 'placeholder' => '']]),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'stockFractionable4' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,4})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números, max 16, y cuatro decimales , ej: 653 o 3600.4532"),
                'inputMaskRegex' => ['type' => 'integer', 'options' => ['rightAlign' => false, 'integerDigits' => '20', 'min' => '0', 'placeholder' => '']]),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'fractionable' => array('validator' => array(), 'value' => ''),
        'decimals' => array(
            'validator' => array(
                'rules' => array('required' => true, 'min_value' => 1, 'max_value' => 4),
                'messages' => array('required' => "La cantidad es requerida", 'min_value' => "Mínimo 1", "max_value" => "Máximo 4")),
            'value' => ''),
        'trackingTypeId' => array('validator' => array(), 'value' => ''),
        'unitMeasureId' => array('validator' => array('rules' => array('required' => true), 'messages' => array('required' => "La unidad es requerida")), 'value' => ''),
        'weight' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[\d]{0,18}(\.\d{0,4})?$'),
                'messages' => array('required' => "El peso es requerido", 'regex' => "Solo se permiten números, max 18, y cuatro decimales , ej: 653 o 0.4515"
                )),
            'value' => ''),
        'serialized' => array('validator' => array(), 'value' => ''),
        'dangerous' => array('validator' => array(), 'value' => ''),
        'radioactive' => array('validator' => array(), 'value' => ''),
        'isotopeId' => array('validator' => array('rules' => array('required' => true), 'messages' => array('required' => "El isótopo es requerido")), 'value' => ''),
        'useVolumeToFulFill' => array('validator' => array(), 'value' => ''),
        'fixedPrice' => array('validator' => array(), 'value' => ''),
        'perishable' => array('validator' => array(), 'value' => ''),
        'autoFulFill' => array('validator' => array(), 'value' => ''),
        'useContainer' => array('validator' => array(), 'value' => ''),
        'minQtyAllowedInCart' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[\d]{1,18}(\.\d{0,4})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números enteros, max 20"),
                'inputMaskRegex' => ['type' => 'integer', 'options' => ['rightAlign' => false, 'integerDigits' => '20', 'min' => '1']]),
            'value' => ''),
        'maxQtyAllowedInCart' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[\d]{1,18}(\.\d{0,4})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números enteros, max 20"),
                'inputMaskRegex' => ['type' => 'integer', 'options' => ['rightAlign' => false, 'integerDigits' => '20', 'min' => '1']]),
            'value' => ''),
        'notifyForQtyBelow' => [
            'validator' => [
                'rules' => ['required' => false, 'regex' => '^[\d]{1,18}(\.\d{0,4})?$'],
                'messages' => ['required' => "La cantidad es requerida", 'regex' => "Solo se permiten números enteros, max 20"]],
            'value' => ''],
        'qtyToBecomeOutOfStock' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[\d]{1,18}(\.\d{0,4})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números enteros, max 20"),
                'inputMaskRegex' => ['type' => 'integer', 'options' => ['rightAlign' => false, 'integerDigits' => '20', 'min' => '1']]),
            'value' => ''),
        'comment' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ+*$%&#,.()\/\'\`\-]{0,500})$'),
                'messages' => array('required' => "Las observaciones son requeridas", 'regex' => "Las observaciones deben contar solo con letras o números, mínimo 1, y algunos caracteres especiales &,.'`-"
                )),
            'value' => ''),
        'variant' => array('validator' => array('rules' => array('required' => false), 'messages' => array('required' => "")), 'value' => ''),
        'sequence' => array('validator' => array('rules' => array('required' => false), 'messages' => array('required' => "")), 'value' => '')
    );
    public static $meta = array(
        'metaTitle' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ+*$%#,.()_\/\'\`\-]{1,60})$'),
                'messages' => array(
                    'required' => "El título es requerido",
                    'regex' => "El título debe contar con letras o números, mínimo 1, y algunos caracteres especiales #,._'`-"
                )),
            'value' => ''),
        'metaKeywords' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ+*$%#,.()_\/\'\`\-]{1,255})$'),
                'messages' => array(
                    'required' => "Keywords requeridos",
                    'regex' => "Los keywords deben contar con letras o números, mínimo 1, y algunos caracteres especiales #,._'`-"
                )),
            'value' => ''),
        'metaDescription' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ+*$%#,.()_\/\'\`\-]{1,160})$'),
                'messages' => array(
                    'required' => "Descripción requerido",
                    'regex' => "La descripción debe contar con letras o números, mínimo 1, y algunos caracteres especiales #,._'`-"
                )),
            'value' => ''),
        'urlKey' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z_\-]{1,255})$',
                    'remote' => array(
                        'url' => "/api/panel/item/checkUrlKey",
                        'type' => "post"
                    )),
                'messages' => array(
                    'required' => "El url key es requerido",
                    'regex' => "El url debe contar con letras o números y guiones bajos o medios y sin espacios, mínimo 1 max 255",
                    'remote' => "El url ya está en uso, elija otro por favor."
                )),
            'value' => ''),
    );
    public static $socialMedia = [
        'smFacebook' => array('validator' => array(), 'value' => ''),
        'smLinkedIn' => array('validator' => array(), 'value' => ''),
        'smTwitter' => array('validator' => array(), 'value' => ''),
        'smGoogle' => array('validator' => array(), 'value' => ''),
        'smPinterest' => array('validator' => array(), 'value' => ''),
    ];
    public static $itemValidatorsFilters = array(
        'statusId' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array('required' => "El estatus es requerido")),
            'value' => ''),
        'name' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ+*$%#,.()\/\-]{1,255})$'),
                'messages' => array(
                    'required' => "El nombre es requerido",
                    'regex' => "Solo se permiten letras o números, mínimo 1 max 500, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
        'code' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ+*$%#,.()_\/\'\`\-]{1,255})$'),
                'messages' => array(
                    'required' => "La clave es requerida",
                    'regex' => "La clave debe contar con letras o números, mínimo 1, y algunos caracteres especiales #,._'`-"
                )),
            'value' => ''),
        'categoryName' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ+*$%#,%.()\/\'\`\-]{1,255})$'),
                'messages' => array(
                    'required' => "",
                    'regex' => "El nombredebe contar solo con letras o números, mínimo 1, y algunos caracteres especiales #,.'`-"
                )),
            'value' => ''),
        'regularPriceA' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array(
                    'required' => "El precio es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'regularPriceB' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array(
                    'required' => "El precio es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => '')
    );
    public static $unitMeasure = array(
        'id' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([A-Z_]{1,255})$',),
                'messages' => array('required' => "El identificador es requerido", 'regex' => "El identificador solo debe contar con letras y guiones bajos")),
            'value' => ''),
        'name' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#,.\'\`\-]{1,255})$',
                    'remote' => array(
                        'url' => "/api/panel/catalog/unitMeasure/check",
                        'type' => "post"
                    )),
                'messages' => array(
                    'required' => "La unidad es requerida",
                    'regex' => "La unidad debe contar con letras o números, mínimo 1, y algunos caracteres especiales #,.'`-",
                    'remote' => "El nombre de la unidad ya existe, elija otro por favor"
                )),
            'value' => ''),
        'code' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#,.\'\`\-]{1,255})$'),
                'messages' => array(
                    'required' => "La abreviatura es requerida",
                    'regex' => "La abreviatura debe contar solo con letras o números, mínimo 1, y algunos caracteres especiales #,.'`-"
                )),
            'value' => '')
    );
    public static $category = array(
        'categoryId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "La categoría es requerida"
                )),
            'value' => '')
    );
    public static $tag = array(
        'tagId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "La etiqueta es requerida"
                )),
            'value' => '')
    );
    public static $crossSell = array(
        'proximity' => array(
            'validator' => array(
                'rules' => array('required' => false, 'min' => 1, 'max' => 50),
                'messages' => array(
                    'required' => "Requerido",
                    'min' => "Min 1, Max 50",
                    'max' => "Min 1, Max 50"
                )),
            'value' => '')
    );
    public static $review = array(
        'statusId' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array('required' => "El estatus es requerido")),
            'value' => '')
    );
    public static $bulk = [
        'from' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^[0-9]{1,20}?$'],
                'messages' => ['required' => "El dato es requerido", 'regex' => "Solo se permiten números, max 20"]],
            'value' => ''],
        'from1' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,1})?$'],
                'messages' => ['required' => "El dato es requerido", 'regex' => "Solo se permiten números, max(16), y un decimal, ej: 653 o 3600.4"]],
            'value' => ''],
        'from2' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'],
                'messages' => ['required' => "El dato es requerido", 'regex' => "Solo se permiten números, max(16), y dos decimales, ej: 653 o 3600.45"]],
            'value' => ''],
        'from3' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,3})?$'],
                'messages' => ['required' => "El dato es requerido", 'regex' => "Solo se permiten números, max(16), y tres decimales, ej: 653 o 3600.456"]],
            'value' => ''],
        'from4' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,4})?$'],
                'messages' => ['required' => "El dato es requerido", 'regex' => "Solo se permiten números, max(16), y cuatro decimales, ej: 653 o 3600.4567"]],
            'value' => ''],
        'to' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^[0-9]{1,20}?$'],
                'messages' => ['required' => "El dato es requerido", 'regex' => "Solo se permiten números, max 20"]],
            'value' => ''],
        'to1' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,1})?$'],
                'messages' => ['required' => "El dato es requerido", 'regex' => "Solo se permiten números, max(16), y un decimal, ej: 653 o 3600.4"]],
            'value' => ''],
        'to2' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'],
                'messages' => ['required' => "El dato es requerido", 'regex' => "Solo se permiten números, max(16), y dos decimales, ej: 653 o 3600.45"]],
            'value' => ''],
        'to3' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,3})?$'],
                'messages' => ['required' => "El dato es requerido", 'regex' => "Solo se permiten números, max(16), y tres decimales, ej: 653 o 3600.456"]],
            'value' => ''],
        'to4' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,4})?$'],
                'messages' => ['required' => "El dato es requerido", 'regex' => "Solo se permiten números, max(16), y cuatro decimales, ej: 653 o 3600.4567"]],
            'value' => ''],
        'discount' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array(
                    'required' => "El descuento es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'discountType' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array('required' => "El tipo es requerido")),
            'value' => ''),
        'salePrice' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array('required' => "El precio es requerido", 'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
    ];
    public static $sku = [
        'name' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ+*$%#,.()\/\-]{1,255})$',
                    'remote' => array(
                        'url' => "/api/panel/item/checkName",
                        'type' => "post"
                    )),
                'messages' => array(
                    'required' => "El nombre es requerido",
                    'regex' => "Solo se permiten letras o números, mínimo 1 max 500, y algunos caracteres especiales #.-",
                    'remote' => "El nombre ya está en uso, elija otro por favor."
                )),
            'value' => ''),
        'sku' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ+*$%#,.()_\/\'\`\-]{1,255})$'),
                'messages' => array(
                    'required' => "El SKU es requerido",
                    'regex' => "El SKU debe contar con letras o números, mínimo 1, y algunos caracteres especiales #,._'`-"
                )),
            'value' => ''),
        'unitMeasureId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array('required' => "La unidad es requerida")),
            'value' => ''),
        'discount' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array(
                    'required' => "El descuento es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'discountType' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array('required' => "El tipo es requerido")),
            'value' => ''),
        'salePrice' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array('required' => "El precio es requerido", 'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
    ];
    public static $dispersion = [
        'quantityToDisperse' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,4})?$'],
                'messages' => ['required' => "El dato es requerido", 'regex' => "Solo se permiten números, max(16), y cuatro decimales, ej: 653 o 3600.4210"]],
            'value' => ''],
    ];

}
