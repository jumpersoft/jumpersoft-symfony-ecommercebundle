<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of AttributeValidator
 *
 * @author Angel
 */
class AttributeValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $attributeSet = array(
        'name' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#,.\'\`\-]{1,255})$', 'remote' => ''),
                'messages' => array(
                    'required' => "EL nombre es requerido",
                    'regex' => "La etiqueta debe contar con letras o números, mínimo 1, y algunos caracteres especiales #,.'`-",
                    'remote' => "El nombre de la etiqueta ya existe, elija otro por favor"
                )),
            'value' => ''),
        'description' => array(
            'validator' => array(
                'rules' => array(
                    'required' => false, 'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#.,\-\n]{0,500})$'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'regex' => "La descripción debe contar con letras o números, mínimo 1, y algunos caracteres especiales #.-"
                )),
            'value' => '')
    );
    public static $attribute = array(
        'name' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => "^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#(),._\'\`\-]{1,255})$"),
                'messages' => array('required' => "EL nombre es requerido", 'regex' => "La etiqueta debe contar con letras o números, mínimo 1, y algunos caracteres especiales _#,.'`-()")
            ),
            'value' => ''),
        'description' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => "^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#().,\-\n]{0,500})$"),
                'messages' => array('required' => "La descripción es requerida", 'regex' => "La descripción debe contar con letras o números, mínimo 1, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
        'code' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => "^([0-9a-z\-_]{0,100})$"),
                'messages' => array('required' => "El dato es requerido", 'regex' => "El código solo puede tener letras mínusculas, números, guiones o guiones bajos, sin espacios, max 100 "
                )),
            'value' => ''),
        'typeId' => array('validator' => array('rules' => array('required' => true), 'messages' => array('required' => "El tipo es requerido")), 'value' => ''),
        'options' => array('validator' => array(), 'value' => ''),
        'required' => array('validator' => array(), 'value' => ''),
        'internalUse' => array('validator' => array(), 'value' => ''),
        'useRange' => array('validator' => array(), 'value' => ''),
        'useMinCurrentDate' => array('validator' => array(), 'value' => ''),
        'defaultOption' => array('validator' => array(), 'value' => ''),
        'defaultText' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => "^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#().,\-\n]{0,1000})$"),
                'messages' => array('required' => "La descripción es requerida", 'regex' => "La descripción debe contar con letras o números, mínimo 1 max 1000, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
        'defaultValue' => ['validator' => [
                'rules' => ['required' => false, 'min_value' => 0, 'max_value' => 99999],
                'messages' => ['required' => "El dato es requerido", 'min_value' => "Mínimo 1", "max_value" => "Máximo 99999"]], 'value' => ''],
        'defaultValueDecimal' => [
            'validator' => [
                'rules' => ['required' => false, 'min_value' => 0.0001, 'max_value' => 99999.9999, 'regex' => '^[0-9]{1,5}(\.[0-9]{0,4})?$'],
                'messages' => ['required' => "La cantidad es requerida", 'min_value' => "Mínimo 0.0001", "max_value" => "Máximo 99999.99999", 'regex' => "Solo se permiten números, max 5 y 4 decimales , ej: 653 o 3600.4321"]],
            'value' => ''],
        'min' => ['validator' => [
                'rules' => ['required' => true, 'min_value' => 1, 'max_value' => 99999],
                'messages' => ['required' => "El dato es requerido", 'min_value' => "Mínimo 1", "max_value" => "Máximo 99999"]], 'value' => ''],
        'max' => ['validator' => [
                'rules' => ['required' => true, 'min_value' => 1, 'max_value' => 99999],
                'messages' => ['required' => "El dato es requerido", 'min_value' => "Mínimo 1", "max_value" => "Máximo 99999"]], 'value' => ''],
        'minDecimal' => [
            'validator' => [
                'rules' => ['required' => false, 'min_value' => 0.0001, 'max_value' => 99999.9999, 'regex' => '^[0-9]{1,5}(\.[0-9]{0,4})?$'],
                'messages' => ['required' => "La cantidad es requerida", 'min_value' => "Mínimo 0.0001", "max_value" => "Máximo 99999.99999", 'regex' => "Solo se permiten números, max 5 y 4 decimales , ej: 653 o 3600.4321"]],
            'value' => ''],
        'maxDecimal' => [
            'validator' => [
                'rules' => ['required' => false, 'min_value' => 0.0001, 'max_value' => 99999.9999, 'regex' => '^[0-9]{1,5}(\.[0-9]{0,4})?$'],
                'messages' => ['required' => "La cantidad es requerida", 'min_value' => "Mínimo 0.0001", "max_value" => "Máximo 99999.99999", 'regex' => "Solo se permiten números, max 5 y 4 decimales , ej: 653 o 3600.4321"]],
            'value' => '']
    );
    public static $attributeOption = array(
        'name' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#(),._\'\`\-]{1,255})$',
                    'remote' => array(
                        'url' => "/api/panel/catalog/tag/check",
                        'type' => "post"
                    )),
                'messages' => array(
                    'required' => "EL nombre es requerido",
                    'regex' => "La etiqueta debe contar con letras o números, mínimo 1, y algunos caracteres especiales #,.'`-_()",
                    'remote' => "El nombre de la etiqueta ya existe, elija otro por favor"
                )),
            'value' => ''),
        'value' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#(),._\'\`\-]{1,255})$'),
                'messages' => array(
                    'required' => "EL valor es requerido",
                    'regex' => "El valor debe contar con letras o números, mínimo 1, y algunos caracteres especiales #,.'`-_()",
                )),
            'value' => ''),
    );

}
