<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of ServerValidator
 *
 * @author Angel
 */
class ServerValidator extends JumpersoftValidatorExtension
{

    //Objeto enviado para validaciones en javascript y validaciones en php. Agregar nombredelcampo, expresión a validar, y los flags
    public $serverValidators = array(
        'name' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#.\-]{1,255})$',
                    'remote' => array(
                        'url' => "/api/panel/hosting/catalog/server/check",
                        'type' => "post"
                    )),
                'messages' => array(
                    'required' => "El nombre es requerido",
                    'regex' => "El nombre debe contar con letras o números, mínimo 1, y algunos caracteres especiales #.-",
                    'remote' => "El nombre yá está en uso, elija otra por favor"
                )),
            'value' => ''),
        'description' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#.\-]{1,500})$'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'regex' => "La descripción debe contar con letras o números, mínimo 1, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
        'ip1' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'),
                'messages' => array(
                    'required' => "Valor requerido",
                    'regex' => "El ip no tiene el formato correcto, ej. 10.0.0.1"
                )),
            'value' => ''),
        'ip2' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'),
                'messages' => array(
                    'required' => "Valor requerido",
                    'regex' => "El ip no tiene el formato correcto, ej. 10.0.0.1"
                )),
            'value' => ''),
        'ns1' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^(?:[a-zA-Z0-9][a-zA-Z0-9-]+){1}(?:\.[a-zA-Z]{2,255})+$'),
                'messages' => array(
                    'required' => "Valor requerido",
                    'regex' => "El dominio no tiene el formato correcto, ns1.jumpersoft.com"
                )),
            'value' => ''),
        'ns2' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^(?:[a-zA-Z0-9][a-zA-Z0-9-]+){1}(?:\.[a-zA-Z]{2,255})+$'),
                'messages' => array(
                    'required' => "Valor requerido",
                    'regex' => "El dominio no tiene el formato correcto, ns2.jumpersoft.com"
                )),
            'value' => ''),
        'domain' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^(?:[a-zA-Z0-9][a-zA-Z0-9-]+){1}(?:\.[a-zA-Z]{2,255})+$'),
                'messages' => array(
                    'required' => "El dominio es requerido",
                    'regex' => "El dominio no tiene el formato correcto, jumpersoft.com"
                )),
            'value' => ''),
        'comment' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#.\-]{1,500})$'),
                'messages' => array(
                    'required' => "",
                    'regex' => "Solo se permiten letras o números, mínimo 1 max 500, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
        'serviceProvider' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#.\-]{1,255})$'),
                'messages' => array(
                    'required' => "Valor requerido",
                    'regex' => "Solo se permiten letras o números, mínimo 1 max 255, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
        'emailAdmin' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$',
                    'maxlength' => 100),
                'messages' => array(
                    'required' => "El correo es requerido",
                    'maxlength' => "El valor m&aacute;ximo para el correo son 100 caracteres",
                    'regex' => "El correo no tiene un formato correcto, ejemplo: tu@gmail.com"
                )),
            'value' => ''),
        'statusId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El estatus es requerido"
                )),
            'value' => '')
    );

}
