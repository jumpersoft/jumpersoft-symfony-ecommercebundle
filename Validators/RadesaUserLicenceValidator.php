<?php

namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of RadesaUserLicenceValidator
 *
 * @author Angel
 */
class RadesaUserLicenceValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $userLicence = [
        'folio' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => "([0-9A-Za-z\-]{1,100})$"),
                'messages' => array('required' => "El número es requerido", 'regex' => "Solo se permiten letras y números, max 10")),
            'value' => ''),
        'startDate' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => array(
                    'required' => "La fecha inicio es requerida")),
            'value' => ''),
        'endDate' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => array(
                    'required' => "La fecha fin es requerida")),
            'value' => ''),
        'statusId' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => array(
                    'required' => "El estatus es requerido")),
            'value' => ''),
        'notes' => array(
            'validator' => array(
                'rules' => array('required' => false, 'maxlength' => '1000'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'maxlength' => "La descripción debe contar solo con letras o números, max 1000 caraceteres"
                )),
            'value' => ''),
    ];
    public static $userLicenceIsotope = [
        'isotopeId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El isótopo es requerido"
                )),
            'value' => ''),
        'activityByDay' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => "^[0-9]{1,16}(\.[0-9]{1,4})?$"),
                'messages' => array('required' => "Actividad requerida", 'regex' => "Solo se permiten números, max 16, y cuatro decimales , ej: 653 o 3600.4532")),
            'value' => ''),
        'unitMeasureId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array('required' => "La unidad es requerida")),
            'value' => ''),
    ];
}
