<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of CertificateValidator
 *
 * @author Angel
 */
class CertificateValidator extends JumpersoftValidatorExtension
{

    //Objeto enviado para validaciones en javascript y validaciones en php. Agregar nombredelcampo, expresión a validar, y los flags
    public $certificateValidators = array(
        'name' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-zsáéíóúñÁÉÍÓÚÑ#.\-]{1,100})$',
                    'remote' => array(
                        'url' => "/api/panel/catalog/certificate/check",
                        'type' => "post"
                    )),
                'messages' => array(
                    'required' => "La serie es requerida",
                    'regex' => "La serie debe contar con letras o números, mínimo 1, y algunos caracteres especiales #.-",
                    'remote' => "La serie yá está en uso, elija otra por favor"
                )),
            'value' => ''),
        'initialFolio' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^\d{1,20}$'),
                'messages' => array(
                    'required' => "El folio inicial es requerido",
                    'regex' => "El folio inicial debe contar solo con números, mínimo 1"
                )),
            'value' => ''),
        'lastFolioGenerated' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^\d{1,20}$'),
                'messages' => array(
                    'required' => "El último folio generado es requerido",
                    'regex' => "Solo se permiten números"
                )),
            'value' => ''),
        'receiptTypeId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El tipo de comprobante es requerido"
                )),
            'value' => ''),
    );

}
