<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of NotificationValidator
 *
 * @author Angel
 */
class NotificationValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $notification = array(
        'subject' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true,
                    'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#;,.\-]{1,255})$'),
                'messages' => array(
                    'required' => "El asunto es requerido",
                    'regex' => "El asunto debe contar con letras o números, mínimo 1, max 255 y algunos caracteres especiales #:;,.\-"
                )),
            'value' => ''),
        'typeId' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => ['required' => "El tipo de notificación es requerida"]),
            'value' => ''),
        'fromEmail' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => ['required' => "El emisor de la notificación es requerido"]),
            'value' => ''),
        'toId' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => ['required' => "El destinatario es requerido"]),
            'value' => ''),
        'orderRecordItemId' => array(
            'validator' => array(
                'rules' => array(
                    'required' => false),
                'messages' => ['required' => "El producto o servicio es requerido"]),
            'value' => ''),
        'toId' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => ['required' => "El mensaje es requerido"]),
            'value' => ''),
        'message' => array('validator' => [
                'rules' => [
                    'required' => true,
                    'max' => '5000'
                ],
                'messages' => [
                    'required' => "El mensaje es requerido",
                    'max' => 'El máximo de caracteres es 5000'
                ]
            ], 'value' => ''),
    );

}
