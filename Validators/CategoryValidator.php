<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of CategoryValidator
 *
 * @author Angel
 */
class CategoryValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $category = array(
        'name' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[\w\W]{1,255}$'),
                'messages' => array('required' => "El nombre es requerido", 'regex' => "Solo se permite de 1 a 255 caracteres")),
            'value' => ''),
        'label' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[\w\W]{1,100}$'),
                'messages' => array('required' => "El dato es requerido", 'regex' => "Solo se permite de 1 a 100 caracteres")),
            'value' => ''),
        'cssIcon' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[\w\W]{1,100}$'),
                'messages' => array('regex' => "Solo se permite de 1 a 100 caracteres")),
            'value' => ''),
        'description' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[\w\W]{1,500}$'),
                'messages' => array('required' => "La descripción es requerida", 'regex' => "Solo se permite de 1 a 500 caracteres")),
            'value' => ''),
        'parentId' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array('required' => "La categoría es requerida")),
            'value' => ''),
        'typeId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array('required' => "El tipo de categoría es requerida")),
            'value' => ''),
        'title' => [
            'validator' => ['rules' => ['required' => false, 'regex' => '^[\w\W]{1,255}$'], 'messages' => ['required' => "El título es requerido", 'regex' => "Mínimo 1 max 255"]],
            'value' => ''],
        'subtitle' => [
            'validator' => ['rules' => ['required' => false, 'regex' => '^[\w\W]{1,255}$'], 'messages' => ['required' => "El subtítulo es requerido", 'regex' => "Mínimo 1 max 255"]],
            'value' => ''],
        'text' => [
            'validator' => ['rules' => ['required' => false, 'regex' => '^[\w\W]{1,100000}$'], 'messages' => ['required' => "El texto es requerido", 'regex' => "Mínimo 1 max 100,000"]],
            'value' => ''],
        'externalUrl' => [
            'validator' => [
                'rules' => ['required' => false, 'regex' => '^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:?#\/[\]@!\$&\'\(\)\*\+,;=.]+$'],
                'messages' => ['required' => "El url es requerido", 'regex' => "Ingrese una dirección web válida"]],
            'value' => ''],
        'urlKey' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z_\-]{1,255})$',
                    'remote' => array(
                        'url' => "/api/panel/store/checkUrlKey",
                        'type' => "post"
                    )),
                'messages' => array(
                    'required' => "El url key es requerido",
                    'regex' => "El url debe contar con letras o números y guiones bajos o medios, mínimo 1 max 255",
                    'remote' => "El url ya está en uso, elija otro por favor."
                )),
            'value' => ''),
        'active' => array('validator' => array(), 'value' => ''),
        'notShowFilters' => array('validator' => array(), 'value' => ''),
        'metaTitle' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ+*$%#,.()_\/\'\`\-]{0,60})$'),
                'messages' => array(
                    'required' => "El título es requerido",
                    'regex' => "El título debe contar con letras o números, mínimo 1, y algunos caracteres especiales #,._'`-"
                )),
            'value' => ''),
        'metaKeywords' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ+*$%#,.()_\/\'\`\-]{0,255})$'),
                'messages' => array(
                    'required' => "Keywords requeridos",
                    'regex' => "Los keywords deben contar con letras o números, mínimo 1, y algunos caracteres especiales #,._'`-"
                )),
            'value' => ''),
        'metaDescription' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ+*$%#,.()_\/\'\`\-]{0,160})$'),
                'messages' => array(
                    'required' => "Descripción requerido",
                    'regex' => "La descripción debe contar con letras o números, mínimo 1, y algunos caracteres especiales #,._'`-"
                )),
            'value' => ''),
    );
    public static $filterValidators = array(
        'name' => array(
            'validator' => array(
                'rules' => array(
                    'required' => false,
                    'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#.,\-¿?¡!]{1,255})$'),
                'messages' => array(
                    'required' => "El nombre es requerido",
                    'regex' => "El nombre debe contar con letras o números, mínimo 1 max 255, y algunos caracteres especiales #.,-¿?¡!"
                )),
            'value' => ''),
        'typeId' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array(
                    'required' => "El tipo de categoría es requerida"
                )),
            'value' => '')
    );

}
