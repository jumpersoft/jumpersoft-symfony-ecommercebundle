<?php

namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of UserValidator
 *
 * @author Angel
 */
class UserValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $user = array(
        'username' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$', 'maxlength' => 100],
                'messages' => ['required' => "El dato es requerido", 'regex' => "El dato no tiene un formato correcto de email, ejemplo: tu@gmail.com", 'maxlength' => "El valor m&aacute;ximo para el correo son 100 caracteres"]],
            'value' => ''],
        'usernameCust' => [
            'validator' => [
                'rules' => ['required' => false, 'regex' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$',],
                'messages' => ['required' => "El dato es requerido", 'regex' => "El dato no tiene un formato correcto de email, ejemplo: tu@gmail.com", 'maxlength' => "El valor m&aacute;ximo para el correo son 100 caracteres"]],
            'value' => ''],
        'email' => [
            'validator' => [
                'rules' => ['required' => true, 'regex' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$', 'maxlength' => 100],
                'messages' => ['required' => "El correo es requerido", 'regex' => "El correo no tiene un formato correcto, ejemplo: tu@gmail.com", 'maxlength' => "El valor m&aacute;ximo para el correo son 100 caracteres"]],
            'value' => ''],
        'password' => array(
            'validator' => array(
                'expval' => array('exp' => '^((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=.\-_*])([a-zA-Z0-9sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=*.\-_]){8,36})$', 'f' => 'i'),
                'rules' => array('required' => true, 'passwordVal' => true),
                'messages' => array(
                    'required' => "La contraseña es requerida",
                    'passwordVal' => "La contraseña debe contar con al menos 8 caracteres, una letra mayúscula, una minúscula, un número y un caracter especial @!?#$%^&+=.\-_*"
                )),
            'value' => ''),
        'confirmPassword' => array(
            'validator' => array(
                'rules' => array('required' => true, 'equalTo' => 'password'),
                'messages' => array(
                    'required' => "La contraseña es requerida",
                    'equalTo' => "La contraseña debe ser la misma que la contraseña anterior"
                )),
            'value' => ''),
        'currentPassword' => array(
            'validator' => array(
                'expval' => array('exp' => '^((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=.\-_*])([a-zA-Z0-9sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=*.\-_]){8,36})$', 'f' => 'i'),
                'rules' => array('required' => true, 'currentPasswordVal' => true),
                'messages' => array(
                    'required' => "La contraseña es requerida",
                    'currentPasswordVal' => "La contraseña debe contar con al menos 8 caracteres, una letra mayúscula, una minúscula, un número y un caracter especial @!?#$%^&+=.\-_*",
                    'equalTo' => "La contraseña debe ser la misma que la contraseña anterior"
                )),
            'value' => ''),
        'alternateEmail' => array(
            'validator' => array(
                'expval' => array('exp' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$', 'f' => 'i'),
                'rules' => array('required' => false, 'alternateEmailVal' => true, 'maxlength' => 100),
                'messages' => array(
                    'required' => "El correo es requerido",
                    'maxlength' => "El valor m&aacute;ximo para el correo son 100 caracteres",
                    'alternateEmailVal' => "El correo no tiene un formato correcto, ejemplo: tu@gmail.com"
                )),
            'value' => ''),
        'name' => array(
            'validator' => array(
                'expval' => array('exp' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ&,.\'\`\-()]{2,100})$', 'f' => 'i'),
                'rules' => array('required' => true, 'nameVal' => true),
                'messages' => array(
                    'required' => "El nombre es requerido",
                    'nameVal' => "El nombre debe contar solo con letras, mínimo 2, y algunos caracteres especiales &,.'`-"
                )),
            'value' => ''),
        'lastName' => array(
            'validator' => array(
                'expval' => array('exp' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ&,.\'\`\-]{2,100})$', 'f' => 'i'),
                'rules' => array('required' => true, 'lastNameVal' => true),
                'messages' => array(
                    'required' => "El apellido paterno es requerido",
                    'lastNameVal' => "El apellido paterno debe contar solo con letras, mínimo 2, y algunos caracteres especiales &,.'`-"
                )),
            'value' => ''),
        'mothersLastName' => array(
            'validator' => array(
                'expval' => array('exp' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ&,.\'\`\-]{0,100})$', 'f' => 'i'),
                'rules' => array('required' => false, 'mothersLastNameVal' => true),
                'messages' => array(
                    'mothersLastNameVal' => "Solo se permiten letras y algunos caracteres especiales &,.'`-"
                )),
            'value' => ''),
        'phone' => array(
            'validator' => array(
                'expval' => array('exp' => '^([0-9A-Za-z .,:\-\(\)]{0,50})$', 'f' => 'i'),
                'rules' => array('required' => false, 'phoneVal' => true),
                'messages' => array(
                    'required' => "El teléfono es requerido",
                    'phoneVal' => "Solo se permiten letras y números"
                )),
            'value' => ''),
        'mobile' => array(
            'validator' => array(
                'expval' => array('exp' => '^([0-9A-Za-z .,:\-\(\)]{0,50})$', 'f' => 'i'),
                'rules' => array('required' => true, 'mobileVal' => 'true'),
                'messages' => array(
                    'required' => "El movil es requerido",
                    'mobileVal' => "Solo se permiten letras y números"
                )),
            'value' => ''),
        'agree' => array('validator' => array('rules' => ["required" => true], 'messages' => "Acepte los términos y condiciones"), 'value' => ''),
        'newsletter' => array('validator' => [], 'value' => ''),
        'roleId' => array('validator' => array('rules' => array('required' => true), 'messages' => array('required' => "El rol es requerido")), 'value' => ''),
        'entityTypeId' => ['validator' => ['rules' => ['required' => true], 'messages' => ['required' => "El tipo es requerido"]], 'value' => ''],
        'tradeName' => ['validator' => ['rules' => ['required' => false, 'regex' => "^([0-9 A-Za-zsáéíóúÁÉÍÓÚñÑ]{2,255})$"], 'messages' => ['required' => 'El campo es requerido', 'regex' => 'Escriba entre 2 y 30 caracteres']], 'value' => ''],
        'rfc' => ['validator' => ['rules' => ['required' => true, 'regex' => '^([A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z])?$'], 'messages' => ['required' => "El rfc es requerido", 'rfcVal' => 'Ingresa un RFC válido']], 'value' => ''],
        'rfcNotRequired' => ['validator' => ['rules' => ['required' => false, 'regex' => '^([A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z])?$'], 'messages' => ['required' => "El rfc es requerido", 'rfcVal' => 'Ingresa un RFC válido']], 'value' => ''],
    );
    
    public static $login = array(
        'email' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true,
                    'regex' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$'),
                'messages' => array(
                    'required' => "El correo es requerido",
                    'regex' => "El correo no tiene un formato correcto, ejemplo: tu@gmail.com"
                )),
            'value' => ''),
        'password' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=.\-_*])([a-zA-Z0-9sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=*.\-_]){8,36})$'),
                'messages' => array(
                    'required' => "La contraseña es requerida",
                    'regex' => "La contraseña debe contar con al menos 8 caracteres, una letra mayúscula, una minúscula, un número y un caracter especial @!?#$%^&+=.\-_*"
                )),
            'value' => '')
    );
    
    public static $emailValidator = [
        'email' => array(
            'validator' => array(
                'expval' => array('exp' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$', 'f' => 'i'),
                'rules' => array('required' => true, 'emailVal' => true, 'maxlength' => 100),
                'messages' => array(
                    'required' => "El correo es requerido",
                    'maxlength' => "El valor m&aacute;ximo para el correo son 100 caracteres",
                    'emailVal' => "El correo no tiene un formato correcto, ejemplo: tu@gmail.com"
                )),
            'value' => ''),
    ];
    
    public static $recoverPasswordValidators = array(
        'password' => array(
            'validator' => array(
                'expval' => array('exp' => '^((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=.\-_*])([a-zA-Z0-9sáéíóúÁÉÍÓÚñÑ@!?#$%^&+=*.\-_]){8,36})$', 'f' => 'i'),
                'rules' => array('required' => true, 'passwordVal' => true),
                'messages' => array(
                    'required' => "La contraseña es requerida",
                    'passwordVal' => "La contraseña debe contar con al menos 8 caracteres, una letra mayúscula, una minúscula, un número y un caracter especial @!?#$%^&+=.\-_*"
                )),
            'value' => ''),
        'confirmPassword' => array(
            'validator' => array(
                'expval' => array('exp' => '', 'f' => ''),
                'rules' => array('required' => true, 'equalTo' => '#password'),
                'messages' => array(
                    'required' => "La contraseña es requerida",
                    'equalTo' => "La contraseña debe ser la misma que la contraseña anterior"
                )),
            'value' => '')
    );
    
    public static $userRules = [
        'allowsReturns' => ['validator' => []],
        'returnPercentage' => ['validator' => [
                'rules' => ['required' => true, 'regex' => '^[0-9]{0,3}?$', "min_value" => 1, "max_value" => "100"],
                'messages' => ['required' => 'El campo es requerido', 'regex' => 'Ingrese un valor correcto min 1 max 100', "min_value" => "Min 1", "max_value" => "Max 100"],
                'value' => '']],
    ];
}
