<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of AnnouncementValidator
 *
 * @author Angel
 */
class AnnouncementValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $announcement = array(
        'description' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true, 'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#.\-]{1,255})$'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'regex' => "La descripción debe contar con letras o números, mínimo 1, max 255 y algunos caracteres especiales #.-"
                )),
            'value' => ''),
        'text' => array('validator' => [
                'rules' => [
                    'required' => true,
                    'max' => '5000'
                ],
                'messages' => [
                    'required' => "El texto es requerido",
                    'max' => 'El máximo de caracteres es 5000'
                ]
            ], 'value' => ''),
        'startDate' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => array(
                    'required' => "La fecha inicial es requerida")),
            'value' => ''),
        'endDate' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => array(
                    'required' => "La fecha final es requerida")),
            'value' => ''),
        'roleIds' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true,
                    'minlength' => 1),
                'messages' => ['required' => "Al menos un rol es requerido"]),
            'value' => ''),
        'moduleIds' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => ['required' => "Al menos un módulo es requerido"]),
            'value' => '')
    );

}
