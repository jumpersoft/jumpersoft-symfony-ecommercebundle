<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of CompanyValidator
 *
 * @author Angel
 */
class CompanyValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $company = array(
        'rfc' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z])$'),
                'messages' => array(
                    'required' => "El rfc es requerido",
                    'regex' => 'Ingresa un RFC válido'
                )),
            'value' => ''),
        'name' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([A-Za-z sáéíóúñÁÉÍÓÚÑ&,.\'\`\-]{2,100})$'),
                'messages' => array(
                    'required' => "El nombre o razon social es requerida",
                    'regex' => "El nombre o razon social debe contar solo con letras, mínimo 2, y algunos caracteres especiales &,.'`-"
                )),
            'value' => ''),
        'tradeName' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ&,.\'\`\-]{1,255})$'),
                'messages' => array(
                    'required' => "El nombre comercial es requerido",
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &,.'`-"
                )),
            'value' => ''),
        'country' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([A-Za-z sáéíóúñÁÉÍÓÚÑ&,.\'\`\-]{2,100})$'),
                'messages' => array(
                    'required' => "El pais es requerido",
                    'regex' => "El pais debe contar solo con letras, mínimo 2, y algunos caracteres especiales &,.'`-"
                )),
            'value' => ''),
        'state' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([A-Za-z sáéíóúñÁÉÍÓÚÑ&,.\'\`\-]{2,100})$'),
                'messages' => array(
                    'required' => "El estado es requerido",
                    'regex' => "El estado debe contar solo con letras, mínimo 2, y algunos caracteres especiales &,.'`-"
                )),
            'value' => ''),
        'city' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ&,.\'\`\-]{0,100})$'),
                'messages' => array(
                    'required' => "La ciudad es requerida",
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &,.'`-"
                )),
            'value' => ''),
        'streetName' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#&,.\'\`\-]{1,100})$'),
                'messages' => array(
                    'required' => "La calle es obligatoria",
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &#,.'`-"
                )),
            'value' => ''),
        'neighborhood' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#&,.\'\`\-]{1,100})$'),
                'messages' => array(
                    'required' => "La colonia es obligatoria",
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &#,.'`-"
                )),
            'value' => ''),
        'numExt' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#&,.\'\`\-]{1,100})$'),
                'messages' => array(
                    'required' => "El número exterior es obligatorio",
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &#,.'`-"
                )),
            'value' => ''),
        'numInt' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#&,.\'\`\-]{0,100})$'),
                'messages' => array(
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &#,.'`-"
                )),
            'value' => ''),
        'postalCode' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z]{1,20})$'),
                'messages' => array(
                    'required' => "El código postal es obligatorio",
                    'regex' => "Solo se permiten letras y números"
                )),
            'value' => ''),
        'emailToSendInvoice' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$',
                    'maxlength' => 100),
                'messages' => array(
                    'required' => "El correo es requerido",
                    'maxlength' => "El valor m&aacute;ximo para el correo son 100 caracteres",
                    'regex' => "El correo no tiene un formato correcto, ejemplo: tu@gmail.com"
                )),
            'value' => ''),
        'emailContact' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$',
                    'maxlength' => 100),
                'messages' => array(
                    'required' => "El correo es requerido",
                    'maxlength' => "El valor m&aacute;ximo para el correo son 100 caracteres",
                    'regex' => "El correo no tiene un formato correcto, ejemplo: tu@gmail.com"
                )),
            'value' => ''),
        'userIds' => array(
            'validator' => array(
                'rules' => array(
                    'required' => false),
                'messages' => ["required" => "Al menos un usuario es requerido"]),
            'value' => '')
    );

}
