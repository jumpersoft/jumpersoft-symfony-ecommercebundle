<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of OrderRecordValidator
 *
 * @author Angel
 */
class OrderRecordValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $orderRecord = array(
        'customerId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El cliente es requerido"
                )),
            'value' => ''),
        'date' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => array(
                    'required' => "La fecha es requerida")),
            'value' => ''),
        'shipmentDate' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => array(
                    'required' => "La fecha es requerida")),
            'value' => ''),
        'paymentMethodId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El método de pago es requerido"
                )),
            'value' => ''),
        'paymentFormId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "La forma de pago es requerida"
                )),
            'value' => ''),
        'comment' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#.\-]{0,1000})$'),
                'messages' => array(
                    'required' => "",
                    'regex' => "Solo se permiten letras o números, max 1000, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
        'shippingAddressTypeId' => array('validator' => array('rules' => array('required' => true), 'messages' => array('required' => "El tipo de dirección es requerido")), 'value' => ''),
        'billingAddressTypeId' => array('validator' => array('rules' => array('required' => true), 'messages' => array('required' => "El tipo de dirección es requerido")), 'value' => ''),
        'shippingAddressId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "La dirección de envío es requerida"
                )),
            'value' => ''),
        'billingUserId' => array('validator' => array('rules' => array('required' => false), 'messages' => array('required' => "El cliente de facturación es requerido")), 'value' => ''),
    );
    public static $orderRecordItem = array(
        'itemId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El producto/servicio es requerido"
                )),
            'value' => ''),
        'bulkSale' => array('validator' => array(), 'value' => ''),
        'quantity' => array('validator' => array('rules' => array('required' => true), 'messages' => array('required' => "Cantidad requerida"),), 'value' => ''),
        'quantityFractionable1' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,1})?$'),
                'messages' => array('required' => "Cantidad requerida", 'regex' => "Solo se permiten números, max 16, y un decimal , ej: 653 o 3600.4")),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'quantityFractionable2' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array('required' => "Cantidad requerida", 'regex' => "Solo se permiten números, max 16, y dos decimales , ej: 653 o 3600.45")),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'quantityFractionable3' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,3})?$'),
                'messages' => array('required' => "Cantidad requerida", 'regex' => "Solo se permiten números, max 16, y tres decimales , ej: 653 o 3600.532")),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'quantityFractionable4' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,4})?$'),
                'messages' => array('required' => "Cantidad requerida", 'regex' => "Solo se permiten números, max 16, y cuatro decimales , ej: 653 o 3600.4532")),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'orderRecordId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El estatus es requerido"
                )),
            'value' => ''),
        'attributeValues' => array('validator' => array(), 'value' => ''),
        'volume' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,4})?$'),
                'messages' => array('required' => "Volumen requerido", 'regex' => "Solo se permiten números, max 16, y cuatro decimales , ej: 653 o 3600.4532")),
            'value' => ''),
        'calibrationDate' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => array(
                    'required' => "La fecha es requerida")),
            'value' => ''),
    );
    public static $orderRecordFilter = array(
        'startDate' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array('required' => "La fecha es requerida")),
            'value' => ''),
        'endDate' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array('required' => "La fecha es requerida")),
            'value' => ''),
        'statusId' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array('required' => "El estatus es requerido")),
            'value' => ''),
        'folio' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9]{1,10})$'),
                'messages' => array(
                    'required' => "",
                    'regex' => "Solo se permiten números, max 10"
                )),
            'value' => ''),
        'customer' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([A-Za-z sáéíóúñÁÉÍÓÚÑ&,.\'\`\-]{2,100})$'),
                'messages' => array(
                    'required' => "El nombre o razon social es requerida",
                    'regex' => "Solo se permiten letras, mínimo 2, y algunos caracteres especiales &,.'`-"
                )),
            'value' => ''),
        'email' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^(([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?))?$',
                    'maxlength' => 100),
                'messages' => array(
                    'required' => "El correo es requerido",
                    'maxlength' => "M&aacute;ximo 100 caracteres",
                    'regex' => "Formato incorrecto, ejemplo: tu@gmail.com"
                )),
            'value' => ''),
        'total' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array(
                    'required' => "El total es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
    );
    public static $shipmentPending = array(
        'notes' => array(
            'validator' => array(
                'rules' => array(
                    'required' => false,
                    'regex' => '^(.{0,1000})$'),
                'messages' => array(
                    'required' => "",
                    'regex' => "Solo se permiten letras o números, max 1000, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
        'packageList' => array(
            'validator' => array(
                'rules' => array('required' => false, 'min_value' => 1, 'max_value' => 10),
                'messages' => array(
                    'required' => "Requerido",
                    'min_value' => "Max 1",
                    'max_value' => "Max 50"
                )),
            'value' => ''),
    );
    public static $cancelOrder = [
        'statusId' => ['validator' => [
                "rules" => ['required' => true],
                "messages" => ["required" => "El tipo de cancelación es requerido"]
            ]],
        'reasonCancellation' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true,
                    'regex' => "^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#.,\-\n]{0,500})$"),
                'messages' => array(
                    'required' => "El motivo es requerido",
                    'regex' => "El motivo de cancelación debe contar con letras o números, mínimo 1, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
        'returnFulFill' => ['validator' => []],
    ];
    //Process step temporary
    public static $stepProcess = [
        'ORD_FUL_SUC' => ['from' => ['ORD_SHI_PEN']],
        'ORD_SHI_PEN' => ['from' => ['ORD_FUL_SUC', 'ORD_SHI_INP']],
        'ORD_SHI_INP' => ['from' => ['ORD_SHI_PEN']],
        'ORD_DLV' => ['from' => ['ORD_SHI_INP']],
        'ORD_PAI' => ['from' => ['ORD_DLV']],
        'ORD_INV' => ['from' => ['ORD_PAI']],
        'ORD_CAN_NOR' => ['from' => ['ORD_PEN', 'ORD_FUL_SUC', 'ORD_FUL_PEN', 'ORD_SHI_PEN', 'ORD_SHI_INP']],
        'ORD_CAN_RET' => ['from' => ['ORD_PEN', 'ORD_FUL_SUC', 'ORD_FUL_PEN', 'ORD_SHI_PEN', 'ORD_SHI_INP']],
    ];

}
