<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of PromotionValidator
 *
 * @author Angel
 */
class PromotionValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $promotion = array(
        // General data
        'code' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z-]{1,23})$'),
                'messages' => array(
                    'required' => "El código es requerido",
                    'regex' => "El código debe contar solo con letras, números y guiones, mínimo 1 max 23"
                )),
            'value' => ''),
        'typeId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El tipo de promoción es requerida"
                )),
            'value' => ''),
        'startDate' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => array(
                    'required' => "La fecha es requerida")),
            'value' => ''),
        'endDate' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => array(
                    'required' => "La fecha es requerida")),
            'value' => ''),
        'name' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ+*$%#,%.()\/\'\`\-]{1,2000})$'),
                'messages' => array(
                    'required' => "El nombre es requerido",
                    'regex' => "El nombre debe contar solo con letras o números, mínimo 1 max 2000, y algunos caracteres especiales #,.'`-"
                )),
            'value' => ''),
        'terms' => array(
            'validator' => array(
                'rules' => array('required' => false, 'maxlength' => '1000'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'maxlength' => "La descripción debe contar solo con letras o números, mínimo 1 max 1000"
                )),
            'value' => ''),
        'description' => array(
            'validator' => array(
                'rules' => array('required' => false, 'maxlength' => '1000'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'maxlength' => "La descripción debe contar solo con letras o números, mínimo 1 max 1000"
                )),
            'value' => ''),
        'discount' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array(
                    'required' => "El descuento es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'discountType' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array('required' => "El tipo es requerido")),
            'value' => ''),
        'minimumPurchase' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9]{1,16}(\.[0-9]{0,2})?)?$'),
                'messages' => array(
                    'required' => "El mínimo es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'limitTotalNumberUses' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9]{1,20}?)?$'),
                'messages' => array(
                    'required' => "El límite es requerido",
                    'regex' => "Solo se permiten números, max(20) ej: 1 o 3600"
                )),
            'value' => ''),
        'limitNumberUsesPerCustomer' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9]{1,20}?)?$'),
                'messages' => array(
                    'required' => "El límite es requerido",
                    'regex' => "Solo se permiten números, max(20) ej: 1 o 3600"
                )),
            'value' => ''),
        'worksWithOtherPromotions' => array('validator' => array(), 'value' => '')
    );

}
