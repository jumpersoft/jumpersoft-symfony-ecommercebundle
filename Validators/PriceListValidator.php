<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of PriceListValidator
 *
 * @author Angel
 */
class PriceListValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $priceList = array(
        'name' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ+*$%#,%.()\/\'\`\-]{1,2000})$'),
                'messages' => array(
                    'required' => "El nombre es requerido",
                    'regex' => "El nombre debe contar solo con letras o números, mínimo 1 max 2000, y algunos caracteres especiales #,.'`-"
                )),
            'value' => ''),
        'description' => array(
            'validator' => array(
                'rules' => array('required' => false, 'maxlength' => '1000'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'maxlength' => "La descripción debe contar solo con letras o números, mínimo 1 max 1000"
                )),
            'value' => ''),
        'active' => array('validator' => array(), 'value' => ''),
    );
    public static $priceListItem = [
        'currencyId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "La moneda es requerida"
                )),
            'value' => ''),
        'regularPrice' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array('required' => "El precio es requerido", 'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'discount' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[0-9]{0,16}(\.[0-9]{0,2})?$'),
                'messages' => array(
                    'required' => "El descuento es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'discountType' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array('required' => "El tipo es requerido")),
            'value' => ''),
        'salePrice' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array('required' => "El precio es requerido", 'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => '')
    ];

}
