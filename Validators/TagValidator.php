<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of TagValidator
 *
 * @author Angel
 */
class TagValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $tag = array(
        'name' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true,
                    'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#,.\'\`\-]{1,255})$',
                    'remote' => array(
                        'url' => "/api/panel/catalog/tag/check",
                        'type' => "post"
                    )),
                'messages' => array(
                    'required' => "EL nombre es requerido",
                    'nameVal' => "La etiqueta debe contar con letras o números, mínimo 1, y algunos caracteres especiales #,.'`-",
                    'remote' => "El nombre de la etiqueta ya existe, elija otro por favor"
                )),
            'value' => ''),
        'description' => array(
            'validator' => array(
                'rules' => array(
                    'required' => false,
                    'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#.,\-\n]{0,500})$'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'descriptionVal' => "La descripción debe contar con letras o números, mínimo 1, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
        'urlKey' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true,
                    'regex' => '^([0-9A-Za-z_\-]{1,255})$',
                    'remote' => array(
                        'url' => "/api/panel/store/checkUrlKey",
                        'type' => "post"
                    )),
                'messages' => array(
                    'required' => "El url key es requerido",
                    'urlKeyVal' => "El url debe contar con letras o números y guiones bajos o medios, mínimo 1 max 255",
                    'remote' => "El url ya está en uso, elija otro por favor."
                )),
            'value' => '')
    );

}
