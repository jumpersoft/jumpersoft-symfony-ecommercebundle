<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of InventoryValidator
 *
 * @author Angel
 */
class InventoryValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $inventoryIn = array(
        'itemId' => array(
            'validator' => array('rules' => array('required' => true), 'messages' => array('required' => "El producto es requerido")),
            'value' => ''),
        'eventDate' => array(
            'validator' => array('rules' => array('required' => true), 'messages' => array('required' => "La fecha es requerida")),
            'value' => ''),
        'expirationDate' => array(
            'validator' => array('rules' => array('required' => true), 'messages' => array('required' => "La fecha de expiración es requerida")),
            'value' => ''),
        'lot' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-zsáéíóúñÁÉÍÓÚÑ+*$%#.()\/\-]{1,255})$',
                    'remote' => array(
                        'url' => "/api/panel/inventory/checklot",
                        'type' => "post"
                    )),
                'messages' => array(
                    'required' => "El lote es requerido",
                    'regex' => "Solo se permiten letras o números, mínimo 1 max 500, y algunos caracteres especiales, sin espacios",
                    'remote' => "El lote ya está en uso, elija otro por favor."
                )),
            'value' => ''),
        'serial' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-zsáéíóúñÁÉÍÓÚÑ+*$%#.()\/\-]{1,255})$',
                    'remote' => array(
                        'url' => "/api/panel/inventory/checkserial",
                        'type' => "post"
                    )),
                'messages' => array(
                    'required' => "La serie es requerido",
                    'regex' => "Solo se permiten letras o números, mínimo 1 max 500, y algunos caracteres especiales, sin espacios",
                    'remote' => "La serie ya está en uso, elija otra por favor."
                )),
            'value' => ''),
        'notes' => array(
            'validator' => array(
                'rules' => array('required' => false, 'maxlength' => '1000'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'maxlength' => "La descripción debe contar solo con letras o números, mínimo 1 max 1000"
                )),
            'value' => ''),
        'quantityReceived' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,20}?$',
                    'min_value' => 1),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números enteros, max 20", "min_value" => "Mínimo 1")),
            'value' => ''),
        'quantityReceivedFractionable1' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,1})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números, max 16, y un decimal , ej: 653 o 3600.1")),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'quantityReceivedFractionable2' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números, max 16, y dos decimales , ej: 653 o 3600.12")),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'quantityReceivedFractionable3' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,3})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números, max 16, y tres decimales , ej: 653 o 3600.123")),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'quantityReceivedFractionable4' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,4})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números, max 16, y cuatro decimales , ej: 653 o 3600.1234")),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'calibrationDate' => array(
            'validator' => array('rules' => array('required' => true), 'messages' => array('required' => "La fecha de calibración es requerida")),
            'value' => ''),
        'volumeReceived' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,20}?$',
                    'min_value' => 1),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números enteros, max 20", "min_value" => "Mínimo 1")),
            'value' => ''),
        'useNoEco' => array('validator' => array(), 'value' => ''),
        'noEco' => array(
            'validator' => array(
                'rules' => array('required' => true, 'min_value' => 1, 'max_value' => 99),
                'messages' => array('required' => "El mínimo es requerido", 'min_value' => "El mínimo es 1", 'max_value' => "El máximo es 99",
                )),
            'value' => ''),
        'startNoEco' => array(
            'validator' => array(
                'rules' => array('required' => true, 'min_value' => 1, 'max_value' => 99),
                'messages' => array('required' => "El mínimo es requerido", 'min_value' => "El mínimo es 1", 'max_value' => "El máximo es 99",
                )),
            'value' => '',
            'notValidateInBackEnd' => true),
        'endNoEco' => array(
            'validator' => array(
                'rules' => array('required' => true, 'min_value' => 1, 'max_value' => 99),
                'messages' => array('required' => "El mínimo es requerido", 'min_value' => "El mínimo es 1", 'max_value' => "El máximo es 99",
                )),
            'value' => '',
            'notValidateInBackEnd' => true),
    );
    public static $inventoryOut = array(
        'itemId' => array(
            'validator' => array('rules' => array('required' => true), 'messages' => array('required' => "El producto es requerido")),
            'value' => ''),
        'operationId' => array(
            'validator' => array('rules' => array('required' => true), 'messages' => array('required' => "La operación es requerida")),
            'value' => ''),
        'typeId' => array(
            'validator' => array('rules' => array('required' => true), 'messages' => array('required' => "El tipo es requerido")),
            'value' => ''),
        'eventDate' => array(
            'validator' => array('rules' => array('required' => true), 'messages' => array('required' => "La fecha es requerida")),
            'value' => ''),
        'notes' => array(
            'validator' => array(
                'rules' => array('required' => false, 'maxlength' => '1000'),
                'messages' => array(
                    'required' => "La descripción es requerida",
                    'maxlength' => "La descripción debe contar solo con letras o números, mínimo 1 max 1000"
                )),
            'value' => ''),
        'quantityOutput' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,20}?$',
                    'min_value' => 1),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números enteros, max 20", "min_value" => "Mínimo 1")),
            'value' => ''),
        //Fractionables
        'quantityOutput1' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,1})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números, max 16, y un decimal , ej: 653 o 3600.4")),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'quantityOutput2' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,2})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números, max 16, y dos decimales , ej: 653 o 3600.45")),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'quantityOutput3' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,3})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números, max 16, y tres decimales , ej: 653 o 3600.453")),
            "notValidateInBackEnd" => true,
            'value' => ''),
        'quantityOutput4' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,16}(\.[0-9]{0,4})?$'),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números, max 16, y cuatro decimales , ej: 653 o 3600.4532")),
            "notValidateInBackEnd" => true,
            'value' => ''),
    );
    public static $inventoryDispersion = array(
        'itemId' => array(
            'validator' => array('rules' => array('required' => true), 'messages' => array('required' => "El producto es requerido")),
            'value' => ''),
        'itemRelatedId' => array(
            'validator' => array('rules' => array('required' => true), 'messages' => array('required' => "El producto es requerido")),
            'value' => ''),
        'eventDate' => array(
            'validator' => array('rules' => array('required' => true), 'messages' => array('required' => "La fecha es requerida")),
            'value' => ''),
        'quantityToDisperse' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[0-9]{1,20}?$', 'min_value' => 1),
                'messages' => array('required' => "La cantidad es requerida", 'regex' => "Solo se permiten números enteros, max 20", "min_value" => "Mínimo 1")),
            'value' => '')
    );

}
