<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of InvoiceValidator
 *
 * @author Angel
 */
class InvoiceValidator extends JumpersoftValidatorExtension
{

    //Objeto enviado para validaciones en javascript y validaciones en php. Agregar nombredelcampo, expresión a validar, y los flags
    public $invoice = array(
        'customerId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El cliente es requerido"
                )),
            'value' => ''),
        'date' => array(
            'validator' => array(
                'rules' => array(
                    'required' => true),
                'messages' => array(
                    'required' => "La fecha es requerida")),
            'value' => ''),
        'paymentMethodId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El método de pago es requerido"
                )),
            'value' => ''),
        'paymentMethodId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "La forma de pago es requerida"
                )),
            'value' => ''),
        'statusId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El estatus es requerido"
                )),
            'value' => ''),
        'comment' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#.\-]{1,1000})$'),
                'messages' => array(
                    'required' => "",
                    'regex' => "Solo se permiten letras o números, mínimo 1 max 1000, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
        'subTotal' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[-]?[\d]{1,16}(\.\d{0,2})?$'),
                'messages' => array(
                    'required' => "El sub total es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'total' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[-]?[\d]{1,16}(\.\d{0,2})?$'),
                'messages' => array(
                    'required' => "El total es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
    );
    //Objeto enviado para validaciones en javascript y validaciones en php. Agregar nombredelcampo, expresión a validar, y los flags
    public $item = array(
        'itemId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El producto/servicio es requerido"
                )),
            'value' => ''),
        'quantity' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[-]?[\d]{1,16}(\.\d{0,2})?$'),
                'messages' => array(
                    'required' => "La cantidad es requerida",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'regularPrice' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[-]?[\d]{1,16}(\.\d{0,2})?$'),
                'messages' => array(
                    'required' => "El precio unitario es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'subTotal' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[-]?[\d]{1,16}(\.\d{0,2})?$'),
                'messages' => array(
                    'required' => "El precio unitario es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'total' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^[-]?[\d]{1,16}(\.\d{0,2})?$'),
                'messages' => array(
                    'required' => "El precio unitario es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'statusId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El estatus es requerido"
                )),
            'value' => ''),
        'invoiceId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El estatus es requerido"
                )),
            'value' => ''),
        'discount' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^[-]?[\d]{1,16}(\.\d{0,2})?$'),
                'messages' => array(
                    'required' => "El discount es requerido",
                    'regex' => "Solo se permiten números, max(16), y dos decimales , ej: 653 o 3600.45"
                )),
            'value' => ''),
        'discountType' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array(
                    'required' => "El tipo es requerido"
                )),
            'value' => ''),
        //Seccion de campos para productos tipo hosting y domain
        'mainDomain' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-zs@.\-]{1,100})$'),
                'messages' => array(
                    'required' => "El dominio es requerido",
                    'regex' => "Solo se permiten letras y números sin espacios, mínimo 1 max 100, y algunos caracteres especiales @.-"
                )),
            'value' => ''),
        'numDomains' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9]{1,2})$'),
                'messages' => array(
                    'required' => "El número de dominios son requeridos",
                    'regex' => "Solo se permiten números, max 2"
                )),
            'value' => ''),
        'dataTransfer' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Z a-z#.\-]{1,20})$'),
                'messages' => array(
                    'required' => "Valor requerido",
                    'regex' => "Solo se permiten letras o números, mínimo 1 max 20, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
        'diskSpace' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Z a-z#.\-]{1,20})$'),
                'messages' => array(
                    'required' => "Valor requerido",
                    'regex' => "Solo se permiten letras o números, mínimo 1 max 20, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
        'contractDate' => array(
            'validator' => array(
                'rules' => array(
                    'required' => false),
                'messages' => array(
                    'required' => "La fecha es requerida")),
            'value' => ''),
        'expirationDate' => array(
            'validator' => array(
                'rules' => array(
                    'required' => false),
                'messages' => array(
                    'required' => "La fecha es requerida")),
            'value' => '')
    );

}
