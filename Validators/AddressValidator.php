<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of AddressValidator
 *
 * @author Angel
 */
class AddressValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $address = array(
        'reference' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#&,.\'\`\-]{1,100})$'),
                'messages' => array(
                    'required' => "La referencia es obligatoria",
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &#,.'`-"
                )),
            'value' => ''),
        'receiver' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#&,.\'\`\-]{1,100})$'),
                'messages' => array(
                    'required' => "El nombre es obligatorio",
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &#,.'`-"
                )),
            'value' => ''),
        'phone' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z .,:\-\(\)]{0,50})$'),
                'messages' => array(
                    'regex' => "Solo se permiten letras y números"
                )),
            'value' => ''),
        'country' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([A-Za-z sáéíóúñÁÉÍÓÚÑ&,.\'\`\-]{2,100})$'),
                'messages' => array(
                    'required' => "El pais es requerido",
                    'regex' => "El pais debe contar solo con letras, mínimo 2, y algunos caracteres especiales &,.'`-"
                )),
            'defaultValue' => "México",
            'value' => ''),
        'state' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([A-Za-z sáéíóúñÁÉÍÓÚÑ&,.\'\`\-]{0,100})$'),
                'messages' => array(
                    'required' => "El estado es requerido",
                    'regex' => "El estado debe contar solo con letras, mínimo 2, y algunos caracteres especiales &,.'`-"
                )),
            'value' => ''),
        'town' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([A-Za-z sáéíóúñÁÉÍÓÚÑ&,.\'\`\-]{0,100})$'),
                'messages' => array(
                    'required' => "El estado es requerido",
                    'regex' => "El estado debe contar solo con letras, mínimo 2, y algunos caracteres especiales &,.'`-"
                )),
            'value' => ''),
        'city' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ&,.\'\`\-]{0,100})$'),
                'messages' => array(
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &,.'`-"
                )),
            'value' => ''),
        'streetName' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#&,.\'\`\-]{1,100})$'),
                'messages' => array(
                    'required' => "La calle es obligatoria",
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &#,.'`-"
                )),
            'value' => ''),
        'neighborhood' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#&,.\'\`\-]{1,100})$'),
                'messages' => array(
                    'required' => "La colonia es obligatoria",
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &#,.'`-"
                )),
            'value' => ''),
        'numExt' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#&,.\'\`\-]{1,100})$'),
                'messages' => array(
                    'required' => "El número exterior es obligatorio",
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &#,.'`-"
                )),
            'value' => ''),
        'numInt' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#&,.\'\`\-]{0,100})$'),
                'messages' => array(
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &#,.'`-"
                )),
            'value' => ''),
        'postalCode' => array(
            'validator' => array(
                'rules' => array('required' => true, 'regex' => '^([0-9A-Za-z]{1,20})$'),
                'messages' => array(
                    'required' => "El código postal es obligatorio",
                    'regex' => "Solo se permiten letras y números"
                )),
            'value' => ''),
        'propertyType' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array(
                    'required' => "El tipo de inmueble es requerido"
                )),
            'value' => ''),
        'otherReferences' => array(
            'validator' => array(
                'rules' => array('required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#&,.\'\`\-]{0,255})$'),
                'messages' => array(
                    'required' => "Laas referencias son obligatorias",
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &#,.'`-"
                )),
            'value' => ''),
        'defaultShippingAddress' => array('validator' => array(), 'value' => ''),
        'defaultBillingAddress' => array('validator' => array(), 'value' => ''),
        'coordinates' => [
            'validator' => [
                'rules' => ['required' => false, 'regex' => '^([0-9A-Za-z sáéíóúñÁÉÍÓÚÑ#&,.\'\`\-]{0,255})$'],
                'messages' => [
                    'required' => "Las coordenadas son requeridas",
                    'regex' => "Solo se permiten letras y algunos caracteres especiales &#,.'`-"
                ]],
            'value' => ''],
        'addressType' => array('validator' => array(), 'value' => ''),
    );

}
