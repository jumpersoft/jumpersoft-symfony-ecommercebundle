<?php
namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of OrderRecordValidator
 *
 * @author Angel
 */
class TransactionValidator extends JumpersoftValidatorExtension
{

    public $transaction = array(
        'startDate' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array('required' => "La fecha es requerida")),
            'value' => ''),
        'endDate' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array('required' => "La fecha es requerida")),
            'value' => ''),
        'folio' => array(
            'validator' => array(
                'rules' => array(
                    'required' => false,
                    'regex' => '^([0-9]{1,10})$'),
                'messages' => array(
                    'required' => "",
                    'regex' => "Solo se permiten números, max 10"
                )),
            'value' => ''),
        'paymentMethodId' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array('required' => "El método es requerido")),
            'value' => ''),
        'operationTypeId' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array('required' => "La operación es requerida")),
            'value' => ''),
        'transactionTypeId' => array(
            'validator' => array(
                'rules' => array('required' => false),
                'messages' => array('required' => "La transacción es requerida")),
            'value' => ''),
    );

}
