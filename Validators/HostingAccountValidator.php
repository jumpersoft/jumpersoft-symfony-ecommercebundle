<?php

namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of HostingAccountValidator
 *
 * @author Angel
 */
class HostingAccountValidator extends JumpersoftValidatorExtension
{

    //Objeto enviado para validaciones en javascript y validaciones en php. Agregar nombredelcampo, expresión a validar, y los flags
    public $accountValidators = array(
        'userId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El usuario es requerido"
                )),
            'value' => ''),
        'serverId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El servidor es requerido"
                )),
            'value' => ''),
        'orderRecordId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El pedido es requerido"
                )),
            'value' => ''),
        'orderRecordItemId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El producto o servicio es requerido"
                )),
            'value' => ''),
        'userAdminId' => array(
            'validator' => array(
                'rules' => array('required' => true),
                'messages' => array(
                    'required' => "El administrador es requerido"
                )),
            'value' => ''),
        'comment' => array(
            'validator' => array(
                'expval' => array('exp' => '^([0-9A-Z a-zsáéíóúñÁÉÍÓÚÑ#.\-]{1,500})$', 'f' => 'i'),
                'rules' => array(
                    'required' => false,
                    'commentVal' => true),
                'messages' => array(
                    'required' => "La observaciones es requerida",
                    'commentVal' => "La observaciones debe contar con letras o números, mínimo 1 max 500, y algunos caracteres especiales #.-"
                )),
            'value' => ''),
    );
}
