<?php

namespace Jumpersoft\EcommerceBundle\Validators;

use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftValidatorExtension;

/**
 * Description of Document
 *
 * @author Angel
 */
class DocumentValidator extends JumpersoftValidatorExtension
{

    /**
     * Esta función debe usarse en cada clase estática de este tipo de otra forma no podra ver de forma dinámica sus propiedades estáticas.
     */
    public static function getValidators()
    {
        $validators = func_get_args();
        foreach ($validators as $v) {
            $res[$v] = self::getValidatorArray(self::${$v});
        }
        return $res ?? [];
    }

    public static $document = [
        'title' => [
            'validator' => ['rules' => ['required' => false, 'regex' => '^[\w\W]{1,255}$'], 'messages' => ['required' => "El dato es requerido", 'regex' => "El dato debe contar con letras o números, max 255"]],
            'value' => ''],
        'alt' => [
            'validator' => ['rules' => ['required' => false, 'regex' => '^[\w\W]{1,255}$'], 'messages' => ['required' => "El dato es requerido", 'regex' => "El dato debe contar con letras o números, max 255"]],
            'value' => ''],
        'typeId' => [
            'validator' => [
                'rules' => ['required' => false],
                'messages' => ['required' => "El dato es requerido"]],
            'value' => '']
    ];
}
