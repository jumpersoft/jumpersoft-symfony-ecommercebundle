Installation
============

Step 1: Download the Bundle
---------------------------

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer require jumpersoft/symfony-ecommerce-bundle
```

This command requires you to have Composer installed globally, as explained
in the [installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

Step 2: Enable the Bundle
-------------------------

Then, enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...            
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),            
            new Jumpersoft\BaseBundle\JumpersoftBaseBundle(),
            new Jumpersoft\EcommerceBundle\JumpersoftEcommerceBundle(),                        
            new Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle(),
            new Nelmio\CorsBundle\NelmioCorsBundle()            
        );

        // ...
    }

    // ...
}
```

Step 3: Configure Bundle
-------------------------

1. In "app/config/parameters.yml" add next basic configuration, you can view more in parameters.yml inside bundle:

    jumpersoft.recaptcha.secretCaptchaKey: ????
    jumpersoft.security.token_key: ?????
    # Not forget make your keys in order to work with JSON web tokens authentication
    jumpersoft.security.jwt_private_key_path: '%kernel.root_dir%/config/var/jwt/private.pem'
    jumpersoft.security.jwt_public_key_path: '%kernel.root_dir%/config/var/jwt/public.pem'
    jumpersoft.security.jwt_key_pass_phrase: ?????
    jumpersoft.security.jwt_token_ttl: 86400
    # Default payment proccesor - OpenPay
    openpay.merchant.id: '?????'
    openpay.merchant.privatekey: '????'
    openpay.merchant.publickey: '????'

2. In "app/config/config.yml" add the follow configurations:

imports:
    ...    
    - { resource: "@JumpersoftEcommerceBundle/Resources/config/services.yml" }
    - { resource: "@JumpersoftEcommerceBundle/Resources/config/parameters.yml" }
framework:
    session:    
        handler_id:  ~
        cookie_secure: true
        cookie_httponly: true
    templating:
        engines: ['twig']
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"
    paths:
        '%kernel.project_dir%/vendor/jumpersoft/symfony-base-bundle/Resources/views': baseViews        
        '%kernel.project_dir%/vendor/jumpersoft/symfony-ecommerce-bundle/Resources/views/site': siteViews
        '%kernel.project_dir%/vendor/jumpersoft/symfony-ecommerce-bundle/Resources/views/document': documentViews
assetic:
    debug:          "%kernel.debug%"
    use_controller: false
    bundles: [ JumpersoftBaseBundle, JumpersoftEcommerceBundle ]    
    filters:        
        cssrewrite: ~
doctrine:
    dbal:
        ...             
        mapping_types:
             bit: boolean
    orm:
        ...
        mappings:
            MainEntities:
                type: annotation                
                dir: '%kernel.project_dir%/vendor/jumpersoft/symfony-ecommerce-bundle/Entity'
                prefix: Jumpersoft\EcommerceBundle\Entity
                alias: E
        dql:
            string_functions:
                FORMAT: Jumpersoft\BaseBundle\DoctrineExtensions\Format
                GROUP_CONCAT: Jumpersoft\BaseBundle\DoctrineExtensions\GroupConcat
                CONCAT_WS: DoctrineExtensions\Query\Mysql\ConcatWs
                REPLACE: DoctrineExtensions\Query\Mysql\Replace
            datetime_functions:
                DATE_FORMAT: Jumpersoft\BaseBundle\DoctrineExtensions\DateFormat
            numeric_functions:
                ROUND: DoctrineExtensions\Query\Mysql\Round
swiftmailer:
    ...
    port:      "%mailer_port%"
    encryption: "%mailer_encryption%"
nelmio_cors:
    defaults:
        allow_credentials: false
        allow_origin: ['*']
        allow_headers: ['*']
        allow_methods: ['POST', 'PUT', 'GET', 'DELETE']
        max_age: 3600
        hosts: []
        origin_regex: false
    paths:
        '^/panel/':
            allow_origin: ['*']
            allow_headers: ['*']
            allow_methods: ['POST', 'PUT', 'GET', 'DELETE']
            max_age: 3600             
lexik_jwt_authentication:
    private_key_path: "%jumpersoft.security.jwt_private_key_path%"
    public_key_path:  "%jumpersoft.security.jwt_public_key_path%"
    pass_phrase:      "%jumpersoft.security.jwt_key_pass_phrase%"
    token_ttl:        "%jumpersoft.security.jwt_token_ttl%"

3. In "app/config/security.yml" add next configuration, adapt it according to your needs:

security:

    encoders:
        # Used by in_momeory provider wich is used for api_openpay_webhooks
        Symfony\Component\Security\Core\User\User: plaintext
        # Our user class and the algorithm we'll use to encode passwords
        # http://symfony.com/doc/current/book/security.html#encoding-the-user-s-password
        Jumpersoft\EcommerceBundle\Entity\User:
            algorithm: bcrypt
            # The cost can be in the range of 4-31 and determines how long a password will be encoded, the default cost of 13 is used.
            cost:      15                

    # http://symfony.com/doc/current/book/security.html#where-do-users-come-from-user-providers
    providers:
        # To load users from somewhere else: http://symfony.com/doc/current/cookbook/security/custom_provider.html
        user_db:
            entity: 
                class: JumpersoftEcommerceBundle:User
                property: username
        in_memory:
            memory:
                users:
                    openpay_EAAEOuJ4t:
                        password: 44e6b1701c9c6dae62fc6a0c29541297
                        roles: 'ROLE_WEBHOOK'

    firewalls:
        # disables authentication for assets and the profiler, adapt it according to your needs
        # el orden es importante
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js|login)/
            security: false
        api_openpay_webhooks:
            pattern:   ^/api/openpay/webhooks_a2b6457aeb5$
            stateless: true
            http_basic:
                provider: in_memory                        
        main:
            pattern: ^/
            # but the firewall does not require login on every page
            # denying access is done in access_control or in your controllers
            anonymous: ~
            guard:
                authenticators:
                    - Jumpersoft\BaseBundle\Security\JwtAuthenticator
                provider: user_db
            stateless: true
            lexik_jwt:                
                # query_parameter:      # check token in query string parameter
                #   enabled: true
                #   name:    bearer                
                # check token in a cookie
                cookie:               
                    enabled: true
                    name: bearer
                authentication_provider: lexik_jwt_authentication.security.authentication.provider
    access_control:
        - { path: ^/api/openpay/webhooks_a2b6457aeb5, roles: ROLE_WEBHOOK, requires_channel: https }                
        - { path: ^/store/account, roles: ROLE_CUSTOMER, requires_channel: https }
        - { path: ^/store/checkout, roles: ROLE_CUSTOMER, requires_channel: https }
        - { path: ^/api/login_check, roles: IS_AUTHENTICATED_ANONYMOUSLY, requires_channel: https }        
        - { path: ^/api/panel, roles: ROLE_SELLER, requires_channel: https }
        - { path: ^/panel, roles: ROLE_SELLER, requires_channel: https }
        - { path: ^/, roles: IS_AUTHENTICATED_ANONYMOUSLY, requires_channel: https }        
        
    role_hierarchy:        
        ROLE_ANALYST: ROLE_CUSTOMER
        ROLE_SELLER: [ROLE_ANALYST, ROLE_SUPPLIER]
        ROLE_ADMIN: ROLE_SELLER
        ROLE_SUPER_ADMIN: [ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]

4. In "app/config/routing.yml" add next configuration in order to work with the Panel, Site and Store modules:

//...

jumpersoft_ecommerce:
    resource: "@JumpersoftEcommerceBundle/Resources/config/routing.yml"
    prefix: /

Step 4: Use the Bundle
-------------------------

This are the topics related to this bundle:

1. Main functions:

- Site: ...coming soon
- Panel: ...coming soon
- Store: ...coming soon

2. Classes:

...coming soon

Step 5: Notes:
-------------------------

...coming soon