<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="OrderRecordItemInventory", uniqueConstraints={@ORM\UniqueConstraint(columns={"orderRecordItemId","inventoryId"})})
 * @UniqueEntity(
 *     fields={"orderRecordItem", "inventory"},
 *     message="This inventory is already in use on this order record item."
 * )
 * @author Angel
 */
class OrderRecordItemInventory extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="OrderRecordItem", inversedBy="inventory")
     * @ORM\JoinColumn(name="orderRecordItemId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $orderRecordItem;

    /**
     * @ORM\ManyToOne(targetEntity="Inventory")
     * @ORM\JoinColumn(name="inventoryId", referencedColumnName="id", nullable=FALSE)
     */
    protected $inventory;

    /**
     * @ORM\Column(type="decimal", name="quantity", precision=20, scale=4, nullable=FALSE)
     */
    protected $quantity;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=false)
     */
    protected $registerDate;

    public function __construct($id, $orderRecordItem, $inventory, $quantity, $registerDate)
    {
        $this->id = $id;
        $this->orderRecordItem = $orderRecordItem;
        $this->inventory = $inventory;
        $this->registerDate = $registerDate;
        $this->quantity = $quantity;
    }



    /**
     * Set id.
     *
     * @param string $id
     *
     * @return OrderRecordItemInventory
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity.
     *
     * @param string $quantity
     *
     * @return OrderRecordItemInventory
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return OrderRecordItemInventory
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set orderRecordItem.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\OrderRecordItem $orderRecordItem
     *
     * @return OrderRecordItemInventory
     */
    public function setOrderRecordItem(\Jumpersoft\EcommerceBundle\Entity\OrderRecordItem $orderRecordItem)
    {
        $this->orderRecordItem = $orderRecordItem;

        return $this;
    }

    /**
     * Get orderRecordItem.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\OrderRecordItem
     */
    public function getOrderRecordItem()
    {
        return $this->orderRecordItem;
    }

    /**
     * Set inventory.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Inventory $inventory
     *
     * @return OrderRecordItemInventory
     */
    public function setInventory(\Jumpersoft\EcommerceBundle\Entity\Inventory $inventory)
    {
        $this->inventory = $inventory;

        return $this;
    }

    /**
     * Get inventory.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Inventory
     */
    public function getInventory()
    {
        return $this->inventory;
    }
}
