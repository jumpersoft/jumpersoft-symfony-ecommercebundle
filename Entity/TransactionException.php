<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\TransactionRepository")
 * @ORM\Table(name="TransactionException")
 *
 * @author Angel
 */
class TransactionException
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=50)
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="event", length=255, nullable=TRUE)
     */
    protected $event;

    /**
     * @ORM\Column(type="datetime", name="eventDate")
     */
    private $eventDate;

    /**
     * @ORM\Column(type="string", name="description", length=1000, nullable=TRUE)
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="operationTypeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $operationType;

    /**
     * @ORM\ManyToOne(targetEntity="PaymentMethod")
     * @ORM\JoinColumn(name="paymentMethodId", referencedColumnName="id", nullable=FALSE)
     */
    protected $paymentMethod;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="transactionTypeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $transactionType;

    /**
     * @ORM\Column(type="json", name="gatewayResult")
     */
    protected $gatewayResult = array();

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", name="ipSource", length=16, nullable=TRUE)
     */
    private $ipSource;

    /**
     * @ORM\Column(type="string", name="ipSource2", length=16, nullable=TRUE)
     */
    private $ipSource2;

    /*     * *********** START CUSTOM FUNCTIONS ***************** */

    public function __construct($id, $eventDate, $ip, $user)
    {
        $this->id = $id;
        $this->eventDate = $eventDate;
        $this->ip = $ip;
        $this->user = $user;
    }

    /*     * *********** END CUSTOM FUNCTIONS ***************** */

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEvent(): ?string
    {
        return $this->event;
    }

    public function setEvent(?string $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getEventDate(): ?\DateTimeInterface
    {
        return $this->eventDate;
    }

    public function setEventDate(\DateTimeInterface $eventDate): self
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getGatewayResult(): ?array
    {
        return $this->gatewayResult;
    }

    public function setGatewayResult(array $gatewayResult): self
    {
        $this->gatewayResult = $gatewayResult;

        return $this;
    }

    public function getIpSource(): ?string
    {
        return $this->ipSource;
    }

    public function setIpSource(?string $ipSource): self
    {
        $this->ipSource = $ipSource;

        return $this;
    }

    public function getIpSource2(): ?string
    {
        return $this->ipSource2;
    }

    public function setIpSource2(?string $ipSource2): self
    {
        $this->ipSource2 = $ipSource2;

        return $this;
    }

    public function getOperationType(): ?Type
    {
        return $this->operationType;
    }

    public function setOperationType(?Type $operationType): self
    {
        $this->operationType = $operationType;

        return $this;
    }

    public function getPaymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?PaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getTransactionType(): ?Type
    {
        return $this->transactionType;
    }

    public function setTransactionType(?Type $transactionType): self
    {
        $this->transactionType = $transactionType;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
