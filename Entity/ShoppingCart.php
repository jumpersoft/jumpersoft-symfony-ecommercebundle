<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="ShoppingCart")
 *
 * @author Angel
 */
class ShoppingCart extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumn(name="storeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $store;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", nullable=TRUE)
     */
    protected $user;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;
    
    /**
     * @ORM\Column(type="datetime", name="updateDate", nullable=TRUE)
     */
    protected $updateDate;
    
    /**
     *
     * @ORM\Column(type="datetime", name="expirationDate", nullable=FALSE)
     */
    protected $expirationDate;

    /**
     * @ORM\Column(type="decimal", name="quantity", precision=16, scale=4, nullable=FALSE)
     */
    protected $quantity;
    
    /**
     * @ORM\Column(type="decimal", name="discount", precision=18, scale=2, nullable=TRUE)
     */
    protected $discount;
    
    /**
     * @ORM\Column(type="decimal", name="subTotal", precision=18, scale=2, nullable=FALSE)
     */
    protected $subTotal;

    /**
     * @ORM\Column(type="decimal", name="total", precision=18, scale=2, nullable=FALSE)
     */
    protected $total;

    /**
     * @ORM\OneToMany(targetEntity="ShoppingCartItem", mappedBy="shoppingCart")
     */
    protected $items;

    /**
     * @ORM\OneToMany(targetEntity="ShoppingCartPromotion", mappedBy="shoppingCart")
     */
    protected $promotions;
    
    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id", nullable=FALSE)
     */
    protected $status;
    
    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->promotions = new ArrayCollection();
    }

    /**
     * Set id
     *
     * @param string $id
     *
     * @return ShoppingCart
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return ShoppingCart
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     *
     * @return ShoppingCart
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set expirationDate
     *
     * @param \DateTime $expirationDate
     *
     * @return ShoppingCart
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    /**
     * Get expirationDate
     *
     * @return \DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     *
     * @return ShoppingCart
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set subTotal
     *
     * @param string $subTotal
     *
     * @return ShoppingCart
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;

        return $this;
    }

    /**
     * Get subTotal
     *
     * @return string
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return ShoppingCart
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set store
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Store $store
     *
     * @return ShoppingCart
     */
    public function setStore(\Jumpersoft\EcommerceBundle\Entity\Store $store)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Add shoppingCartItem
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ShoppingCartItem $shoppingCartItem
     *
     * @return ShoppingCart
     */
    public function addShoppingCartItem(\Jumpersoft\EcommerceBundle\Entity\ShoppingCartItem $shoppingCartItem)
    {
        $this->items[] = $shoppingCartItem;

        return $this;
    }

    /**
     * Remove shoppingCartItem
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ShoppingCartItem $shoppingCartItem
     */
    public function removeShoppingCartItem(\Jumpersoft\EcommerceBundle\Entity\ShoppingCartItem $shoppingCartItem)
    {
        $this->items->removeElement($shoppingCartItem);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set status
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Status $status
     *
     * @return ShoppingCart
     */
    public function setStatus(\Jumpersoft\EcommerceBundle\Entity\Status $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set discount
     *
     * @param string $discount
     *
     * @return ShoppingCart
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Add promotion
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ShoppingCartPromotion $promotion
     *
     * @return ShoppingCart
     */
    public function addPromotion(\Jumpersoft\EcommerceBundle\Entity\ShoppingCartPromotion $promotion)
    {
        $this->promotions[] = $promotion;

        return $this;
    }

    /**
     * Remove promotion
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ShoppingCartPromotion $promotion
     */
    public function removePromotion(\Jumpersoft\EcommerceBundle\Entity\ShoppingCartPromotion $promotion)
    {
        $this->promotions->removeElement($promotion);
    }

    /**
     * Get promotions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPromotions()
    {
        return $this->promotions;
    }

    /**
     * Set user
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User $user
     *
     * @return ShoppingCart
     */
    public function setUser(\Jumpersoft\EcommerceBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add item
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ShoppingCartItem $item
     *
     * @return ShoppingCart
     */
    public function addItem(\Jumpersoft\EcommerceBundle\Entity\ShoppingCartItem $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ShoppingCartItem $item
     */
    public function removeItem(\Jumpersoft\EcommerceBundle\Entity\ShoppingCartItem $item)
    {
        $this->items->removeElement($item);
    }
}
