<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="StoreViewGeneral")
 *
 * @author Angel
 */
class StoreViewGeneral extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="Store", inversedBy="viewGeneral")
     * @ORM\JoinColumn(name="id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $store;

    /**
     * @ORM\Column(type="boolean", name="showNewsletter", nullable=TRUE)
     */
    protected $showNewsletter;

    /**
     * @ORM\Column(type="boolean", name="showTestimonial", nullable=TRUE)
     */
    protected $showTestimonial;

    /**
     * @ORM\Column(type="boolean", name="showPosts", nullable=TRUE)
     */
    protected $showPosts;

    /**
     * @ORM\Column(type="string", name="copyright", length=255, nullable=TRUE)
     */
    protected $copyright;
    
    /**
     * @ORM\Column(type="string", name="storeName", length=255, nullable=TRUE)
     */
    protected $storeName;



    /**
     * Set showNewsletter.
     *
     * @param bool|null $showNewsletter
     *
     * @return StoreViewGeneral
     */
    public function setShowNewsletter($showNewsletter = null)
    {
        $this->showNewsletter = $showNewsletter;

        return $this;
    }

    /**
     * Get showNewsletter.
     *
     * @return bool|null
     */
    public function getShowNewsletter()
    {
        return $this->showNewsletter;
    }

    /**
     * Set showTestimonial.
     *
     * @param bool|null $showTestimonial
     *
     * @return StoreViewGeneral
     */
    public function setShowTestimonial($showTestimonial = null)
    {
        $this->showTestimonial = $showTestimonial;

        return $this;
    }

    /**
     * Get showTestimonial.
     *
     * @return bool|null
     */
    public function getShowTestimonial()
    {
        return $this->showTestimonial;
    }

    /**
     * Set showPosts.
     *
     * @param bool|null $showPosts
     *
     * @return StoreViewGeneral
     */
    public function setShowPosts($showPosts = null)
    {
        $this->showPosts = $showPosts;

        return $this;
    }

    /**
     * Get showPosts.
     *
     * @return bool|null
     */
    public function getShowPosts()
    {
        return $this->showPosts;
    }

    /**
     * Set copyright.
     *
     * @param string|null $copyright
     *
     * @return StoreViewGeneral
     */
    public function setCopyright($copyright = null)
    {
        $this->copyright = $copyright;

        return $this;
    }

    /**
     * Get copyright.
     *
     * @return string|null
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    /**
     * Set store.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Store $store
     *
     * @return StoreViewGeneral
     */
    public function setStore(\Jumpersoft\EcommerceBundle\Entity\Store $store)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set storeName.
     *
     * @param string|null $storeName
     *
     * @return StoreViewGeneral
     */
    public function setStoreName($storeName = null)
    {
        $this->storeName = $storeName;

        return $this;
    }

    /**
     * Get storeName.
     *
     * @return string|null
     */
    public function getStoreName()
    {
        return $this->storeName;
    }
}
