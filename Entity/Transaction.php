<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\TransactionRepository")
 * @ORM\Table(name="Transaction", indexes={@ORM\Index(name="search_idx", columns={"eventDate", "gatewayTransactionId", "gatewayStatusId"})})
 *
 * @author Angel
 */
class Transaction extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=50)
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="event", length=255, nullable=TRUE)
     */
    protected $event;

    /**
     * @ORM\Column(type="datetime", name="eventDate", options={"comment":"Register date in system"})
     */
    private $eventDate;

    /**
     * @ORM\Column(type="string", name="creationDate", length=30, options={"comment":"Creation date in gateway"}, nullable=TRUE)
     */
    private $creationDate;

    /**
     * @ORM\Column(type="string", name="operationDate", length=30, nullable=TRUE)
     */
    private $operationDate;

    /**
     * @ORM\Column(type="string", name="description", length=1000, nullable=TRUE)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", name="errorMessage", length=1000, nullable=TRUE)
     */
    protected $errorMessage;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="operationTypeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $operationType;

    /**
     * @ORM\ManyToOne(targetEntity="PaymentMethod")
     * @ORM\JoinColumn(name="paymentMethodId", referencedColumnName="id", nullable=FALSE)
     */
    protected $paymentMethod;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="transactionTypeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $transactionType;

    /**
     * @ORM\Column(type="json", name="gatewayResult", nullable=TRUE)
     */
    protected $gatewayResult = array();
    
    /**
     * @ORM\Column(type="json", name="card", nullable=TRUE)
     */
    protected $card;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", name="ipSource", length=16, nullable=TRUE)
     */
    private $ipSource;

    /**
     * @ORM\Column(type="string", name="ipSource2", length=16, nullable=TRUE)
     */
    private $ipSource2;

    /**
     * @ORM\ManyToOne(targetEntity="OrderRecord", inversedBy="transactions")
     * @ORM\JoinColumn(name="orderRecordId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     *
     * NOTA: Quitar onDelete cuando esté en producción
     */
    protected $orderRecord;

    /**
     * @ORM\Column(type="string", name="gatewayTransactionId", length=32, nullable=FALSE)
     */
    private $gatewayTransactionId;

    /**
     * @ORM\Column(type="string", name="paynetReferenceId", length=100, nullable=TRUE, options={"comment":"Reference id for Paynet net payments"}, nullable=TRUE)
     */
    protected $paynetReferenceId;

    /**
     * @ORM\Column(type="string", name="gatewayStatusId", length=32, nullable=FALSE)
     */
    private $gatewayStatusId;

    /* INICIAN FUNCIONES ESPECIALES NO BORRAR */

    public function __construct($id, $eventDate, $ip, $user)
    {
        $this->id = $id;
        $this->eventDate = $eventDate;
        $this->ip = $ip;
        $this->user = $user;
    }

    /* TERMINA FUNCIONES ESPECIALES NO BORRAR */

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEvent(): ?string
    {
        return $this->event;
    }

    public function setEvent(?string $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getEventDate(): ?\DateTimeInterface
    {
        return $this->eventDate;
    }

    public function setEventDate(\DateTimeInterface $eventDate): self
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    public function getCreationDate(): ?string
    {
        return $this->creationDate;
    }

    public function setCreationDate(?string $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getOperationDate(): ?string
    {
        return $this->operationDate;
    }

    public function setOperationDate(?string $operationDate): self
    {
        $this->operationDate = $operationDate;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    public function setErrorMessage(?string $errorMessage): self
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    public function getGatewayResult(): ?array
    {
        return $this->gatewayResult;
    }

    public function setGatewayResult(?array $gatewayResult): self
    {
        $this->gatewayResult = $gatewayResult;

        return $this;
    }

    public function getCard(): ?array
    {
        return $this->card;
    }

    public function setCard(?array $card): self
    {
        $this->card = $card;

        return $this;
    }

    public function getIpSource(): ?string
    {
        return $this->ipSource;
    }

    public function setIpSource(?string $ipSource): self
    {
        $this->ipSource = $ipSource;

        return $this;
    }

    public function getIpSource2(): ?string
    {
        return $this->ipSource2;
    }

    public function setIpSource2(?string $ipSource2): self
    {
        $this->ipSource2 = $ipSource2;

        return $this;
    }

    public function getGatewayTransactionId(): ?string
    {
        return $this->gatewayTransactionId;
    }

    public function setGatewayTransactionId(string $gatewayTransactionId): self
    {
        $this->gatewayTransactionId = $gatewayTransactionId;

        return $this;
    }

    public function getPaynetReferenceId(): ?string
    {
        return $this->paynetReferenceId;
    }

    public function setPaynetReferenceId(?string $paynetReferenceId): self
    {
        $this->paynetReferenceId = $paynetReferenceId;

        return $this;
    }

    public function getGatewayStatusId(): ?string
    {
        return $this->gatewayStatusId;
    }

    public function setGatewayStatusId(string $gatewayStatusId): self
    {
        $this->gatewayStatusId = $gatewayStatusId;

        return $this;
    }

    public function getOperationType(): ?Type
    {
        return $this->operationType;
    }

    public function setOperationType(?Type $operationType): self
    {
        $this->operationType = $operationType;

        return $this;
    }

    public function getPaymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?PaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getTransactionType(): ?Type
    {
        return $this->transactionType;
    }

    public function setTransactionType(?Type $transactionType): self
    {
        $this->transactionType = $transactionType;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getOrderRecord(): ?OrderRecord
    {
        return $this->orderRecord;
    }

    public function setOrderRecord(?OrderRecord $orderRecord): self
    {
        $this->orderRecord = $orderRecord;

        return $this;
    }
}
