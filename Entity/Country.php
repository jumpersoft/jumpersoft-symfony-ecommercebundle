<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\CountryRepository")
 * @ORM\Table(name="Country")
 *
 * @author Angel
 */
class Country extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=20)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=FALSE)
     */
    protected $name;
    
    /**
     * @ORM\Column(type="string", name="codeIso3166", length=10, nullable=TRUE)
     */
    protected $codeIso3166;
    
    /**
     * @ORM\Column(type="string", name="codeIsoAlfa2", length=10, nullable=TRUE)
     */
    protected $codeIsoAlfa2;
    
    /**
     * @ORM\Column(type="string", name="codeIsoAlfa3", length=10, nullable=TRUE)
     */
    protected $codeIsoAlfa3;
    

    /**
     * Set id
     *
     * @param string $id
     *
     * @return Country
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set codeIso3166
     *
     * @param string $codeIso3166
     *
     * @return Country
     */
    public function setCodeIso3166($codeIso3166)
    {
        $this->codeIso3166 = $codeIso3166;

        return $this;
    }

    /**
     * Get codeIso3166
     *
     * @return string
     */
    public function getCodeIso3166()
    {
        return $this->codeIso3166;
    }

    /**
     * Set codeIsoAlfa2
     *
     * @param string $codeIsoAlfa2
     *
     * @return Country
     */
    public function setCodeIsoAlfa2($codeIsoAlfa2)
    {
        $this->codeIsoAlfa2 = $codeIsoAlfa2;

        return $this;
    }

    /**
     * Get codeIsoAlfa2
     *
     * @return string
     */
    public function getCodeIsoAlfa2()
    {
        return $this->codeIsoAlfa2;
    }

    /**
     * Set codeIsoAlfa3
     *
     * @param string $codeIsoAlfa3
     *
     * @return Country
     */
    public function setCodeIsoAlfa3($codeIsoAlfa3)
    {
        $this->codeIsoAlfa3 = $codeIsoAlfa3;

        return $this;
    }

    /**
     * Get codeIsoAlfa3
     *
     * @return string
     */
    public function getCodeIsoAlfa3()
    {
        return $this->codeIsoAlfa3;
    }
}
