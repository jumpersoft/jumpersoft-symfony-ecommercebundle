<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="BlackListIpLog")
 *
 * @author Angel
 */
class BlackListIpLog
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint", nullable=FALSE)
     */
    protected $id;
    
    /**
     * @ORM\Column(type="datetime", name="eventDate")
     */
    protected $eventDate;
    
    /**
     * @ORM\Column(type="string", name="ipSource", length=16, nullable=TRUE)
     */
    protected $ipSource;

    /**
     * @ORM\Column(type="string", name="ipSource2", length=16, nullable=TRUE)
     */
    protected $ipSource2;
    
    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id")
     */
    protected $status;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventDate
     *
     * @param \DateTime $eventDate
     *
     * @return BlackListIpLog
     */
    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * Get eventDate
     *
     * @return \DateTime
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * Set ipSource
     *
     * @param string $ipSource
     *
     * @return BlackListIpLog
     */
    public function setIpSource($ipSource)
    {
        $this->ipSource = $ipSource;

        return $this;
    }

    /**
     * Get ipSource
     *
     * @return string
     */
    public function getIpSource()
    {
        return $this->ipSource;
    }

    /**
     * Set ipSource2
     *
     * @param string $ipSource2
     *
     * @return BlackListIpLog
     */
    public function setIpSource2($ipSource2)
    {
        $this->ipSource2 = $ipSource2;

        return $this;
    }

    /**
     * Get ipSource2
     *
     * @return string
     */
    public function getIpSource2()
    {
        return $this->ipSource2;
    }

    /**
     * Set status
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Status $status
     *
     * @return BlackListIpLog
     */
    public function setStatus(\Jumpersoft\EcommerceBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }
}
