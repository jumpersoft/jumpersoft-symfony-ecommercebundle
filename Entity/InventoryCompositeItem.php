<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\ItemRepository")
 * @ORM\Table(name="InventoryCompositeItem", uniqueConstraints={@ORM\UniqueConstraint(columns={"inventoryId","itemCompositeItemId"})})
 * @UniqueEntity(
 *     fields={"inventory", "itemCompositeItem"},
 *     message="This inventory and composite item is already in use on this row"
 * )
 *
 * @author Angel
 */
class InventoryCompositeItem extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Inventory", inversedBy="inventoryCompositeItems")
     * @ORM\JoinColumn(name="inventoryId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $inventory;

    /**
     * @ORM\ManyToOne(targetEntity="ItemCompositeItem")
     * @ORM\JoinColumn(name="itemCompositeItemId", referencedColumnName="id",  nullable=FALSE)
     */
    protected $itemCompositeItem;
    
    /**
    * @ORM\OneToMany(targetEntity="InventoryCompositeItemInventory", mappedBy="inventoryCompositeItem")
    */
    protected $itemInventory;

    /**
     * @ORM\Column(type="decimal", name="quantity", precision=16, scale=4, nullable=FALSE)
     */
    protected $quantity;

    /**
     * @ORM\Column(type="decimal", name="quantityFulfilled", precision=16, scale=4, nullable=FALSE)
     */
    protected $quantityFulfilled;

    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id", nullable=FALSE)
     */
    protected $status;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;
    

    public function __construct($id, $inventory, $itemCompositeItem, $quantity, $quantityFulfilled, $status, $registerDate)
    {
        $this->id = $id;
        $this->inventory = $inventory;
        $this->itemCompositeItem = $itemCompositeItem;
        $this->quantity = $quantity;
        $this->quantityFulfilled = $quantityFulfilled;
        $this->status = $status;
        $this->registerDate = $registerDate;
    }
    

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return InventoryCompositeItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity.
     *
     * @param string $quantity
     *
     * @return InventoryCompositeItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set quantityFulfilled.
     *
     * @param string $quantityFulfilled
     *
     * @return InventoryCompositeItem
     */
    public function setQuantityFulfilled($quantityFulfilled)
    {
        $this->quantityFulfilled = $quantityFulfilled;

        return $this;
    }

    /**
     * Get quantityFulfilled.
     *
     * @return string
     */
    public function getQuantityFulfilled()
    {
        return $this->quantityFulfilled;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return InventoryCompositeItem
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set inventory.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Inventory $inventory
     *
     * @return InventoryCompositeItem
     */
    public function setInventory(\Jumpersoft\EcommerceBundle\Entity\Inventory $inventory)
    {
        $this->inventory = $inventory;

        return $this;
    }

    /**
     * Get inventory.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Inventory
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * Set itemCompositeItem.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ItemCompositeItem $itemCompositeItem
     *
     * @return InventoryCompositeItem
     */
    public function setItemCompositeItem(\Jumpersoft\EcommerceBundle\Entity\ItemCompositeItem $itemCompositeItem)
    {
        $this->itemCompositeItem = $itemCompositeItem;

        return $this;
    }

    /**
     * Get itemCompositeItem.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\ItemCompositeItem
     */
    public function getItemCompositeItem()
    {
        return $this->itemCompositeItem;
    }

    /**
     * Set status.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Status $status
     *
     * @return InventoryCompositeItem
     */
    public function setStatus(\Jumpersoft\EcommerceBundle\Entity\Status $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add itemInventory.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\InventoryCompositeItemInventory $itemInventory
     *
     * @return InventoryCompositeItem
     */
    public function addItemInventory(\Jumpersoft\EcommerceBundle\Entity\InventoryCompositeItemInventory $itemInventory)
    {
        $this->itemInventory[] = $itemInventory;

        return $this;
    }

    /**
     * Remove itemInventory.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\InventoryCompositeItemInventory $itemInventory
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeItemInventory(\Jumpersoft\EcommerceBundle\Entity\InventoryCompositeItemInventory $itemInventory)
    {
        return $this->itemInventory->removeElement($itemInventory);
    }

    /**
     * Get itemInventory.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemInventory()
    {
        return $this->itemInventory;
    }
}
