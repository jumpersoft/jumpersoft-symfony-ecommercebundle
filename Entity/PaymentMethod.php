<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="PaymentMethod")
 *
 * @author Angel
 */
class PaymentMethod
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="name", length=100, nullable=FALSE)
     */
    private $name;

    /**
     * @ORM\Column(type="string", name="description", length=255, nullable=FALSE)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", name="showInOrders", nullable=TRUE)
     */
    protected $showInOrders;

    /**
     * @ORM\ManyToOne(targetEntity="Gateway")
     * @ORM\JoinColumn(name="gatewayId", referencedColumnName="id", nullable=TRUE)
     */
    protected $gateway;

    /**
     * @ORM\Column(type="smallint", name="sequence", nullable=TRUE)
     */
    protected $sequence;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getShowInOrders(): ?bool
    {
        return $this->showInOrders;
    }

    public function setShowInOrders(?bool $showInOrders): self
    {
        $this->showInOrders = $showInOrders;

        return $this;
    }

    public function getGateway(): ?Gateway
    {
        return $this->gateway;
    }

    public function setGateway(?Gateway $gateway): self
    {
        $this->gateway = $gateway;

        return $this;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(?int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }
}
