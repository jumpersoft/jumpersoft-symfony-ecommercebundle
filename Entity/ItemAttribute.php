<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\ItemRepository")
 * @ORM\Table(name="ItemAttribute", uniqueConstraints={@ORM\UniqueConstraint(columns={"itemId","attributeId"})})
 * @UniqueEntity(
 *     fields={"item", "attribute"},
 *     message="The attribute is already in use on this item"
 * )
 * @author Angel
 */
class ItemAttribute extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="attributes")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $item;

    /**
     * @ORM\ManyToOne(targetEntity="Attribute", inversedBy="item")
     * @ORM\JoinColumn(name="attributeId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $attribute;

    /**
     * @ORM\Column(type="boolean", name="isVariant", nullable=true, options={"default":"0"})
     */
    protected $isVariant;

    /**
     * @ORM\Column(type="smallint", name="sequence", nullable=TRUE)
     */
    protected $sequence;

    /**
     * @ORM\OneToMany(targetEntity="ItemAttributeOption", mappedBy="itemAttribute")
     */
    protected $options;

    /**
     * @ORM\Column(type="string", name="defaultOption", length=32, nullable=TRUE)
     */
    protected $defaultOption;

    /**
     * @ORM\Column(type="string", name="urlFiles", length=500, nullable=TRUE)
     */
    protected $urlFiles;

    public function __construct()
    {
        $this->options = new ArrayCollection();
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getAttribute(): ?Attribute
    {
        return $this->attribute;
    }

    public function setAttribute(?Attribute $attribute): self
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @return Collection|ItemAttributeOption[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    public function addOption(ItemAttributeOption $option): self
    {
        if (!$this->options->contains($option)) {
            $this->options[] = $option;
            $option->setItemAttribute($this);
        }

        return $this;
    }

    public function removeOption(ItemAttributeOption $option): self
    {
        if ($this->options->removeElement($option)) {
            // set the owning side to null (unless already changed)
            if ($option->getItemAttribute() === $this) {
                $option->setItemAttribute(null);
            }
        }

        return $this;
    }

    public function getIsVariant(): ?bool
    {
        return $this->isVariant;
    }

    public function setIsVariant(?bool $isVariant): self
    {
        $this->isVariant = $isVariant;

        return $this;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(?int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }

    public function getDefaultOption(): ?string
    {
        return $this->defaultOption;
    }

    public function setDefaultOption(?string $defaultOption): self
    {
        $this->defaultOption = $defaultOption;

        return $this;
    }

    public function getUrlFiles(): ?string
    {
        return $this->urlFiles;
    }

    public function setUrlFiles(?string $urlFiles): self
    {
        $this->urlFiles = $urlFiles;

        return $this;
    }
}
