<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\CategoryRepository")
 * @ORM\Table(name="CategoryType", indexes={@ORM\Index(name="search_idx", columns={"name"})})
 *
 * @author Angel
 */
class CategoryType
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="name", length=255)
     */
    private $name;

    /**
     *
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="type")
     */
    protected $categories;

    /**
     * @ORM\Column(type="boolean", name="showInCatalog", nullable=TRUE)
     */
    protected $showInCatalog = false;

    /**
     * @ORM\Column(type="smallint", name="sequence", nullable=TRUE)
     */
    protected $sequence;

    /**
     * @ORM\Column(type="string", name="slug", length=255, nullable=TRUE)
     */
    private $slug;

    /* INICIAN FUNCIONES ESPECIALES NO BORRAR */

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    /* TERMINA FUNCIONES ESPECIALES NO BORRAR */

    /**
     * Set id
     *
     * @param string $id
     *
     * @return CategoryType
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CategoryType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CategoryType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set showInCatalog
     *
     * @param boolean $showInCatalog
     *
     * @return CategoryType
     */
    public function setShowInCatalog($showInCatalog)
    {
        $this->showInCatalog = $showInCatalog;

        return $this;
    }

    /**
     * Get showInCatalog
     *
     * @return boolean
     */
    public function getShowInCatalog()
    {
        return $this->showInCatalog;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     *
     * @return CategoryType
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Add category
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Category $category
     *
     * @return CategoryType
     */
    public function addCategory(\Jumpersoft\EcommerceBundle\Entity\Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Category $category
     */
    public function removeCategory(\Jumpersoft\EcommerceBundle\Entity\Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }


    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return CategoryType
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
