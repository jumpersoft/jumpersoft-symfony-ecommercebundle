<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\ShipmentRepository")
 * @ORM\Table(name="ShipmentSchedule")
 *
 * @author Angel
 */
class ShipmentSchedule extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=FALSE)
     */
    protected $name;

    /**
     * @ORM\Column(type="smallint", nullable=TRUE)
     */
    protected $minute;

    /**
     * @ORM\Column(type="smallint", nullable=TRUE)
     */
    protected $hour;

    /**
     * @ORM\Column(type="smallint", name="dayOfMonth", nullable=TRUE)
     */
    protected $dayOfMonth;

    /**
     * @ORM\Column(type="smallint", nullable=TRUE)
     */
    protected $month;

    /**
     * @ORM\Column(type="smallint", name="dayOfWeek", nullable=TRUE)
     */
    protected $dayOfWeek;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    /**
     * @ORM\OneToMany(targetEntity="Shipment", mappedBy="schedule")
     */
    protected $shipments;

    public function __construct()
    {
        $this->shipments = new ArrayCollection();
    }


    /**
     * Set id.
     *
     * @param string $id
     *
     * @return ShipmentSchedule
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ShipmentSchedule
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set minute.
     *
     * @param int|null $minute
     *
     * @return ShipmentSchedule
     */
    public function setMinute($minute = null)
    {
        $this->minute = $minute;

        return $this;
    }

    /**
     * Get minute.
     *
     * @return int|null
     */
    public function getMinute()
    {
        return $this->minute;
    }

    /**
     * Set hour.
     *
     * @param int|null $hour
     *
     * @return ShipmentSchedule
     */
    public function setHour($hour = null)
    {
        $this->hour = $hour;

        return $this;
    }

    /**
     * Get hour.
     *
     * @return int|null
     */
    public function getHour()
    {
        return $this->hour;
    }

    /**
     * Set dayOfMonth.
     *
     * @param int|null $dayOfMonth
     *
     * @return ShipmentSchedule
     */
    public function setDayOfMonth($dayOfMonth = null)
    {
        $this->dayOfMonth = $dayOfMonth;

        return $this;
    }

    /**
     * Get dayOfMonth.
     *
     * @return int|null
     */
    public function getDayOfMonth()
    {
        return $this->dayOfMonth;
    }

    /**
     * Set month.
     *
     * @param int|null $month
     *
     * @return ShipmentSchedule
     */
    public function setMonth($month = null)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month.
     *
     * @return int|null
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set dayOfWeek.
     *
     * @param int|null $dayOfWeek
     *
     * @return ShipmentSchedule
     */
    public function setDayOfWeek($dayOfWeek = null)
    {
        $this->dayOfWeek = $dayOfWeek;

        return $this;
    }

    /**
     * Get dayOfWeek.
     *
     * @return int|null
     */
    public function getDayOfWeek()
    {
        return $this->dayOfWeek;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return ShipmentSchedule
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Add shipment.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Shipment $shipment
     *
     * @return ShipmentSchedule
     */
    public function addShipment(\Jumpersoft\EcommerceBundle\Entity\Shipment $shipment)
    {
        $this->shipments[] = $shipment;

        return $this;
    }

    /**
     * Remove shipment.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Shipment $shipment
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeShipment(\Jumpersoft\EcommerceBundle\Entity\Shipment $shipment)
    {
        return $this->shipments->removeElement($shipment);
    }

    /**
     * Get shipments.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShipments()
    {
        return $this->shipments;
    }
}
