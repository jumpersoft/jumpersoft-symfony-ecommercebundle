<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\UserRepository")
 * @ORM\Table(name="UserCard")
 *
 * @author Angel
 */
class UserCard extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;
    

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="cards")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", length=30, nullable=FALSE)
     */
    protected $brand;
    
    /**
     * @ORM\Column(type="string", length=10, nullable=FALSE)
     */
    protected $type;

    /**
     * @ORM\Column(type="string", length=16, nullable=FALSE)
     */
    protected $number;

    /**
     * @ORM\Column(type="string", length=255, nullable=FALSE)
     */
    protected $name;

    /**
     * @ORM\Column(type="smallint", nullable=FALSE)
     */
    protected $year;

    /**
     * @ORM\Column(type="smallint", nullable=FALSE)
     */
    protected $month;

    /**
     * @ORM\Column(type="string", length=100, nullable=FALSE)
     */
    protected $registerDate;

    /**
     * @ORM\Column(type="boolean", name="main", nullable=FALSE)
     */
    protected $main;

    /**
     * @ORM\Column(type="string", name="gatewayCardId", length=45, nullable=FALSE)
     */
    protected $gatewayCardId;

    public function __construct($id = null, $brand = null, $type = null, $number = null, $name = null, $year = null, $month = null, $registerDate = null, $user = null, $gatewayCardId = null, $main = false)
    {
        $this->id = $id;
        $this->brand = $brand;
        $this->type = $type;
        $this->number = $number;
        $this->name = $name;
        $this->year = $year;
        $this->month = $month;
        $this->registerDate = $registerDate;
        $this->user = $user;
        $this->gatewayCardId = $gatewayCardId;
        $this->main = $main;
    }

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return UserCard
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set brand.
     *
     * @param string $brand
     *
     * @return UserCard
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand.
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set number.
     *
     * @param string $number
     *
     * @return UserCard
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number.
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return UserCard
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set year.
     *
     * @param int $year
     *
     * @return UserCard
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year.
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set month.
     *
     * @param int $month
     *
     * @return UserCard
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month.
     *
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    

    /**
     * Set user.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User $user
     *
     * @return UserCard
     */
    public function setUser(\Jumpersoft\EcommerceBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set gatewayCardId.
     *
     * @param string $gatewayCardId
     *
     * @return UserCard
     */
    public function setGatewayCardId($gatewayCardId)
    {
        $this->gatewayCardId = $gatewayCardId;

        return $this;
    }

    /**
     * Get gatewayCardId.
     *
     * @return string
     */
    public function getGatewayCardId()
    {
        return $this->gatewayCardId;
    }

    /**
     * Set main.
     *
     * @param bool $main
     *
     * @return UserCard
     */
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main.
     *
     * @return bool
     */
    public function getMain()
    {
        return $this->main;
    }
   


    /**
     * Set type.
     *
     * @param string $type
     *
     * @return UserCard
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set registerDate.
     *
     * @param string $registerDate
     *
     * @return UserCard
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return string
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }
}
