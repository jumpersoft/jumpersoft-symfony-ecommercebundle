<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\OrderRecordRepository")
 * @ORM\Table(name="OrderRecord", options={"comment":"Order or sales"}, indexes={@ORM\Index(name="search_idx", columns={"folio", "date", "total"})})
 *
 * @author Angel
 */
class OrderRecord extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumn(name="storeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $store;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="customerId", referencedColumnName="id", nullable=FALSE)
     */
    protected $customer;

    /**
     * @ORM\Column(type="string", name="folio", length=100, nullable=FALSE)
     */
    protected $folio;

    /**
     * @ORM\Column(type="datetime", name="date", nullable=FALSE)
     */
    protected $date;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="shippingMethodId", referencedColumnName="id")
     */
    protected $shippingMethod;

    /**
     * @ORM\Column(type="datetime", name="shipmentDate", nullable=TRUE)
     */
    protected $shipmentDate;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="paymentFormId", referencedColumnName="id")
     */
    protected $paymentForm;

    /**
     * @ORM\ManyToOne(targetEntity="PaymentMethod")
     * @ORM\JoinColumn(name="paymentMethodId", referencedColumnName="id")
     */
    protected $paymentMethod;

    /**
     * @ORM\Column(type="decimal", name="exchangeRate", precision=18, scale=2, nullable=TRUE)
     */
    protected $exchangeRate;

    /**
     * @ORM\Column(type="datetime", name="exchangeRateDate", nullable=TRUE)
     */
    protected $exchangeRateDate;

    /**
     * @ORM\Column(type="string", name="currency", length=50, nullable=TRUE)
     */
    protected $currency;

    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id", nullable=FALSE)
     */
    protected $status;

    /**
     * @ORM\Column(type="string", name="comment", length=1000, nullable=TRUE)
     */
    protected $comment;

    /**
     * Fecha de actualización
     *
     * @ORM\Column(type="datetime", name="updateDate", nullable=TRUE)
     */
    protected $updateDate;

    /**
     * Fecha de actualización
     *
     * @ORM\Column(type="datetime", name="paymentConfirmationDate", nullable=TRUE)
     */
    protected $paymentConfirmationDate;

    /**
     * @ORM\Column(type="boolean", name="invoiced", nullable=TRUE)
     */
    protected $invoiced;

    /**
     * @ORM\Column(type="decimal", name="subTotal", precision=18, scale=2, nullable=FALSE, options={"default":"0"})
     */
    protected $subTotal;

    /**
     * @ORM\Column(type="decimal", name="discount", precision=18, scale=2, nullable=TRUE)
     */
    protected $discount;

    /**
     * @ORM\Column(type="string", name="discountReason", length=200, nullable=TRUE)
     */
    protected $discountReason;

    /**
     * @ORM\Column(type="decimal", name="total", precision=18, scale=2, nullable=FALSE, options={"default":"0"})
     */
    protected $total;

    /**
     * @ORM\OneToMany(targetEntity="OrderRecordItem", mappedBy="orderRecord")
     */
    protected $items;

    /**
     * @ORM\OneToMany(targetEntity="Transaction", mappedBy="orderRecord")
     */
    protected $transactions;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="shippingAddressTypeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $shippingAddressType;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="billingAddressTypeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $billingAddressType;

    /**
     * @ORM\Column(type="json", name="shippingAddress", nullable=TRUE)
     */
    protected $shippingAddress;

    /**
     * @ORM\Column(type="json", name="billingAddress", nullable=TRUE)
     */
    protected $billingAddress;

    /**
     * @ORM\OneToMany(targetEntity="OrderRecordPromotion", mappedBy="orderRecord")
     */
    protected $promotions;

    /**
     * @ORM\OneToMany(targetEntity="ProccessStep", mappedBy="orderRecord")
     */
    protected $proccessSteps;

    /**
     * @ORM\Column(type="json", name="cancelation", nullable=TRUE, options={"default":"[]"})
     */
    protected $cancelation;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="sellerId", referencedColumnName="id", nullable=true)
     */
    protected $seller;

    /* INICIAN FUNCIONES ESPECIALES NO BORRAR */

    public function __construct($id)
    {
        $this->id = $id;
        $this->items = new ArrayCollection();
        $this->transactions = new ArrayCollection();
        $this->promotions = new ArrayCollection();
        $this->proccessSteps = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subTotal = 0;
        $this->total = 0;
        $this->cancelation = [];
    }

    /* TERMINA FUNCIONES ESPECIALES NO BORRAR */

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getFolio(): ?string
    {
        return $this->folio;
    }

    public function setFolio(string $folio): self
    {
        $this->folio = $folio;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getShipmentDate(): ?\DateTimeInterface
    {
        return $this->shipmentDate;
    }

    public function setShipmentDate(\DateTimeInterface $shipmentDate): self
    {
        $this->shipmentDate = $shipmentDate;

        return $this;
    }

    public function getExchangeRate(): ?string
    {
        return $this->exchangeRate;
    }

    public function setExchangeRate(?string $exchangeRate): self
    {
        $this->exchangeRate = $exchangeRate;

        return $this;
    }

    public function getExchangeRateDate(): ?\DateTimeInterface
    {
        return $this->exchangeRateDate;
    }

    public function setExchangeRateDate(?\DateTimeInterface $exchangeRateDate): self
    {
        $this->exchangeRateDate = $exchangeRateDate;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getPaymentConfirmationDate(): ?\DateTimeInterface
    {
        return $this->paymentConfirmationDate;
    }

    public function setPaymentConfirmationDate(?\DateTimeInterface $paymentConfirmationDate): self
    {
        $this->paymentConfirmationDate = $paymentConfirmationDate;

        return $this;
    }

    public function getInvoiced(): ?bool
    {
        return $this->invoiced;
    }

    public function setInvoiced(?bool $invoiced): self
    {
        $this->invoiced = $invoiced;

        return $this;
    }

    public function getSubTotal(): ?string
    {
        return $this->subTotal;
    }

    public function setSubTotal(string $subTotal): self
    {
        $this->subTotal = $subTotal;

        return $this;
    }

    public function getDiscount(): ?string
    {
        return $this->discount;
    }

    public function setDiscount(?string $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getDiscountReason(): ?string
    {
        return $this->discountReason;
    }

    public function setDiscountReason(?string $discountReason): self
    {
        $this->discountReason = $discountReason;

        return $this;
    }

    public function getTotal(): ?string
    {
        return $this->total;
    }

    public function setTotal(string $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getShippingAddress(): ?array
    {
        return $this->shippingAddress;
    }

    public function setShippingAddress(?array $shippingAddress): self
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    public function getBillingAddress(): ?array
    {
        return $this->billingAddress;
    }

    public function setBillingAddress(?array $billingAddress): self
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    public function getCancelation(): ?array
    {
        return $this->cancelation;
    }

    public function setCancelation(?array $cancelation): self
    {
        $this->cancelation = $cancelation;

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    public function getCustomer(): ?User
    {
        return $this->customer;
    }

    public function setCustomer(?User $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getPaymentForm(): ?Type
    {
        return $this->paymentForm;
    }

    public function setPaymentForm(?Type $paymentForm): self
    {
        $this->paymentForm = $paymentForm;

        return $this;
    }

    public function getPaymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?PaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|OrderRecordItem[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(OrderRecordItem $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setOrderRecord($this);
        }

        return $this;
    }

    public function removeItem(OrderRecordItem $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getOrderRecord() === $this) {
                $item->setOrderRecord(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setOrderRecord($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getOrderRecord() === $this) {
                $transaction->setOrderRecord(null);
            }
        }

        return $this;
    }

    public function getShippingAddressType(): ?Type
    {
        return $this->shippingAddressType;
    }

    public function setShippingAddressType(?Type $shippingAddressType): self
    {
        $this->shippingAddressType = $shippingAddressType;

        return $this;
    }

    public function getBillingAddressType(): ?Type
    {
        return $this->billingAddressType;
    }

    public function setBillingAddressType(?Type $billingAddressType): self
    {
        $this->billingAddressType = $billingAddressType;

        return $this;
    }

    /**
     * @return Collection|OrderRecordPromotion[]
     */
    public function getPromotions(): Collection
    {
        return $this->promotions;
    }

    public function addPromotion(OrderRecordPromotion $promotion): self
    {
        if (!$this->promotions->contains($promotion)) {
            $this->promotions[] = $promotion;
            $promotion->setOrderRecord($this);
        }

        return $this;
    }

    public function removePromotion(OrderRecordPromotion $promotion): self
    {
        if ($this->promotions->removeElement($promotion)) {
            // set the owning side to null (unless already changed)
            if ($promotion->getOrderRecord() === $this) {
                $promotion->setOrderRecord(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProccessStep[]
     */
    public function getProccessSteps(): Collection
    {
        return $this->proccessSteps;
    }

    public function addProccessStep(ProccessStep $proccessStep): self
    {
        if (!$this->proccessSteps->contains($proccessStep)) {
            $this->proccessSteps[] = $proccessStep;
            $proccessStep->setOrderRecord($this);
        }

        return $this;
    }

    public function removeProccessStep(ProccessStep $proccessStep): self
    {
        if ($this->proccessSteps->removeElement($proccessStep)) {
            // set the owning side to null (unless already changed)
            if ($proccessStep->getOrderRecord() === $this) {
                $proccessStep->setOrderRecord(null);
            }
        }

        return $this;
    }

    public function getSeller(): ?User
    {
        return $this->seller;
    }

    public function setSeller(?User $seller): self
    {
        $this->seller = $seller;

        return $this;
    }

    public function getShippingMethod(): ?Type
    {
        return $this->shippingMethod;
    }

    public function setShippingMethod(?Type $shippingMethod): self
    {
        $this->shippingMethod = $shippingMethod;

        return $this;
    }
}
