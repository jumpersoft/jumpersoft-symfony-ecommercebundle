<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="Gateway")
 *
 * @author Angel
 */
class Gateway
{

    /**
     * @ORM\Id
     * @ORM\Column(type="smallint", unique=true, nullable=FALSE)
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="name", length=100, nullable=FALSE)
     */
    private $name;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Gateway
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Gateway
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
