<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\OrderRecordRepository")
 * @ORM\Table(name="OrderRecordPromotion")
 *
 * @author Angel
 */
class OrderRecordPromotion extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="OrderRecord", inversedBy="promotions")
     * @ORM\JoinColumn(name="orderRecordId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $orderRecord;

    /**
     * @ORM\ManyToOne(targetEntity="Promotion", inversedBy="orderRecords")
     * @ORM\JoinColumn(name="promotionId", referencedColumnName="id", nullable=FALSE)
     */
    protected $promotion;

    /** INICIAN FUNCIONES ESPECIALES NO BORRAR */
    public function __construct()
    {
    }
    


    /**
     * Set id
     *
     * @param string $id
     *
     * @return OrderRecordPromotion
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderRecord
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\OrderRecord $orderRecord
     *
     * @return OrderRecordPromotion
     */
    public function setOrderRecord(\Jumpersoft\EcommerceBundle\Entity\OrderRecord $orderRecord)
    {
        $this->orderRecord = $orderRecord;

        return $this;
    }

    /**
     * Get orderRecord
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\OrderRecord
     */
    public function getOrderRecord()
    {
        return $this->orderRecord;
    }

    /**
     * Set promotion
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Promotion $promotion
     *
     * @return OrderRecordPromotion
     */
    public function setPromotion(\Jumpersoft\EcommerceBundle\Entity\Promotion $promotion)
    {
        $this->promotion = $promotion;

        return $this;
    }

    /**
     * Get promotion
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Promotion
     */
    public function getPromotion()
    {
        return $this->promotion;
    }
}
