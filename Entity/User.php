<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\UserRepository")
 * @ORM\Table(name="User", indexes={@ORM\Index(name="search_idx", columns={"email", "username", "rfc", "tradeName"})})
 *
 * Defines the properties of the User entity to represent the application users.
 * See http://symfony.com/doc/current/book/doctrine.html#creating-an-entity-class
 *
 * Tip: if you have an existing database, you can generate these entity class automatically.
 * See http://symfony.com/doc/current/cookbook/doctrine/reverse_engineering.html
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class User extends JumpersoftModel implements UserInterface, \Serializable
{
    /*
     *  START USER ACCOUNT INFO
     */

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    protected $username;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $email;

    /**
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $isActive = false;

    /**
     * @ORM\Column(type="boolean",  nullable=TRUE)
     */
    private $isAccountNonExpired = true;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $isAccountNonLocked = true;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $isCredentialsNonExpired = true;

    /* END USER ACCOUNT INFO  */

    /**
     *  START GENERAL USER DATA */

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", name="lastName", length=255, nullable=TRUE)
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string", name="mothersLastName", length=255, nullable=TRUE)
     */
    protected $mothersLastName;

    /**
     * @ORM\Column(type="string", name="alternateEmail", length=255, nullable=TRUE)
     */
    protected $alternateEmail;

    /**
     * @ORM\Column(type="blob", nullable=TRUE)
     */
    protected $logo;

    /**
     * @ORM\Column(type="string", name="phone", length=50, nullable=TRUE)
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", name="mobile", length=255, nullable=TRUE)
     */
    protected $mobile;

    /* END GENERAL USER DATA */

    /**
     * START RECOVER FIELDS
     */

    /**
     * @ORM\Column(type="datetime", name="registerDate")
     */
    protected $registerDate;

    /**
     * @ORM\Column(type="datetime", name="userConfirmationDate", nullable=TRUE)
     */
    protected $userConfirmationDate;

    /**
     * @ORM\Column(type="string", name="signupCode", length=32, nullable=TRUE)
     */
    protected $signupCode;

    /**
     * @ORM\Column(type="datetime", name="lastAccessDate", nullable=TRUE)
     */
    protected $lastAccessDate;

    /**
     * @ORM\Column(type="string", name="passwordRecoveryCode", length=32, nullable=TRUE)
     */
    protected $passwordRecoveryCode;

    /**
     * @ORM\Column(type="datetime", name="lastRequestPasswordRecoveryDate",  nullable=TRUE)
     */
    protected $lastRequestPasswordRecoveryDate;

    /**
     * @ORM\Column(type="datetime", name="lastPasswordRecoveryDate", nullable=TRUE)
     */
    protected $lastPasswordRecoveryDate;

    /**
     * @ORM\Column(type="string", name="gatewayUserId", length=45, nullable=TRUE)
     */
    protected $gatewayUserId;


    /* END RECOVER FIELDS */

    /**
     * @ORM\OneToMany(targetEntity="UserLog", mappedBy="user")
     */
    protected $userLog;

    /**
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumn(name="defaultStoreId", referencedColumnName="id", nullable=false)
     */
    protected $defaultStore;

    /**
     * @ORM\ManyToMany(targetEntity="Store", mappedBy="users")
     */
    protected $stores;

    /**
     * @ORM\OneToMany(targetEntity="Address", mappedBy="user")
     */
    protected $addresses;

    /**
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="defaultShippingAddressId", referencedColumnName="id", nullable=TRUE, onDelete="SET NULL")
     */
    protected $defaultShippingAddress;
    
    /**
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="defaultBillingAddressId", referencedColumnName="id", nullable=TRUE, onDelete="SET NULL")
     */
    protected $defaultBillingAddress;

    /**
     * @ORM\OneToOne(targetEntity="UserSuscription", mappedBy="user")
     */
    protected $suscriptions;

    /**
     * @ORM\OneToMany(targetEntity="UserCard", mappedBy="user")
     */
    protected $cards;

    /**
     * @ORM\OneToMany(targetEntity="PriceListCustomer", mappedBy="customer")
     */
    protected $priceList;

    /**
     * @ORM\Column(type="json", nullable=true, options={"default":"[]"})
     */
    protected $rules;

    /**
     * @ORM\OneToMany(targetEntity="UserLicence", mappedBy="user")
     */
    protected $licences;

    /** START TAX DATA */

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="entityTypeId", referencedColumnName="id", nullable=false)
     */
    protected $entityType;

    /**
     * @ORM\Column(type="string", length=20, nullable=TRUE, unique=true)
     */
    protected $rfc;

    /**
     * @ORM\Column(type="string", name="tradeName", length=255, nullable=true)
     */
    protected $tradeName;

    /** END TAX DATA */

    /**
     *  INICIA NO BORRAR */
    public function __construct()
    {
        $this->userLog = new ArrayCollection();
        $this->companies = new ArrayCollection();
        $this->addresses = new ArrayCollection();
        $this->cards = new ArrayCollection();
        $this->licences = new ArrayCollection();
        $this->rules = ["allowsReturns" => 0, "returnPercentage" => null];
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->getName() . ' ' . $this->getLastName() . ' ' . $this->getMothersLastName();
    }

    /* TERMINA NO BORRAR */

    /**
     * @ORM\Column(type="json")
     */
    protected $roles = array();

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getRole()
    {
        return $this->roles[0];
    }

    /**
     * Returns the roles or permissions granted to the user for security.
     */
    public function getRoles()
    {
        $roles = $this->roles;

        // guarantees that a user always has at least one role for security
        if (empty($roles)) {
            $roles[] = 'ROLE_CUSTOMER';
        }

        return array_unique($roles);
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     */
    public function getSalt()
    {
        // See "Do you need to use a Salt?" at http://symfony.com/doc/current/cookbook/security/entity_provider.html
        // we're using bcrypt in security.yml to encode the password, so
        // the salt value is built-in and you don't have to generate one

        return;
    }

    /**
     * Removes sensitive data from the user.
     */
    public function eraseCredentials()
    {
        // if you had a plainPassword property, you'd nullify it here
        // $this->plainPassword = null;
    }

    /**
     * Add userLog
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\UserLog $userLog
     *
     * @return User
     */
    public function addUserLog(\Jumpersoft\EcommerceBundle\Entity\UserLog $userLog)
    {
        $this->userLog[] = $userLog;

        return $this;
    }

    /**
     * Remove userLog
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\UserLog $userLog
     */
    public function removeUserLog(\Jumpersoft\EcommerceBundle\Entity\UserLog $userLog)
    {
        $this->userLog->removeElement($userLog);
    }

    /**
     * Get userLog
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserLog()
    {
        return $this->userLog;
    }

    //AdvancedUserInterface

    public function isAccountNonExpired()
    {
        return $this->isAccountNonExpired;
    }

    public function isAccountNonLocked()
    {
        return $this->isAccountNonLocked;
    }

    public function isCredentialsNonExpired()
    {
        return $this->isCredentialsNonExpired;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }

    // serialize and unserialize must be updated - see below
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->isActive
        ));
    }

    public function unserialize($serialized)
    {
        list(
                $this->id,
                $this->username,
                $this->password,
                $this->isActive
                ) = unserialize($serialized);
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set isAccountNonLocked
     *
     * @param boolean $isAccountNonLocked
     *
     * @return User
     */
    public function setIsAccountNonLocked($isAccountNonLocked)
    {
        $this->isAccountNonLocked = $isAccountNonLocked;

        return $this;
    }

    /**
     * Get isAccountNonLocked
     *
     * @return boolean
     */
    public function getIsAccountNonLocked()
    {
        return $this->isAccountNonLocked;
    }

    /**
     * Set isCredentialsNonExpired
     *
     * @param boolean $isCredentialsNonExpired
     *
     * @return User
     */
    public function setIsCredentialsNonExpired($isCredentialsNonExpired)
    {
        $this->isCredentialsNonExpired = $isCredentialsNonExpired;

        return $this;
    }

    /**
     * Get isCredentialsNonExpired
     *
     * @return boolean
     */
    public function getIsCredentialsNonExpired()
    {
        return $this->isCredentialsNonExpired;
    }

    /**
     * Set isAccountNonExpired
     *
     * @param boolean $isAccountNonExpired
     *
     * @return User
     */
    public function setIsAccountNonExpired($isAccountNonExpired)
    {
        $this->isAccountNonExpired = $isAccountNonExpired;

        return $this;
    }

    /**
     * Get isAccountNonExpired
     *
     * @return boolean
     */
    public function getIsAccountNonExpired()
    {
        return $this->isAccountNonExpired;
    }

    /**
     * Set defaultStore
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Store $defaultStore
     *
     * @return User
     */
    public function setDefaultStore(\Jumpersoft\EcommerceBundle\Entity\Store $defaultStore = null)
    {
        $this->defaultStore = $defaultStore;

        return $this;
    }

    /**
     * Get defaultStore
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Store
     */
    public function getDefaultStore()
    {
        return $this->defaultStore;
    }

    /**
     * Add store
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Store $store
     *
     * @return User
     */
    public function addStore(\Jumpersoft\EcommerceBundle\Entity\Store $store)
    {
        $this->companies[] = $store;

        return $this;
    }

    /**
     * Remove store
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Store $store
     */
    public function removeStore(\Jumpersoft\EcommerceBundle\Entity\Store $store)
    {
        $this->companies->removeElement($store);
    }

    /**
     * Get companies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStores()
    {
        return $this->companies;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set mothersLastName
     *
     * @param string $mothersLastName
     *
     * @return User
     */
    public function setMothersLastName($mothersLastName)
    {
        $this->mothersLastName = $mothersLastName;

        return $this;
    }

    /**
     * Get mothersLastName
     *
     * @return string
     */
    public function getMothersLastName()
    {
        return $this->mothersLastName;
    }

    /**
     * Set alternateEmail
     *
     * @param string $alternateEmail
     *
     * @return User
     */
    public function setAlternateEmail($alternateEmail)
    {
        $this->alternateEmail = $alternateEmail;

        return $this;
    }

    /**
     * Get alternateEmail
     *
     * @return string
     */
    public function getAlternateEmail()
    {
        return $this->alternateEmail;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return User
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return User
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set userConfirmationDate
     *
     * @param \DateTime $userConfirmationDate
     *
     * @return User
     */
    public function setUserConfirmationDate($userConfirmationDate)
    {
        $this->userConfirmationDate = $userConfirmationDate;

        return $this;
    }

    /**
     * Get userConfirmationDate
     *
     * @return \DateTime
     */
    public function getUserConfirmationDate()
    {
        return $this->userConfirmationDate;
    }

    /**
     * Set signupCode
     *
     * @param string $signupCode
     *
     * @return User
     */
    public function setSignupCode($signupCode)
    {
        $this->signupCode = $signupCode;

        return $this;
    }

    /**
     * Get signupCode
     *
     * @return string
     */
    public function getSignupCode()
    {
        return $this->signupCode;
    }

    /**
     * Set lastAccessDate
     *
     * @param \DateTime $lastAccessDate
     *
     * @return User
     */
    public function setLastAccessDate($lastAccessDate)
    {
        $this->lastAccessDate = $lastAccessDate;

        return $this;
    }

    /**
     * Get lastAccessDate
     *
     * @return \DateTime
     */
    public function getLastAccessDate()
    {
        return $this->lastAccessDate;
    }

    /**
     * Set passwordRecoveryCode
     *
     * @param string $passwordRecoveryCode
     *
     * @return User
     */
    public function setPasswordRecoveryCode($passwordRecoveryCode)
    {
        $this->passwordRecoveryCode = $passwordRecoveryCode;

        return $this;
    }

    /**
     * Get passwordRecoveryCode
     *
     * @return string
     */
    public function getPasswordRecoveryCode()
    {
        return $this->passwordRecoveryCode;
    }

    /**
     * Set lastRequestPasswordRecoveryDate
     *
     * @param \DateTime $lastRequestPasswordRecoveryDate
     *
     * @return User
     */
    public function setLastRequestPasswordRecoveryDate($lastRequestPasswordRecoveryDate)
    {
        $this->lastRequestPasswordRecoveryDate = $lastRequestPasswordRecoveryDate;

        return $this;
    }

    /**
     * Get lastRequestPasswordRecoveryDate
     *
     * @return \DateTime
     */
    public function getLastRequestPasswordRecoveryDate()
    {
        return $this->lastRequestPasswordRecoveryDate;
    }

    /**
     * Set lastPasswordRecoveryDate
     *
     * @param \DateTime $lastPasswordRecoveryDate
     *
     * @return User
     */
    public function setLastPasswordRecoveryDate($lastPasswordRecoveryDate)
    {
        $this->lastPasswordRecoveryDate = $lastPasswordRecoveryDate;

        return $this;
    }

    /**
     * Get lastPasswordRecoveryDate
     *
     * @return \DateTime
     */
    public function getLastPasswordRecoveryDate()
    {
        return $this->lastPasswordRecoveryDate;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return User
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Add address
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Address $address
     *
     * @return User
     */
    public function addAddress(\Jumpersoft\EcommerceBundle\Entity\Address $address)
    {
        $this->addresses[] = $address;

        return $this;
    }

    /**
     * Remove address
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Address $address
     */
    public function removeAddress(\Jumpersoft\EcommerceBundle\Entity\Address $address)
    {
        $this->addresses->removeElement($address);
    }

    /**
     * Get addresses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Set defaultShippingAddress
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Address $defaultShippingAddress
     *
     * @return User
     */
    public function setDefaultShippingAddress(\Jumpersoft\EcommerceBundle\Entity\Address $defaultShippingAddress = null)
    {
        $this->defaultShippingAddress = $defaultShippingAddress;

        return $this;
    }

    /**
     * Get defaultShippingAddress
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Address
     */
    public function getDefaultShippingAddress()
    {
        return $this->defaultShippingAddress;
    }

    /**
     * Set suscriptions
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\UserSuscription $suscriptions
     *
     * @return User
     */
    public function setSuscriptions(\Jumpersoft\EcommerceBundle\Entity\UserSuscription $suscriptions = null)
    {
        $this->suscriptions = $suscriptions;

        return $this;
    }

    /**
     * Get suscriptions
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\UserSuscription
     */
    public function getSuscriptions()
    {
        return $this->suscriptions;
    }

    /**
     * Add card.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\UserCard $card
     *
     * @return User
     */
    public function addCard(\Jumpersoft\EcommerceBundle\Entity\UserCard $card)
    {
        $this->cards[] = $card;

        return $this;
    }

    /**
     * Remove card.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\UserCard $card
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCard(\Jumpersoft\EcommerceBundle\Entity\UserCard $card)
    {
        return $this->cards->removeElement($card);
    }

    /**
     * Get cards.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCards()
    {
        return $this->cards;
    }

    /**
     * Set gatewayUserId.
     *
     * @param string $gatewayUserId
     *
     * @return User
     */
    public function setGatewayUserId($gatewayUserId)
    {
        $this->gatewayUserId = $gatewayUserId;

        return $this;
    }

    /**
     * Get gatewayUserId.
     *
     * @return string
     */
    public function getGatewayUserId()
    {
        return $this->gatewayUserId;
    }

    /**
     * Set rules.
     *
     * @param array|null $rules
     *
     * @return Item
     */
    public function setRules($rules = null)
    {
        $this->rules = $rules;

        return $this;
    }

    /**
     * Get rules.
     *
     * @return array|null
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * Get licences.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLicences()
    {
        return $this->cards;
    }

    /**
     * Set tradeName.
     *
     * @param string|null $tradeName
     *
     * @return User
     */
    public function setTradeName($tradeName = null)
    {
        $this->tradeName = $tradeName;

        return $this;
    }

    /**
     * Get tradeName.
     *
     * @return string|null
     */
    public function getTradeName()
    {
        return $this->tradeName;
    }

    /**
     * Set rfc.
     *
     * @param string|null $rfc
     *
     * @return User
     */
    public function setRfc($rfc = null)
    {
        $this->rfc = $rfc;
        return $this;
    }

    /**
     * Get rfc.
     *
     * @return string|null
     */
    public function getRfc()
    {
        return $this->rfc;
    }

    /**
     * Add priceList.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\PriceListCustomer $priceList
     *
     * @return User
     */
    public function addPriceList(\Jumpersoft\EcommerceBundle\Entity\PriceListCustomer $priceList)
    {
        $this->priceList[] = $priceList;

        return $this;
    }

    /**
     * Remove priceList.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\PriceListCustomer $priceList
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePriceList(\Jumpersoft\EcommerceBundle\Entity\PriceListCustomer $priceList)
    {
        return $this->priceList->removeElement($priceList);
    }

    /**
     * Get priceList.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPriceList()
    {
        return $this->priceList;
    }

    /**
     * Add licence.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\UserLicence $licence
     *
     * @return User
     */
    public function addLicence(\Jumpersoft\EcommerceBundle\Entity\UserLicence $licence)
    {
        $this->licences[] = $licence;

        return $this;
    }

    /**
     * Remove licence.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\UserLicence $licence
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeLicence(\Jumpersoft\EcommerceBundle\Entity\UserLicence $licence)
    {
        return $this->licences->removeElement($licence);
    }

    /**
     * Set entityType.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Type $entityType
     *
     * @return User
     */
    public function setEntityType(\Jumpersoft\EcommerceBundle\Entity\Type $entityType)
    {
        $this->entityType = $entityType;

        return $this;
    }

    /**
     * Get entityType.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Type
     */
    public function getEntityType()
    {
        return $this->entityType;
    }


    /**
     * Set defaultBillingAddress.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Address|null $defaultBillingAddress
     *
     * @return User
     */
    public function setDefaultBillingAddress(\Jumpersoft\EcommerceBundle\Entity\Address $defaultBillingAddress = null)
    {
        $this->defaultBillingAddress = $defaultBillingAddress;

        return $this;
    }

    /**
     * Get defaultBillingAddress.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Address|null
     */
    public function getDefaultBillingAddress()
    {
        return $this->defaultBillingAddress;
    }
}
