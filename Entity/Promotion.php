<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\PromotionRepository")
 * @ORM\Table(name="Promotion", indexes={@ORM\Index(name="search_idx", columns={"code", "discount"})})
 *
 * @author Angel
 */
class Promotion extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=23)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="code", length=20, nullable=FALSE)
     */
    protected $code;

    /**
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumn(name="storeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $store;

    /**
     * Name of promotion
     *
     * @ORM\Column(type="string", name="name", length=2000, nullable=FALSE)
     */
    protected $name;

    /**
     * Description
     *
     * @ORM\Column(type="string", name="description", length=2000, nullable=TRUE)
     */
    protected $description;

    /**
     * Terms and conditions of promotion
     *
     * @ORM\Column(type="string", name="terms", length=2000, nullable=TRUE)
     */
    protected $terms;

    /**
     * @ORM\Column(type="datetime", name="startDate", nullable=FALSE)
     */
    protected $startDate;

    /**
     * @ORM\Column(type="datetime", name="endDate", nullable=FALSE)
     */
    protected $endDate;

    /** START COUPON DISCOUNT DETAILS */

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="typeId", referencedColumnName="id", nullable=false)
     */
    protected $type;

    /**
     * @ORM\Column(type="string", name="discountType", length=1, nullable=TRUE)
     */
    protected $discountType;

    /**
     * @ORM\Column(type="decimal", name="discount", precision=18, scale=2, nullable=TRUE)
     */
    protected $discount;

    /** END COUPON DISCOUNT DETAILS */
    /** START COUPON RESTRICTIONS */

    /**
     * Desc: the minimum amount a customer must spend in one order to be able to apply the coupon
     *
     * @ORM\Column(type="decimal", name="minimumPurchase", precision=16, scale=4, nullable=TRUE)
     */
    protected $minimumPurchase;

    /**
     * Desc: limits how many times a single customer (based on customer ID or email address) can use the coupon
     *
     * @ORM\Column(type="integer", name="limitTotalNumberUses", nullable=TRUE)
     */
    protected $limitTotalNumberUses;

    /**
     * Desc: limits how many times a single customer (based on customer ID or email address) can use the coupon
     *
     * @ORM\Column(type="integer", name="limitNumberUsesPerCustomer", nullable=TRUE)
     */
    protected $limitNumberUsesPerCustomer;

    /**
     * @ORM\Column(type="boolean", name="worksWithOtherPromotions", nullable=FALSE)
     */
    protected $worksWithOtherPromotions;

    /** END COUPON RESTRICTIONS */

    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id", nullable=FALSE)
     */
    protected $status;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    /**
     * @ORM\Column(type="datetime", name="updateDate", nullable=TRUE)
     */
    protected $updateDate;

    /**
     * @ORM\OneToMany(targetEntity="PromotionItem", mappedBy="promotion")
     */
    protected $items;

    /**
     * @ORM\OneToMany(targetEntity="OrderRecordPromotion", mappedBy="promotion")
     */
    protected $orderRecords;

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->orderRecords = new ArrayCollection();
        $this->worksWithOtherPromotions = false;
    }

    

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return Promotion
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Promotion
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Promotion
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Promotion
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set terms.
     *
     * @param string|null $terms
     *
     * @return Promotion
     */
    public function setTerms($terms = null)
    {
        $this->terms = $terms;

        return $this;
    }

    /**
     * Get terms.
     *
     * @return string|null
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return Promotion
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime $endDate
     *
     * @return Promotion
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set discountType.
     *
     * @param string|null $discountType
     *
     * @return Promotion
     */
    public function setDiscountType($discountType = null)
    {
        $this->discountType = $discountType;

        return $this;
    }

    /**
     * Get discountType.
     *
     * @return string|null
     */
    public function getDiscountType()
    {
        return $this->discountType;
    }

    /**
     * Set discount.
     *
     * @param string|null $discount
     *
     * @return Promotion
     */
    public function setDiscount($discount = null)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount.
     *
     * @return string|null
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set minimumPurchase.
     *
     * @param string|null $minimumPurchase
     *
     * @return Promotion
     */
    public function setMinimumPurchase($minimumPurchase = null)
    {
        $this->minimumPurchase = $minimumPurchase;

        return $this;
    }

    /**
     * Get minimumPurchase.
     *
     * @return string|null
     */
    public function getMinimumPurchase()
    {
        return $this->minimumPurchase;
    }

    /**
     * Set limitTotalNumberUses.
     *
     * @param int|null $limitTotalNumberUses
     *
     * @return Promotion
     */
    public function setLimitTotalNumberUses($limitTotalNumberUses = null)
    {
        $this->limitTotalNumberUses = $limitTotalNumberUses;

        return $this;
    }

    /**
     * Get limitTotalNumberUses.
     *
     * @return int|null
     */
    public function getLimitTotalNumberUses()
    {
        return $this->limitTotalNumberUses;
    }

    /**
     * Set limitNumberUsesPerCustomer.
     *
     * @param int|null $limitNumberUsesPerCustomer
     *
     * @return Promotion
     */
    public function setLimitNumberUsesPerCustomer($limitNumberUsesPerCustomer = null)
    {
        $this->limitNumberUsesPerCustomer = $limitNumberUsesPerCustomer;

        return $this;
    }

    /**
     * Get limitNumberUsesPerCustomer.
     *
     * @return int|null
     */
    public function getLimitNumberUsesPerCustomer()
    {
        return $this->limitNumberUsesPerCustomer;
    }

    /**
     * Set worksWithOtherPromotions.
     *
     * @param bool $worksWithOtherPromotions
     *
     * @return Promotion
     */
    public function setWorksWithOtherPromotions($worksWithOtherPromotions)
    {
        $this->worksWithOtherPromotions = $worksWithOtherPromotions;

        return $this;
    }

    /**
     * Get worksWithOtherPromotions.
     *
     * @return bool
     */
    public function getWorksWithOtherPromotions()
    {
        return $this->worksWithOtherPromotions;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return Promotion
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set updateDate.
     *
     * @param \DateTime|null $updateDate
     *
     * @return Promotion
     */
    public function setUpdateDate($updateDate = null)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate.
     *
     * @return \DateTime|null
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set store.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Store $store
     *
     * @return Promotion
     */
    public function setStore(\Jumpersoft\EcommerceBundle\Entity\Store $store)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set type.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Type $type
     *
     * @return Promotion
     */
    public function setType(\Jumpersoft\EcommerceBundle\Entity\Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Status $status
     *
     * @return Promotion
     */
    public function setStatus(\Jumpersoft\EcommerceBundle\Entity\Status $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add item.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\PromotionItem $item
     *
     * @return Promotion
     */
    public function addItem(\Jumpersoft\EcommerceBundle\Entity\PromotionItem $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\PromotionItem $item
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeItem(\Jumpersoft\EcommerceBundle\Entity\PromotionItem $item)
    {
        return $this->items->removeElement($item);
    }

    /**
     * Get items.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Add orderRecord.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\OrderRecordPromotion $orderRecord
     *
     * @return Promotion
     */
    public function addOrderRecord(\Jumpersoft\EcommerceBundle\Entity\OrderRecordPromotion $orderRecord)
    {
        $this->orderRecords[] = $orderRecord;

        return $this;
    }

    /**
     * Remove orderRecord.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\OrderRecordPromotion $orderRecord
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeOrderRecord(\Jumpersoft\EcommerceBundle\Entity\OrderRecordPromotion $orderRecord)
    {
        return $this->orderRecords->removeElement($orderRecord);
    }

    /**
     * Get orderRecords.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderRecords()
    {
        return $this->orderRecords;
    }
}
