<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="ShoppingCartPromotion")
 *
 * @author Angel
 */
class ShoppingCartPromotion extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="ShoppingCart", inversedBy="promotions")
     * @ORM\JoinColumn(name="shoppingCartId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $shoppingCart;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Promotion")
     * @ORM\JoinColumn(name="promotionId", referencedColumnName="id", nullable=TRUE)
     */
    protected $promotion;


    /**
     * Set shoppingCart
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ShoppingCart $shoppingCart
     *
     * @return ShoppingCartPromotion
     */
    public function setShoppingCart(\Jumpersoft\EcommerceBundle\Entity\ShoppingCart $shoppingCart)
    {
        $this->shoppingCart = $shoppingCart;

        return $this;
    }

    /**
     * Get shoppingCart
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\ShoppingCart
     */
    public function getShoppingCart()
    {
        return $this->shoppingCart;
    }

    /**
     * Set promotion
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Promotion $promotion
     *
     * @return ShoppingCartPromotion
     */
    public function setPromotion(\Jumpersoft\EcommerceBundle\Entity\Promotion $promotion)
    {
        $this->promotion = $promotion;

        return $this;
    }

    /**
     * Get promotion
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Promotion
     */
    public function getPromotion()
    {
        return $this->promotion;
    }
}
