<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\InventoryRepository")
 * @ORM\Table(name="Inventory", uniqueConstraints={@ORM\UniqueConstraint(columns={"lot","noEco"})})
 * @UniqueEntity(
 *     fields={"lot", "noEco"},
 *     message="The lot and economic number has already in use."
 * )
 * @author Angel
 */
class Inventory extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $item;

    /**
     * @ORM\ManyToOne(targetEntity="Inventory", inversedBy="childs")
     * @ORM\JoinColumn(name="parentId", referencedColumnName="id", nullable=TRUE)
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="Inventory", mappedBy="parent")
     */
    protected $childs;

    /**
     * @ORM\OneToMany(targetEntity="InventoryCompositeItem", mappedBy="inventory")f
     */
    protected $inventoryCompositeItems;

    /**
     * @ORM\Column(type="datetime", name="eventDate", nullable=FALSE)
     */
    protected $eventDate;

    /**
     * @ORM\Column(type="datetime", name="expirationDate", nullable=TRUE)
     */
    protected $expirationDate;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    /**
     * @ORM\Column(type="decimal", name="quantityReceived", precision=20, scale=4, nullable=FALSE)
     */
    protected $quantityReceived;

    /**
     * @ORM\Column(type="decimal", name="quantityOutput", precision=20, scale=4, nullable=FALSE)
     */
    protected $quantityOutput;

    /**
     * @ORM\Column(type="decimal", name="stock", precision=20, scale=4, nullable=FALSE)
     */
    protected $stock;

    /**
     * @ORM\Column(type="string", name="lot", length=255, nullable=TRUE)
     */
    protected $lot;

    /**
     * @ORM\Column(type="smallint", name="noEco", nullable=TRUE)
     */
    protected $noEco;

    /**
     * @ORM\Column(type="string", name="serial", length=255, nullable=TRUE, unique=true)
     */
    protected $serial;

    /**
     * @ORM\Column(type="string", name="notes", length=1000, nullable=TRUE)
     */
    protected $notes;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="typeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="operationId", referencedColumnName="id", nullable=FALSE)
     */
    protected $operation;

    /**
     * @ORM\ManyToOne(targetEntity="OrderRecordItemInventory")
     * @ORM\JoinColumn(name="orderRecordItemInventoryId", referencedColumnName="id", nullable=TRUE, onDelete="CASCADE")
     */
    protected $orderRecordItemInventory;
    
    /**
     * @ORM\ManyToOne(targetEntity="InventoryCompositeItemInventory", inversedBy="inventoryOuts")
     * @ORM\JoinColumn(name="inventoryCompositeItemInventoryId", referencedColumnName="id", nullable=TRUE)
     */
    protected $inventoryCompositeItemInventory;

    /** RADIOACTIVE DATA */

    /**
     * @ORM\Column(type="datetime", name="calibrationDate", nullable=TRUE)
     */
    protected $calibrationDate;
    
    /**
     * @ORM\Column(type="decimal", name="volumeReceived", precision=20, scale=4, nullable=true)
     */
    protected $volumeReceived;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->childs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->inventoryCompositeItems = new \Doctrine\Common\Collections\ArrayCollection();
        $this->quantityOutput = 0;
        $this->quantityReceived = 0;
        $this->stock = 0;
    }

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return Inventory
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventDate.
     *
     * @param \DateTime $eventDate
     *
     * @return Inventory
     */
    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * Get eventDate.
     *
     * @return \DateTime
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * Set expirationDate.
     *
     * @param \DateTime|null $expirationDate
     *
     * @return Inventory
     */
    public function setExpirationDate($expirationDate = null)
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    /**
     * Get expirationDate.
     *
     * @return \DateTime|null
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return Inventory
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set quantityReceived.
     *
     * @param string $quantityReceived
     *
     * @return Inventory
     */
    public function setQuantityReceived($quantityReceived)
    {
        $this->quantityReceived = $quantityReceived;

        return $this;
    }

    /**
     * Get quantityReceived.
     *
     * @return string
     */
    public function getQuantityReceived()
    {
        return $this->quantityReceived;
    }

    /**
     * Set quantityOutput.
     *
     * @param string $quantityOutput
     *
     * @return Inventory
     */
    public function setQuantityOutput($quantityOutput)
    {
        $this->quantityOutput = $quantityOutput;

        return $this;
    }

    /**
     * Get quantityOutput.
     *
     * @return string
     */
    public function getQuantityOutput()
    {
        return $this->quantityOutput;
    }

    /**
     * Set stock.
     *
     * @param string $stock
     *
     * @return Inventory
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock.
     *
     * @return string
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set lot.
     *
     * @param string|null $lot
     *
     * @return Inventory
     */
    public function setLot($lot = null)
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * Get lot.
     *
     * @return string|null
     */
    public function getLot()
    {
        return $this->lot;
    }

    /**
     * Set noEco.
     *
     * @param int|null $noEco
     *
     * @return Inventory
     */
    public function setNoEco($noEco = null)
    {
        $this->noEco = $noEco;

        return $this;
    }

    /**
     * Get noEco.
     *
     * @return int|null
     */
    public function getNoEco()
    {
        return $this->noEco;
    }

    /**
     * Set serial.
     *
     * @param string|null $serial
     *
     * @return Inventory
     */
    public function setSerial($serial = null)
    {
        $this->serial = $serial;

        return $this;
    }

    /**
     * Get serial.
     *
     * @return string|null
     */
    public function getSerial()
    {
        return $this->serial;
    }

    /**
     * Set notes.
     *
     * @param string|null $notes
     *
     * @return Inventory
     */
    public function setNotes($notes = null)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes.
     *
     * @return string|null
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set calibrationDate.
     *
     * @param \DateTime|null $calibrationDate
     *
     * @return Inventory
     */
    public function setCalibrationDate($calibrationDate = null)
    {
        $this->calibrationDate = $calibrationDate;

        return $this;
    }

    /**
     * Get calibrationDate.
     *
     * @return \DateTime|null
     */
    public function getCalibrationDate()
    {
        return $this->calibrationDate;
    }

    /**
     * Set item.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Item $item
     *
     * @return Inventory
     */
    public function setItem(\Jumpersoft\EcommerceBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set parent.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Inventory|null $parent
     *
     * @return Inventory
     */
    public function setParent(\Jumpersoft\EcommerceBundle\Entity\Inventory $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Inventory|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Inventory $child
     *
     * @return Inventory
     */
    public function addChild(\Jumpersoft\EcommerceBundle\Entity\Inventory $child)
    {
        $this->childs[] = $child;

        return $this;
    }

    /**
     * Remove child.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Inventory $child
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChild(\Jumpersoft\EcommerceBundle\Entity\Inventory $child)
    {
        return $this->childs->removeElement($child);
    }

    /**
     * Get childs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChilds()
    {
        return $this->childs;
    }

    /**
     * Add inventoryCompositeItem.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\InventoryCompositeItem $inventoryCompositeItem
     *
     * @return Inventory
     */
    public function addInventoryCompositeItem(\Jumpersoft\EcommerceBundle\Entity\InventoryCompositeItem $inventoryCompositeItem)
    {
        $this->inventoryCompositeItems[] = $inventoryCompositeItem;

        return $this;
    }

    /**
     * Remove inventoryCompositeItem.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\InventoryCompositeItem $inventoryCompositeItem
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeInventoryCompositeItem(\Jumpersoft\EcommerceBundle\Entity\InventoryCompositeItem $inventoryCompositeItem)
    {
        return $this->inventoryCompositeItems->removeElement($inventoryCompositeItem);
    }

    /**
     * Get inventoryCompositeItems.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventoryCompositeItems()
    {
        return $this->inventoryCompositeItems;
    }

    /**
     * Set type.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Type $type
     *
     * @return Inventory
     */
    public function setType(\Jumpersoft\EcommerceBundle\Entity\Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set operation.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Type $operation
     *
     * @return Inventory
     */
    public function setOperation(\Jumpersoft\EcommerceBundle\Entity\Type $operation)
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * Get operation.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Type
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * Set orderRecordItemInventory.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\OrderRecordItemInventory|null $orderRecordItemInventory
     *
     * @return Inventory
     */
    public function setOrderRecordItemInventory(\Jumpersoft\EcommerceBundle\Entity\OrderRecordItemInventory $orderRecordItemInventory = null)
    {
        $this->orderRecordItemInventory = $orderRecordItemInventory;

        return $this;
    }

    /**
     * Get orderRecordItemInventory.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\OrderRecordItemInventory|null
     */
    public function getOrderRecordItemInventory()
    {
        return $this->orderRecordItemInventory;
    }

    /**
     * Set inventoryCompositeItemInventory.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\InventoryCompositeItemInventory|null $inventoryCompositeItemInventory
     *
     * @return Inventory
     */
    public function setInventoryCompositeItemInventory(\Jumpersoft\EcommerceBundle\Entity\InventoryCompositeItemInventory $inventoryCompositeItemInventory = null)
    {
        $this->inventoryCompositeItemInventory = $inventoryCompositeItemInventory;

        return $this;
    }

    /**
     * Get inventoryCompositeItemInventory.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\InventoryCompositeItemInventory|null
     */
    public function getInventoryCompositeItemInventory()
    {
        return $this->inventoryCompositeItemInventory;
    }

    /**
     * Set volumeReceived.
     *
     * @param string|null $volumeReceived
     *
     * @return Inventory
     */
    public function setVolumeReceived($volumeReceived = null)
    {
        $this->volumeReceived = $volumeReceived;

        return $this;
    }

    /**
     * Get volumeReceived.
     *
     * @return string|null
     */
    public function getVolumeReceived()
    {
        return $this->volumeReceived;
    }
}
