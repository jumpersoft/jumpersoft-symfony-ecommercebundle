<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\UserRepository")
 * @ORM\Table(name="UserLicenceIsotope", uniqueConstraints={@ORM\UniqueConstraint(columns={"licenceId","isotopeId"})})
 * @UniqueEntity(
 *     fields={"licence", "isotope"},
 *     message="This isotope is already in use on this licence"
 * )
 * @author Angel
 */
class UserLicenceIsotope extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserLicence", inversedBy="isotopes")
     * @ORM\JoinColumn(name="licenceId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $licence;

    /**
     * @ORM\ManyToOne(targetEntity="Isotope")
     * @ORM\JoinColumn(name="isotopeId", referencedColumnName="id",  nullable=FALSE)
     */
    protected $isotope;

    /**
     * @ORM\Column(type="decimal", name="activityByDay", precision=16, scale=4, nullable=FALSE)
     */
    protected $activityByDay;

    /**
     * @ORM\ManyToOne(targetEntity="UnitMeasure")
     * @ORM\JoinColumn(name="unitMeasureId", referencedColumnName="id", nullable=false)
     */
    protected $unitMeasure;

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return UserLicenceIsotope
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activityByDay.
     *
     * @param string|null $activityByDay
     *
     * @return UserLicenceIsotope
     */
    public function setActivityByDay($activityByDay = null)
    {
        $this->activityByDay = $activityByDay;

        return $this;
    }

    /**
     * Get activityByDay.
     *
     * @return string|null
     */
    public function getActivityByDay()
    {
        return $this->activityByDay;
    }

    /**
     * Set licence.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\UserLicence $licence
     *
     * @return UserLicenceIsotope
     */
    public function setLicence(\Jumpersoft\EcommerceBundle\Entity\UserLicence $licence)
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\UserLicence
     */
    public function getLicence()
    {
        return $this->licence;
    }

    /**
     * Set isotope.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Isotope $isotope
     *
     * @return UserLicenceIsotope
     */
    public function setIsotope(\Jumpersoft\EcommerceBundle\Entity\Isotope $isotope)
    {
        $this->isotope = $isotope;

        return $this;
    }

    /**
     * Get isotope.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Isotope
     */
    public function getIsotope()
    {
        return $this->isotope;
    }

    /**
     * Set unitMeasure.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\UnitMeasure $unitMeasure
     *
     * @return UserLicenceIsotope
     */
    public function setUnitMeasure(\Jumpersoft\EcommerceBundle\Entity\UnitMeasure $unitMeasure)
    {
        $this->unitMeasure = $unitMeasure;

        return $this;
    }

    /**
     * Get unitMeasure.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\UnitMeasure
     */
    public function getUnitMeasure()
    {
        return $this->unitMeasure;
    }
}
