<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\ShipmentRepository")
 * @ORM\Table(name="Shipment")
 *
 * @author Angel
 */
class Shipment extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * FORMAT: yyyy-mm-dd-hhMM
     * y=year, m=month, d=day, h=hour, M=minute
     * @ORM\Column(type="string", length=50, name="folio", unique=true, nullable=FALSE)
     */
    protected $folio;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="typeId", referencedColumnName="id", nullable=false)
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="methodId", referencedColumnName="id", nullable=false)
     */
    protected $method;

    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id", nullable=false)
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="customerId", referencedColumnName="id", nullable=FALSE)
     */
    protected $customer;

    /**
     * @ORM\Column(type="json", name="shippingAddress", nullable=TRUE)
     */
    protected $shippingAddress;

    /**
     * @ORM\Column(type="datetime", name="date", nullable=FALSE)
     */
    protected $date;

    /**
     * @ORM\ManyToOne(targetEntity="ShipmentSchedule", inversedBy="shipments")
     * @ORM\JoinColumn(name="shipmentScheduleId", referencedColumnName="id", nullable=TRUE)
     */
    protected $schedule;

    /**
     * @ORM\Column(type="string", name="notes", length=1000, nullable=TRUE)
     */
    protected $notes;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    /**
     * @ORM\OneToMany(targetEntity="ShipmentItem", mappedBy="shipment")
     */
    protected $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }




    /**
     * Set id.
     *
     * @param string $id
     *
     * @return Shipment
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set folio.
     *
     * @param string $folio
     *
     * @return Shipment
     */
    public function setFolio($folio)
    {
        $this->folio = $folio;

        return $this;
    }

    /**
     * Get folio.
     *
     * @return string
     */
    public function getFolio()
    {
        return $this->folio;
    }

    /**
     * Set shippingAddress.
     *
     * @param json|null $shippingAddress
     *
     * @return Shipment
     */
    public function setShippingAddress($shippingAddress = null)
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    /**
     * Get shippingAddress.
     *
     * @return json|null
     */
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Shipment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set notes.
     *
     * @param string|null $notes
     *
     * @return Shipment
     */
    public function setNotes($notes = null)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes.
     *
     * @return string|null
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return Shipment
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set type.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Type $type
     *
     * @return Shipment
     */
    public function setType(\Jumpersoft\EcommerceBundle\Entity\Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set method.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Type $method
     *
     * @return Shipment
     */
    public function setMethod(\Jumpersoft\EcommerceBundle\Entity\Type $method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Type
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set status.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Status $status
     *
     * @return Shipment
     */
    public function setStatus(\Jumpersoft\EcommerceBundle\Entity\Status $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set customer.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User $customer
     *
     * @return Shipment
     */
    public function setCustomer(\Jumpersoft\EcommerceBundle\Entity\User $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set schedule.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ShipmentSchedule|null $schedule
     *
     * @return Shipment
     */
    public function setSchedule(\Jumpersoft\EcommerceBundle\Entity\ShipmentSchedule $schedule = null)
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Get schedule.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\ShipmentSchedule|null
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * Add item.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ShipmentItem $item
     *
     * @return Shipment
     */
    public function addItem(\Jumpersoft\EcommerceBundle\Entity\ShipmentItem $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ShipmentItem $item
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeItem(\Jumpersoft\EcommerceBundle\Entity\ShipmentItem $item)
    {
        return $this->items->removeElement($item);
    }

    /**
     * Get items.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }
}
