<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\StoreViewRepository")
 * @ORM\Table(name="StoreView", uniqueConstraints={@ORM\UniqueConstraint(columns={"typeId","noEco"})})
 * @UniqueEntity(
 *     fields={"type", "noEco"},
 *     message="The type and economic number has already in use."
 * )
 *
 * @author Angel
 */
class StoreView extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="StoreViewVersion", inversedBy="views")
     * @ORM\JoinColumn(name="versionId", referencedColumnName="id", nullable=FALSE)
     */
    protected $version;

    /**
     * @ORM\ManyToOne(targetEntity="StoreViewType")
     * @ORM\JoinColumn(name="typeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $type;

    /**
     * @ORM\Column(type="smallint", name="noEco", nullable=TRUE)
     */
    protected $noEco;

    /**
     * @ORM\Column(type="string", name="description", length=255, nullable=TRUE)
     */
    protected $description;

    /**
     * @ORM\Column(type="boolean", name="active", nullable=TRUE)
     */
    protected $active;

    /**
     * @ORM\Column(type="smallint", name="sequence", nullable=TRUE)
     */
    protected $sequence;

    /**
     * @ORM\OneToMany(targetEntity="StoreViewCategory", mappedBy="storeView")
     */
    protected $categories;

    /**
     * @ORM\OneToMany(targetEntity="StoreViewItem", mappedBy="storeView")
     */
    protected $items;
    
    /**
     * @ORM\OneToMany(targetEntity="StoreViewSubGroup", mappedBy="storeView")
     */
    protected $subGroups;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->items = new ArrayCollection();
        $this->subGroups = new ArrayCollection();
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNoEco(): ?int
    {
        return $this->noEco;
    }

    public function setNoEco(?int $noEco): self
    {
        $this->noEco = $noEco;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(?int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }

    public function getVersion(): ?StoreViewVersion
    {
        return $this->version;
    }

    public function setVersion(?StoreViewVersion $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getType(): ?StoreViewType
    {
        return $this->type;
    }

    public function setType(?StoreViewType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|StoreViewCategory[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(StoreViewCategory $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->setStoreView($this);
        }

        return $this;
    }

    public function removeCategory(StoreViewCategory $category): self
    {
        if ($this->categories->removeElement($category)) {
            // set the owning side to null (unless already changed)
            if ($category->getStoreView() === $this) {
                $category->setStoreView(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StoreViewItem[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(StoreViewItem $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setStoreView($this);
        }

        return $this;
    }

    public function removeItem(StoreViewItem $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getStoreView() === $this) {
                $item->setStoreView(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StoreViewSubGroup[]
     */
    public function getSubGroups(): Collection
    {
        return $this->subGroups;
    }

    public function addSubGroup(StoreViewSubGroup $subGroup): self
    {
        if (!$this->subGroups->contains($subGroup)) {
            $this->subGroups[] = $subGroup;
            $subGroup->setStoreView($this);
        }

        return $this;
    }

    public function removeSubGroup(StoreViewSubGroup $subGroup): self
    {
        if ($this->subGroups->removeElement($subGroup)) {
            // set the owning side to null (unless already changed)
            if ($subGroup->getStoreView() === $this) {
                $subGroup->setStoreView(null);
            }
        }

        return $this;
    }
}
