<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\ItemReviewRepository")
 * @ORM\Table(name="ItemReview")
 *
 * @author Angel
 */
class ItemReview extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE", nullable=FALSE)
     */
    protected $item;

    /**
     * @ORM\Column(type="string", name="title", length=255, nullable=FALSE)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", name="review", length=1000, nullable=FALSE)
     */
    protected $review;

    /**
     * @ORM\Column(type="smallint", name="rating", nullable=FALSE)
     */
    protected $rating;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", nullable=FALSE)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id", nullable=FALSE)
     */
    protected $status;

    /**
     *
     * @ORM\Column(type="datetime", name="date", nullable=FALSE)
     */
    protected $date;

    /** INICIAN FUNCIONES ESPECIALES NO BORRAR */
    public function __construct()
    {
    }


    /**
     * Set id
     *
     * @param string $id
     *
     * @return ItemReview
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ItemReview
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set review
     *
     * @param string $review
     *
     * @return ItemReview
     */
    public function setReview($review)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return string
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return ItemReview
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ItemReview
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set item
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Item $item
     *
     * @return ItemReview
     */
    public function setItem(\Jumpersoft\EcommerceBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set user
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User $user
     *
     * @return ItemReview
     */
    public function setUser(\Jumpersoft\EcommerceBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set status
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Status $status
     *
     * @return ItemReview
     */
    public function setStatus(\Jumpersoft\EcommerceBundle\Entity\Status $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }
}
