<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\TransactionRepository")
 * @ORM\Table(name="OperationType")
 *
 * @author Angel
 *
 * NOTA: ESTA TABLA NO DEBE EXISTIR, ESTOS VALORES SE DEBEN IR A LA TABLA TYPE, MANEJAR LOS IDS: GAT_OPE_IN y GAT_OPE_OUT
 */
class OperationType
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", unique=true, nullable=FALSE)
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", name="name", length=255, nullable=FALSE)
     */
    private $name;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return OperationType
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return OperationType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
