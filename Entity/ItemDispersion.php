<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\ItemRepository")
 * @ORM\Table(name="ItemDispersion", uniqueConstraints={@ORM\UniqueConstraint(columns={"itemId","itemRelatedId"})})
 * @UniqueEntity(
 *     fields={"item", "itemRelated"},
 *     message="This item related is already in use on this item."
 * )
 *
 * @author Angel
 */
class ItemDispersion extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="itemDispersions")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $item;

    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="itemDispersionsRelated")
     * @ORM\JoinColumn(name="itemRelatedId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $itemRelated;

    /**
     * @ORM\Column(type="decimal", name="quantityToDisperse", precision=16, scale=4, nullable=TRUE)
     */
    protected $quantityToDisperse;

    /**
     * @ORM\Column(type="smallint", name="sequence", nullable=TRUE)
     */
    protected $sequence;

    public function __construct()
    {
    }

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return ItemDispersion
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sequence.
     *
     * @param int|null $sequence
     *
     * @return ItemDispersion
     */
    public function setSequence($sequence = null)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence.
     *
     * @return int|null
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set item.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Item $item
     *
     * @return ItemDispersion
     */
    public function setItem(\Jumpersoft\EcommerceBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set itemRelated.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Item $itemRelated
     *
     * @return ItemDispersion
     */
    public function setItemRelated(\Jumpersoft\EcommerceBundle\Entity\Item $itemRelated)
    {
        $this->itemRelated = $itemRelated;

        return $this;
    }

    /**
     * Get itemRelated.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Item
     */
    public function getItemRelated()
    {
        return $this->itemRelated;
    }

    /**
     * Set quantityToDisperse.
     *
     * @param string|null $quantityToDisperse
     *
     * @return ItemDispersion
     */
    public function setQuantityToDisperse($quantityToDisperse = null)
    {
        $this->quantityToDisperse = $quantityToDisperse;

        return $this;
    }

    /**
     * Get quantityToDisperse.
     *
     * @return string|null
     */
    public function getQuantityToDisperse()
    {
        return $this->quantityToDisperse;
    }
}
