<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\PriceListRepository")
 * @ORM\Table(name="PriceList", indexes={@ORM\Index(name="search_idx", columns={"name"})})
 *
 * @author Angel
 */
class PriceList extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * Store
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumn(name="storeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $store;

    /**
     * @ORM\Column(type="string", name="name", length=255, nullable=FALSE)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", name="description", length=1000, nullable=TRUE)
     */
    protected $description;

    /**
     * @ORM\Column(type="json", name="customerGroup", nullable=TRUE, options={"default":"{}"})
     */
    protected $customerGroup;
    
    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $active;
    
    /**
     * @ORM\OneToMany(targetEntity="PriceListItem", mappedBy="priceList")
     */
    protected $items;
    
    /**
     * @ORM\OneToMany(targetEntity="PriceListCustomer", mappedBy="priceList")
     */
    protected $customers;
    
    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;
    
    /**
     * @ORM\Column(type="datetime", name="updateDate", nullable=TRUE)
     */
    protected $updateDate;

    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
        $this->customers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return PriceList
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return PriceList
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return PriceList
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set customerGroup.
     *
     * @param json $customerGroup
     *
     * @return PriceList
     */
    public function setCustomerGroup($customerGroup)
    {
        $this->customerGroup = $customerGroup;

        return $this;
    }

    /**
     * Get customerGroup.
     *
     * @return json
     */
    public function getCustomerGroup()
    {
        return $this->customerGroup;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return PriceList
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set store.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Store $store
     *
     * @return PriceList
     */
    public function setStore(\Jumpersoft\EcommerceBundle\Entity\Store $store)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Add item.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\PriceListItem $item
     *
     * @return PriceList
     */
    public function addItem(\Jumpersoft\EcommerceBundle\Entity\PriceListItem $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\PriceListItem $item
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeItem(\Jumpersoft\EcommerceBundle\Entity\PriceListItem $item)
    {
        return $this->items->removeElement($item);
    }

    /**
     * Get items.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set updateDate.
     *
     * @param \DateTime $updateDate
     *
     * @return PriceList
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate.
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set active.
     *
     * @param bool|null $active
     *
     * @return PriceList
     */
    public function setActive($active = null)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool|null
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add customer.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\PriceListCustomer $customer
     *
     * @return PriceList
     */
    public function addCustomer(\Jumpersoft\EcommerceBundle\Entity\PriceListCustomer $customer)
    {
        $this->customers[] = $customer;

        return $this;
    }

    /**
     * Remove customer.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\PriceListCustomer $customer
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCustomer(\Jumpersoft\EcommerceBundle\Entity\PriceListCustomer $customer)
    {
        return $this->customers->removeElement($customer);
    }

    /**
     * Get customers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomers()
    {
        return $this->customers;
    }
}
