<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\ShipmentRepository")
 * @ORM\Table(name="ShipmentItem")
 *
 * @author Angel
 */
class ShipmentItem extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Shipment", inversedBy="items")
     * @ORM\JoinColumn(name="shipmentId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $shipment;

    /**
     * @ORM\ManyToOne(targetEntity="OrderRecordItem", inversedBy="shipmentItems")
     * @ORM\JoinColumn(name="orderRecordItemId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE", unique=true)
     */
    protected $orderRecordItem;

    /**
     * @ORM\Column(type="decimal", name="quantity", precision=20, scale=4, nullable=FALSE)
     */
    protected $quantity;

    /**
     * @ORM\Column(type="string", name="notes", length=1000, nullable=TRUE)
     */
    protected $notes;

    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id", nullable=TRUE)
     */
    protected $status;

    /**
     * Fecha de registro del servicio
     *
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return ShipmentItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set notes.
     *
     * @param string|null $notes
     *
     * @return ShipmentItem
     */
    public function setNotes($notes = null)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes.
     *
     * @return string|null
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return ShipmentItem
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set shipment.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Shipment $shipment
     *
     * @return ShipmentItem
     */
    public function setShipment(\Jumpersoft\EcommerceBundle\Entity\Shipment $shipment)
    {
        $this->shipment = $shipment;

        return $this;
    }

    /**
     * Get shipment.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Shipment
     */
    public function getShipment()
    {
        return $this->shipment;
    }

    /**
     * Set orderRecordItem.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\OrderRecordItem $orderRecordItem
     *
     * @return ShipmentItem
     */
    public function setOrderRecordItem(\Jumpersoft\EcommerceBundle\Entity\OrderRecordItem $orderRecordItem)
    {
        $this->orderRecordItem = $orderRecordItem;

        return $this;
    }

    /**
     * Get orderRecordItem.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\OrderRecordItem
     */
    public function getOrderRecordItem()
    {
        return $this->orderRecordItem;
    }

    /**
     * Set status.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Status|null $status
     *
     * @return ShipmentItem
     */
    public function setStatus(\Jumpersoft\EcommerceBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Status|null
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Set quantity.
     *
     * @param string $quantity
     *
     * @return ShipmentItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
