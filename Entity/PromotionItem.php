<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\PromotionRepository")
 * @ORM\Table(name="PromotionItem")
 *
 * @author Angel
 */
class PromotionItem extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Promotion", inversedBy="items")
     * @ORM\JoinColumn(name="promotionId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $promotion;

    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="promotions")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $item;
    
    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;
    
    /** INICIAN FUNCIONES ESPECIALES NO BORRAR */
    public function __construct()
    {
    }

    /**
     * Set id
     *
     * @param string $id
     *
     * @return PromotionItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set promotion
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Promotion $promotion
     *
     * @return PromotionItem
     */
    public function setPromotion(\Jumpersoft\EcommerceBundle\Entity\Promotion $promotion)
    {
        $this->promotion = $promotion;

        return $this;
    }

    /**
     * Get promotion
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Promotion
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * Set item
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Item $item
     *
     * @return PromotionItem
     */
    public function setItem(\Jumpersoft\EcommerceBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime|null $registerDate
     *
     * @return PromotionItem
     */
    public function setRegisterDate($registerDate = null)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime|null
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }
}
