<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\AttributeRepository")
 * @ORM\Table(name="Attribute")
 *
 * @author Angel
 */
class Attribute extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AttributeSet", inversedBy="attributes")
     * @ORM\JoinColumn(name="attributeSetId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $attributeSet;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="typeId", referencedColumnName="id", nullable=false)
     */
    protected $type;

    /**
     * @ORM\Column(type="string", length=100, nullable=FALSE)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $code;

    /**
     * @ORM\Column(type="string", name="description", length=500, nullable=TRUE)
     */
    protected $description;

    /**
     * @ORM\Column(type="boolean", name="required", nullable=true, options={"default":"1"})
     */
    protected $required;

    /**
     * @ORM\Column(type="boolean", name="internalUse", nullable=true, options={"default":"0"})
     */
    protected $internalUse;

    /**
     * @ORM\Column(type="boolean", name="useRange", nullable=true, options={"default":"0"})
     */
    protected $useRange;

    /**
     * @ORM\Column(type="boolean", name="useMinCurrentDate", nullable=true, options={"default":"0"})
     */
    protected $useMinCurrentDate;

    /**
     * @ORM\Column(type="string", name="defaultOption", length=32, nullable=TRUE)
     */
    protected $defaultOption;

    /**
     * @ORM\Column(type="string", name="defaultText", length=100, nullable=TRUE)
     */
    protected $defaultText;

    /**
     * @ORM\Column(type="integer", name="defaultValue", nullable=TRUE)
     */
    protected $defaultValue;

    /**
     * @ORM\Column(type="decimal", name="defaultValueDecimal", precision=18, scale=2, nullable=TRUE)
     */
    protected $defaultValueDecimal;

    /**
     * @ORM\Column(type="integer", name="min", nullable=TRUE)
     */
    protected $min;

    /**
     * @ORM\Column(type="integer", name="max", nullable=TRUE)
     */
    protected $max;

    /**
     * @ORM\Column(type="decimal", name="minDecimal", precision=18, scale=2, nullable=TRUE)
     */
    protected $minDecimal;

    /**
     * @ORM\Column(type="decimal", name="maxDecimal", precision=18, scale=2, nullable=TRUE)
     */
    protected $maxDecimal;

    /**
     * @ORM\OneToMany(targetEntity="ItemAttribute", mappedBy="attribute")
     */
    protected $item;

    /**
     * @ORM\OneToMany(targetEntity="AttributeOption", mappedBy="attribute")
     */
    protected $options;

    /**
     * @ORM\Column(type="smallint", name="sequence", nullable=TRUE)
     */
    protected $sequence;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    public function __construct()
    {
        $this->options = new ArrayCollection();
        $this->item = new ArrayCollection();
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRequired(): ?bool
    {
        return $this->required;
    }

    public function setRequired(?bool $required): self
    {
        $this->required = $required;

        return $this;
    }

    public function getInternalUse(): ?bool
    {
        return $this->internalUse;
    }

    public function setInternalUse(?bool $internalUse): self
    {
        $this->internalUse = $internalUse;

        return $this;
    }

    public function getUseRange(): ?bool
    {
        return $this->useRange;
    }

    public function setUseRange(?bool $useRange): self
    {
        $this->useRange = $useRange;

        return $this;
    }

    public function getUseMinCurrentDate(): ?bool
    {
        return $this->useMinCurrentDate;
    }

    public function setUseMinCurrentDate(?bool $useMinCurrentDate): self
    {
        $this->useMinCurrentDate = $useMinCurrentDate;

        return $this;
    }

    public function getDefaultText(): ?string
    {
        return $this->defaultText;
    }

    public function setDefaultText(?string $defaultText): self
    {
        $this->defaultText = $defaultText;

        return $this;
    }

    public function getDefaultValue(): ?int
    {
        return $this->defaultValue;
    }

    public function setDefaultValue(?int $defaultValue): self
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    public function getDefaultValueDecimal(): ?string
    {
        return $this->defaultValueDecimal;
    }

    public function setDefaultValueDecimal(?string $defaultValueDecimal): self
    {
        $this->defaultValueDecimal = $defaultValueDecimal;

        return $this;
    }

    public function getMin(): ?int
    {
        return $this->min;
    }

    public function setMin(?int $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getMax(): ?int
    {
        return $this->max;
    }

    public function setMax(?int $max): self
    {
        $this->max = $max;

        return $this;
    }

    public function getMinDecimal(): ?string
    {
        return $this->minDecimal;
    }

    public function setMinDecimal(?string $minDecimal): self
    {
        $this->minDecimal = $minDecimal;

        return $this;
    }

    public function getMaxDecimal(): ?string
    {
        return $this->maxDecimal;
    }

    public function setMaxDecimal(?string $maxDecimal): self
    {
        $this->maxDecimal = $maxDecimal;

        return $this;
    }

    public function getRegisterDate(): ?\DateTimeInterface
    {
        return $this->registerDate;
    }

    public function setRegisterDate(\DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    public function getAttributeSet(): ?AttributeSet
    {
        return $this->attributeSet;
    }

    public function setAttributeSet(?AttributeSet $attributeSet): self
    {
        $this->attributeSet = $attributeSet;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|AttributeOption[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    public function addOption(AttributeOption $option): self
    {
        if (!$this->options->contains($option)) {
            $this->options[] = $option;
            $option->setAttribute($this);
        }

        return $this;
    }

    public function removeOption(AttributeOption $option): self
    {
        if ($this->options->removeElement($option)) {
            // set the owning side to null (unless already changed)
            if ($option->getAttribute() === $this) {
                $option->setAttribute(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItem(): Collection
    {
        return $this->item;
    }

    public function addItem(Item $item): self
    {
        if (!$this->item->contains($item)) {
            $this->item[] = $item;
            $item->setAttribute($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->item->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getAttribute() === $this) {
                $item->setAttribute(null);
            }
        }

        return $this;
    }

    public function getDefaultOption(): ?string
    {
        return $this->defaultOption;
    }

    public function setDefaultOption(?string $defaultOption): self
    {
        $this->defaultOption = $defaultOption;

        return $this;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(?int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }
}
