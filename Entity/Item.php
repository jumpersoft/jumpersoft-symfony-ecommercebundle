<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\ItemRepository")
 * @ORM\Table(name="Item", indexes={@ORM\Index(name="search_idx", columns={"name"})})
 *
 * @author Angel
 */
class Item extends JumpersoftModel
{
    /* --------------------------------------------------------------------------------------- */
    /* INICIAN CAMPOS GENERALES  */

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="typeId", referencedColumnName="id", nullable=false)
     */
    protected $type;

    /**
     * Store
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumn(name="storeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $store;

    /**
     * @ORM\Column(type="string", name="name", length=255, nullable=FALSE)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", name="description", length=1000, nullable=TRUE)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", name="shortDescription", length=255, nullable=TRUE)
     */
    protected $shortDescription;

    /**
     * @ORM\Column(type="string", name="specs", length=2000, nullable=TRUE)
     */
    protected $specs;

    /**
     * @ORM\Column(type="boolean", name="handleAttributes", nullable=false, options={"default":"0"})
     */
    protected $handleAttributes;

    /**
     * @ORM\OneToMany(targetEntity="ItemAttribute", mappedBy="item")
     */
    protected $attributes;
    
    /**
    * @ORM\ManyToOne(targetEntity="ItemAttribute")
    * @ORM\JoinColumn(name="itemAttributeId", referencedColumnName="id", nullable=true, onDelete="SET NULL")
    * Desc: default attribute to handle image variants
    */
    protected $itemAttribute;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="brandId", referencedColumnName="id", nullable=true)
     */
    protected $brand;

    /* TERMINAN CAMPOS GENERALES  */
    /* --------------------------------------------------------------------------------------- */

    /* INICIAN CAMPOS DE PRECIO  */
    /* --------------------------------------------------------------------------------------- */

    /**
     * @ORM\Column(type="boolean", name="handleGlobalPrice", nullable=true, options={"default":"0"})
     */
    protected $handleGlobalPrice;

    /**
     * @ORM\Column(type="boolean", name="workWithPricesParent", nullable=true, options={"default":"0"})
     */
    protected $workWithPricesParent;

    /**
     * @ORM\Column(type="decimal", name="regularPrice", precision=18, scale=2, nullable=FALSE)
     */
    protected $regularPrice;

    /**
     * @ORM\Column(type="decimal", name="discount", precision=18, scale=2, nullable=TRUE)
     */
    protected $discount;

    /**
     * @ORM\Column(type="string", name="discountType", length=1, nullable=TRUE)
     */
    protected $discountType;

    /**
     * Fecha de descuento límite
     *
     * @ORM\Column(type="datetime", name="discountUntil", nullable=TRUE)
     */
    protected $discountUntil;

    /**
     * @ORM\Column(type="decimal", name="salePrice", precision=18, scale=2, nullable=FALSE)
     */
    protected $salePrice;

    /**
     * @ORM\OneToMany(targetEntity="PriceListItem", mappedBy="item")
     */
    protected $priceLists;

    /**
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(name="currency", referencedColumnName="id", nullable=FALSE)
     */
    protected $currency;

    /**
     * @ORM\Column(type="json", name="bulk", nullable=TRUE, options={"default":"[]"})
     */
    protected $bulk = [];


    /* TERMINAN CAMPOS DE PRECIO  */
    /* --------------------------------------------------------------------------------------- */

    /* --------------------------------------------------------------------------------------- */
    /* INICIAN CAMPOS DE INVENTARIO  */

    /**
     * @ORM\ManyToOne(targetEntity="UnitMeasure")
     * @ORM\JoinColumn(name="unitMeasureId", referencedColumnName="id", nullable=false)
     */
    protected $unitMeasure;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="trackingTypeId", referencedColumnName="id", nullable=false)
     */
    protected $trackingType;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $sku;

    /**
     * @ORM\Column(type="smallint", name="decimals", nullable=TRUE)
     */
    protected $decimals;

    /**
     * @ORM\Column(type="decimal", name="stock", precision=20, scale=4, nullable=TRUE)
     */
    protected $stock;

    /**
     * @ORM\Column(type="decimal", name="minQtyAllowedInCart", precision=20, scale=4, nullable=TRUE)
     */
    protected $minQtyAllowedInCart;

    /**
     * @ORM\Column(type="decimal", name="maxQtyAllowedInCart", precision=20, scale=4, nullable=TRUE)
     */
    protected $maxQtyAllowedInCart;

    /**
     * @ORM\Column(type="decimal", name="notifyForQtyBelow", precision=20, scale=4, nullable=TRUE)
     */
    protected $notifyForQtyBelow;

    /**
     * @ORM\Column(type="decimal", name="qtyToBecomeOutOfStock", precision=20, scale=4, nullable=TRUE)
     * Qty for Item's Status to Become Out of Stock
     */
    protected $qtyToBecomeOutOfStock;

    /**
     * @ORM\Column(type="decimal", name="weight", precision=20, scale=4, nullable=TRUE)
     */
    protected $weight;

    /**
     * @ORM\Column(type="boolean", name="serialized", nullable=TRUE, options={"default":"0"})
     */
    protected $serialized;

    /**
     * @ORM\Column(type="boolean", name="dangerous", nullable=TRUE, options={"default":"0"})
     */
    protected $dangerous;

    /**
     * @ORM\Column(type="boolean", name="perishable", nullable=TRUE, options={"default":"0"})
     */
    protected $perishable;

    /**
     * @ORM\Column(type="boolean", name="autoFulFill", nullable=TRUE, options={"default":"0"})
     */
    protected $autoFulFill;

    /**
     * @ORM\Column(type="boolean", name="useContainer", nullable=false, options={"default":"0"})
     */
    protected $useContainer;

    /* TERMINAN CAMPOS DE INVENTARIO */
    /* --------------------------------------------------------------------------------------- */

    /* INICIAN CAMPOS DE VENTA A GRANEL  */
    /* --------------------------------------------------------------------------------------- */

    /**
     * @ORM\Column(type="boolean", name="bulkSale", nullable=false, options={"default":"0"})
     */
    protected $bulkSale;

    /**
     * @ORM\ManyToOne(targetEntity="UnitMeasure")
     * @ORM\JoinColumn(name="unitMeasureBulkSaleId", referencedColumnName="id", nullable=TRUE)
     */
    protected $unitMeasureBulkSale;

    /**
     * @ORM\Column(type="decimal", name="quantityByUnitBulkSale", precision=16, scale=4, nullable=TRUE)
     */
    protected $quantityByUnitBulkSale;

    /**
     * @ORM\Column(type="smallint", name="decimalsBulkSale", nullable=TRUE)
     */
    protected $decimalsBulkSale;

    /* TERMINAN CAMPOS DE VENTA A GRANEL */
    /* --------------------------------------------------------------------------------------- */


    /* INICIAN CAMPOS DE PRODUCTO RADIOACTIVO  */
    /* --------------------------------------------------------------------------------------- */

    /**
     * @ORM\Column(type="boolean", nullable=TRUE, options={"default":"0"})
     */
    protected $radioactive;

    /**
     * @ORM\ManyToOne(targetEntity="Isotope")
     * @ORM\JoinColumn(name="isotopeId", referencedColumnName="id", nullable=true)
     */
    protected $isotope;

    /**
     * @ORM\Column(type="boolean", name="useVolumeToFulFill", nullable=true, options={"default":"0"})
     */
    protected $useVolumeToFulFill;

    /**
     * @ORM\Column(type="boolean", name="fixedPrice", nullable=true, options={"default":"0"})
     */
    protected $fixedPrice;

    /* --------------------------------------------------------------------------------------- */

    /* INICIAN CAMPOS DE IMPUESTOS  */
    /* --------------------------------------------------------------------------------------- */

    /**
     * Es Ingreso
     *
     * @ORM\Column(type="boolean", name="income", nullable=TRUE)
     */
    protected $income;

    /**
     * Es Gravable
     *
     * @ORM\Column(type="boolean", name="taxable", nullable=TRUE)
     */
    protected $taxable;

    /* TERMINAN CAMPOS DE IMPUESTOS  */
    /* --------------------------------------------------------------------------------------- */

    /**
     * @ORM\Column(type="string", name="comment", length=500, nullable=TRUE)
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id", nullable=FALSE)
     */
    protected $status;

    /* --------------------------------------------------------------------------------------- */
    /* INICIAN CAMPOS META  */

    /**
     * @ORM\Column(type="string", name="urlKey", length=255, nullable=TRUE)
     */
    protected $urlKey;

    /**
     * @ORM\Column(type="string", name="metaTitle", length=100, nullable=TRUE)
     */
    protected $metaTitle;

    /**
     * @ORM\Column(type="string", name="metaKeywords", length=255, nullable=TRUE)
     */
    protected $metaKeywords;

    /**
     * @ORM\Column(type="string", name="metaDescription", length=255, nullable=TRUE)
     */
    protected $metaDescription;

    /* TERMINAN CAMPOS META  */
    /* --------------------------------------------------------------------------------------- */

    /**
     * Ruta de archivos del producto
     *
     * @ORM\Column(type="string", name="urlFiles", length=500, nullable=TRUE)
     */
    protected $urlFiles;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="categoryId", referencedColumnName="id", nullable=true)
     */
    protected $category;

    /**
     * @ORM\OneToMany(targetEntity="ItemCategory", mappedBy="item")
     */
    protected $categories;

    /**
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="items")
     * @ORM\JoinTable(name="ItemTag",
     *      joinColumns={@ORM\JoinColumn(name="itemId", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tagId", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    protected $tags;

    /**
     * @ORM\OneToMany(targetEntity="Document", mappedBy="item")
     */
    protected $images;

    /**
     * @ORM\Column(type="json", name="socialLinks", nullable=TRUE)
     */
    protected $socialLinks = array();

    /**
     * @ORM\OneToMany(targetEntity="PromotionItem", mappedBy="item")
     */
    protected $promotions;

    /* INICIAN CAMPOS RELACIONADOS A VARIANTES */

    /**
     * @ORM\Column(type="smallint", name="sequence", nullable=TRUE)
     */
    protected $sequence;

    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="variants")
     * @ORM\JoinColumn(name="parentId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="Item", mappedBy="parent")
     */
    protected $variants;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $variant;

    /**
     * @ORM\Column(type="json", name="variantsGroup", nullable=TRUE)
     * Note: variants with id options grouped
     */
    protected $variantsGroup = [];

    /* TERMINAN CAMPOS RELACIONADOS A VARIANTES */

    /**
     * @ORM\Column(type="bigint", nullable=TRUE, options={"unsigned":true})
     */
    protected $views;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    /**
     * @ORM\Column(type="datetime", name="updateDate", nullable=TRUE)
     */
    protected $updateDate;

    /**
     * @ORM\OneToMany(targetEntity="ItemCompositeOption", mappedBy="item")
     */
    protected $compositeOptions;

    /**
     * @ORM\OneToMany(targetEntity="ItemCompositeItem", mappedBy="item")
     */
    protected $compositeItem;

    /**
     * @ORM\OneToMany(targetEntity="ItemDispersion", mappedBy="item")
     */
    protected $itemDispersions;

    /**
     * @ORM\OneToMany(targetEntity="ItemDispersion", mappedBy="itemRelated")
     */
    protected $itemDispersionsRelated;

    public function __construct()
    {
        $this->income = true;
        $this->bulkSale = false;
        $this->perishable = false;
        $this->dangerous = false;
        $this->radioactive = false;
        $this->useVolumeToFulFill = false;
        $this->handleAttributes = false;
        $this->attributes = [];
        $this->useContainer = false;
        $this->tags = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->promotions = new ArrayCollection();
        $this->variants = new ArrayCollection();
        $this->priceLists = new ArrayCollection();
        $this->compositeOptions = new ArrayCollection();
        $this->compositeItem = new ArrayCollection();
        $this->itemDispersions = new ArrayCollection();
        $this->itemDispersionsRelated = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(?string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    public function getSpecs(): ?string
    {
        return $this->specs;
    }

    public function setSpecs(?string $specs): self
    {
        $this->specs = $specs;

        return $this;
    }

    public function getHandleAttributes(): ?bool
    {
        return $this->handleAttributes;
    }

    public function setHandleAttributes(bool $handleAttributes): self
    {
        $this->handleAttributes = $handleAttributes;

        return $this;
    }

    public function getHandleGlobalPrice(): ?bool
    {
        return $this->handleGlobalPrice;
    }

    public function setHandleGlobalPrice(?bool $handleGlobalPrice): self
    {
        $this->handleGlobalPrice = $handleGlobalPrice;

        return $this;
    }

    public function getWorkWithPricesParent(): ?bool
    {
        return $this->workWithPricesParent;
    }

    public function setWorkWithPricesParent(?bool $workWithPricesParent): self
    {
        $this->workWithPricesParent = $workWithPricesParent;

        return $this;
    }

    public function getRegularPrice(): ?string
    {
        return $this->regularPrice;
    }

    public function setRegularPrice(string $regularPrice): self
    {
        $this->regularPrice = $regularPrice;

        return $this;
    }

    public function getDiscount(): ?string
    {
        return $this->discount;
    }

    public function setDiscount(?string $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getDiscountType(): ?string
    {
        return $this->discountType;
    }

    public function setDiscountType(?string $discountType): self
    {
        $this->discountType = $discountType;

        return $this;
    }

    public function getDiscountUntil(): ?\DateTimeInterface
    {
        return $this->discountUntil;
    }

    public function setDiscountUntil(?\DateTimeInterface $discountUntil): self
    {
        $this->discountUntil = $discountUntil;

        return $this;
    }

    public function getSalePrice(): ?string
    {
        return $this->salePrice;
    }

    public function setSalePrice(string $salePrice): self
    {
        $this->salePrice = $salePrice;

        return $this;
    }

    public function getBulk(): ?array
    {
        return $this->bulk;
    }

    public function setBulk(?array $bulk): self
    {
        $this->bulk = $bulk;

        return $this;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(?string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getDecimals(): ?int
    {
        return $this->decimals;
    }

    public function setDecimals(?int $decimals): self
    {
        $this->decimals = $decimals;

        return $this;
    }

    public function getStock(): ?string
    {
        return $this->stock;
    }

    public function setStock(?string $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getMinQtyAllowedInCart(): ?string
    {
        return $this->minQtyAllowedInCart;
    }

    public function setMinQtyAllowedInCart(?string $minQtyAllowedInCart): self
    {
        $this->minQtyAllowedInCart = $minQtyAllowedInCart;

        return $this;
    }

    public function getMaxQtyAllowedInCart(): ?string
    {
        return $this->maxQtyAllowedInCart;
    }

    public function setMaxQtyAllowedInCart(?string $maxQtyAllowedInCart): self
    {
        $this->maxQtyAllowedInCart = $maxQtyAllowedInCart;

        return $this;
    }

    public function getNotifyForQtyBelow(): ?string
    {
        return $this->notifyForQtyBelow;
    }

    public function setNotifyForQtyBelow(?string $notifyForQtyBelow): self
    {
        $this->notifyForQtyBelow = $notifyForQtyBelow;

        return $this;
    }

    public function getQtyToBecomeOutOfStock(): ?string
    {
        return $this->qtyToBecomeOutOfStock;
    }

    public function setQtyToBecomeOutOfStock(?string $qtyToBecomeOutOfStock): self
    {
        $this->qtyToBecomeOutOfStock = $qtyToBecomeOutOfStock;

        return $this;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setWeight(?string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getSerialized(): ?bool
    {
        return $this->serialized;
    }

    public function setSerialized(?bool $serialized): self
    {
        $this->serialized = $serialized;

        return $this;
    }

    public function getDangerous(): ?bool
    {
        return $this->dangerous;
    }

    public function setDangerous(?bool $dangerous): self
    {
        $this->dangerous = $dangerous;

        return $this;
    }

    public function getPerishable(): ?bool
    {
        return $this->perishable;
    }

    public function setPerishable(?bool $perishable): self
    {
        $this->perishable = $perishable;

        return $this;
    }

    public function getAutoFulFill(): ?bool
    {
        return $this->autoFulFill;
    }

    public function setAutoFulFill(?bool $autoFulFill): self
    {
        $this->autoFulFill = $autoFulFill;

        return $this;
    }

    public function getUseContainer(): ?bool
    {
        return $this->useContainer;
    }

    public function setUseContainer(bool $useContainer): self
    {
        $this->useContainer = $useContainer;

        return $this;
    }

    public function getBulkSale(): ?bool
    {
        return $this->bulkSale;
    }

    public function setBulkSale(bool $bulkSale): self
    {
        $this->bulkSale = $bulkSale;

        return $this;
    }

    public function getQuantityByUnitBulkSale(): ?string
    {
        return $this->quantityByUnitBulkSale;
    }

    public function setQuantityByUnitBulkSale(?string $quantityByUnitBulkSale): self
    {
        $this->quantityByUnitBulkSale = $quantityByUnitBulkSale;

        return $this;
    }

    public function getDecimalsBulkSale(): ?int
    {
        return $this->decimalsBulkSale;
    }

    public function setDecimalsBulkSale(?int $decimalsBulkSale): self
    {
        $this->decimalsBulkSale = $decimalsBulkSale;

        return $this;
    }

    public function getRadioactive(): ?bool
    {
        return $this->radioactive;
    }

    public function setRadioactive(?bool $radioactive): self
    {
        $this->radioactive = $radioactive;

        return $this;
    }

    public function getUseVolumeToFulFill(): ?bool
    {
        return $this->useVolumeToFulFill;
    }

    public function setUseVolumeToFulFill(?bool $useVolumeToFulFill): self
    {
        $this->useVolumeToFulFill = $useVolumeToFulFill;

        return $this;
    }

    public function getFixedPrice(): ?bool
    {
        return $this->fixedPrice;
    }

    public function setFixedPrice(?bool $fixedPrice): self
    {
        $this->fixedPrice = $fixedPrice;

        return $this;
    }

    public function getIncome(): ?bool
    {
        return $this->income;
    }

    public function setIncome(?bool $income): self
    {
        $this->income = $income;

        return $this;
    }

    public function getTaxable(): ?bool
    {
        return $this->taxable;
    }

    public function setTaxable(?bool $taxable): self
    {
        $this->taxable = $taxable;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getUrlKey(): ?string
    {
        return $this->urlKey;
    }

    public function setUrlKey(?string $urlKey): self
    {
        $this->urlKey = $urlKey;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(?string $metaKeywords): self
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getUrlFiles(): ?string
    {
        return $this->urlFiles;
    }

    public function setUrlFiles(?string $urlFiles): self
    {
        $this->urlFiles = $urlFiles;

        return $this;
    }

    public function getSocialLinks(): ?array
    {
        return $this->socialLinks;
    }

    public function setSocialLinks(?array $socialLinks): self
    {
        $this->socialLinks = $socialLinks;

        return $this;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(?int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }

    public function getVariant(): ?string
    {
        return $this->variant;
    }

    public function setVariant(?string $variant): self
    {
        $this->variant = $variant;

        return $this;
    }

    public function getVariantsGroup(): ?array
    {
        return $this->variantsGroup;
    }

    public function setVariantsGroup(?array $variantsGroup): self
    {
        $this->variantsGroup = $variantsGroup;

        return $this;
    }

    public function getViews(): ?string
    {
        return $this->views;
    }

    public function setViews(?string $views): self
    {
        $this->views = $views;

        return $this;
    }

    public function getRegisterDate(): ?\DateTimeInterface
    {
        return $this->registerDate;
    }

    public function setRegisterDate(\DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    /**
     * @return Collection|ItemAttribute[]
     */
    public function getAttributes(): Collection
    {
        return $this->attributes;
    }

    public function addAttribute(ItemAttribute $attribute): self
    {
        if (!$this->attributes->contains($attribute)) {
            $this->attributes[] = $attribute;
            $attribute->setItem($this);
        }

        return $this;
    }

    public function removeAttribute(ItemAttribute $attribute): self
    {
        if ($this->attributes->removeElement($attribute)) {
            // set the owning side to null (unless already changed)
            if ($attribute->getItem() === $this) {
                $attribute->setItem(null);
            }
        }

        return $this;
    }

    public function getBrand(): ?Category
    {
        return $this->brand;
    }

    public function setBrand(?Category $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return Collection|PriceListItem[]
     */
    public function getPriceLists(): Collection
    {
        return $this->priceLists;
    }

    public function addPriceList(PriceListItem $priceList): self
    {
        if (!$this->priceLists->contains($priceList)) {
            $this->priceLists[] = $priceList;
            $priceList->setItem($this);
        }

        return $this;
    }

    public function removePriceList(PriceListItem $priceList): self
    {
        if ($this->priceLists->removeElement($priceList)) {
            // set the owning side to null (unless already changed)
            if ($priceList->getItem() === $this) {
                $priceList->setItem(null);
            }
        }

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getUnitMeasure(): ?UnitMeasure
    {
        return $this->unitMeasure;
    }

    public function setUnitMeasure(?UnitMeasure $unitMeasure): self
    {
        $this->unitMeasure = $unitMeasure;

        return $this;
    }

    public function getTrackingType(): ?Type
    {
        return $this->trackingType;
    }

    public function setTrackingType(?Type $trackingType): self
    {
        $this->trackingType = $trackingType;

        return $this;
    }

    public function getUnitMeasureBulkSale(): ?UnitMeasure
    {
        return $this->unitMeasureBulkSale;
    }

    public function setUnitMeasureBulkSale(?UnitMeasure $unitMeasureBulkSale): self
    {
        $this->unitMeasureBulkSale = $unitMeasureBulkSale;

        return $this;
    }

    public function getIsotope(): ?Isotope
    {
        return $this->isotope;
    }

    public function setIsotope(?Isotope $isotope): self
    {
        $this->isotope = $isotope;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|ItemCategory[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(ItemCategory $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->setItem($this);
        }

        return $this;
    }

    public function removeCategory(ItemCategory $category): self
    {
        if ($this->categories->removeElement($category)) {
            // set the owning side to null (unless already changed)
            if ($category->getItem() === $this) {
                $category->setItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Document $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setItem($this);
        }

        return $this;
    }

    public function removeImage(Document $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getItem() === $this) {
                $image->setItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PromotionItem[]
     */
    public function getPromotions(): Collection
    {
        return $this->promotions;
    }

    public function addPromotion(PromotionItem $promotion): self
    {
        if (!$this->promotions->contains($promotion)) {
            $this->promotions[] = $promotion;
            $promotion->setItem($this);
        }

        return $this;
    }

    public function removePromotion(PromotionItem $promotion): self
    {
        if ($this->promotions->removeElement($promotion)) {
            // set the owning side to null (unless already changed)
            if ($promotion->getItem() === $this) {
                $promotion->setItem(null);
            }
        }

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getVariants(): Collection
    {
        return $this->variants;
    }

    public function addVariant(Item $variant): self
    {
        if (!$this->variants->contains($variant)) {
            $this->variants[] = $variant;
            $variant->setParent($this);
        }

        return $this;
    }

    public function removeVariant(Item $variant): self
    {
        if ($this->variants->removeElement($variant)) {
            // set the owning side to null (unless already changed)
            if ($variant->getParent() === $this) {
                $variant->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ItemCompositeOption[]
     */
    public function getCompositeOptions(): Collection
    {
        return $this->compositeOptions;
    }

    public function addCompositeOption(ItemCompositeOption $compositeOption): self
    {
        if (!$this->compositeOptions->contains($compositeOption)) {
            $this->compositeOptions[] = $compositeOption;
            $compositeOption->setItem($this);
        }

        return $this;
    }

    public function removeCompositeOption(ItemCompositeOption $compositeOption): self
    {
        if ($this->compositeOptions->removeElement($compositeOption)) {
            // set the owning side to null (unless already changed)
            if ($compositeOption->getItem() === $this) {
                $compositeOption->setItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ItemCompositeItem[]
     */
    public function getCompositeItem(): Collection
    {
        return $this->compositeItem;
    }

    public function addCompositeItem(ItemCompositeItem $compositeItem): self
    {
        if (!$this->compositeItem->contains($compositeItem)) {
            $this->compositeItem[] = $compositeItem;
            $compositeItem->setItem($this);
        }

        return $this;
    }

    public function removeCompositeItem(ItemCompositeItem $compositeItem): self
    {
        if ($this->compositeItem->removeElement($compositeItem)) {
            // set the owning side to null (unless already changed)
            if ($compositeItem->getItem() === $this) {
                $compositeItem->setItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ItemDispersion[]
     */
    public function getItemDispersions(): Collection
    {
        return $this->itemDispersions;
    }

    public function addItemDispersion(ItemDispersion $itemDispersion): self
    {
        if (!$this->itemDispersions->contains($itemDispersion)) {
            $this->itemDispersions[] = $itemDispersion;
            $itemDispersion->setItem($this);
        }

        return $this;
    }

    public function removeItemDispersion(ItemDispersion $itemDispersion): self
    {
        if ($this->itemDispersions->removeElement($itemDispersion)) {
            // set the owning side to null (unless already changed)
            if ($itemDispersion->getItem() === $this) {
                $itemDispersion->setItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ItemDispersion[]
     */
    public function getItemDispersionsRelated(): Collection
    {
        return $this->itemDispersionsRelated;
    }

    public function addItemDispersionsRelated(ItemDispersion $itemDispersionsRelated): self
    {
        if (!$this->itemDispersionsRelated->contains($itemDispersionsRelated)) {
            $this->itemDispersionsRelated[] = $itemDispersionsRelated;
            $itemDispersionsRelated->setItemRelated($this);
        }

        return $this;
    }

    public function removeItemDispersionsRelated(ItemDispersion $itemDispersionsRelated): self
    {
        if ($this->itemDispersionsRelated->removeElement($itemDispersionsRelated)) {
            // set the owning side to null (unless already changed)
            if ($itemDispersionsRelated->getItemRelated() === $this) {
                $itemDispersionsRelated->setItemRelated(null);
            }
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getItemAttribute(): ?ItemAttribute
    {
        return $this->itemAttribute;
    }

    public function setItemAttribute(?ItemAttribute $itemAttribute): self
    {
        $this->itemAttribute = $itemAttribute;

        return $this;
    }
}
