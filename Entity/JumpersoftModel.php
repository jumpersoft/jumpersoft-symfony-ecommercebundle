<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Jumpersoft\BaseBundle\Entity\JumpersoftBaseModel;

/**
 * Utils functions for models
 *
 */
class JumpersoftModel extends JumpersoftBaseModel
{
}
