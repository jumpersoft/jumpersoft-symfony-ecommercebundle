<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="ComparisonItem")
 *
 * @author Angel
 */
class ComparisonItem extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Comparison", inversedBy="items")
     * @ORM\JoinColumn(name="comparisonId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $comparison;

    /**
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id", nullable=FALSE)
     */
    protected $item;
   
    /**
     * Comment
     *
     * @ORM\Column(type="string", name="comment", length=1000, nullable=TRUE)
     */
    protected $comment;


    /**
     * Set id
     *
     * @param string $id
     *
     * @return ComparisonItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ComparisonItem
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set comparison
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Comparison $comparison
     *
     * @return ComparisonItem
     */
    public function setComparison(\Jumpersoft\EcommerceBundle\Entity\Comparison $comparison)
    {
        $this->comparison = $comparison;

        return $this;
    }

    /**
     * Get comparison
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Comparison
     */
    public function getComparison()
    {
        return $this->comparison;
    }

    /**
     * Set item
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Item $item
     *
     * @return ComparisonItem
     */
    public function setItem(\Jumpersoft\EcommerceBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }
}
