<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\PriceListRepository")
 * @ORM\Table(name="PriceListItem")
 *
 * @author Angel
 */
class PriceListItem extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="PriceList", inversedBy="items")
     * @ORM\JoinColumn(name="priceListId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $priceList;

    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="priceLists")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $item;

    /**
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(name="currencyId", referencedColumnName="id", nullable=FALSE)
     */
    protected $currency;

    /**
     * @ORM\Column(type="decimal", name="regularPrice", precision=18, scale=2, nullable=FALSE)
     */
    protected $regularPrice;

    /**
     * @ORM\Column(type="decimal", name="discount", precision=18, scale=2, nullable=TRUE)
     */
    protected $discount;

    /**
     * @ORM\Column(type="string", name="discountType", length=1, nullable=TRUE)
     */
    protected $discountType;

    /**
     * @ORM\Column(type="decimal", name="salePrice", precision=18, scale=2, nullable=FALSE)
     */
    protected $salePrice;

    /**
     * @ORM\Column(type="decimal", name="startDate", precision=20, scale=4, nullable=true)
     */
    protected $startDate;

    /**
     * @ORM\Column(type="decimal", name="endDate", precision=20, scale=4, nullable=true)
     */
    protected $endDate;

    /**
     * @ORM\Column(type="json", name="bulk", nullable=TRUE, options={"default":"[]"})
     */
    protected $bulk = [];

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    /**
     * @ORM\Column(type="datetime", name="updateDate", nullable=TRUE)
     */
    protected $updateDate;

    

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return PriceListItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set regularPrice.
     *
     * @param string $regularPrice
     *
     * @return PriceListItem
     */
    public function setRegularPrice($regularPrice)
    {
        $this->regularPrice = $regularPrice;

        return $this;
    }

    /**
     * Get regularPrice.
     *
     * @return string
     */
    public function getRegularPrice()
    {
        return $this->regularPrice;
    }

    /**
     * Set discount.
     *
     * @param string|null $discount
     *
     * @return PriceListItem
     */
    public function setDiscount($discount = null)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount.
     *
     * @return string|null
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set discountType.
     *
     * @param string|null $discountType
     *
     * @return PriceListItem
     */
    public function setDiscountType($discountType = null)
    {
        $this->discountType = $discountType;

        return $this;
    }

    /**
     * Get discountType.
     *
     * @return string|null
     */
    public function getDiscountType()
    {
        return $this->discountType;
    }

    /**
     * Set salePrice.
     *
     * @param string $salePrice
     *
     * @return PriceListItem
     */
    public function setSalePrice($salePrice)
    {
        $this->salePrice = $salePrice;

        return $this;
    }

    /**
     * Get salePrice.
     *
     * @return string
     */
    public function getSalePrice()
    {
        return $this->salePrice;
    }

    /**
     * Set startDate.
     *
     * @param string|null $startDate
     *
     * @return PriceListItem
     */
    public function setStartDate($startDate = null)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return string|null
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param string|null $endDate
     *
     * @return PriceListItem
     */
    public function setEndDate($endDate = null)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return string|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set bulk.
     *
     * @param array|null $bulk
     *
     * @return PriceListItem
     */
    public function setBulk($bulk = null)
    {
        $this->bulk = $bulk;

        return $this;
    }

    /**
     * Get bulk.
     *
     * @return array|null
     */
    public function getBulk()
    {
        return $this->bulk;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return PriceListItem
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set updateDate.
     *
     * @param \DateTime|null $updateDate
     *
     * @return PriceListItem
     */
    public function setUpdateDate($updateDate = null)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate.
     *
     * @return \DateTime|null
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set priceList.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\PriceList $priceList
     *
     * @return PriceListItem
     */
    public function setPriceList(\Jumpersoft\EcommerceBundle\Entity\PriceList $priceList)
    {
        $this->priceList = $priceList;

        return $this;
    }

    /**
     * Get priceList.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\PriceList
     */
    public function getPriceList()
    {
        return $this->priceList;
    }

    /**
     * Set item.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Item $item
     *
     * @return PriceListItem
     */
    public function setItem(\Jumpersoft\EcommerceBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set currency.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Currency $currency
     *
     * @return PriceListItem
     */
    public function setCurrency(\Jumpersoft\EcommerceBundle\Entity\Currency $currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}
