<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\HostingAccountRepository")
 * @ORM\Table(name="HostingAccount")
 *
 * @author Angel
 */
class HostingAccount extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", nullable=FALSE)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="OrderRecordItem")
     * @ORM\JoinColumn(name="orderRecordItemId", referencedColumnName="id", nullable=FALSE)
     */
    protected $orderRecordItem;
    
    /**
     * @ORM\ManyToOne(targetEntity="Server")
     * @ORM\JoinColumn(name="serverId", referencedColumnName="id", nullable=FALSE)
     */
    protected $server;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userAdminId", referencedColumnName="id", nullable=FALSE)
     */
    protected $userAdmin;

    /**
     * @ORM\Column(type="string", name="comment", length=500, nullable=TRUE)
     */
    protected $comment;


    /**
     * Set id
     *
     * @param string $id
     *
     * @return HostingAccount
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return HostingAccount
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set user
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User $user
     *
     * @return HostingAccount
     */
    public function setUser(\Jumpersoft\EcommerceBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set orderRecordItem
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\OrderRecordItem $orderRecordItem
     *
     * @return HostingAccount
     */
    public function setOrderRecordItem(\Jumpersoft\EcommerceBundle\Entity\OrderRecordItem $orderRecordItem)
    {
        $this->orderRecordItem = $orderRecordItem;

        return $this;
    }

    /**
     * Get orderRecordItem
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\OrderRecordItem
     */
    public function getOrderRecordItem()
    {
        return $this->orderRecordItem;
    }

    /**
     * Set server
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Server $server
     *
     * @return HostingAccount
     */
    public function setServer(\Jumpersoft\EcommerceBundle\Entity\Server $server)
    {
        $this->server = $server;

        return $this;
    }

    /**
     * Get server
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Server
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * Set userAdmin
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User $userAdmin
     *
     * @return HostingAccount
     */
    public function setUserAdmin(\Jumpersoft\EcommerceBundle\Entity\User $userAdmin)
    {
        $this->userAdmin = $userAdmin;

        return $this;
    }

    /**
     * Get userAdmin
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User
     */
    public function getUserAdmin()
    {
        return $this->userAdmin;
    }
}
