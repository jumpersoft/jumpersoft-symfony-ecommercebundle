<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\UserRepository")
 * @ORM\Table(name="UserLog")
 *
 * @author Angel
 */
class UserLog
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=32)
     */
    protected $id;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userLog")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\Column(type="datetime", name="eventDate")
     */
    protected $eventDate;

    /**
     * @ORM\Column(type="string", name="ipSource", length=16, nullable=TRUE)
     */
    protected $ipSource;

    /**
     * @ORM\Column(type="string", name="ipSource2", length=16, nullable=TRUE)
     */
    protected $ipSource2;

    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id")
     */
    protected $status;

    /**
     * @ORM\Column(type="string", name="comment", length=500, nullable=TRUE)
     */
    protected $comment;

    /**
     * @ORM\Column(type="boolean", name="notificationSentToUser", nullable=TRUE)
     */
    protected $notificationSentToUser;

    public function __construct($id, \Jumpersoft\EcommerceBundle\Entity\User $user, $ipSource, $eventDate, \Jumpersoft\EcommerceBundle\Entity\Status $status = null)
    {
        $this->id = $id;
        $this->user = $user;
        $this->ipSource = $ipSource;
        $this->eventDate = $eventDate;
        $this->status = $status;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return UserLog
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventDate
     *
     * @param \DateTime $eventDate
     *
     * @return UserLog
     */
    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * Get eventDate
     *
     * @return \DateTime
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * Set ipSource
     *
     * @param string $ipSource
     *
     * @return UserLog
     */
    public function setIpSource($ipSource)
    {
        $this->ipSource = $ipSource;

        return $this;
    }

    /**
     * Get ipSource
     *
     * @return string
     */
    public function getIpSource()
    {
        return $this->ipSource;
    }

    /**
     * Set ipSource2
     *
     * @param string $ipSource2
     *
     * @return UserLog
     */
    public function setIpSource2($ipSource2)
    {
        $this->ipSource2 = $ipSource2;

        return $this;
    }

    /**
     * Get ipSource2
     *
     * @return string
     */
    public function getIpSource2()
    {
        return $this->ipSource2;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return UserLog
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set notificationSentToUser
     *
     * @param boolean $notificationSentToUser
     *
     * @return UserLog
     */
    public function setNotificationSentToUser($notificationSentToUser)
    {
        $this->notificationSentToUser = $notificationSentToUser;

        return $this;
    }

    /**
     * Get notificationSentToUser
     *
     * @return boolean
     */
    public function getNotificationSentToUser()
    {
        return $this->notificationSentToUser;
    }

    /**
     * Set user
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User $user
     *
     * @return UserLog
     */
    public function setUser(\Jumpersoft\EcommerceBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set status
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Status $status
     *
     * @return UserLog
     */
    public function setStatus(\Jumpersoft\EcommerceBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }
}
