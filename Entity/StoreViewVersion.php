<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="StoreViewVersion", indexes={@ORM\Index(name="search_idx", columns={"name"})})
 *
 * @author Angel
 */
class StoreViewVersion
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="name", length=100, nullable=FALSE)
     */
    private $name;

    /**
     * @ORM\Column(type="string", name="description", length=255, nullable=TRUE)
     */
    private $description;
    
    /**
     * @ORM\Column(type="boolean", name="active", nullable=FALSE, options={"default":"0"})
     */
    private $active;
    
    /**
     * @ORM\OneToMany(targetEntity="StoreView", mappedBy="version")
     */
    protected $views;

    public function __construct()
    {
        $this->views = new ArrayCollection();
    }


    /**
     * Set id.
     *
     * @param string $id
     *
     * @return StoreViewVersion
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection|StoreView[]
     */
    public function getViews(): Collection
    {
        return $this->views;
    }

    public function addView(StoreView $view): self
    {
        if (!$this->views->contains($view)) {
            $this->views[] = $view;
            $view->setVersion($this);
        }

        return $this;
    }

    public function removeView(StoreView $view): self
    {
        if ($this->views->removeElement($view)) {
            // set the owning side to null (unless already changed)
            if ($view->getVersion() === $this) {
                $view->setVersion(null);
            }
        }

        return $this;
    }
}
