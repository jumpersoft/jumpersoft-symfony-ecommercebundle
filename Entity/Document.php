<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\DocumentRepository")
 * @ORM\Table(name="Document")
 *
 * @author Angel
 */
class Document extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="fileName", length=255, nullable=FALSE)
     */
    protected $fileName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $thumbnail;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $alt;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $title;

    /**
     * @ORM\Column(type="integer", name="fileSize", nullable=FALSE)
     */
    protected $fileSize;

    /**
     * @ORM\Column(type="string", name="size", length=15, nullable=TRUE)
     */
    protected $size;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $active;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="typeId", referencedColumnName="id", nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(type="smallint", nullable=TRUE)
     */
    protected $sequence;

    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="images")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id",  nullable=TRUE, onDelete="CASCADE")
     */
    protected $item;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="images")
     * @ORM\JoinColumn(name="categoryId", referencedColumnName="id",  nullable=TRUE, onDelete="CASCADE")
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="Store", inversedBy="images")
     * @ORM\JoinColumn(name="storeId", referencedColumnName="id",  nullable=TRUE, onDelete="CASCADE")
     */
    protected $store;

    /**
     * @ORM\ManyToOne(targetEntity="ItemAttributeOption", inversedBy="images")
     * @ORM\JoinColumn(name="itemAttributeOptionId", referencedColumnName="id",  nullable=TRUE, onDelete="CASCADE")
     */
    protected $itemAttributeOption;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    /**
     * @ORM\Column(type="datetime", name="updateDate", nullable=TRUE)
     */
    protected $updateDate;

    /**
     * Set id
     *
     * @param string $id
     *
     * @return ItemImage
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getAlt(): ?string
    {
        return $this->alt;
    }

    public function setAlt(?string $alt): self
    {
        $this->alt = $alt;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getFileSize(): ?int
    {
        return $this->fileSize;
    }

    public function setFileSize(int $fileSize): self
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(?string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(?int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }

    public function getRegisterDate(): ?\DateTimeInterface
    {
        return $this->registerDate;
    }

    public function setRegisterDate(\DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function setThumbnail(string $thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    public function getItemAttributeOption(): ?ItemAttributeOption
    {
        return $this->itemAttributeOption;
    }

    public function setItemAttributeOption(?ItemAttributeOption $itemAttributeOption): self
    {
        $this->itemAttributeOption = $itemAttributeOption;

        return $this;
    }
}
