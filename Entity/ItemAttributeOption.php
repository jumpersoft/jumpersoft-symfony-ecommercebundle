<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\ItemRepository")
 * @ORM\Table(name="ItemAttributeOption")
 *
 * @author Angel
 */
class ItemAttributeOption extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ItemAttribute", inversedBy="options")
     * @ORM\JoinColumn(name="itemAttributeId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $itemAttribute;

    /**
     * @ORM\ManyToOne(targetEntity="AttributeOption", inversedBy="itemAttributeOptions")
     * @ORM\JoinColumn(name="attributeOptionId", referencedColumnName="id",  nullable=FALSE)
     */
    protected $attributeOption;

    /**
     * @ORM\Column(type="smallint", name="sequence", nullable=TRUE)
     */
    protected $sequence;

    /**
     * @ORM\OneToMany(targetEntity="Document", mappedBy="itemAttributeOption")
     */
    protected $images;

    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getItemAttribute(): ?ItemAttribute
    {
        return $this->itemAttribute;
    }

    public function setItemAttribute(?ItemAttribute $itemAttribute): self
    {
        $this->itemAttribute = $itemAttribute;

        return $this;
    }

    public function getAttributeOption(): ?AttributeOption
    {
        return $this->attributeOption;
    }

    public function setAttributeOption(?AttributeOption $attributeOption): self
    {
        $this->attributeOption = $attributeOption;

        return $this;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(?int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Document $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setItemAttributeOption($this);
        }

        return $this;
    }

    public function removeImage(Document $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getItemAttributeOption() === $this) {
                $image->setItemAttributeOption(null);
            }
        }

        return $this;
    }
}
