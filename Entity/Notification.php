<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * INFO: Esta tabla permiteguardar notifications enviadas a un user creadas ya se de forma automática o de forma manual,
 * esta tabla puede estar o no, relacionada a un servicio o producto.
 *
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\NotificationRepository")
 * @ORM\Table(name="Notification")
 * @author Angel
 */
class Notification extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", nullable=FALSE)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="typeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $type;
       

    /**
     * Producto o servicio asociado a la notificacion
     *
     * @ORM\ManyToOne(targetEntity="OrderRecordItem")
     * @ORM\JoinColumn(name="orderRecordItemId", referencedColumnName="id")
     */
    protected $orderRecordItem;

    /**
     * @ORM\Column(type="string", name="fromEmail", length=255, nullable=TRUE)
     */
    protected $fromEmail;
    
    /**
     * Usuario al que se enviaá la notificación
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="toId", referencedColumnName="id")
     */
    protected $to;
    
    /**
     * @ORM\Column(type="string", name="subject", length=255, nullable=TRUE)
     */
    protected $subject;
    
    /**
     * @ORM\Column(type="text", name="message", nullable=FALSE)
     */
    protected $message;
    
    /**
     * Fecha de envio
     *
     * @ORM\Column(type="datetime", name="sendDate", nullable=TRUE)
     */
    protected $sendDate;

    /**
     * @ORM\Column(type="string", name="comment", length=500, nullable=TRUE)
     */
    protected $comment;

    /**
     * Fecha de envio
     *
     * @ORM\Column(type="datetime", name="registerDate", nullable=TRUE)
     */
    protected $registerDate;
    
    /**
     * Fecha de envio
     *
     * @ORM\Column(type="datetime", name="updateDate", nullable=TRUE)
     */
    protected $updateDate;


    /**
     * Set id.
     *
     * @param string $id
     *
     * @return Notification
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fromEmail.
     *
     * @param string|null $fromEmail
     *
     * @return Notification
     */
    public function setFromEmail($fromEmail = null)
    {
        $this->fromEmail = $fromEmail;

        return $this;
    }

    /**
     * Get fromEmail.
     *
     * @return string|null
     */
    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    /**
     * Set subject.
     *
     * @param string|null $subject
     *
     * @return Notification
     */
    public function setSubject($subject = null)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject.
     *
     * @return string|null
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set message.
     *
     * @param string $message
     *
     * @return Notification
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message.
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set sendDate.
     *
     * @param \DateTime|null $sendDate
     *
     * @return Notification
     */
    public function setSendDate($sendDate = null)
    {
        $this->sendDate = $sendDate;

        return $this;
    }

    /**
     * Get sendDate.
     *
     * @return \DateTime|null
     */
    public function getSendDate()
    {
        return $this->sendDate;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return Notification
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime|null $registerDate
     *
     * @return Notification
     */
    public function setRegisterDate($registerDate = null)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime|null
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set updateDate.
     *
     * @param \DateTime|null $updateDate
     *
     * @return Notification
     */
    public function setUpdateDate($updateDate = null)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate.
     *
     * @return \DateTime|null
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set user.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User $user
     *
     * @return Notification
     */
    public function setUser(\Jumpersoft\EcommerceBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set type.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Type $type
     *
     * @return Notification
     */
    public function setType(\Jumpersoft\EcommerceBundle\Entity\Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set orderRecordItem.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\OrderRecordItem|null $orderRecordItem
     *
     * @return Notification
     */
    public function setOrderRecordItem(\Jumpersoft\EcommerceBundle\Entity\OrderRecordItem $orderRecordItem = null)
    {
        $this->orderRecordItem = $orderRecordItem;

        return $this;
    }

    /**
     * Get orderRecordItem.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\OrderRecordItem|null
     */
    public function getOrderRecordItem()
    {
        return $this->orderRecordItem;
    }

    /**
     * Set to.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User|null $to
     *
     * @return Notification
     */
    public function setTo(\Jumpersoft\EcommerceBundle\Entity\User $to = null)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User|null
     */
    public function getTo()
    {
        return $this->to;
    }
}
