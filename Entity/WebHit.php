<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Jumpersoft\BaseBundle\Entity\JumpersoftBaseModel;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\WebHitRepository")
 * @ORM\Table(name="WebHit")
 *
 * @author Angel
 */
class WebHit extends JumpersoftBaseModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="typeId", referencedColumnName="id", nullable=false)
     */
    protected $type;

    /**
     * @ORM\Column(type="string", name="ipEncoded", length=255, nullable=FALSE)
     */
    protected $ipEncoded;
    
    /**
     * @ORM\Column(type="string", name="ipAnonymized", length=50, nullable=FALSE)
     */
    protected $ipAnonymized;
    
    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;
    
    /**
     * @ORM\OneToMany(targetEntity="WebHitItem", mappedBy="webHit")
     */
    protected $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getIpEncoded(): ?string
    {
        return $this->ipEncoded;
    }

    public function setIpEncoded(string $ipEncoded): self
    {
        $this->ipEncoded = $ipEncoded;

        return $this;
    }

    public function getIpAnonymized(): ?string
    {
        return $this->ipAnonymized;
    }

    public function setIpAnonymized(string $ipAnonymized): self
    {
        $this->ipAnonymized = $ipAnonymized;

        return $this;
    }

    public function getRegisterDate(): ?\DateTimeInterface
    {
        return $this->registerDate;
    }

    public function setRegisterDate(\DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|WebHitItem[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(WebHitItem $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setWebHit($this);
        }

        return $this;
    }

    public function removeItem(WebHitItem $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getWebHit() === $this) {
                $item->setWebHit(null);
            }
        }

        return $this;
    }
}
