<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\TermsRepository")
 * @ORM\Table(name="Terms")
 *
 * @author Angel
 */
class Terms extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;
    
    /**
     * Store
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumn(name="storeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $store;

    /**
     * @ORM\Column(type="text", length=100000, nullable=FALSE)
     */
    protected $text;

    /**
     * @ORM\Column(type="integer", name="paymentDays", nullable=TRUE)
     */
    protected $paymentDays;
    
    /**
     *
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;
    
    /**
     *
     * @ORM\Column(type="datetime", name="updateDate", nullable=TRUE)
     */
    protected $updateDate;

    /**
     *
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $description;

    /**
     * @ORM\Column(type="boolean", name="active", nullable=TRUE)
     */
    protected $active;
    
    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="typeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $type;


    /**
     * Set id.
     *
     * @param string $id
     *
     * @return Terms
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text.
     *
     * @param string $text
     *
     * @return Terms
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text.
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set paymentDays.
     *
     * @param int|null $paymentDays
     *
     * @return Terms
     */
    public function setPaymentDays($paymentDays = null)
    {
        $this->paymentDays = $paymentDays;

        return $this;
    }

    /**
     * Get paymentDays.
     *
     * @return int|null
     */
    public function getPaymentDays()
    {
        return $this->paymentDays;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return Terms
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set updateDate.
     *
     * @param \DateTime|null $updateDate
     *
     * @return Terms
     */
    public function setUpdateDate($updateDate = null)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate.
     *
     * @return \DateTime|null
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Terms
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return Terms
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set store.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Store $store
     *
     * @return Terms
     */
    public function setStore(\Jumpersoft\EcommerceBundle\Entity\Store $store)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set type.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Type $type
     *
     * @return Terms
     */
    public function setType(\Jumpersoft\EcommerceBundle\Entity\Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }
}
