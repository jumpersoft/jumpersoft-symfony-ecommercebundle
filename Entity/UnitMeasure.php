<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\UnitMeasureRepository")
 * @ORM\Table(name="UnitMeasure")
 *
 * @author Angel
 */
class UnitMeasure extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $code;
    
    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $active;
    
    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $fractionable;
    
    /**
     * @ORM\Column(type="boolean",nullable=TRUE)
     */
    protected $grouper;

    /**
     * @ORM\Column(type="smallint", nullable=TRUE)
     */
    protected $sequence;
    
    /**
     * @ORM\Column(type="string", name="groupId", length=20, nullable=TRUE)
     */
    protected $groupId;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    /**
     * @ORM\Column(type="datetime", name="updateDate", nullable=TRUE)
     */
    protected $updateDate;
    
    /**
     * @ORM\Column(type="json", nullable=TRUE, options={"default":"[]"})
     */
    protected $conversion = [];

    /**
     * Set id
     *
     * @param string $id
     *
     * @return UnitMeasure
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return UnitMeasure
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return UnitMeasure
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return UnitMeasure
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     *
     * @return UnitMeasure
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }


    /**
     * Set sequence.
     *
     * @param int|null $sequence
     *
     * @return UnitMeasure
     */
    public function setSequence($sequence = null)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence.
     *
     * @return int|null
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set fractionable.
     *
     * @param bool|null $fractionable
     *
     * @return UnitMeasure
     */
    public function setFractionable($fractionable = null)
    {
        $this->fractionable = $fractionable;

        return $this;
    }

    /**
     * Get fractionable.
     *
     * @return bool|null
     */
    public function getFractionable()
    {
        return $this->fractionable;
    }

    /**
     * Set grouper.
     *
     * @param bool|null $grouper
     *
     * @return UnitMeasure
     */
    public function setGrouper($grouper = null)
    {
        $this->grouper = $grouper;

        return $this;
    }

    /**
     * Get grouper.
     *
     * @return bool|null
     */
    public function getGrouper()
    {
        return $this->grouper;
    }

    /**
     * Set active.
     *
     * @param bool|null $active
     *
     * @return UnitMeasure
     */
    public function setActive($active = null)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool|null
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set groupId.
     *
     * @param string|null $groupId
     *
     * @return UnitMeasure
     */
    public function setGroupId($groupId = null)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId.
     *
     * @return string|null
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set conversion.
     *
     * @param json|null $conversion
     *
     * @return UnitMeasure
     */
    public function setConversion($conversion = null)
    {
        $this->conversion = $conversion;

        return $this;
    }

    /**
     * Get conversion.
     *
     * @return json|null
     */
    public function getConversion()
    {
        return $this->conversion;
    }
}
