<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\ItemRepository")
 * @ORM\Table(name="ItemCompositeItem", uniqueConstraints={@ORM\UniqueConstraint(columns={"itemCompositeOptionId","itemId"})})
 * @UniqueEntity(
 *     fields={"option", "item"},
 *     message="This item is already in use on this option."
 * )
 *
 * @author Angel
 */
class ItemCompositeItem extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ItemCompositeOption", inversedBy="items")
     * @ORM\JoinColumn(name="itemCompositeOptionId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $option;

    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="compositeItem")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $item;

    /**
     * @ORM\Column(type="decimal", name="defaultQuantity", precision=16, scale=4, nullable=TRUE)
     */
    protected $defaultQuantity;

    /**
     * @ORM\Column(type="boolean", name="userDefined", nullable=TRUE, options={"default":"0"})
     */
    protected $userDefined;

    /**
     * @ORM\Column(type="smallint", nullable=TRUE)
     */
    protected $sequence;

    public function __construct()
    {
    }


    /**
     * Set id.
     *
     * @param string $id
     *
     * @return ItemCompositeItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set defaultQuantity.
     *
     * @param string|null $defaultQuantity
     *
     * @return ItemCompositeItem
     */
    public function setDefaultQuantity($defaultQuantity = null)
    {
        $this->defaultQuantity = $defaultQuantity;

        return $this;
    }

    /**
     * Get defaultQuantity.
     *
     * @return string|null
     */
    public function getDefaultQuantity()
    {
        return $this->defaultQuantity;
    }

    /**
     * Set userDefined.
     *
     * @param bool|null $userDefined
     *
     * @return ItemCompositeItem
     */
    public function setUserDefined($userDefined = null)
    {
        $this->userDefined = $userDefined;

        return $this;
    }

    /**
     * Get userDefined.
     *
     * @return bool|null
     */
    public function getUserDefined()
    {
        return $this->userDefined;
    }

    /**
     * Set sequence.
     *
     * @param int|null $sequence
     *
     * @return ItemCompositeItem
     */
    public function setSequence($sequence = null)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence.
     *
     * @return int|null
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set option.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ItemCompositeOption $option
     *
     * @return ItemCompositeItem
     */
    public function setOption(\Jumpersoft\EcommerceBundle\Entity\ItemCompositeOption $option)
    {
        $this->option = $option;

        return $this;
    }

    /**
     * Get option.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\ItemCompositeOption
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * Set item.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Item $item
     *
     * @return ItemCompositeItem
     */
    public function setItem(\Jumpersoft\EcommerceBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }
}
