<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Jumpersoft\BaseBundle\Entity\JumpersoftBaseModel;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\StoreMiscRepository")
 * @ORM\Table(name="Wishlist")
 *
 * @author Angel
 */
class Wishlist extends JumpersoftBaseModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", nullable=TRUE)
     */
    protected $user;

    /**
     * Fecha de registro del servicio
     *
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    /**
     * @ORM\OneToMany(targetEntity="WishlistItem", mappedBy="wishlist")
     */
    protected $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * Set id
     *
     * @param string $id
     *
     * @return Wishlist
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return Wishlist
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set user
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User $user
     *
     * @return Wishlist
     */
    public function setUser(\Jumpersoft\EcommerceBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add item
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\WishlistItem $item
     *
     * @return Wishlist
     */
    public function addItem(\Jumpersoft\EcommerceBundle\Entity\WishlistItem $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\WishlistItem $item
     */
    public function removeItem(\Jumpersoft\EcommerceBundle\Entity\WishlistItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }
}
