<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="Isotope", indexes={@ORM\Index(name="search_idx", columns={"name"})})
 *
 * @author Angel
 */
class Isotope
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=20)
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="name", length=100, nullable=FALSE)
     */
    private $name;
    
    /**
     * @ORM\Column(type="decimal", name="halfLife", precision=14, scale=4, nullable=FALSE)
     */
    protected $halfLife;

    /**
     * @ORM\Column(type="string", name="unitTime", length=5, nullable=false)
     */
    private $unitTime;
    
    /**
     * @ORM\Column(type="string", name="description", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="smallint", nullable=TRUE)
     */
    protected $sequence;
    
    

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return Isotope
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Isotope
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set halfLife.
     *
     * @param string $halfLife
     *
     * @return Isotope
     */
    public function setHalfLife($halfLife)
    {
        $this->halfLife = $halfLife;

        return $this;
    }

    /**
     * Get halfLife.
     *
     * @return string
     */
    public function getHalfLife()
    {
        return $this->halfLife;
    }

    /**
     * Set unitTime.
     *
     * @param string $unitTime
     *
     * @return Isotope
     */
    public function setUnitTime($unitTime)
    {
        $this->unitTime = $unitTime;

        return $this;
    }

    /**
     * Get unitTime.
     *
     * @return string
     */
    public function getUnitTime()
    {
        return $this->unitTime;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Isotope
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sequence.
     *
     * @param int|null $sequence
     *
     * @return Isotope
     */
    public function setSequence($sequence = null)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence.
     *
     * @return int|null
     */
    public function getSequence()
    {
        return $this->sequence;
    }
}
