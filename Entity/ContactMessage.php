<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="ContactMessage")
 *
 * @author Angel
 */
class ContactMessage extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", name="lastName", length=255, nullable=TRUE)
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string", name="mothersLastName", length=255, nullable=TRUE)
     */
    protected $mothersLastName;

    /**
     * @ORM\Column(type="string", name="email", length=100, nullable=TRUE)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", name="subject", length=255, nullable=TRUE)
     */
    protected $subject;

    /**
     * @ORM\Column(type="text", length=100000, nullable=TRUE)
     */
    protected $text;

    /**
     * @ORM\Column(type="string", name="phone", length=50, nullable=TRUE)
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", name="mobile", length=255, nullable=TRUE)
     */
    protected $mobile;
    
    /**
     * @ORM\Column(type="string", name="orderFolio", length=100, nullable=true)
     */
    protected $orderFolio;

    /**
     * @ORM\Column(type="datetime", name="registerDate")
     */
    protected $registerDate;

    /**
     * @ORM\Column(type="json", name="meta", nullable=TRUE)
     */
    protected $meta;

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return FormContactMessage
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getMothersLastName(): ?string
    {
        return $this->mothersLastName;
    }

    public function setMothersLastName(?string $mothersLastName): self
    {
        $this->mothersLastName = $mothersLastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(?string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getOrderFolio(): ?string
    {
        return $this->orderFolio;
    }

    public function setOrderFolio(?string $orderFolio): self
    {
        $this->orderFolio = $orderFolio;

        return $this;
    }

    public function getRegisterDate(): ?\DateTimeInterface
    {
        return $this->registerDate;
    }

    public function setRegisterDate(\DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    public function getMeta(): ?array
    {
        return $this->meta;
    }

    public function setMeta(?array $meta): self
    {
        $this->meta = $meta;

        return $this;
    }
}
