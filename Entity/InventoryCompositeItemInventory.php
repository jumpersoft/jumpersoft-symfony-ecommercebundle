<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="InventoryCompositeItemInventory", uniqueConstraints={@ORM\UniqueConstraint(columns={"inventoryCompositeItemId","inventoryId"})})
 * @UniqueEntity(
 *     fields={"inventoryCompositeItem", "inventory"},
 *     message="This inventoryCompositeItem and inventory is already in use on this row."
 * )
 * @author Angel
 */
class InventoryCompositeItemInventory extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="InventoryCompositeItem", inversedBy="itemInventory")
     * @ORM\JoinColumn(name="inventoryCompositeItemId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $inventoryCompositeItem;

    /**
     * @ORM\ManyToOne(targetEntity="Inventory")
     * @ORM\JoinColumn(name="inventoryId", referencedColumnName="id", nullable=FALSE)
     */
    protected $inventory;

    /**
     * @ORM\Column(type="decimal", name="quantity", precision=20, scale=4, nullable=FALSE)
     */
    protected $quantity;

    /**
     * @ORM\OneToMany(targetEntity="Inventory", mappedBy="inventoryCompositeItemInventory")
     */
    protected $inventoryOuts;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=false)
     */
    protected $registerDate;

    public function __construct($id, $inventoryCompositeItem, $inventory, $quantity, $registerDate)
    {
        $this->id = $id;
        $this->inventoryCompositeItem = $inventoryCompositeItem;
        $this->inventory = $inventory;
        $this->registerDate = $registerDate;
        $this->quantity = $quantity;
        $this->inventoryOuts = new ArrayCollection();
    }

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return InventoryCompositeItemInventory
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity.
     *
     * @param string $quantity
     *
     * @return InventoryCompositeItemInventory
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return InventoryCompositeItemInventory
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set inventoryCompositeItem.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\InventoryCompositeItem $inventoryCompositeItem
     *
     * @return InventoryCompositeItemInventory
     */
    public function setInventoryCompositeItem(\Jumpersoft\EcommerceBundle\Entity\InventoryCompositeItem $inventoryCompositeItem)
    {
        $this->inventoryCompositeItem = $inventoryCompositeItem;

        return $this;
    }

    /**
     * Get inventoryCompositeItem.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\InventoryCompositeItem
     */
    public function getInventoryCompositeItem()
    {
        return $this->inventoryCompositeItem;
    }

    /**
     * Set inventory.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Inventory $inventory
     *
     * @return InventoryCompositeItemInventory
     */
    public function setInventory(\Jumpersoft\EcommerceBundle\Entity\Inventory $inventory)
    {
        $this->inventory = $inventory;

        return $this;
    }

    /**
     * Get inventory.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Inventory
     */
    public function getInventory()
    {
        return $this->inventory;
    }


    /**
     * Add inventoryOut.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Inventory $inventoryOut
     *
     * @return InventoryCompositeItemInventory
     */
    public function addInventoryOut(\Jumpersoft\EcommerceBundle\Entity\Inventory $inventoryOut)
    {
        $this->inventoryOuts[] = $inventoryOut;

        return $this;
    }

    /**
     * Remove inventoryOut.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Inventory $inventoryOut
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeInventoryOut(\Jumpersoft\EcommerceBundle\Entity\Inventory $inventoryOut)
    {
        return $this->inventoryOuts->removeElement($inventoryOut);
    }

    /**
     * Get inventoryOuts.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventoryOuts()
    {
        return $this->inventoryOuts;
    }
}
