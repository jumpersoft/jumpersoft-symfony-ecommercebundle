<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="StoreViewItem")
 *
 * @author Angel
 */
class StoreViewItem extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="StoreView", inversedBy="items")
     * @ORM\JoinColumn(name="storeViewId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $storeView;

    /**
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $item;

    /**
     * @ORM\Column(type="boolean", name="active", nullable=TRUE)
     */
    protected $active;

    /**
     * @ORM\Column(type="smallint", name="sequence", nullable=TRUE)
     */
    protected $sequence;

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getStoreView(): ?StoreView
    {
        return $this->storeView;
    }

    public function setStoreView(?StoreView $storeView): self
    {
        $this->storeView = $storeView;

        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(?int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }
}
