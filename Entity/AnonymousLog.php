<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="AnonymousLog", indexes={@ORM\Index(name="search_idx", columns={"eventDate", "ipSource"})})
 *
 * @author Angel
 */
class AnonymousLog
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="bigint", nullable=FALSE)
     */
    private $id;
    
    /**
     * @ORM\Column(type="datetime", name="eventDate")
     */
    private $eventDate;
    
    /**
     * @ORM\Column(type="string", name="ipSource", length=16, nullable=TRUE)
     */
    private $ipSource;

    /**
     * @ORM\Column(type="string", name="ipSource2", length=16, nullable=TRUE)
     */
    private $ipSource2;
    
    /**
     * @ORM\Column(type="string", name="userName", length=255, nullable=TRUE)
     */
    private $userName;
    
    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id")
     */
    private $status;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventDate
     *
     * @param \DateTime $eventDate
     *
     * @return AnonymousLog
     */
    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * Get eventDate
     *
     * @return \DateTime
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * Set ipSource
     *
     * @param string $ipSource
     *
     * @return AnonymousLog
     */
    public function setIpSource($ipSource)
    {
        $this->ipSource = $ipSource;

        return $this;
    }

    /**
     * Get ipSource
     *
     * @return string
     */
    public function getIpSource()
    {
        return $this->ipSource;
    }

    /**
     * Set ipSource2
     *
     * @param string $ipSource2
     *
     * @return AnonymousLog
     */
    public function setIpSource2($ipSource2)
    {
        $this->ipSource2 = $ipSource2;

        return $this;
    }

    /**
     * Get ipSource2
     *
     * @return string
     */
    public function getIpSource2()
    {
        return $this->ipSource2;
    }

    /**
     * Set userName
     *
     * @param string $userName
     *
     * @return AnonymousLog
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set status
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Status $status
     *
     * @return AnonymousLog
     */
    public function setStatus(\Jumpersoft\EcommerceBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }
}
