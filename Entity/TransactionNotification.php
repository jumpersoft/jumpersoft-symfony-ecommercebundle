<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="TransactionNotification", indexes={@ORM\Index(name="search_idx", columns={"eventDate", "gatewayTransactionId"})})
 *
 * @author Angel
 */
class TransactionNotification extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=20)
     */
    private $id;
    
    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    private $registerDate;
    
    /**
     * @ORM\Column(type="string", name="type", length=255, nullable=FALSE)
     */
    protected $type;
    
    /**
     * @ORM\Column(type="string", name="eventDate", length=255, nullable=FALSE)
     */
    private $eventDate;
    
    /**
     * @ORM\Column(type="string", name="gatewayTransactionId", length=32, nullable=TRUE)
     */
    private $gatewayTransactionId;

    /**
     * @ORM\Column(type="string", name="transaction", length=2000, nullable=TRUE)
     */
    protected $transaction;
    
    /**
     * @ORM\Column(type="string", name="verificationCode", length=255, nullable=TRUE)
     */
    protected $verificationCode;
    
    /**
     * @ORM\Column(type="string", name="gateway", length=100, nullable=TRUE)
     */
    protected $gateway;
    
    /**
     * @ORM\Column(type="string", name="ipSource", length=16, nullable=TRUE)
     */
    private $ipSource;

    

    /**
     * Set id
     *
     * @param string $id
     *
     * @return TransactionNotification
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return TransactionNotification
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return TransactionNotification
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set eventDate
     *
     * @param string $eventDate
     *
     * @return TransactionNotification
     */
    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * Get eventDate
     *
     * @return string
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * Set transaction
     *
     * @param string $transaction
     *
     * @return TransactionNotification
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;

        return $this;
    }

    /**
     * Get transaction
     *
     * @return string
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * Set verificationCode
     *
     * @param string $verificationCode
     *
     * @return TransactionNotification
     */
    public function setVerificationCode($verificationCode)
    {
        $this->verificationCode = $verificationCode;

        return $this;
    }

    /**
     * Get verificationCode
     *
     * @return string
     */
    public function getVerificationCode()
    {
        return $this->verificationCode;
    }

    /**
     * Set gateway
     *
     * @param string $gateway
     *
     * @return TransactionNotification
     */
    public function setGateway($gateway)
    {
        $this->gateway = $gateway;

        return $this;
    }

    /**
     * Get gateway
     *
     * @return string
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * Set ipSource
     *
     * @param string $ipSource
     *
     * @return TransactionNotification
     */
    public function setIpSource($ipSource)
    {
        $this->ipSource = $ipSource;

        return $this;
    }

    /**
     * Get ipSource
     *
     * @return string
     */
    public function getIpSource()
    {
        return $this->ipSource;
    }

    /**
     * Set gatewayTransactionId
     *
     * @param string $gatewayTransactionId
     *
     * @return TransactionNotification
     */
    public function setGatewayTransactionId($gatewayTransactionId)
    {
        $this->gatewayTransactionId = $gatewayTransactionId;

        return $this;
    }

    /**
     * Get gatewayTransactionId
     *
     * @return string
     */
    public function getGatewayTransactionId()
    {
        return $this->gatewayTransactionId;
    }
}
