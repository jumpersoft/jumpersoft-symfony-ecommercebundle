<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\ProccessStepRepository")
 * @ORM\Table(name="ProccessStep")
 *
 * @author Angel
 */
class ProccessStep extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="OrderRecord", inversedBy="proccessSteps")
     * @ORM\JoinColumn(name="orderRecordId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $orderRecord;

    /**
     * @ORM\ManyToOne(targetEntity="Step")
     * @ORM\JoinColumn(name="stepId", referencedColumnName="id", nullable=FALSE)
     */
    protected $step;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    protected $data;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", nullable=FALSE)
     */
    protected $user;

    /**
     * Constructor
     */
    public function __construct($id, $orderRecord, $step, $data, $registerDate, $user)
    {
        $this->setId($id);
        $this->setOrderRecord($orderRecord);
        $this->setStep($step);
        $this->setData($data);
        $this->setRegisterDate($registerDate);
        $this->setUser($user);
    }

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return ProccessStep
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data.
     *
     * @param json|null $data
     *
     * @return ProccessStep
     */
    public function setData($data = null)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data.
     *
     * @return json|null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return ProccessStep
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set orderRecord.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\OrderRecord $orderRecord
     *
     * @return ProccessStep
     */
    public function setOrderRecord(\Jumpersoft\EcommerceBundle\Entity\OrderRecord $orderRecord)
    {
        $this->orderRecord = $orderRecord;

        return $this;
    }

    /**
     * Get orderRecord.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\OrderRecord
     */
    public function getOrderRecord()
    {
        return $this->orderRecord;
    }

    /**
     * Set step.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Step $step
     *
     * @return ProccessStep
     */
    public function setStep(\Jumpersoft\EcommerceBundle\Entity\Step $step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get step.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Step
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Set user.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User $user
     *
     * @return ProccessStep
     */
    public function setUser(\Jumpersoft\EcommerceBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
