<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\AddressRepository")
 * @ORM\Table(name="Address")
 *
 * @author Angel
 */
class Address extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="addresses")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", name="reference", length=100, nullable=FALSE)
     */
    protected $reference;
    
    /**
     * @ORM\Column(type="string", name="receiver", length=100, nullable=TRUE)
     */
    protected $receiver;
    
    /**
     * @ORM\Column(type="string", name="phone", length=50, nullable=TRUE)
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", name="streetName", length=255, nullable=TRUE)
     */
    protected $streetName;

    /**
     * @ORM\Column(type="string", name="numExt", length=100, nullable=TRUE)
     */
    protected $numExt;

    /**
     * @ORM\Column(type="string", name="numInt", length=100, nullable=TRUE)
     */
    protected $numInt;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $neighborhood;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $town;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $country;

    /**
     * @ORM\Column(type="string", name="postalCode", length=50, nullable=TRUE)
     */
    protected $postalCode;
    
    /**
     * @ORM\Column(type="string", name="coordinates", length=50, nullable=TRUE)
     */
    protected $coordinates;
    
    /**
     * @ORM\Column(type="string", name="otherReferences", length=255, nullable=TRUE)
     */
    protected $otherReferences;
    
    /**
     * @ORM\Column(type="string", name="addressType", length=20, nullable=TRUE)
     */
    protected $addressType;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    /**
     * @ORM\Column(type="datetime", name="updateDate", nullable=TRUE)
     */
    protected $updateDate;
        


    /**
     * Set id.
     *
     * @param string $id
     *
     * @return Address
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference.
     *
     * @param string $reference
     *
     * @return Address
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference.
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set receiver.
     *
     * @param string|null $receiver
     *
     * @return Address
     */
    public function setReceiver($receiver = null)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver.
     *
     * @return string|null
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set phone.
     *
     * @param string|null $phone
     *
     * @return Address
     */
    public function setPhone($phone = null)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set streetName.
     *
     * @param string|null $streetName
     *
     * @return Address
     */
    public function setStreetName($streetName = null)
    {
        $this->streetName = $streetName;

        return $this;
    }

    /**
     * Get streetName.
     *
     * @return string|null
     */
    public function getStreetName()
    {
        return $this->streetName;
    }

    /**
     * Set numExt.
     *
     * @param string|null $numExt
     *
     * @return Address
     */
    public function setNumExt($numExt = null)
    {
        $this->numExt = $numExt;

        return $this;
    }

    /**
     * Get numExt.
     *
     * @return string|null
     */
    public function getNumExt()
    {
        return $this->numExt;
    }

    /**
     * Set numInt.
     *
     * @param string|null $numInt
     *
     * @return Address
     */
    public function setNumInt($numInt = null)
    {
        $this->numInt = $numInt;

        return $this;
    }

    /**
     * Get numInt.
     *
     * @return string|null
     */
    public function getNumInt()
    {
        return $this->numInt;
    }

    /**
     * Set neighborhood.
     *
     * @param string|null $neighborhood
     *
     * @return Address
     */
    public function setNeighborhood($neighborhood = null)
    {
        $this->neighborhood = $neighborhood;

        return $this;
    }

    /**
     * Get neighborhood.
     *
     * @return string|null
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * Set city.
     *
     * @param string|null $city
     *
     * @return Address
     */
    public function setCity($city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set town.
     *
     * @param string|null $town
     *
     * @return Address
     */
    public function setTown($town = null)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town.
     *
     * @return string|null
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set state.
     *
     * @param string|null $state
     *
     * @return Address
     */
    public function setState($state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state.
     *
     * @return string|null
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set country.
     *
     * @param string|null $country
     *
     * @return Address
     */
    public function setCountry($country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return string|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set postalCode.
     *
     * @param string|null $postalCode
     *
     * @return Address
     */
    public function setPostalCode($postalCode = null)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode.
     *
     * @return string|null
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set coordinates.
     *
     * @param string|null $coordinates
     *
     * @return Address
     */
    public function setCoordinates($coordinates = null)
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * Get coordinates.
     *
     * @return string|null
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * Set otherReferences.
     *
     * @param string|null $otherReferences
     *
     * @return Address
     */
    public function setOtherReferences($otherReferences = null)
    {
        $this->otherReferences = $otherReferences;

        return $this;
    }

    /**
     * Get otherReferences.
     *
     * @return string|null
     */
    public function getOtherReferences()
    {
        return $this->otherReferences;
    }

    /**
     * Set addressType.
     *
     * @param string|null $addressType
     *
     * @return Address
     */
    public function setAddressType($addressType = null)
    {
        $this->addressType = $addressType;

        return $this;
    }

    /**
     * Get addressType.
     *
     * @return string|null
     */
    public function getAddressType()
    {
        return $this->addressType;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return Address
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set updateDate.
     *
     * @param \DateTime|null $updateDate
     *
     * @return Address
     */
    public function setUpdateDate($updateDate = null)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate.
     *
     * @return \DateTime|null
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set user.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User $user
     *
     * @return Address
     */
    public function setUser(\Jumpersoft\EcommerceBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
