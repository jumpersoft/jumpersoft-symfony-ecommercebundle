<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\BlogRepository")
 * @ORM\Table(name="BlogPost")
 *
 * @author Angel
 */
class BlogPost extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=20)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=FALSE)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=FALSE)
     */
    protected $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", nullable=FALSE)
     */
    protected $author;

    /**
     * @ORM\Column(type="datetime", name="publishedAt")
     */
    private $publishedAt;

    /**
     * @ORM\OneToMany(targetEntity="BlogComment", mappedBy="post")
     */
    protected $comments;

    /**
     * @ORM\Column(type="json", nullable=false, options={"default":"{}"})
     */
    protected $seo;
    
    /**
     * @ORM\Column(type="json", nullable=false, options={"default":"{}"})
     */
    protected $meta;
    
    /**
     * @ORM\OneToMany(targetEntity="BlogPostCategory", mappedBy="blogPost")
     */
    protected $blogPostCategories;

    /**
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(name="BlogPostTag",
     *      joinColumns={@ORM\JoinColumn(name="blogPostId", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tagId", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    protected $tags;
    

    /**
     * @ORM\Column(type="datetime", name="registerDate")
     */
    private $registerDate;

    /**
     * @ORM\Column(type="datetime", name="updateDate", nullable=TRUE)
     */
    private $updateDate;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->blogPostCategories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->seo = ["metaTitle" => "", "metaKeywords" => "", "metaDescription" => ""];
    }
    
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getSeo(): ?array
    {
        return $this->seo;
    }

    public function setSeo(array $seo): self
    {
        $this->seo = $seo;

        return $this;
    }

    public function getMeta(): ?array
    {
        return $this->meta;
    }

    public function setMeta(array $meta): self
    {
        $this->meta = $meta;

        return $this;
    }

    public function getRegisterDate(): ?\DateTimeInterface
    {
        return $this->registerDate;
    }

    public function setRegisterDate(\DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|BlogComment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(BlogComment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setPost($this);
        }

        return $this;
    }

    public function removeComment(BlogComment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getPost() === $this) {
                $comment->setPost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BlogPostCategory[]
     */
    public function getBlogPostCategories(): Collection
    {
        return $this->blogPostCategories;
    }

    public function addBlogPostCategory(BlogPostCategory $blogPostCategory): self
    {
        if (!$this->blogPostCategories->contains($blogPostCategory)) {
            $this->blogPostCategories[] = $blogPostCategory;
            $blogPostCategory->setBlogPost($this);
        }

        return $this;
    }

    public function removeBlogPostCategory(BlogPostCategory $blogPostCategory): self
    {
        if ($this->blogPostCategories->removeElement($blogPostCategory)) {
            // set the owning side to null (unless already changed)
            if ($blogPostCategory->getBlogPost() === $this) {
                $blogPostCategory->setBlogPost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }
}
