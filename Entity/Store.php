<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\StoreRepository")
 * @ORM\Table(name="Store")
 *
 * @author Angel
 */
class Store extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * Nombre o Razon social
     * @ORM\Column(type="string", name="legalName", length=255, nullable=TRUE)
     */
    protected $legalName;

    /**
     * Nombre comercial
     * @ORM\Column(type="string", name="tradeName", length=255, nullable=FALSE)
     */
    protected $tradeName;

    /**
     * @ORM\Column(type="string", length=20, nullable=TRUE)
     */
    protected $rfc;

    /**
     * @ORM\Column(type="string", name="contactPhone", length=30, nullable=TRUE)
     */
    protected $contactPhone;

    /**
     * @ORM\Column(type="string", name="contactEmail", length=30, nullable=TRUE)
     */
    protected $contactEmail;

    /**
     * @ORM\Column(type="string", name="salesPhone", length=30, nullable=TRUE)
     */
    protected $salesPhone;

    /**
     * @ORM\Column(type="string", name="salesEmail", length=30, nullable=TRUE)
     */
    protected $salesEmail;

    /**
     * @ORM\Column(type="string", name="lowStockNotificationEmail", length=30, nullable=TRUE)
     */
    protected $lowStockNotificationEmail;

    /**
     * @ORM\Column(type="string", name="outOfStockNotificationEmail", length=30, nullable=TRUE)
     */
    protected $outOfStockNotificationEmail;

    /**
     * @ORM\Column(type="string", name="streetName", length=255, nullable=TRUE)
     */
    protected $streetName;

    /**
     * @ORM\Column(type="string", name="numExt", length=100, nullable=TRUE)
     */
    protected $numExt;

    /**
     * @ORM\Column(type="string", name="numInt", length=100, nullable=TRUE)
     */
    protected $numInt;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $neighborhood;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $town;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=TRUE)
     */
    protected $country;

    /**
     * @ORM\Column(type="string", name="postalCode", length=50, nullable=TRUE)
     */
    protected $postalCode;

    /**
     * @ORM\Column(type="string", name="coordinates", length=50, nullable=TRUE)
     */
    protected $coordinates;

    /**
     * @ORM\Column(type="datetime", name="startAt", nullable=FALSE)
     */
    protected $startAt;

    /**
     * @ORM\Column(type="datetime", name="endAt", nullable=FALSE)
     */
    protected $endAt;

    /**
     * @ORM\Column(type="string", name="urlKey", length=255, nullable=TRUE, unique=true)
     */
    protected $urlKey;

    /**
     * @ORM\OneToOne(targetEntity="StoreProfile", mappedBy="store")
     */
//    protected $profile;

    /**
     * @ORM\OneToOne(targetEntity="StoreViewGeneral", mappedBy="store")
     */
    protected $viewGeneral;

    /**
     * @ORM\OneToMany(targetEntity="StoreSocialNetwork", mappedBy="store")
     */
    protected $socialNetworks;

    /**
     * @ORM\OneToMany(targetEntity="Document", mappedBy="store")
     */
    protected $images;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="stores")
     * @ORM\JoinTable(name="StoreUser",
     *      joinColumns={@ORM\JoinColumn(name="storeId", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="userId", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    protected $users;

    /**
     * @ORM\Column(type="json", nullable=true, options={"default":"[]"})
     */
    protected $meta;

    /**
     * @ORM\ManyToOne(targetEntity="StoreViewVersion")
     * @ORM\JoinColumn(name="viewVersionId", referencedColumnName="id", nullable=TRUE)
     */
    protected $viewVersion;

    /**
     * @ORM\Column(type="string", name="urlFiles", length=100, nullable=TRUE)
     */
    protected $urlFiles;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    /**
     * @ORM\Column(type="datetime", name="updateDate", nullable=TRUE)
     */
    protected $updateDate;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->socialNetworks = new ArrayCollection();
    }

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return Store
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLegalName(): ?string
    {
        return $this->legalName;
    }

    public function setLegalName(?string $legalName): self
    {
        $this->legalName = $legalName;

        return $this;
    }

    public function getTradeName(): ?string
    {
        return $this->tradeName;
    }

    public function setTradeName(string $tradeName): self
    {
        $this->tradeName = $tradeName;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(?string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getContactPhone(): ?string
    {
        return $this->contactPhone;
    }

    public function setContactPhone(?string $contactPhone): self
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(?string $contactEmail): self
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    public function getSalesPhone(): ?string
    {
        return $this->salesPhone;
    }

    public function setSalesPhone(?string $salesPhone): self
    {
        $this->salesPhone = $salesPhone;

        return $this;
    }

    public function getSalesEmail(): ?string
    {
        return $this->salesEmail;
    }

    public function setSalesEmail(?string $salesEmail): self
    {
        $this->salesEmail = $salesEmail;

        return $this;
    }

    public function getLowStockNotificationEmail(): ?string
    {
        return $this->lowStockNotificationEmail;
    }

    public function setLowStockNotificationEmail(?string $lowStockNotificationEmail): self
    {
        $this->lowStockNotificationEmail = $lowStockNotificationEmail;

        return $this;
    }

    public function getOutOfStockNotificationEmail(): ?string
    {
        return $this->outOfStockNotificationEmail;
    }

    public function setOutOfStockNotificationEmail(?string $outOfStockNotificationEmail): self
    {
        $this->outOfStockNotificationEmail = $outOfStockNotificationEmail;

        return $this;
    }

    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    public function setStreetName(?string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function getNumExt(): ?string
    {
        return $this->numExt;
    }

    public function setNumExt(?string $numExt): self
    {
        $this->numExt = $numExt;

        return $this;
    }

    public function getNumInt(): ?string
    {
        return $this->numInt;
    }

    public function setNumInt(?string $numInt): self
    {
        $this->numInt = $numInt;

        return $this;
    }

    public function getNeighborhood(): ?string
    {
        return $this->neighborhood;
    }

    public function setNeighborhood(?string $neighborhood): self
    {
        $this->neighborhood = $neighborhood;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getTown(): ?string
    {
        return $this->town;
    }

    public function setTown(?string $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCoordinates(): ?string
    {
        return $this->coordinates;
    }

    public function setCoordinates(?string $coordinates): self
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    public function getUrlKey(): ?string
    {
        return $this->urlKey;
    }

    public function setUrlKey(?string $urlKey): self
    {
        $this->urlKey = $urlKey;

        return $this;
    }

    public function getRegisterDate(): ?\DateTimeInterface
    {
        return $this->registerDate;
    }

    public function setRegisterDate(\DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getMeta(): ?array
    {
        return $this->meta;
    }

    public function setMeta(?array $meta): self
    {
        $this->meta = $meta;

        return $this;
    }

    public function getViewGeneral(): ?StoreViewGeneral
    {
        return $this->viewGeneral;
    }

    public function setViewGeneral(?StoreViewGeneral $viewGeneral): self
    {
        // unset the owning side of the relation if necessary
        if ($viewGeneral === null && $this->viewGeneral !== null) {
            $this->viewGeneral->setStore(null);
        }

        // set the owning side of the relation if necessary
        if ($viewGeneral !== null && $viewGeneral->getStore() !== $this) {
            $viewGeneral->setStore($this);
        }

        $this->viewGeneral = $viewGeneral;

        return $this;
    }

    /**
     * @return Collection|StoreSocialNetwork[]
     */
    public function getSocialNetworks(): Collection
    {
        return $this->socialNetworks;
    }

    public function addSocialNetwork(StoreSocialNetwork $socialNetwork): self
    {
        if (!$this->socialNetworks->contains($socialNetwork)) {
            $this->socialNetworks[] = $socialNetwork;
            $socialNetwork->setStore($this);
        }

        return $this;
    }

    public function removeSocialNetwork(StoreSocialNetwork $socialNetwork): self
    {
        if ($this->socialNetworks->removeElement($socialNetwork)) {
            // set the owning side to null (unless already changed)
            if ($socialNetwork->getStore() === $this) {
                $socialNetwork->setStore(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Document $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setStore($this);
        }

        return $this;
    }

    public function removeImage(Document $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getStore() === $this) {
                $image->setStore(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    public function getViewVersion(): ?StoreViewVersion
    {
        return $this->viewVersion;
    }

    public function setViewVersion(?StoreViewVersion $viewVersion): self
    {
        $this->viewVersion = $viewVersion;

        return $this;
    }

    public function getStartAt(): ?\DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(\DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getUrlFiles(): ?string
    {
        return $this->urlFiles;
    }

    public function setUrlFiles(?string $urlFiles): self
    {
        $this->urlFiles = $urlFiles;

        return $this;
    }
}
