<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="ShoppingCartItem")
 *
 * @author Angel
 */
class ShoppingCartItem extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ShoppingCart", inversedBy="items")
     * @ORM\JoinColumn(name="shoppingCartId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $shoppingCart;

    /**
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id", nullable=FALSE)
     */
    protected $item;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    /**
     * @ORM\Column(type="decimal", name="quantity", precision=16, scale=4, nullable=FALSE)
     */
    protected $quantity;

    /**
     * @ORM\Column(type="decimal", name="subTotal", precision=18, scale=2, nullable=FALSE)
     */
    protected $subTotal;
    
    /**
     * @ORM\Column(type="decimal", name="discount", precision=18, scale=2, nullable=TRUE)
     */
    protected $discount;

    /**
     * @ORM\Column(type="decimal", name="total", precision=18, scale=2, nullable=FALSE)
     */
    protected $total;

    /**
     * Set id
     *
     * @param string $id
     *
     * @return ShoppingCartItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return ShoppingCartItem
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     *
     * @return ShoppingCartItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set shoppingCart
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ShoppingCart $shoppingCart
     *
     * @return ShoppingCartItem
     */
    public function setShoppingCart(\Jumpersoft\EcommerceBundle\Entity\ShoppingCart $shoppingCart)
    {
        $this->shoppingCart = $shoppingCart;

        return $this;
    }

    /**
     * Get shoppingCart
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\ShoppingCart
     */
    public function getShoppingCart()
    {
        return $this->shoppingCart;
    }

    /**
     * Set item
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Item $item
     *
     * @return ShoppingCartItem
     */
    public function setItem(\Jumpersoft\EcommerceBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set subTotal
     *
     * @param string $subTotal
     *
     * @return ShoppingCartItem
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;

        return $this;
    }

    /**
     * Get subTotal
     *
     * @return string
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return ShoppingCartItem
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set discount
     *
     * @param string $discount
     *
     * @return ShoppingCartItem
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }
}
