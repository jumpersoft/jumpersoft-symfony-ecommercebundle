<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\ServerRepository")
 * @ORM\Table(name="Server")
 *
 * @author Angel
 */
class Server extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", name="name", length=255, nullable=FALSE)
     */
    protected $name;
    
    /**
     * @ORM\Column(type="string", name="description", length=500, nullable=TRUE)
     */
    protected $description;
    
    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;
    
    /**
     * @ORM\Column(type="string", name="ip1", length=16, nullable=FALSE)
     */
    protected $ip1;

    /**
     * @ORM\Column(type="string", name="ip2", length=16, nullable=FALSE)
     */
    protected $ip2;
    
    /**
     * @ORM\Column(type="string", name="ns1", length=255, nullable=TRUE)
     */
    protected $ns1;
    
    /**
     * @ORM\Column(type="string", name="ns2", length=255, nullable=FALSE)
     */
    protected $ns2;
    
    /**
     * @ORM\Column(type="string", name="comment", length=500, nullable=TRUE)
     */
    protected $comment;
    
    /**
     * @ORM\Column(type="string", name="domain", length=255, nullable=FALSE)
     */
    protected $domain;
    
    /**
     * @ORM\Column(type="string", name="emailAdmin", length=255, nullable=FALSE)
     */
    protected $emailAdmin;
    
    /**
     * @ORM\Column(type="string", name="serviceProvider", length=255, nullable=TRUE)
     */
    protected $serviceProvider;
    
    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id")
     */
    protected $status;

    /*
     * INICIAN FUNCIONES PERSONALIZADAS NO BORRAR
     */
 
    /*
     * TERMINAN FUNCIONES PERSONALIZADAS NO BORRAR
     */



    /**
     * Set id
     *
     * @param string $id
     *
     * @return Server
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Server
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Server
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return Server
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set ip1
     *
     * @param string $ip1
     *
     * @return Server
     */
    public function setIp1($ip1)
    {
        $this->ip1 = $ip1;

        return $this;
    }

    /**
     * Get ip1
     *
     * @return string
     */
    public function getIp1()
    {
        return $this->ip1;
    }

    /**
     * Set ip2
     *
     * @param string $ip2
     *
     * @return Server
     */
    public function setIp2($ip2)
    {
        $this->ip2 = $ip2;

        return $this;
    }

    /**
     * Get ip2
     *
     * @return string
     */
    public function getIp2()
    {
        return $this->ip2;
    }

    /**
     * Set ns1
     *
     * @param string $ns1
     *
     * @return Server
     */
    public function setNs1($ns1)
    {
        $this->ns1 = $ns1;

        return $this;
    }

    /**
     * Get ns1
     *
     * @return string
     */
    public function getNs1()
    {
        return $this->ns1;
    }

    /**
     * Set ns2
     *
     * @param string $ns2
     *
     * @return Server
     */
    public function setNs2($ns2)
    {
        $this->ns2 = $ns2;

        return $this;
    }

    /**
     * Get ns2
     *
     * @return string
     */
    public function getNs2()
    {
        return $this->ns2;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Server
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set domain
     *
     * @param string $domain
     *
     * @return Server
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set emailAdmin
     *
     * @param string $emailAdmin
     *
     * @return Server
     */
    public function setEmailAdmin($emailAdmin)
    {
        $this->emailAdmin = $emailAdmin;

        return $this;
    }

    /**
     * Get emailAdmin
     *
     * @return string
     */
    public function getEmailAdmin()
    {
        return $this->emailAdmin;
    }

    /**
     * Set serviceProvider
     *
     * @param string $serviceProvider
     *
     * @return Server
     */
    public function setServiceProvider($serviceProvider)
    {
        $this->serviceProvider = $serviceProvider;

        return $this;
    }

    /**
     * Get serviceProvider
     *
     * @return string
     */
    public function getServiceProvider()
    {
        return $this->serviceProvider;
    }

    /**
     * Set status
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Status $status
     *
     * @return Server
     */
    public function setStatus(\Jumpersoft\EcommerceBundle\Entity\Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }
}
