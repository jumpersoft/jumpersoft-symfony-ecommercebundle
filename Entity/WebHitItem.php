<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Jumpersoft\BaseBundle\Entity\JumpersoftBaseModel;

/**
 * @ORM\Entity()
 * @ORM\Table(name="WebHitItem")
 *
 * @author Angel
 */
class WebHitItem extends JumpersoftBaseModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="WebHit", inversedBy="items")
     * @ORM\JoinColumn(name="webHitId", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $webHit;

    /**
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $item;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getRegisterDate(): ?\DateTimeInterface
    {
        return $this->registerDate;
    }

    public function setRegisterDate(\DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    public function getWebHit(): ?WebHit
    {
        return $this->webHit;
    }

    public function setWebHit(?WebHit $webHit): self
    {
        $this->webHit = $webHit;

        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }
}
