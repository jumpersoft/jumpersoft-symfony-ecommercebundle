<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="UserSuscription")
 *
 * @author Angel
 */
class UserSuscription extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="User", inversedBy="suscriptions")
     * @ORM\JoinColumn(name="id", referencedColumnName="id", onDelete="CASCADE")
     *
     */
    protected $user;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $newsletter;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $photography;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $electronics;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $cellphones;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $computing;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $fashion;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $homeappliances;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $health;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $home;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $children;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $books;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $videogames;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $sports;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $smsActive;

    /**
     * Set newsletter
     *
     * @param boolean $newsletter
     *
     * @return UserSuscription
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get newsletter
     *
     * @return boolean
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * Set user
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User $user
     *
     * @return UserSuscription
     */
    public function setUser(\Jumpersoft\EcommerceBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set photography
     *
     * @param boolean $photography
     *
     * @return UserSuscription
     */
    public function setPhotography($photography)
    {
        $this->photography = $photography;

        return $this;
    }

    /**
     * Get photography
     *
     * @return boolean
     */
    public function getPhotography()
    {
        return $this->photography;
    }

    /**
     * Set electronics
     *
     * @param boolean $electronics
     *
     * @return UserSuscription
     */
    public function setElectronics($electronics)
    {
        $this->electronics = $electronics;

        return $this;
    }

    /**
     * Get electronics
     *
     * @return boolean
     */
    public function getElectronics()
    {
        return $this->electronics;
    }

    /**
     * Set cellphones
     *
     * @param boolean $cellphones
     *
     * @return UserSuscription
     */
    public function setCellphones($cellphones)
    {
        $this->cellphones = $cellphones;

        return $this;
    }

    /**
     * Get cellphones
     *
     * @return boolean
     */
    public function getCellphones()
    {
        return $this->cellphones;
    }

    /**
     * Set computing
     *
     * @param boolean $computing
     *
     * @return UserSuscription
     */
    public function setComputing($computing)
    {
        $this->computing = $computing;

        return $this;
    }

    /**
     * Get computing
     *
     * @return boolean
     */
    public function getComputing()
    {
        return $this->computing;
    }

    /**
     * Set fashion
     *
     * @param boolean $fashion
     *
     * @return UserSuscription
     */
    public function setFashion($fashion)
    {
        $this->fashion = $fashion;

        return $this;
    }

    /**
     * Get fashion
     *
     * @return boolean
     */
    public function getFashion()
    {
        return $this->fashion;
    }

    /**
     * Set homeappliances
     *
     * @param boolean $homeappliances
     *
     * @return UserSuscription
     */
    public function setHomeappliances($homeappliances)
    {
        $this->homeappliances = $homeappliances;

        return $this;
    }

    /**
     * Get homeappliances
     *
     * @return boolean
     */
    public function getHomeappliances()
    {
        return $this->homeappliances;
    }

    /**
     * Set health
     *
     * @param boolean $health
     *
     * @return UserSuscription
     */
    public function setHealth($health)
    {
        $this->health = $health;

        return $this;
    }

    /**
     * Get health
     *
     * @return boolean
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * Set home
     *
     * @param boolean $home
     *
     * @return UserSuscription
     */
    public function setHome($home)
    {
        $this->home = $home;

        return $this;
    }

    /**
     * Get home
     *
     * @return boolean
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * Set children
     *
     * @param boolean $children
     *
     * @return UserSuscription
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get children
     *
     * @return boolean
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set books
     *
     * @param boolean $books
     *
     * @return UserSuscription
     */
    public function setBooks($books)
    {
        $this->books = $books;

        return $this;
    }

    /**
     * Get books
     *
     * @return boolean
     */
    public function getBooks()
    {
        return $this->books;
    }

    /**
     * Set videogames
     *
     * @param boolean $videogames
     *
     * @return UserSuscription
     */
    public function setVideogames($videogames)
    {
        $this->videogames = $videogames;

        return $this;
    }

    /**
     * Get videogames
     *
     * @return boolean
     */
    public function getVideogames()
    {
        return $this->videogames;
    }

    /**
     * Set sports
     *
     * @param boolean $sports
     *
     * @return UserSuscription
     */
    public function setSports($sports)
    {
        $this->sports = $sports;

        return $this;
    }

    /**
     * Get sports
     *
     * @return boolean
     */
    public function getSports()
    {
        return $this->sports;
    }


    /**
     * Set smsActive
     *
     * @param boolean $smsActive
     *
     * @return UserSuscription
     */
    public function setSmsActive($smsActive)
    {
        $this->smsActive = $smsActive;

        return $this;
    }

    /**
     * Get smsActive
     *
     * @return boolean
     */
    public function getSmsActive()
    {
        return $this->smsActive;
    }
}
