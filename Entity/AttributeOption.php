<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\AttributeRepository")
 * @ORM\Table(name="AttributeOption")
 *
 * @author Angel
 */
class AttributeOption extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Attribute", inversedBy="options")
     * @ORM\JoinColumn(name="attributeId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $attribute;

    /**
     * @ORM\Column(type="string", length=100, nullable=FALSE)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=FALSE)
     */
    protected $value;

    /**
     * @ORM\OneToMany(targetEntity="ItemAttributeOption", mappedBy="attributeOption")
     */
    protected $itemAttributeOptions;

    /**
     * @ORM\Column(type="smallint", name="sequence", nullable=TRUE)
     */
    protected $sequence;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    public function __construct()
    {
        $this->itemAttributeOptions = new ArrayCollection();
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getRegisterDate(): ?\DateTimeInterface
    {
        return $this->registerDate;
    }

    public function setRegisterDate(\DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    public function getAttribute(): ?Attribute
    {
        return $this->attribute;
    }

    public function setAttribute(?Attribute $attribute): self
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @return Collection|ItemAttributeOption[]
     */
    public function getItemAttributeOptions(): Collection
    {
        return $this->itemAttributeOptions;
    }

    public function addItemAttributeOption(ItemAttributeOption $itemAttributeOption): self
    {
        if (!$this->itemAttributeOptions->contains($itemAttributeOption)) {
            $this->itemAttributeOptions[] = $itemAttributeOption;
            $itemAttributeOption->setAttributeOption($this);
        }

        return $this;
    }

    public function removeItemAttributeOption(ItemAttributeOption $itemAttributeOption): self
    {
        if ($this->itemAttributeOptions->removeElement($itemAttributeOption)) {
            // set the owning side to null (unless already changed)
            if ($itemAttributeOption->getAttributeOption() === $this) {
                $itemAttributeOption->setAttributeOption(null);
            }
        }

        return $this;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(?int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }
}
