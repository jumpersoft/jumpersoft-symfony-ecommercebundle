<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\PriceListRepository")
 * @ORM\Table(name="PriceListCustomer")
 *
 * @author Angel
 */
class PriceListCustomer extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="PriceList", inversedBy="customers")
     * @ORM\JoinColumn(name="priceListId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $priceList;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="priceList")
     * @ORM\JoinColumn(name="customerId", referencedColumnName="id",  nullable=FALSE, unique=true, onDelete="CASCADE")
     */
    protected $customer;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;


    /**
     * Set id.
     *
     * @param string $id
     *
     * @return PriceListCustomer
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return PriceListCustomer
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set priceList.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\PriceList $priceList
     *
     * @return PriceListCustomer
     */
    public function setPriceList(\Jumpersoft\EcommerceBundle\Entity\PriceList $priceList)
    {
        $this->priceList = $priceList;

        return $this;
    }

    /**
     * Get priceList.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\PriceList
     */
    public function getPriceList()
    {
        return $this->priceList;
    }

    /**
     * Set customer.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User $customer
     *
     * @return PriceListCustomer
     */
    public function setCustomer(\Jumpersoft\EcommerceBundle\Entity\User $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
