<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\ItemRepository")
 * @ORM\Table(name="ItemCompositeOption")
 *
 * @author Angel
 */
class ItemCompositeOption extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="compositeOptions")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $item;

    /**
     * @ORM\Column(type="string", length=255, nullable=FALSE)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="typeId", referencedColumnName="id", nullable=false)
     */
    protected $type;
    
    /**
     * @ORM\Column(type="boolean", name="required", options={"default":"0"})
     */
    protected $required;

    /**
     * @ORM\Column(type="smallint", nullable=TRUE)
     */
    protected $sequence;
    
    /**
     * @ORM\OneToMany(targetEntity="ItemCompositeItem", mappedBy="option")
     */
    protected $items;

    public function __construct()
    {
        $this->required = 0;
        $this->items = new ArrayCollection();
    }



    /**
     * Set id.
     *
     * @param string $id
     *
     * @return ItemCompositeOption
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ItemCompositeOption
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set required.
     *
     * @param bool $required
     *
     * @return ItemCompositeOption
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * Get required.
     *
     * @return bool
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set sequence.
     *
     * @param int|null $sequence
     *
     * @return ItemCompositeOption
     */
    public function setSequence($sequence = null)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence.
     *
     * @return int|null
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set item.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Item $item
     *
     * @return ItemCompositeOption
     */
    public function setItem(\Jumpersoft\EcommerceBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set type.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Type $type
     *
     * @return ItemCompositeOption
     */
    public function setType(\Jumpersoft\EcommerceBundle\Entity\Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add item.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ItemCompositeItem $item
     *
     * @return ItemCompositeOption
     */
    public function addItem(\Jumpersoft\EcommerceBundle\Entity\ItemCompositeItem $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ItemCompositeItem $item
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeItem(\Jumpersoft\EcommerceBundle\Entity\ItemCompositeItem $item)
    {
        return $this->items->removeElement($item);
    }

    /**
     * Get items.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }
}
