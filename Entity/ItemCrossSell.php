<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\ItemRepository")
 * @ORM\Table(name="ItemCrossSell")
 *
 * @author Angel
 */
class ItemCrossSell extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $item;
    
    /**
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumn(name="itemRelatedId", referencedColumnName="id",  nullable=FALSE, onDelete="CASCADE")
     */
    protected $itemRelated;

    /**
     * @ORM\Column(type="smallint", name="proximity", nullable=TRUE)
     */
    protected $proximity;
    
    /** INICIAN FUNCIONES ESPECIALES NO BORRAR */
    public function __construct()
    {
    }

    

    /**
     * Set id
     *
     * @param string $id
     *
     * @return ItemCrossSell
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set proximity
     *
     * @param integer $proximity
     *
     * @return ItemCrossSell
     */
    public function setProximity($proximity)
    {
        $this->proximity = $proximity;

        return $this;
    }

    /**
     * Get proximity
     *
     * @return integer
     */
    public function getProximity()
    {
        return $this->proximity;
    }

    /**
     * Set item
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Item $item
     *
     * @return ItemCrossSell
     */
    public function setItem(\Jumpersoft\EcommerceBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set itemRelated
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Item $itemRelated
     *
     * @return ItemCrossSell
     */
    public function setItemRelated(\Jumpersoft\EcommerceBundle\Entity\Item $itemRelated)
    {
        $this->itemRelated = $itemRelated;

        return $this;
    }

    /**
     * Get itemRelated
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Item
     */
    public function getItemRelated()
    {
        return $this->itemRelated;
    }
}
