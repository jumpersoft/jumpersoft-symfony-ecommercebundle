<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\StatusRepository")
 * @ORM\Table(name="Status", indexes={@ORM\Index(name="search_idx", columns={"name"})})
 *
 * @author Angel
 */
class Status
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="name", length=100, nullable=FALSE)
     */
    private $name;

    /**
     * @ORM\Column(type="string", name="description", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="smallint", nullable=TRUE)
     */
    protected $sequence;
    
    /**
     * @ORM\Column(type="boolean", name="visibleInMenus", nullable=true, options={"default":"0"})
     */
    protected $visibleInMenus;

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return Status
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Status
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Status
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sequence.
     *
     * @param int|null $sequence
     *
     * @return Status
     */
    public function setSequence($sequence = null)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence.
     *
     * @return int|null
     */
    public function getSequence()
    {
        return $this->sequence;
    }


    /**
     * Set visibleInMenus.
     *
     * @param bool|null $visibleInMenus
     *
     * @return Status
     */
    public function setVisibleInMenus($visibleInMenus = null)
    {
        $this->visibleInMenus = $visibleInMenus;

        return $this;
    }

    /**
     * Get visibleInMenus.
     *
     * @return bool|null
     */
    public function getVisibleInMenus()
    {
        return $this->visibleInMenus;
    }
}
