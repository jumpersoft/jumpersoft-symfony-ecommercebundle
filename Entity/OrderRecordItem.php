<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Jumpersoft\BaseBundle\Entity\JumpersoftBaseModel;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\OrderRecordItemRepository")
 * @ORM\Table(name="OrderRecordItem")
 *
 * @author Angel
 */
class OrderRecordItem extends JumpersoftBaseModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="OrderRecord", inversedBy="items")
     * @ORM\JoinColumn(name="orderRecordId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $orderRecord;

    /**
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id", nullable=FALSE)
     */
    protected $item;
    
    /**
     * @ORM\Column(type="smallint", name="sequence", nullable=true)
     */
    protected $sequence;

    /**
     * @ORM\OneToMany(targetEntity="OrderRecordItemInventory", mappedBy="orderRecordItem")
     */
    protected $inventory;

    /**
     *
     * @ORM\Column(type="datetime", name="registerDate", nullable=false)
     */
    protected $registerDate;

    /**
     * Fecha de confirmación de pago de servicio
     *
     * @ORM\Column(type="datetime", name="paymentConfirmationDate", nullable=TRUE)
     */
    protected $paymentConfirmationDate;

    /**
     * @ORM\Column(type="decimal", name="regularPrice", precision=18, scale=2, nullable=FALSE)
     */
    protected $regularPrice;
    
    /**
     * @ORM\Column(type="decimal", name="salePrice", precision=18, scale=2, nullable=FALSE)
     */
    protected $salePrice;

    /**
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(name="currency", referencedColumnName="id", nullable=FALSE)
     */
    protected $currency;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="priceTypeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $priceType;

    /**
     * @ORM\Column(type="decimal", name="quantity", precision=16, scale=4, nullable=FALSE)
     */
    protected $quantity;

    /**
     * @ORM\Column(type="decimal", name="quantityFulFilled", precision=16, scale=4, nullable=FALSE)
     */
    protected $quantityFulFilled;

    /**
     * @ORM\Column(type="decimal", name="subTotal", precision=18, scale=2, nullable=FALSE)
     */
    protected $subTotal;

    /**
     * @ORM\Column(type="decimal", name="discount", precision=18, scale=2, nullable=TRUE)
     */
    protected $discount;

    /**
     * @ORM\Column(type="string", name="discountType", length=1, nullable=TRUE)
     */
    protected $discountType;

    /**
     * @ORM\Column(type="decimal", name="total", precision=18, scale=2, nullable=FALSE)
     */
    protected $total;

    /**
     * @ORM\Column(type="boolean", name="bulkSale", nullable=false, options={"default":"0"})
     */
    protected $bulkSale;

    /**
     * @ORM\Column(type="string", name="comment", length=1000, nullable=TRUE)
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id", nullable=FALSE)
     */
    protected $status;

    /**
     * @ORM\Column(type="json", name="shippingAddress", nullable=TRUE)
     * TODO Aun no se le da uso, pero la idea es que se puedan enviar los productos a direcciones diferentes
     */
    protected $shippingAddress;

    /**
     * @ORM\OneToMany(targetEntity="ShipmentItem", mappedBy="orderRecordItem")
     */
    protected $shipmentItems;
    
    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="containerStatusId", referencedColumnName="id", nullable=true)
     */
    protected $containerStatus;

    /**
     * @ORM\Column(type="json", name="attributeValues", nullable=true, options={"default":"{}"})
     */
    protected $attributeValues;
    
    

    /* INICIAN CAMPOS DE PRODUCTO RADIOACTIVO  */
    /* --------------------------------------------------------------------------------------- */

    /**
     * @ORM\Column(type="decimal", name="volume", precision=16, scale=4, nullable=true)
     */
    protected $volume;

    /**
     * @ORM\Column(type="datetime", name="calibrationDate", nullable=true)
     */
    protected $calibrationDate;

    /* --------------------------------------------------------------------------------------- */

    /* INICIAN FUNCIONES ESPECIALES NO BORRAR */

    public function __construct()
    {
        $this->quantityFulFilled = 0;
        $this->bulkSale = 0;
        $this->shipmentItems = new ArrayCollection();
        $this->inventory = new ArrayCollection();
    }

    /* TERMINA FUNCIONES ESPECIALES NO BORRAR */
    
    


    /**
     * Set id.
     *
     * @param string $id
     *
     * @return OrderRecordItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sequence.
     *
     * @param int|null $sequence
     *
     * @return OrderRecordItem
     */
    public function setSequence($sequence = null)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence.
     *
     * @return int|null
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return OrderRecordItem
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set paymentConfirmationDate.
     *
     * @param \DateTime|null $paymentConfirmationDate
     *
     * @return OrderRecordItem
     */
    public function setPaymentConfirmationDate($paymentConfirmationDate = null)
    {
        $this->paymentConfirmationDate = $paymentConfirmationDate;

        return $this;
    }

    /**
     * Get paymentConfirmationDate.
     *
     * @return \DateTime|null
     */
    public function getPaymentConfirmationDate()
    {
        return $this->paymentConfirmationDate;
    }

    /**
     * Set regularPrice.
     *
     * @param string $regularPrice
     *
     * @return OrderRecordItem
     */
    public function setRegularPrice($regularPrice)
    {
        $this->regularPrice = $regularPrice;

        return $this;
    }

    /**
     * Get regularPrice.
     *
     * @return string
     */
    public function getRegularPrice()
    {
        return $this->regularPrice;
    }

    /**
     * Set quantity.
     *
     * @param string $quantity
     *
     * @return OrderRecordItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set quantityFulFilled.
     *
     * @param string $quantityFulFilled
     *
     * @return OrderRecordItem
     */
    public function setQuantityFulFilled($quantityFulFilled)
    {
        $this->quantityFulFilled = $quantityFulFilled;

        return $this;
    }

    /**
     * Get quantityFulFilled.
     *
     * @return string
     */
    public function getQuantityFulFilled()
    {
        return $this->quantityFulFilled;
    }

    /**
     * Set subTotal.
     *
     * @param string $subTotal
     *
     * @return OrderRecordItem
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;

        return $this;
    }

    /**
     * Get subTotal.
     *
     * @return string
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * Set discount.
     *
     * @param string|null $discount
     *
     * @return OrderRecordItem
     */
    public function setDiscount($discount = null)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount.
     *
     * @return string|null
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set discountType.
     *
     * @param string|null $discountType
     *
     * @return OrderRecordItem
     */
    public function setDiscountType($discountType = null)
    {
        $this->discountType = $discountType;

        return $this;
    }

    /**
     * Get discountType.
     *
     * @return string|null
     */
    public function getDiscountType()
    {
        return $this->discountType;
    }

    /**
     * Set total.
     *
     * @param string $total
     *
     * @return OrderRecordItem
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total.
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set bulkSale.
     *
     * @param bool $bulkSale
     *
     * @return OrderRecordItem
     */
    public function setBulkSale($bulkSale)
    {
        $this->bulkSale = $bulkSale;

        return $this;
    }

    /**
     * Get bulkSale.
     *
     * @return bool
     */
    public function getBulkSale()
    {
        return $this->bulkSale;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return OrderRecordItem
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set shippingAddress.
     *
     * @param json|null $shippingAddress
     *
     * @return OrderRecordItem
     */
    public function setShippingAddress($shippingAddress = null)
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    /**
     * Get shippingAddress.
     *
     * @return json|null
     */
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    /**
     * Set attributeValues.
     *
     * @param json|null $attributeValues
     *
     * @return OrderRecordItem
     */
    public function setAttributeValues($attributeValues = null)
    {
        $this->attributeValues = $attributeValues;

        return $this;
    }

    /**
     * Get attributeValues.
     *
     * @return json|null
     */
    public function getAttributeValues()
    {
        return $this->attributeValues;
    }

    /**
     * Set volume.
     *
     * @param string|null $volume
     *
     * @return OrderRecordItem
     */
    public function setVolume($volume = null)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume.
     *
     * @return string|null
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set calibrationDate.
     *
     * @param \DateTime|null $calibrationDate
     *
     * @return OrderRecordItem
     */
    public function setCalibrationDate($calibrationDate = null)
    {
        $this->calibrationDate = $calibrationDate;

        return $this;
    }

    /**
     * Get calibrationDate.
     *
     * @return \DateTime|null
     */
    public function getCalibrationDate()
    {
        return $this->calibrationDate;
    }

    /**
     * Set orderRecord.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\OrderRecord $orderRecord
     *
     * @return OrderRecordItem
     */
    public function setOrderRecord(\Jumpersoft\EcommerceBundle\Entity\OrderRecord $orderRecord)
    {
        $this->orderRecord = $orderRecord;

        return $this;
    }

    /**
     * Get orderRecord.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\OrderRecord
     */
    public function getOrderRecord()
    {
        return $this->orderRecord;
    }

    /**
     * Set item.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Item $item
     *
     * @return OrderRecordItem
     */
    public function setItem(\Jumpersoft\EcommerceBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Add inventory.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\OrderRecordItemInventory $inventory
     *
     * @return OrderRecordItem
     */
    public function addInventory(\Jumpersoft\EcommerceBundle\Entity\OrderRecordItemInventory $inventory)
    {
        $this->inventory[] = $inventory;

        return $this;
    }

    /**
     * Remove inventory.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\OrderRecordItemInventory $inventory
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeInventory(\Jumpersoft\EcommerceBundle\Entity\OrderRecordItemInventory $inventory)
    {
        return $this->inventory->removeElement($inventory);
    }

    /**
     * Get inventory.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * Set currency.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Currency $currency
     *
     * @return OrderRecordItem
     */
    public function setCurrency(\Jumpersoft\EcommerceBundle\Entity\Currency $currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set priceType.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Type $priceType
     *
     * @return OrderRecordItem
     */
    public function setPriceType(\Jumpersoft\EcommerceBundle\Entity\Type $priceType)
    {
        $this->priceType = $priceType;

        return $this;
    }

    /**
     * Get priceType.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Type
     */
    public function getPriceType()
    {
        return $this->priceType;
    }

    /**
     * Set status.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Status $status
     *
     * @return OrderRecordItem
     */
    public function setStatus(\Jumpersoft\EcommerceBundle\Entity\Status $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add shipmentItem.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ShipmentItem $shipmentItem
     *
     * @return OrderRecordItem
     */
    public function addShipmentItem(\Jumpersoft\EcommerceBundle\Entity\ShipmentItem $shipmentItem)
    {
        $this->shipmentItems[] = $shipmentItem;

        return $this;
    }

    /**
     * Remove shipmentItem.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\ShipmentItem $shipmentItem
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeShipmentItem(\Jumpersoft\EcommerceBundle\Entity\ShipmentItem $shipmentItem)
    {
        return $this->shipmentItems->removeElement($shipmentItem);
    }

    /**
     * Get shipmentItems.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShipmentItems()
    {
        return $this->shipmentItems;
    }

    /**
     * Set containerStatus.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Status|null $containerStatus
     *
     * @return OrderRecordItem
     */
    public function setContainerStatus(\Jumpersoft\EcommerceBundle\Entity\Status $containerStatus = null)
    {
        $this->containerStatus = $containerStatus;

        return $this;
    }

    /**
     * Get containerStatus.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Status|null
     */
    public function getContainerStatus()
    {
        return $this->containerStatus;
    }

    public function getSalePrice(): ?string
    {
        return $this->salePrice;
    }

    public function setSalePrice(string $salePrice): self
    {
        $this->salePrice = $salePrice;

        return $this;
    }
}
