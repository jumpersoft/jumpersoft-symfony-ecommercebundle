<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="StoreViewType", indexes={@ORM\Index(name="search_idx", columns={"name"})})
 *
 * @author Angel
 */
class StoreViewType
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="name", length=100, nullable=FALSE)
     */
    private $name;

    /**
     * @ORM\Column(type="string", name="description", length=255, nullable=TRUE)
     */
    private $description;

    /**
     * Desc: indica si permitira ingresar mas de una tipo en StoreViewCategory o StoreViewItem
     * @ORM\Column(type="boolean", name="multipleChildren", nullable=TRUE)
     */
    protected $multipleChildren;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="groupId", referencedColumnName="id", nullable=FALSE)
     */
    protected $group;

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return StoreViewType
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getMultipleChildren(): ?bool
    {
        return $this->multipleChildren;
    }

    public function setMultipleChildren(?bool $multipleChildren): self
    {
        $this->multipleChildren = $multipleChildren;

        return $this;
    }

    public function getGroup(): ?Type
    {
        return $this->group;
    }

    public function setGroup(?Type $group): self
    {
        $this->group = $group;

        return $this;
    }
}
