<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="StoreViewSubGroup")
 * @author Angel
 */
class StoreViewSubGroup extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="StoreView", inversedBy="subGroups")
     * @ORM\JoinColumn(name="storeViewId", referencedColumnName="id", nullable=FALSE)
     */
    protected $storeView;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="typeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $type;

    /**
     * @ORM\Column(type="string", name="title", length=255, nullable=TRUE)
     */
    protected $title;
    
    /**
     * @ORM\Column(type="string", name="subtitle", length=255, nullable=TRUE)
     */
    protected $subtitle;

    /**
     * @ORM\Column(type="boolean", name="active", nullable=TRUE)
     */
    protected $active;

    /**
     * @ORM\Column(type="smallint", name="sequence", nullable=TRUE)
     */
    protected $sequence;
    
    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="categoryId", referencedColumnName="id", nullable=TRUE, onDelete="CASCADE")
     */
    protected $category;
    
    /**
     * @ORM\Column(type="smallint", name="maxItems", nullable=FALSE)
     */
    protected $maxItems;
    
    /**
     * @ORM\ManyToMany(targetEntity="Type")
     * @ORM\JoinTable(name="StoreViewSubGroupFilter",
     *      joinColumns={@ORM\JoinColumn(name="subGroupId", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="filterId", referencedColumnName="id")}
     *      )
     */
    protected $filters;

    public function __construct()
    {
        $this->filters = new ArrayCollection();
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setSubtitle(?string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(?int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }

    public function getStoreView(): ?StoreView
    {
        return $this->storeView;
    }

    public function setStoreView(?StoreView $storeView): self
    {
        $this->storeView = $storeView;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }
   

    /**
     * @return Collection|Type[]
     */
    public function getFilters(): Collection
    {
        return $this->filters;
    }

    public function addFilter(Type $filter): self
    {
        if (!$this->filters->contains($filter)) {
            $this->filters[] = $filter;
        }

        return $this;
    }

    public function removeFilter(Type $filter): self
    {
        $this->filters->removeElement($filter);

        return $this;
    }

    public function getMaxItems(): ?int
    {
        return $this->maxItems;
    }

    public function setMaxItems(int $maxItems): self
    {
        $this->maxItems = $maxItems;

        return $this;
    }
}
