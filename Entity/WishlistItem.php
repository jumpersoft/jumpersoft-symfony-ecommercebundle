<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="WishlistItem")
 *
 * @author Angel
 */
class WishlistItem extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Wishlist", inversedBy="items")
     * @ORM\JoinColumn(name="wishlistId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $wishlist;

    /**
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumn(name="itemId", referencedColumnName="id", nullable=FALSE)
     */
    protected $item;
   
    /**
     * Comment
     *
     * @ORM\Column(type="string", name="comment", length=1000, nullable=TRUE)
     */
    protected $comment;


    /**
     * Set id
     *
     * @param string $id
     *
     * @return WishlistItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return WishlistItem
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set wishlist
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Wishlist $wishlist
     *
     * @return WishlistItem
     */
    public function setWishlist(\Jumpersoft\EcommerceBundle\Entity\Wishlist $wishlist)
    {
        $this->wishlist = $wishlist;

        return $this;
    }

    /**
     * Get wishlist
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Wishlist
     */
    public function getWishlist()
    {
        return $this->wishlist;
    }

    /**
     * Set item
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Item $item
     *
     * @return WishlistItem
     */
    public function setItem(\Jumpersoft\EcommerceBundle\Entity\Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }
}
