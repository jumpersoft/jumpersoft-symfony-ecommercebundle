<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\CategoryRepository")
 * @ORM\Table(name="Category", indexes={@ORM\Index(name="search_idx", columns={"name"})})
 *
 * @author Angel
 */
class Category extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=20)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Store")
     * @ORM\JoinColumn(name="storeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $store;

    /**
     * @ORM\Column(type="string", name="name", length=255, nullable=FALSE)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", name="label", length=100, nullable=TRUE)
     * Desc: descripción alterna que puede acompañar a la categoría, ej, menú llamado Moda, la etiqueta estaría señalando como ribbons, tag, tooltip
     *       diría algo como nuevas promociones a manera de que hay algo nuevo sobre ese menú o categoría
     */
    protected $label;

    /**
     * @ORM\Column(type="string", name="cssIcon", length=100, nullable=TRUE)
     */
    protected $cssIcon;
    
    /**
     * @ORM\Column(type="string", name="description", length=500, nullable=TRUE)
     */
    protected $description;

    /**
     * @ORM\Column(type="smallint", name="sequence", nullable=TRUE)
     */
    protected $sequence;

    /**
     * @ORM\Column(type="boolean", name="active", nullable=TRUE)
     */
    protected $active;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     * @ORM\JoinColumn(name="parentId", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $parent;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="categoryRelatedId", referencedColumnName="id", nullable=TRUE)
     */
    protected $categoryRelated;

    /**
     * @ORM\Column(type="boolean", nullable=TRUE)
     */
    protected $folder;

    /**
     * @ORM\ManyToOne(targetEntity="CategoryType", inversedBy="categories")
     * @ORM\JoinColumn(name="typeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $type;

    /**
     * TODO Se debe quitar cuando esten completados los módulos de catregorías tanto del panel como de la tienda en VUE
     * @ORM\Column(type="string", name="title", length=255, nullable=TRUE)
     */
    protected $title;

    /**
     * TODO Se debe quitar cuando esten completados los módulos de catregorías tanto del panel como de la tienda en VUE
     * @ORM\Column(type="string", name="subtitle", length=255, nullable=TRUE)
     */
    protected $subtitle;

    /**
     * TODO Se debe quitar cuando esten completados los módulos de catregorías tanto del panel como de la tienda en VUE
     * @ORM\Column(type="text", length=100000, nullable=TRUE)
     */
    protected $text;

    /**
     * @ORM\Column(type="string", name="externalUrl", length=255, nullable=TRUE)
     */
    protected $externalUrl;

    /**
     * @ORM\Column(type="string", name="urlKey", length=255, nullable=TRUE)
     */
    protected $urlKey;

    /**
     * @ORM\Column(type="string", name="metaTitle", length=100, nullable=TRUE)
     */
    protected $metaTitle;

    /**
     * @ORM\Column(type="string", name="metaKeywords", length=255, nullable=TRUE)
     */
    protected $metaKeywords;

    /**
     * @ORM\Column(type="string", name="metaDescription", length=255, nullable=TRUE)
     */
    protected $metaDescription;

    /**
     * @ORM\OneToMany(targetEntity="Document", mappedBy="category")
     */
    protected $images;
    
    /**
     * @ORM\Column(type="boolean", name="notShowFilters", nullable=TRUE)
     */
    protected $notShowFilters;

    /**
     * Indica que se usara para marcar productos nuevos, solo debe haber un valor 1 en todas las categorías, para evitar sobrecargar los subqueries cuando se busques productos nuevos
     * @ORM\Column(type="boolean", name="useForNewItem", nullable=TRUE)
     */
    protected $useForNewItem;

    /**
     * Fecha de etiquetado como Nuevo
     * @ORM\Column(type="datetime", name="newUntil", nullable=TRUE)
     */
    protected $newUntil;

    /**
     * @ORM\OneToMany(targetEntity="ItemCategory", mappedBy="category")
     */
    protected $items;

    /**
     * Ruta de archivos del producto
     *
     * @ORM\Column(type="string", name="urlFiles", length=500, nullable=TRUE)
     */
    protected $urlFiles;

    /**
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="viewTypeId", referencedColumnName="id", nullable=FALSE)
     */
    protected $viewType;

    /**
     * @ORM\ManyToOne(targetEntity="StoreViewVersion")
     * @ORM\JoinColumn(name="viewVersionId", referencedColumnName="id", nullable=TRUE)
     */
    protected $viewVersion;

    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    /**
     * @ORM\Column(type="datetime", name="updateDate", nullable=TRUE)
     */
    protected $updateDate;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->items = new ArrayCollection();
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(?int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getFolder(): ?bool
    {
        return $this->folder;
    }

    public function setFolder(?bool $folder): self
    {
        $this->folder = $folder;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setSubtitle(?string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getUrlKey(): ?string
    {
        return $this->urlKey;
    }

    public function setUrlKey(?string $urlKey): self
    {
        $this->urlKey = $urlKey;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(?string $metaKeywords): self
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getUseForNewItem(): ?bool
    {
        return $this->useForNewItem;
    }

    public function setUseForNewItem(?bool $useForNewItem): self
    {
        $this->useForNewItem = $useForNewItem;

        return $this;
    }

    public function getNewUntil(): ?\DateTimeInterface
    {
        return $this->newUntil;
    }

    public function setNewUntil(?\DateTimeInterface $newUntil): self
    {
        $this->newUntil = $newUntil;

        return $this;
    }

    public function getUrlFiles(): ?string
    {
        return $this->urlFiles;
    }

    public function setUrlFiles(?string $urlFiles): self
    {
        $this->urlFiles = $urlFiles;

        return $this;
    }

    public function getRegisterDate(): ?\DateTimeInterface
    {
        return $this->registerDate;
    }

    public function setRegisterDate(\DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(Category $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(Category $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getCategoryRelated(): ?self
    {
        return $this->categoryRelated;
    }

    public function setCategoryRelated(?self $categoryRelated): self
    {
        $this->categoryRelated = $categoryRelated;

        return $this;
    }

    public function getType(): ?CategoryType
    {
        return $this->type;
    }

    public function setType(?CategoryType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Document $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setCategory($this);
        }

        return $this;
    }

    public function removeImage(Document $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getCategory() === $this) {
                $image->setCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ItemCategory[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(ItemCategory $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->addCategory($this);
        }

        return $this;
    }

    public function removeItem(ItemCategory $item): self
    {
        if ($this->items->removeElement($item)) {
            $item->removeCategory($this);
        }

        return $this;
    }

    public function getExternalUrl(): ?string
    {
        return $this->externalUrl;
    }

    public function setExternalUrl(?string $externalUrl): self
    {
        $this->externalUrl = $externalUrl;

        return $this;
    }

    public function getViewVersion(): ?StoreViewVersion
    {
        return $this->viewVersion;
    }

    public function setViewVersion(?StoreViewVersion $viewVersion): self
    {
        $this->viewVersion = $viewVersion;

        return $this;
    }

    public function getViewType(): ?Type
    {
        return $this->viewType;
    }

    public function setViewType(?Type $viewType): self
    {
        $this->viewType = $viewType;

        return $this;
    }

    public function getNotShowFilters(): ?bool
    {
        return $this->notShowFilters;
    }

    public function setNotShowFilters(?bool $notShowFilters): self
    {
        $this->notShowFilters = $notShowFilters;

        return $this;
    }

    public function getCssIcon(): ?string
    {
        return $this->cssIcon;
    }

    public function setCssIcon(?string $cssIcon): self
    {
        $this->cssIcon = $cssIcon;

        return $this;
    }
}
