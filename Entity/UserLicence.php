<?php

namespace Jumpersoft\EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Jumpersoft\EcommerceBundle\Repository\UserLicenceRepository")
 * @ORM\Table(name="UserLicence")
 *
 * @author Angel
 */
class UserLicence extends JumpersoftModel
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="id", length=32)
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="licences")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", length=30, nullable=FALSE, unique=true)
     */
    protected $folio;

    /**
     * @ORM\Column(type="datetime", name="startDate", nullable=FALSE)
     */
    protected $startDate;

    /**
     * @ORM\Column(type="datetime", name="endDate", nullable=FALSE)
     */
    protected $endDate;

    /**
     * @ORM\Column(type="string", name="notes", length=1000, nullable=TRUE)
     */
    protected $notes;

    /**
     * @ORM\OneToMany(targetEntity="UserLicenceIsotope", mappedBy="licence")
     */
    protected $isotopes;

    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="statusId", referencedColumnName="id", nullable=FALSE)
     */
    protected $status;
    
    /**
     * @ORM\Column(type="datetime", name="registerDate", nullable=FALSE)
     */
    protected $registerDate;

    public function __construct()
    {
        $this->isotopes = new ArrayCollection();
    }

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return UserLicence
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set folio.
     *
     * @param string $folio
     *
     * @return UserLicence
     */
    public function setFolio($folio)
    {
        $this->folio = $folio;

        return $this;
    }

    /**
     * Get folio.
     *
     * @return string
     */
    public function getFolio()
    {
        return $this->folio;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return UserLicence
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime $endDate
     *
     * @return UserLicence
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set user.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\User $user
     *
     * @return UserLicence
     */
    public function setUser(\Jumpersoft\EcommerceBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add isotope.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\UserLicenceIsotope $isotope
     *
     * @return UserLicence
     */
    public function addIsotope(\Jumpersoft\EcommerceBundle\Entity\UserLicenceIsotope $isotope)
    {
        $this->isotopes[] = $isotope;

        return $this;
    }

    /**
     * Remove isotope.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\UserLicenceIsotope $isotope
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeIsotope(\Jumpersoft\EcommerceBundle\Entity\UserLicenceIsotope $isotope)
    {
        return $this->isotopes->removeElement($isotope);
    }

    /**
     * Get isotopes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIsotopes()
    {
        return $this->isotopes;
    }

    /**
     * Set notes.
     *
     * @param string|null $notes
     *
     * @return UserLicence
     */
    public function setNotes($notes = null)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes.
     *
     * @return string|null
     */
    public function getNotes()
    {
        return $this->notes;
    }


    /**
     * Set status.
     *
     * @param \Jumpersoft\EcommerceBundle\Entity\Status $status
     *
     * @return UserLicence
     */
    public function setStatus(\Jumpersoft\EcommerceBundle\Entity\Status $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \Jumpersoft\EcommerceBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set registerDate.
     *
     * @param \DateTime $registerDate
     *
     * @return UserLicence
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate.
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }
}
