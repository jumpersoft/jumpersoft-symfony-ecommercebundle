<?php

namespace Jumpersoft\EcommerceBundle\DependencyInjection;

use Doctrine\ORM\EntityManagerInterface;
use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftUtilExtension;

class RadesaActions
{
    private $em;
    private $jsUtil;

    public function __construct(EntityManagerInterface $em, JumpersoftUtilExtension $jsUtil)
    {
        $this->em = $em;
        $this->jsUtil = $jsUtil;
    }

    public function validateActivityByDay($customerId, $itemId, $activity, $orderRecordItemId = null)
    {
        $item = $this->em->getRepository("E:Item")->findOneById($itemId);
        if ($item->getRadioactive()) {
            $isotopeId = $item->getIsotope()->getId();
            $parameters = [
                "customerId" => $customerId,
                "isotopeId" => $isotopeId,
                "startDate" => $this->jsUtil->getLocalDateTime()->format('Y-m-d 00:00:00'),
                "endDate" => $this->jsUtil->getLocalDateTime()->format('Y-m-d 23:59:59'),
                "orderRecordItemId" => $orderRecordItemId ?? 0
            ];

            //Get order record items by day, isotope and shipping customer
            $orItems = $this->em->createQuery("select oi.quantity activity, um.id unitMeasureId, identity(i.isotope) isotopeId, i.name, i.id itemId
                                            from E:OrderRecordItem oi
                                            join oi.orderRecord o
                                            join o.customer c
                                            join oi.item i
                                            join i.unitMeasureBulkSale um                                                        
                                           where c.id = :customerId
                                             and identity(o.status) not in ('ORD_CAN', 'ORD_RET')                                                         
                                             and i.radioactive = 1
                                             and identity(i.isotope) = :isotopeId
                                             and o.date >= :startDate
                                             and o.date <= :endDate
                                             and oi.id <> :orderRecordItemId
                                             ")->setParameters($parameters)->getResult();

            //Convert to same unit (mCi) and get sum
            $activityAcum = 0;
            $unitMeasures = $this->em->getRepository("E:UnitMeasure")->findByGroupId("RAD");
            foreach ($orItems as $oi) {
                if ($oi["unitMeasureId"] != "UNI_MCI") {
                    $conversions = $this->jsUtil->findOneInArray($unitMeasures, fn ($d) => $d->getId() == $oi["unitMeasureId"])->getConversion();
                    $toMcI = $this->jsUtil->findOneInArray($conversions, fn ($d) => $d["to"] == "mCi");
                    $activityAcum += $oi["activity"] * $toMcI["factor"];
                } else {
                    $activityAcum += $oi["activity"];
                }
            }

            //Convert orderRecordItem to mCi
            if ($item->getUnitMeasureBulkSale()->getId() != "UNI_MCI") {
                $conversions = $this->jsUtil->findOneInArray($unitMeasures, fn ($d) => $d->getId() == $item->getUnitMeasureBulkSale()->getId())->getConversion();
                $toMcI = $this->jsUtil->findOneInArray($conversions, fn ($d) => $d["to"] == "mCi");
                $activity = $activity * $toMcI["factor"];
            }

            //Get licence data
            $isotope = $this->em->createQuery("select iso.id isotopeId, max(lic.endDate) endDate, lic.startDate, IFNULL(isos.activityByDay, 0) activityByDay, um.id unitMeasureId
                                                  from E:User c                                                        
                                                  join c.licences lic
                                                  join lic.isotopes isos
                                                  join isos.isotope iso
                                                  join isos.unitMeasure um
                                                 where c.id = :customerId
                                                   and iso.id = :isotopeId
                                                   and lic.endDate >= now()
                                                   and lic.startDate <= now()
                                                       ")->setParameters(["customerId" => $customerId, "isotopeId" => $isotopeId])->getSingleResult();

            if ($isotope["activityByDay"] == 0) {
                throw new \Exception("La licencia del cliente no existe o a caducado y no pueden venderse productos radiactivos");
            }

            if ($activityAcum + $activity > $isotope["activityByDay"]) {
                throw new \Exception("La actividad límite por día para venta de productos radioactivos es rebasada, actividad disponible: " . number_format($isotope["activityByDay"] - $activityAcum, 4) . " mCi");
            }
        }
    }

    public function validateActivityCustomerShipping($customerId, $orderRecordId)
    {
        $customer = $this->em->getRepository("E:User")->findOneById($customerId);
        $orItems = $this->em->createQuery("select oi
                                             from E:OrderRecordItem oi
                                             join oi.item i
                                            where identity(oi.orderRecord) = :orderRecordId
                                              and i.radioactive = 1 ")->setParameters(["orderRecordId" => $orderRecordId])->getResult();
        if (count($orItems) > 0) {
            foreach ($orItems as $oi) {
                $this->validateActivityByDay($customerId, $oi->getItem()->getId(), $oi->getQuantity(), $oi->getId());
            }
        }
    }
}
