<?php
namespace Jumpersoft\EcommerceBundle\DependencyInjection;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\IpUtils;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftUtilExtension;

/**
 * @author Angel
 */
class StoreActionsResult
{

    public $success = false;
    public $msg = "";
    public $cookieName = 'wishlistId';
    public $createNewCookie = false;
    public $cookieId = "";
    public $cookieExpiration = null;
    public $removeCookie = false;
    public $jsUtil;

}

class StoreActions
{

    private $em;
    private $request = null;
    private $result = null;
    private $defaultStoreId = null;

    public function __construct($defaultStoreId, RequestStack $requestStack = null, EntityManagerInterface $em, JumpersoftUtilExtension $jsUtil)
    {
        $this->defaultStoreId = $defaultStoreId;
        $this->request = $requestStack->getCurrentRequest();
        $this->em = $em;
        $this->result = new StoreActionsResult();
        $this->jsUtil = $jsUtil;
    }

    /**
     * mergeWishlist
     */
    public function mergeWishlist($wishlistId, $user)
    {
        try {
            if ($this->em->transactional(function ($em) use ($wishlistId, $user) {
                    $wlAnon = $this->em->getRepository('E:Wishlist')->getStoreWishlist($wishlistId);
                    $wlUser = $this->em->getRepository('E:Wishlist')->getStoreWishlist(null, $user);
                    $opItems = $wlAnon ? $wlAnon->getItems() : null;
                    //Merge to an existig user wishlist
                    if ($wlUser && $wlAnon) {
                        $items = $wlUser->getItems();
                        foreach ($opItems as $opItem) {
                            if (count($item = current($items->filter(function ($i) use ($opItem) {
                                        return $i->getItem()->getId() === $opItem->getItem()->getId();
                                    }))) == 0) {
                                //Add Item
                                $opItem->setWishlist($this->em->getReference('E:Wishlist', $wlUser->getId()));
                                $wlUser->addItem($opItem);
                                $this->em->flush();
                            }
                        }
                        //Remove cookie from client
                        $this->result->cookieId = $wlAnon->getId();
                        $this->result->removeCookie = true;
                        //Remove orphan cart and update customer cookie
                        $this->em->remove($wlAnon);
                    } elseif ($wlAnon) {
                        //Make orphan cart the new customer cart
                        $wlUser = $wlAnon;
                        $wlUser->setUser($this->em->getReference('E:User', $user->getId()));
                        //Remove cookie from client
                        $this->result->cookieId = $wlUser->getId();
                        $this->result->removeCookie = true;
                    }
                })) {
                $this->result->success = true;
            } else {
                $this->result->msg = $this->result->msg ? $this->result->msg : "Error al actualizar la lista";
                throw new \Exception();
            }
        } catch (\Exception $e) {
            $this->result->success = false;
            return $this->result;
        }

        return $this->result;
    }

    /**
     * mergeComparisonlist
     */
    public function mergeComparisonlist($comparisonId, $user)
    {
        try {
            if ($this->em->transactional(function ($em) use ($comparisonId, $user) {
                    $wlAnon = $this->em->getRepository('E:Comparison')->getStoreComparison($comparisonId);
                    $wlUser = $this->em->getRepository('E:Comparison')->getStoreComparison(null, $user);
                    $opItems = $wlAnon ? $wlAnon->getItems() : null;
                    //Merge to an existig user comparison list
                    if ($wlUser && $wlAnon) {
                        $items = $wlUser->getItems();
                        foreach ($opItems as $opItem) {
                            if (count($item = current($items->filter(function ($i) use ($opItem) {
                                        return $i->getItem()->getId() === $opItem->getItem()->getId();
                                    }))) == 0) {
                                //Add Item
                                $opItem->setComparison($this->em->getReference('E:Comparison', $wlUser->getId()));
                                $wlUser->addItem($opItem);
                                $this->em->flush();
                            }
                        }
                        //Remove cookie from client
                        $this->result->cookieId = $wlAnon->getId();
                        $this->result->removeCookie = true;
                        //Remove orphan cart and update customer cookie
                        $this->em->remove($wlAnon);
                    } elseif ($wlAnon) {
                        //Make orphan cart the new customer cart
                        $wlUser = $wlAnon;
                        $wlUser->setUser($this->em->getReference('E:User', $user->getId()));
                        //Remove cookie from client
                        $this->result->cookieId = $wlUser->getId();
                        $this->result->removeCookie = true;
                    }
                })) {
                $this->result->success = true;
            } else {
                $this->result->msg = $this->result->msg ? $this->result->msg : "Error al actualizar la lista";
                throw new \Exception();
            }
        } catch (\Exception $e) {
            $this->result->success = false;
            return $this->result;
        }

        return $this->result;
    }

    public function setWebHit($item, $request)
    {
        // basic crawler detection and block script (no legit browser should match this)
        if (!empty($item["id"]) && !empty($request->headers->get('User-Agent')) and!preg_match('~(bot|crawl)~i', $request->headers->get('User-Agent'))) {
            //TODO Revisar posteriormente que $request->getClientIp si traiga el IP correcto, pues si la aplicación esta detras de un proxy puede que no obtenga el IP de cliente correcto. https://symfony.com/doc/4.4/deployment/proxies.html
            $ipEncoded = password_hash($request->getClientIp(), PASSWORD_BCRYPT, ["salt" => "$@3F3RR3f@$12Vdqzqz21|14"]);
            if (!$this->em->getRepository("E:WebHit")->getItemByIp($ipEncoded, $item["id"])) {
                $statement = $this->em->getConnection()->prepare("update item set views = ifnull(views,0) + 1 where id = :id ");
                $statement->bindValue('id', $item["id"]);
                $statement->execute();
                // ADD WEBHITS Y WEBHITSITEM
                $eventDate = $this->jsUtil->getLocalDateTime();
                if (!($webHit = $this->em->getRepository("E:WebHit")->getByIp($ipEncoded))) {
                    $webHit = new Entity\WebHit();
                    $this->jsUtil->setValuesToEntity($webHit, ["id" => $this->jsUtil->getUid(), "ipEncoded" => $ipEncoded, "ipAnonymized" => IPUtils::anonymize($request->getClientIp()),
                        "registerDate" => $eventDate, "type" => "HIT_ITE"]);
                    $this->em->persist($webHit);
                }
                $webHitItem = new Entity\WebHitItem();
                $this->jsUtil->setValuesToEntity($webHitItem, ["id" => $this->jsUtil->getUid(), "registerDate" => $eventDate, "webHit" => $webHit, "item" => $item["id"]]);
                $this->em->persist($webHitItem);
                $this->em->flush();
            }
        }
    }
}
