<?php

namespace Jumpersoft\EcommerceBundle\DependencyInjection;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Jumpersoft\EcommerceBundle\Validators\OrderRecordValidator;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\EcommerceBundle\DependencyInjection\PaymentGateway;
use Jumpersoft\EcommerceBundle\DependencyInjection;
use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftUtilExtension;

/**
 * @author Angel
 */
class OrderRecordActions
{
    private $em;
    private $jsUtil;
    private $request = null;
    private $defaultStoreId = null;
    public $result = ["success" => true, "msg" => "", "data" => []];
    private $priceTypeId = '';
    private $finalPrice = null;
    private $msg = [
        "noStock" => "El stock no es suficiente para surtir el producto.",
        "fulFilledCompleteOk" => "Surtido completo",
        "fulFilledPartialOk" => "Surtido parcial exitoso",
        "fulFilledAlready" => "El producto ya está surtido",
        "returnOk" => "Producto regresado al almacen",
        "noInventoryToReturn" => "La partida no tenía inventario que regresar al almacen",
        "errorToFulFill" => "No puede surtirse el pedido",
        "errorToReturn" => "Hubo problemas para regresar el surtido"
    ];

    public function __construct($defaultStoreId, RequestStack $requestStack = null, EntityManagerInterface $em, JumpersoftUtilExtension $jsUtil)
    {
        $this->defaultStoreId = $defaultStoreId;
        $this->request = $requestStack->getCurrentRequest();
        $this->em = $em;
        $this->jsUtil = $jsUtil;
    }

    /**
     * Desc: Pull stock and update based on quantity
     */
    public function pullStock($type, &$orItem, $quantity, $inventoryId = null, $orderRecordId = null)
    {
        $item = $orItem ? $orItem->getItem() : null;
        $eventDate = $this->jsUtil->getLocalDateTime();
        if ($type == "fromOrderItemAutoFill") {
            if ($item->getTrackingType()->getId() == "INV_TYP_SIM") {
                //INVENTORY SIMPLE AUTOFILL
                if ($orItem->getStatus()->getId() == "ORI_FUL_PEN") {
                    if ($item->getStock() >= $quantity) {
                        //PULL STOCK
                        $quantity = $this->getQuantityToPull($orItem, $quantity);
                        $item->setStock($item->getStock() + ($quantity * -1));
                        $this->em->persist($item);
                        $this->result["msg"] = $this->msg["fulFilledCompleteOk"];
                    } else {
                        $this->result["success"] = false;
                        $this->result["msg"] = $this->msg["noStock"];
                    }
                    //UPDATE ORDERITEM
                    $orItem->setQuantityFulFilled($quantity);
                    $orItem->setStatus($this->em->getReference('E:Status', 'ORI_FUL_SUC'));
                    $this->em->persist($orItem);
                } else {
                    $this->result["success"] = false;
                    $this->result["msg"] = $this->msg["fulFilledAlready"];
                }
            } elseif ($item->getTrackingType()->getId() == "INV_TYP_DET") {
                //INVENTORY DETAILED AUTOFILL
                $quantityRequired = $orItem->getQuantity() - $orItem->getQuantityFulFilled();
                if (($orItem->getStatus()->getId() == "ORI_FUL_PEN" || $orItem->getStatus()->getId() == "ORI_FUL_PAR") && $quantityRequired > 0) {
                    $realQuantityRequired = $this->getQuantityToPull($orItem, $quantityRequired);
                    //SEARCH AVAILABLE STOCK GROUPED
                    $inventoryItems = $this->em->getRepository("E:OrderRecordItem")->getInventoryItemsForAutoFill("grouped", $orItem->getItem()->getId(), $realQuantityRequired);
                    //SEARCH AVAILABLE STOCK COMPLETE BY ONE ITEM INVENTORY
                    $inventoryItems = count($inventoryItems) > 0 ? $inventoryItems : $this->em->getRepository("E:OrderRecordItem")->getInventoryItemsForAutoFill("complete", $orItem->getItem()->getId(), $realQuantityRequired);
                    if (count($inventoryItems) > 0) {
                        foreach ($inventoryItems as $inventory) {
                            $this->pullStock("fromOrderItemInventory", $orItem, $inventory["stock"] >= $realQuantityRequired ? $quantityRequired : $inventory["stock"], $inventory["id"]);
                            if (($quantityRequired = $orItem->getQuantity() - $orItem->getQuantityFulFilled()) > 0) {
                                continue;
                            } else {
                                break;
                            }
                        }
                        $this->result["msg"] = $quantityRequired == 0 ? $this->msg["fulFilledCompleteOk"] : $this->msg["noStock"];
                        $this->result["success"] = $quantityRequired == 0 ? true : false;
                    } else {
                        $this->result["success"] = false;
                        $this->result["msg"] = $this->msg["noStock"];
                    }
                } else {
                    $this->result["success"] = false;
                    $this->result["msg"] = $this->msg["fulFilledAlready"];
                }
            }
        } elseif ($type == "fromOrderAutoFill") {
            //AUTOFILL FROM ORDER
            if (count($orItems = $this->em->getRepository("E:OrderRecordItem")->getOrderRecordItemFulFilledPending($orderRecordId)) > 0) {
                foreach ($orItems as $orItem) {
                    $this->pullStock("fromOrderItemAutoFill", $orItem, $orItem->getQuantity());
                }
                $orderRecord = $orItems[0]->getOrderRecord();
                $this->updateOrderStatus("itemEdited", $orderRecord);
                $this->result["msg"] = $orderRecord->getStatus()->getId() == "ORD_FUL_SUC" ? $this->msg["fulFilledCompleteOk"] : $this->msg["noStock"];
                $this->result["success"] = $orderRecord->getStatus()->getId() == "ORD_FUL_SUC" ? true : false;
            } else {
                $this->result["success"] = false;
                $this->result["msg"] = $this->msg["errorToFulFill"];
            }
        } elseif ($type == "fromOrderItemInventory") {
            //FULFILL FROM INVENTORY
            $inventory = $this->em->getRepository("E:Inventory")->findOneById($inventoryId);
            $realQuantity = round($this->getQuantityToPull($orItem, $quantity, $inventory), 4);
            $stock = $this->getStock($orItem, $inventory);
            if ($stock >= $realQuantity) {
                //PULL STOCK
                $inventory->setStock($stock - $realQuantity);
                $this->em->persist($inventory);

                //ADD OR UPDATE ORDER ITEM INVENTORY
                if (($orInventory = $this->em->getRepository("E:OrderRecordItemInventory")->findOneBy(["orderRecordItem" => $orItem, "inventory" => $inventory]))) {
                    $orInventory->setQuantity($orInventory->getQuantity() + $quantity);
                    $orInventory->setRegisterDate($eventDate);
                } else {
                    $orInventory = new Entity\OrderRecordItemInventory($this->jsUtil->getUid(), $orItem, $this->em->getReference("E:Inventory", $inventoryId), $quantity, $eventDate);
                }
                $this->em->persist($orInventory);
                $this->em->flush();
                //UPDATE QUANTITY FULFILLED
                $orItem->setQuantityFulFilled($this->em->getRepository("E:OrderRecordItem")->getORItemInventoryQuantitySum($orItem->getId()));
                $orItem->setStatus($this->em->getReference("E:Status", $orItem->getQuantity() == $orItem->getQuantityFulFilled() ? "ORI_FUL_SUC" : "ORI_FUL_PAR"));
                $this->em->persist($orItem);

                //ADD INVENTORY OUT BY ORDER
                $out = new Entity\Inventory();
                $this->jsUtil->setValuesToEntity($out, [
                    "id" => $this->jsUtil->getUid(),
                    "registerDate" => $eventDate,
                    "eventDate" => $eventDate,
                    "operation" => "INV_OPE_OUT",
                    "type" => "INV_OUT_ORD",
                    "quantityOutput" => $realQuantity,
                    "item" => $inventory->getItem(),
                    "parent" => $inventory,
                    "orderRecordItemInventory" => $orInventory,
                    "serial" => $inventory->getItem()->getSerialized() ? $inventory->getSerial() : null,
                    "lot" => $inventory->getItem()->getSerialized() ? null : $inventory->getLot()
                ]);
                $this->em->persist($out);

                $this->result["msg"] = $orItem->getStatus()->getId() == "ORI_FUL_SUC" ? $this->msg["fulFilledCompleteOk"] : $this->msg["fulFilledPartialOk"];
            } else {
                $this->result["success"] = false;
                $this->result["msg"] = $this->msg["noStock"];
            }
        }
        $this->em->flush();
        return $this->result;
    }

    public function getQuantityToPull($orItem, $quantity, $inventory = null)
    {
        if ($inventory && $inventory->getItem()->getId() != $orItem->getItem()->getId()) {
            // DISTINCT ITEM IDS
            if (!$orItem->getItem()->getRadioactive()) {
                if ($orItem->getBulkSale()) {
                    return $quantity / $orItem->getItem()->getQuantityByUnitBulkSale() / $inventory->getItem()->getQuantityByUnitBulkSale();
                } else {
                    return $quantity / $inventory->getItem()->getQuantityByUnitBulkSale();
                }
            } else {
                return $quantity;
            }
        } elseif ($orItem->getBulkSale()) {
            // SAME ITEM ID BULK
            return $quantity / $orItem->getItem()->getQuantityByUnitBulkSale();
        } else {
            // SAME ITEM
            return $quantity;
        }
    }

    public function getStock($orItem, $inventory)
    {
        if ($inventory->getItem()->getId() != $orItem->getItem()->getId()) {
            if (!$inventory->getItem()->getRadioactive()) {
                return $inventory->getStock() * $inventory->getItem()->getQuantityByUnitBulkSale();
            } else {
                return $inventory->getStock();
            }//TODO Temporal en lo que se hacen los calculos
        } else {
            return $inventory->getStock();
        }
    }

    /**
     * Desc: Return stock and update based on quantity
     */
    public function returnStock($type, &$orItem, $quantity, $inventoryId = null, $orderRecordId = null)
    {
        $item = $orItem ? $orItem->getItem() : null;
        if ($type == "fromOrderItem") {
            if ($item->getTrackingType()->getId() == "INV_TYP_SIM") {
                //Inventory simple
                if ($orItem->getStatus()->getId() == "ORI_FUL_SUC") {
                    $quantity = $orItem->getBulkSale() ? $quantity / $item->getQuantityByUnitBulkSale() : $quantity;
                    $item->setStock($item->getStock() + $quantity);
                    $this->em->persist($item);
                    $this->result["msg"] = $this->msg["returnOk"];
                    //UPDATE ORDER ITEM STATUS
                    $orItem->setStatus($this->em->getReference("E:Status", "ORI_FUL_PEN"));
                    $orItem->setQuantityFulFilled(0);
                    $this->em->persist($orItem);
                } else {
                    $this->result["success"] = false;
                    $this->result["msg"] = $this->msg["noInventoryToReturn"];
                }
            } elseif ($item->getTrackingType()->getId() == "INV_TYP_DET") {
                //Inventory detailed
                if (count($rows = $this->em->getRepository("E:OrderRecordItemInventory")->findBy(["orderRecordItem" => $orItem])) > 0) {
                    foreach ($rows as $orItemInventory) {
                        $inventory = $orItemInventory->getInventory();
                        $realQuantity = $this->getQuantityToPull($orItem, $orItemInventory->getQuantity(), $inventory);
                        $inventory->setStock($inventory->getStock() + $realQuantity);
                        $this->em->persist($inventory);
                        $this->em->remove($orItemInventory);
                        $orItem->setQuantityFulFilled($orItem->getQuantityFulFilled() - $orItemInventory->getQuantity());
                    }
                    //UPDATE ORDER ITEM STATUS
                    $orItem->setStatus($this->em->getReference("E:Status", "ORI_FUL_PEN"));
                    $this->em->persist($orItem);

                    $this->result["msg"] = $this->msg["returnOk"];
                } else {
                    $this->result["success"] = false;
                    $this->result["msg"] = $this->msg["noInventoryToReturn"];
                }
            }
        } elseif ($type == "fromOrderItemInventory") {
            $orItemInventory = $this->em->getRepository("E:OrderRecordItemInventory")->findOneById($inventoryId);
            $inventory = $orItemInventory->getInventory();
            $realQuantity = $orItem->getBulkSale() ? $orItemInventory->getQuantity() / $item->getQuantityByUnitBulkSale() : $orItemInventory->getQuantity();
            $inventory->setStock($inventory->getStock() + $realQuantity);
            $this->em->persist($inventory);
            $this->em->remove($orItemInventory);
            $this->result["msg"] = $this->msg["returnOk"];
            $orItem->setQuantityFulFilled($orItem->getQuantityFulFilled() - $orItemInventory->getQuantity());
            //UPDATE ORDER ITEM STATUS
            $orItem->setStatus($this->em->getReference("E:Status", $orItem->getQuantityFulFilled() == 0 ? "ORI_FUL_PEN" : ($orItem->getQuantity() == $orItem->getQuantityFulFilled() ? "ORI_FUL_SUC" : "ORI_FUL_PAR")));
            $this->em->persist($orItem);
        } elseif ($type == "fromOrder") {
            //RETURN ALL INVENTORY FROM ORDER ITEMS
            if (count($orItems = $this->em->getRepository("E:OrderRecordItem")->getOrderRecordItemFulFilled($orderRecordId)) > 0) {
                foreach ($orItems as $orItem) {
                    $this->returnStock("fromOrderItem", $orItem, $orItem->getQuantity());
                }
                $orderRecord = $orItems[0]->getOrderRecord();
                $this->updateOrderStatus("itemEdited", $orderRecord);
                $this->result["msg"] = $orderRecord->getStatus()->getId() == "ORD_FUL_PEN" ? $this->msg["returnOk"] : $this->msg["errorToReturn"];
                $this->result["success"] = $orderRecord->getStatus()->getId() == "ORD_FUL_PEN" ? true : false;
            } else {
                $this->result["success"] = false;
                $this->result["msg"] = $this->msg["errorToReturn"];
            }
        }

        $this->em->flush();
        return $this->result;
    }

    /**
     * Desc: Set totals on OrderRecord once a item was added, modified or changed
     */
    public function setTotals($orderRecord)
    {
        $sums = $this->em->getRepository('E:OrderRecord')->getOrderRecordItemsSum($orderRecord->getId());
        $orderRecord->setSubTotal($sums['subTotal'] ?? 0);
        $orderRecord->setTotal($sums['total'] ?? 0);
        $orderRecord->setDiscount($sums['subTotal'] - $sums['total']);
    }

    /**
     * Desc: Set the lowest prices based on base price, bulk prices or list prices.
     */
    public function setLowestPriceAndTotals($orItem, $item, $data, $exchangeRate)
    {
        /* TODO AQUI FALTA OBTENER LOS DESCUENTOS, EL TIPO DE PRECIO, MONEDA,Y TOTALES DEL CLIENTE PARA COMPARARLOS CON LOS CALCULADOS
         * INTERNAMENTE PARA QUE SEA EXACTA LA INFO MOSTRADA Y LA QUE SE TERMINARÁ TOMANDO EN CUENTA QUE ES LA CALCULADA INTERNAMENTE */
        $this->exchangeRate = $exchangeRate;
        //Get quantity based on trhee types
        $quantity = $orItem->getBulkSale() ? $data["quantity"] / $item["quantityByUnitBulkSale"] : ($item["fixedPrice"] == 1 ? 1 : $data["quantity"]);
        $this->setPriceType($item, $quantity);
        $orItem->setRegularPrice($this->finalPrice["regularPrice"]);
        $orItem->setCurrency($this->em->getReference("E:Currency", $this->finalPrice["currencyId"]));
        $orItem->setQuantity($quantity);
        $orItem->setSubTotal($quantity * $orItem->getRegularPrice());
        $orItem->setDiscount($this->finalPrice["discount"]);
        $orItem->setDiscountType($this->finalPrice["discountType"]);
        $orItem->setPriceType($this->em->getReference("E:Type", $this->priceTypeId));
        $orItem->setTotal($orItem->getSubTotal() - ($this->finalPrice["discount"] > 0 ? ($this->finalPrice["discountType"] == "$" ? $this->finalPrice["discount"] : $orItem->getSubTotal() * ($this->finalPrice["discount"] / 100)) : 0));
    }

    public function getBasePrice($item)
    {
        if ($item["parent"] && ($item["parent"]["handleGlobalPrice"] || $item["workWithPricesParent"])) {
            $item["parent"]["salePriceMXN"] = $item["parent"]["currencyId"] == "usd" ? $this->exchangeRate * $item["parent"]["salePrice"] : $item["parent"]["salePrice"];
            return $item["parent"];
        } else {
            $item["salePriceMXN"] = $item["currencyId"] == "usd" ? $this->exchangeRate * $item["salePrice"] : $item["salePrice"];
            return $item;
        }
    }

    public function getBulkPrice($item, $quantity)
    {
        if ($item["parent"] && ($item["parent"]["handleGlobalPrice"] || $item["workWithPricesParent"])) {
            if (count($item["parent"]["bulk"]) > 0 && ($parentBulk = $this->getItemInParentBulk($item, $quantity))) {
                return $parentBulk;
            }
        } elseif ($item["bulk"] && count($item["bulk"]) > 0 && ($itemBulk = $this->getItemInBulk($item, $quantity))) {
            return $itemBulk;
        }
        return ["salePriceMXN" => 0];
    }

    public function getListPrice($item)
    {
        if ($item["parent"] && ($item["parent"]["handleGlobalPrice"] || $item["workWithPricesParent"])) {
            if ($item["parentPriceList"] && $item["parentPriceList"]["salePrice"]) {
                $item["parentPriceList"]["salePriceMXN"] = $item["parent"]["currencyId"] == "usd" ? $this->exchangeRate * $item["parent"]["salePrice"] : $item["parent"]["salePrice"];
                return $item["parentPriceList"];
            }
        } elseif ($item["priceList"] && $item["priceList"]["salePrice"]) {
            $item["priceList"]["salePriceMXN"] = $item["priceList"]["currencyId"] == "usd" ? $this->exchangeRate * $item["priceList"]["salePrice"] : $item["priceList"]["salePrice"];
            return $item["priceList"];
        }
        return ["salePriceMXN" => 0];
    }

    public function getListBulkPrice($item, $quantity)
    {
        $listPrice = $this->getListPrice($item);
        if (isset($listPrice["bulk"]) && count($listPrice["bulk"]) > 0 && ($itemBulk = $this->getItemInBulk($listPrice, $quantity))) {
            return $itemBulk;
        }
        return ["salePriceMXN" => 0];
    }

    public function setPriceType($item, $quantity)
    {
        $listBulkPrice = $this->getListBulkPrice($item, $quantity);
        $basePrice = $this->getBasePrice($item);
        $bulkPrice = $this->getBulkPrice($item, $quantity);
        $listPrice = $this->getListPrice($item);
        if ($listBulkPrice["salePriceMXN"] > 0 && $listBulkPrice["salePriceMXN"] < $basePrice["salePriceMXN"] && ($listBulkPrice["salePriceMXN"] < $bulkPrice["salePriceMXN"] || $bulkPrice["salePriceMXN"] * 1.0 == 0) &&
                ($listBulkPrice["salePriceMXN"] < $listPrice["salePriceMXN"] || $listPrice["salePriceMXN"] * 1.0 == 0)) {
            $this->priceTypeId = "PRI_LBU";
            $this->finalPrice = $listBulkPrice;
        } elseif ($listPrice["salePriceMXN"] > 0 && $listPrice["salePriceMXN"] < $basePrice["salePriceMXN"] && ($listPrice["salePriceMXN"] < $bulkPrice["salePriceMXN"] || $bulkPrice["salePriceMXN"] * 1.0 == 0) &&
                ($listPrice["salePriceMXN"] < $listBulkPrice["salePriceMXN"] || $listBulkPrice["salePriceMXN"] * 1.0 == 0)) {
            $this->priceTypeId = "PRI_LIS";
            $this->finalPrice = $listPrice;
        } elseif ($bulkPrice["salePriceMXN"] > 0 && $bulkPrice["salePriceMXN"] < $basePrice["salePriceMXN"] && ($bulkPrice["salePriceMXN"] < $listPrice["salePriceMXN"] || $listPrice["salePriceMXN"] * 1.0 == 0) &&
                ($bulkPrice["salePriceMXN"] < $listBulkPrice["salePriceMXN"] || $listBulkPrice["salePriceMXN"] * 1.0 == 0)) {
            $this->priceTypeId = "PRI_BUL";
            $this->finalPrice = $bulkPrice;
        } else {
            $this->priceTypeId = "PRI_BAS";
            $this->finalPrice = $basePrice;
        }
    }

    public function getItemInParentBulk($item, $quantity)
    {
        if (($itemBulk = array_filter($item["parent"]["bulk"], function ($d) use ($quantity) {
            return $quantity >= $d["from"] * 1 && $quantity <= $d["to"] * 1;
        }))) {
            $itemBulk = reset($itemBulk);
            $itemBulk["salePriceMXN"] = $item["parent"]["currencyId"] == "usd" ? $this->exchangeRate * $itemBulk["salePrice"] : $itemBulk["salePrice"];
            $itemBulk["currencyId"] = $item["parent"]["currencyId"];
            return $itemBulk;
        } else {
            return null;
        }
    }

    public function getItemInBulk($item, $quantity)
    {
        if (($itemBulk = array_filter($item["bulk"], function ($d) use ($quantity) {
            return $quantity >= $d["from"] * 1 && $quantity <= $d["to"] * 1;
        }))) {
            $itemBulk = reset($itemBulk);
            $itemBulk["salePriceMXN"] = $item["currencyId"] == "usd" ? $this->exchangeRate * $itemBulk["salePrice"] : $itemBulk["salePrice"];
            $itemBulk["currencyId"] = $item["currencyId"];
            return $itemBulk;
        } else {
            return null;
        }
    }

    /**
     * Desc: Set status based on diferent actions
     */
    public function updateOrderStatus($action, $orderRecord)
    {
        $statusId = $orderRecord->getStatus()->getId();
        if ($action == "itemEdited") {
            if (count($items = $orderRecord->getItems()) > 0) {
                if ($items->forAll(function ($k, $v) {
                    return $v->getStatus()->getId() == "ORI_FUL_SUC";
                })) {
                    $statusId = "ORD_FUL_SUC";
                } else {
                    $statusId = "ORD_FUL_PEN";
                }
            } else {
                $statusId = "ORD_PEN";
            }
        }
        $orderRecord->setStatus($this->em->getReference("E:Status", $statusId));
        $this->result["success"] = true;
    }
}
