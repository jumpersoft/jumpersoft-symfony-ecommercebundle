<?php

namespace Jumpersoft\EcommerceBundle\DependencyInjection;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @author Angel
 */
class ShoppingCartSession
{
    private $cart = [];
    private $session = null;
    private $logger;
    private $emptyCart = [
        "items" => [], // "items" => ['itemdummyid' => [ 'regularPrice' => 0, 'quantity' => 0 ]]
        "count" => 0,
        "subTotal" => 0,
        "iva" => 0,
        "total" => 0,
        "lastItemInCart" => true //for currently item and response actions, this can help to update actions from client-side
    ];
    public $itemEmpty = [
        'regularPrice' => 0,
        'url' => '',
        'name' => '',
        'description' => ''
    ];

    public function __construct($logger, RequestStack $requestStack = null)
    {
        $this->logger = $logger;
        $this->requestStack = $requestStack;
        if ($this->requestStack->getCurrentRequest()) {
            $this->session = $this->requestStack->getCurrentRequest()->getSession();
        }
        $this->cart = $this->getCart();
    }

    /**
     * getCart
     * @return cart session or empty cart
     */
    public function getCart()
    {
        return $this->cart = $this->session == null ? $this->emptyCart : ($this->session->get("cart") ? $this->session->get("cart") : $this->emptyCart);
    }

    /**
     * addToCart
     * @param type $item
     */
    public function addToCart($item, $detail)
    {
        if (isset($this->cart["items"][$item])) {
            $this->cart["items"][$item]["quantity"] ++;
        } else {
            $this->cart["items"][$item]["quantity"] = 1;
            $this->cart["items"][$item]["regularPrice"] = $detail['regularPrice'];
            $this->cart["items"][$item]["url"] = $detail['url'];
            $this->cart["items"][$item]["name"] = $detail['name'];
            $this->cart["items"][$item]["description"] = $detail['description'];
        }
        $this->cart["lastItemInCart"] = true;
        $this->updateSession();
    }

    /**
     * deleteFromCart
     * @param type $item
     */
    public function deleteFromCart($item)
    {
        if (isset($this->cart["items"][$item])) {
            $this->cart["items"][$item]["quantity"] --;
            $this->cart["lastItemInCart"] = true;
            if ($this->cart["items"][$item]["quantity"] == 0) {
                unset($this->cart["items"][$item]);
                $this->cart["lastItemInCart"] = false;
            }
        }
        $this->updateSession();
    }

    /**
     * updateSession
     */
    private function updateSession()
    {
        $this->cart["count"] = count($this->cart["items"]);
        //totales
        $subTotal = 0;
        foreach ($this->cart["items"] as $item) {
            $subTotal += round($item["quantity"] * $item["regularPrice"], 2);
        }
        $this->cart["subTotal"] = $subTotal;
        $this->cart["total"] = round($subTotal * 1.16, 2);
        $this->cart["iva"] = round($this->cart["total"] - $subTotal, 2);
        $this->session->set("cart", $this->cart);
    }

    /**
     * getItemsCount
     * @return int
     */
    public function getItemsCount()
    {
        return $this->cart["count"];
    }

    /*
     * existItemInCart
     */

    public function existItemInCart($item)
    {
        return isset($this->cart["items"][$item]);
    }

    /**
     * getItemsIds
     * @return array
     */
    public function getItemsIds()
    {
        return $this->cart["items"];
    }

    /**
     * emptyCart
     */
    public function emptyCart()
    {
        $this->cart = $this->emptyCart;
        $this->session->set("cart", $this->cart);
    }

    /**
     * getCartInfo
     * Desc: Resumen de información del carrito
     * @return array
     */
    public function getCartInfo()
    {
        return $this->cart;
    }
}
