<?php

namespace Jumpersoft\EcommerceBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Description of PaymentGateway
 *
 * @author Angel
 */
class PaymentGateway
{
    private $container;
    private $logger;
    private $sandboxmode = true;
    private $errMsg = "";
    private $gateway = "";
    public $transactionResult = ["success" => false, "message" => "", "result" => [], "jsonResult" => ""];
    //General Constants
    public static $TR_PAYMENT_COMPLETE = "Transaction complete";
    public static $TR_PAYMENT_STATUS_IN_PROGRESS = "Transaction in progress";
    public static $ERROR_GENERAL = "General Error";
    public static $ERROR_PAYMENTTRANSACTION = "Error in payment transaction";
    public static $PM_CARD = "CARD";
    public static $PM_CARD_ONE_STEP = "CARD_ONE_STEP";
    public static $PM_BANK_ACCOUNT = "BANK_ACCOUNT";
    public static $PM_STORE = "STORE";
    public static $PM_FORM_ONE_EXIBITION = 'PAY_FRM_ONE';
    public static $PM_FORM_ONE_PARTIALS = 'PAY_FRM_PRT';

    public function __construct(LoggerInterface $logger, ContainerInterface $container)
    {
        $this->logger = $logger;
        $this->container = $container;
        $this->gateway = $this->container->getParameter('paymentgateway.provider');
        $this->sandboxmode = $this->container->getParameter('paymentgateway.sandboxmode');
        $this->instance = $this->getInstanceGateway();
        $this->setSandBoxIf();
    }

    //Add here charge functions for each gateway
    public function makeCharge($transactionData, $userProcessorId)
    {
        switch ($this->gateway) {
            case "openpay":
                return $this->op_makeCharge($transactionData, $userProcessorId);
        }
    }

    //Add add customer or card
    public function add($type, $data, $customerProcessorId = null)
    {
        switch ($type) {
            case "customer":
                switch ($this->gateway) {
                    case "openpay":
                        return $this->op_addCustomer($data);
                }
                break;
            case "card":
                switch ($this->gateway) {
                    case "openpay":
                        return $this->op_addCard($data, $customerProcessorId);
                }
                break;
        }
    }

    //Delete customer or card
    public function delete($type, $customerId, $cardId = null)
    {
        switch ($type) {
            case "card":
                switch ($this->gateway) {
                    case "openpay":
                        return $this->op_deleteCard($customerId, $cardId);
                }
                break;
        }
    }

    /**
     * STARTS CUSTOM FUNCTIONS BY GATEWAY
     */
    public function op_makeCharge($transactionData, $customerProcessorId)
    {
        try {
            /////////////////////////////////////////////////////////////////////////////////////////////
            //START Transaction event
            //Make charge and save result - return a transaction object and can not be serializable
            //
            // NOTE: -> For debug puporse uncommente this lines and commente all below transactions lines
            /* $this->transactionResult["result"] = [ "id" => substr(md5(uniqid(rand(), true)), 0, 32), "status" => "OK" ]; */
            ////////////

            if ($customerProcessorId) {
                $customer = $this->instance->customers->get($customerProcessorId);
                $tr = $customer->charges->create($transactionData);
            } else {
                $tr = $this->instance->charges->create($transactionData);
            }

            // General transaction data
            $this->transactionResult["result"] = [
                "creation_date" => $tr->creation_date,
                "currency" => $tr->currency,
                "operation_date" => $tr->operation_date,
                "operation_type" => $tr->operation_type,
                "status" => $tr->status,
                "transaction_type" => $tr->transaction_type,
                "error_message" => $tr->error_message,
                "id" => $tr->id
            ];

            //$this->transactionResult["result"]["serializableData"] = $tr->serializableData;
            $this->transactionResult["result"]["serializableData"] = [
                "method" => $tr->serializableData["method"],
                "conciliated" => $tr->serializableData["conciliated"],
                "operation_date" => $tr->serializableData["operation_date"],
                "description" => $tr->serializableData["description"],
                "order_id" => $tr->serializableData["order_id"],
                "amount" => $tr->serializableData["amount"]
            ];

            if ($tr->serializableData["method"] == 'store') {
                $this->transactionResult["result"]["serializableData"]["payment_method"] = [
                    "type" => $tr->serializableData["payment_method"]->type,
                    "reference" => $tr->serializableData["payment_method"]->reference,
                    "barcode_url" => $tr->serializableData["payment_method"]->barcode_url];
            } elseif ($tr->serializableData["method"] == 'bank_account') {
                $this->transactionResult["result"]["serializableData"]["payment_method"] = [
                    "bank" => $tr->serializableData["payment_method"]->bank,
                    "clabe" => $tr->serializableData["payment_method"]->clabe,
                    "name" => $tr->serializableData["payment_method"]->name,
                ];
            } elseif ($tr->serializableData["method"] == 'card') {
                $this->transactionResult["result"]["serializableData"]["card"] = [
                    "type" => $tr->card->type,
                    "brand" => $tr->card->brand,
                    "bank_name" => $tr->card->bank_name,
                    "card_number" => $tr->card->serializableData["card_number"],
                    "holder_name" => $tr->card->serializableData["holder_name"],
                ];
            }
            //END Transaction event
            /////////////////////////////////////////////////////////////////////////////////////////////
            //
            //Handle result
            $this->transactionResult["jsonResult"] = json_encode($this->transactionResult["result"]);
            $this->transactionResult["message"] = "transaction complete";
            $this->transactionResult["success"] = true;
        } catch (\OpenpayApiTransactionError $e) {
            $this->transactionResult["message"] = 'OpenPay.error en la transacción: ' . $e->getMessage() . ' [código de error: ' . $e->getErrorCode() . ', categoría de error: ' . $e->getCategory() . ', código HTTP: ' . $e->getHttpCode() . ', id petición: ' . $e->getRequestId() . ']';
        } catch (\OpenpayApiRequestError $e) {
            $this->transactionResult["message"] = 'OpenPay.error en la petición: ' . $e->getMessage();
        } catch (\OpenpayApiConnectionError $e) {
            $this->transactionResult["message"] = 'OpenPay.error en la conexión al API: ' . $e->getMessage();
        } catch (\OpenpayApiAuthError $e) {
            $this->transactionResult["message"] = 'OpenPay.error en la autenticación: ' . $e->getMessage();
        } catch (\OpenpayApiError $e) {
            $this->transactionResult["message"] = 'OpenPay.error en el API: ' . $e->getMessage();
        } catch (\Exception $e) {
            $this->transactionResult["message"] = "OpenPay.error General: " . $e->getMessage();
        }

        return $this->transactionResult;
    }

    public function op_addCustomer($data)
    {
        $this->errMsg = "Error al añadir al cliente";
        try {
            $customerData = ['name' => $data["name"], 'email' => $data["email"], 'requires_account' => false];
            $customer = $this->instance->customers->add($customerData);
            $this->transactionResult = ["success" => true, "message" => "", "result" => $customer];
        } catch (\OpenpayApiTransactionError $e) {
            $this->transactionResult["message"] = $this->errMsg;
        } catch (\Exception $e) {
            $this->transactionResult["message"] = $this->errMsg;
        }
        return $this->transactionResult;
    }

    public function op_addCard($data, $customerGatewayId)
    {
        $this->errMsg = "Error al añadir la tarjeta";
        try {
            $token = ['token_id' => $data["token_id"], 'device_session_id' => $data["device_session_id"]];
            $customer = $this->instance->customers->get($customerGatewayId);
            $card = $customer->cards->add($token);
            $this->transactionResult = ["success" => true, "message" => "", "result" => $card];
        } catch (\OpenpayApiTransactionError $e) {
            $this->errMsg = $e->getErrorCode() == "3001" ? "La tarjeta fue declinada" : $this->errMsg;
            $this->errMsg = $e->getErrorCode() == "3008" ? "La tarjeta no es soportada para transacciones en internet" : $this->errMsg;
            $this->transactionResult["message"] = $this->errMsg;
        } catch (\Exception $e) {
            $this->transactionResult["message"] = $this->errMsg;
        }
        return $this->transactionResult;
    }

    public function op_deleteCard($customerId, $cardId)
    {
        $this->errMsg = "Error al remover la tarjeta";
        try {
            $customer = $this->instance->customers->get($customerId);
            $card = $customer->cards->get($cardId);
            $res = $card->delete();
            $this->transactionResult = ["success" => true, "message" => "", "result" => $card];
        } catch (\Exception $e) {
            $this->transactionResult["message"] = $this->errMsg;
        }
        return $this->transactionResult;
    }

    /**
     * END CUSTOM FUNCTIONS BY GATEWAY
     */
    public function getMerchantId()
    {
        return $this->container->getParameter($this->gateway . '.merchant.id');
    }

    public function getPublicKey()
    {
        return $this->container->getParameter($this->gateway . '.merchant.publickey');
    }
    
    private function getPrivateKey()
    {
        return $this->container->getParameter($this->gateway . '.merchant.privatekey');
    }

    public function getGatewayId()
    {
        return $this->container->getParameter($this->gateway . '.db.id');
    }

    public function getPaymentMethodId($method)
    {
        return $this->container->getParameter($this->gateway . '.paymentmethod')[$method]["id"];
    }

    public function getPaymentMethodDesc($method)
    {
        return $this->container->getParameter($this->gateway . '.paymentmethod')[$method]["desc"];
    }

    public function getOperationTypeId($type)
    {
        return $this->container->getParameter($this->gateway . '.operationtype')[$type]["id"];
    }

    public function getTransactionTypeId($type, $returnId = false)
    {
        return $this->container->getParameter($this->gateway . '.transactiontype')[$type]["id"];
    }

    /**
     * Get Order Status Id equivalence to Gateway status
     */
    public function getOrderStatusEquivalence($statusId)
    {
        $const = $this->container->getParameter($this->gateway . '.order_status_equivalence');
        return $const[$statusId];
    }

    /**
     * Get urls for receipt pdf payments
     */
    public function getReceiptUrls()
    {
        $urls = $this->container->getParameter($this->gateway . '.receiptUrl');
        return $urls[$this->sandboxmode ? "dev" : "prod"];
    }

    /**
     * getInstanceGateway
     * Desc: put here logic to get instance for each gateway
     */
    private function getInstanceGateway()
    {
        $mid = $this->getMerchantId();
        $privkey = $this->getPrivateKey();
        switch ($this->gateway) {
            case "openpay":
                return \Openpay::getInstance($mid, $privkey);
                break;
        }
        return null;
    }

    /**
     * setSandBoxIf
     * Desc: put here logic to set sandbox mode
     */
    private function setSandBoxIf()
    {
        if ($this->sandboxmode) { //Modo Prueba
            switch ($this->gateway) {
                case "openpay":
                    \Openpay::setSandboxMode(true);
                    $sandbox = \Openpay::getSandboxMode();
                    break;
            }
            if (!$sandbox) {
                throw new \Exception();
            }
        }
    }
}
