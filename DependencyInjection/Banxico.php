<?php

namespace Jumpersoft\EcommerceBundle\DependencyInjection;

/**
 * Tipo cambio dolar en Banxico
 *
 * Notes: http://www.banxico.org.mx/ley-de-transparencia/consultas-frecuentes/%7B960A6514-B048-02B8-4BF2-920034786674%7D.pdf
 */
class Banxico
{
    const banxicourl = 'http://www.banxico.org.mx:80/DgieWSWeb/DgieWS?WSDL';

    private $_client;
    private $_debug = false;
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getExchangeRate()
    {
        $changeRate = null;
        try {
            $usd = $this->em->getRepository("E:Currency")->findOneById("usd");
            $changeRate = $this->getBanxicoExchangeRate();
            if ($changeRate != false && is_numeric($changeRate)) {
                $usd->setExchangeRate($changeRate);
                //TODO Falta agregar la fecha del tipo de cambio
                $this->em->persist($usd);
                $this->em->flush();
            } else {
                throw \Exception();
            }
        } catch (\Exception $e) {
            $changeRate = $externalData->getValue();
        }
        return $changeRate;
    }

    public function getBanxicoExchangeRate()
    {
        $client = $this->_getClient();
        try {
            $result = $client->tiposDeCambioBanxico();
        } catch (\SoapFault $e) {
            //return $e->getMessage();
            return false;
        }
        if (!empty($result)) {
            $dom = new \DOMDocument();
            $dom->loadXML($result);
            $xpath = new \DOMXPath($dom);
            $xpath->registerNamespace('bm', "http://ws.dgie.banxico.org.mx");
            $val = $xpath->evaluate("//*[@IDSERIE='SF60653']/*/@OBS_VALUE");
            return ($val->item(0)->value);
        }
    }

    /**
     * @return SoapClient
     */
    private function _getClient()
    {
        if (empty($this->_client)) {
            $this->_client = $this->_setClient();
        }
        return $this->_client;
    }

    /**
     * @return SoapClient
     */
    private function _setClient()
    {
        return new \SoapClient(null, array('location' => self::banxicourl,
            'uri' => 'http://DgieWSWeb/DgieWS?WSDL',
            'encoding' => 'ISO-8859-1',
            'trace' => $this->_getDebug()
        ));
    }

    private function _getDebug()
    {
        return $this->_debug;
    }
}
