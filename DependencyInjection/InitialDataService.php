<?php

namespace Jumpersoft\EcommerceBundle\DependencyInjection;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Jumpersoft\BaseBundle\DependencyInjection\ExchangeRate;
use Jumpersoft\EcommerceBundle\Validators;

/**
 * @author Angel
 */
class InitialDataService
{
    private $em;
    private $defaultStoreId = null;
    private $container;
    private $exchangeRate;

    public function __construct($defaultStoreId, EntityManagerInterface $em, ContainerInterface $container, ExchangeRate $exchangeRate)
    {
        $this->defaultStoreId = $defaultStoreId;
        $this->em = $em;
        $this->container = $container;
        $this->exchangeRate = $exchangeRate;
    }

    public function getData($user)
    {
        //TODO Completar el filtrado de todos los catalogos relacionados a la tabla Type.
        $types = $this->em->createQuery("select t.id, t.id as value, t.name, t.name as text from E:Type t where t.active = 1 order by t.sequence")->getResult();
        $status = $this->em->createQuery("select e.id as value, e.name as text, e.id, e.name, e.sequence from E:Status e where e.visibleInMenus = 1 ORDER BY e.sequence")->getResult();
        return[
            // USER
            "user" => is_object($user) ? ["username" => $user->getUserName(), "name" => $user->getName(), "role" => $user->getRoles()[0]] : [],
            "store" => $this->em->getRepository("E:Store")->getStorePanelData($this->defaultStoreId),
            // CATALOGS
            "catalog" => [
                // ITEM
                "inputType" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "INP_") === 0)),
                "trackingType" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "INV_TYP_") === 0)),
                "unitMeasure" => $this->em->getRepository('E:UnitMeasure')->getUnitsMeasuresToComboHTML(),
                "itemType" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "ITE_") === 0)),
                "currency" => $this->em->createQuery("select c.id, c.name, c.id as value, c.name as text, DATE_FORMAT(c.exchangeRate, '%Y-%m-%d %H:%i:%s') exchangeRate, c.updateDate from E:Currency c")->getResult(),
                // TERMS
                "termsType" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "TER_") === 0)),
                // CATEGORY
                "categoryType" => $this->em->getRepository('E:Category')->getCategoryTypeToComboHTML(),
                // IMAGES
                "imageType" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "IMG_") === 0)),
                // PROMOTIONS
                "promotionType" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "PRO_") === 0)),
                // ROLES
                "roles" => $this->em->getRepository('E:User')->getRoles($user),
                "entityTypes" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "CUS_") === 0)),
                // MODULES
                "modules" => $this->em->getRepository('E:Module')->getModules(['MOD_PAN']),
                // ORDERRECORD
                "paymentForms" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "PAY_FRM_") === 0)),
                "paymentMethods" => $this->em->getRepository('E:OrderRecord')->getPaymentMethodsToCombo(1),
                "shippingMethod" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "SHI_MET") === 0)),
                "shippingType" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "SHI_TYP") === 0)),
                "addressType" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "ADR_") === 0)),
                // NOTIFICATIONS
                "notificationTypes" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "NOT_") === 0)),
                // INVENTORY
                "inventoryIn" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "INV_IN_") === 0)),
                "inventoryOut" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "INV_OUT_") === 0)),
                "inventoryOperation" => array_values(array_filter($types, fn ($d) => strrpos($d["value"], "INV_OPE_") === 0)),
                "isotope" => $this->em->createQuery("select e.id as value, e.name as text, e.id, e.name from E:Isotope e ORDER BY e.sequence")->getResult(),
                "shipmentSchedule" => $this->em->getRepository("E:Shipment")->getSchedule(),
            ],
            "category" => [
                "item" => $this->em->getRepository('E:Category')->getCategoriesTree(["filters" => ["typeId" => ["type" => "stringEqual", "value" => "ITM"]]], $this->defaultStoreId, "root"),
                "itemFilters" => $this->em->getRepository('E:Category')->getCategoriesTree(["filters" => ["typeId" => ["type" => "stringEqual", "value" => "ITM_FIL"]]], $this->defaultStoreId, "root"),
            ],
            "status" => [
                "item" => array_values(array_filter($status, fn ($d) => strrpos($d["value"], "ITE_") === 0)),
                "order" => array_values(array_filter($status, fn ($d) => strrpos($d["value"], "ORD_") === 0)),
                "orderItem" => array_values(array_filter($status, fn ($d) => strrpos($d["value"], "ORI_") === 0)),
                "licences" => array_values(array_filter($status, fn ($d) => strrpos($d["value"], "RAD_LIC") === 0))
            ],
            // VALIDATORS
            "validator" => array_merge(
                Validators\ItemValidator::getValidators("item", "meta", "bulk", "socialMedia", "unitMeasure", "sku", "dispersion"),
                Validators\AttributeValidator::getValidators("attributeSet", "attribute", "attributeOption"),
                Validators\CategoryValidator::getValidators("category"),
                Validators\PromotionValidator::getValidators("promotion"),
                Validators\StoreCatalogValidator::getValidators("store", "storeView", "socialNetwork"),
                Validators\TagValidator::getValidators("tag"),
                Validators\MetaValidator::getValidators("meta"),
                Validators\UserValidator::getValidators("user", "userRules"),
                Validators\RadesaUserLicenceValidator::getValidators("userLicence", "userLicenceIsotope"),
                Validators\AddressValidator::getValidators("address"),
                Validators\TermsValidator::getValidators("terms"),
                Validators\AnnouncementValidator::getValidators("announcement"),
                Validators\OrderRecordValidator::getValidators("orderRecord", "orderRecordItem", "shipmentPending", "cancelOrder"),
                Validators\DocumentValidator::getValidators("document"),
                Validators\PriceListValidator::getValidators("priceList", "priceListItem"),
                Validators\NotificationValidator::getValidators("notification"),
                Validators\InventoryValidator::getValidators("inventoryIn", "inventoryOut", "inventoryDispersion")
            ),
            // PARÁMETROS INICIALES QUE NO SON VALIDADORES O CATALOGOS.
            "parameters" => [
                "item" => ["typeId" => "ITM"],
                "exchangeRate" => $this->exchangeRate->getExchangeRate("usd", true)
            ]
        ];
    }
}
