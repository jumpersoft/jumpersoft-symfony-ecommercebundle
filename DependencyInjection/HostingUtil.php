<?php
namespace Jumpersoft\EcommerceBundle\DependencyInjection;

/**
 * Description of HostingUtil
 * Clase con funciones base para verificar disponibilidad de dominios y otros.
 * @author Angel
 */
class HostingUtil
{
    public function __construct()
    {
    }
    
    public function verificaDomain($domainio)
    {
        try {
            $pieces = explode(".", $domainio);
            $serverWhoIs = "";
            $params = $domainio . "\r\n";
            
            $tld = strtolower($pieces[count($pieces) - 1]);

            $domArray = array(
                "mx" => array("serv" => "whois.nic.mx", "resMsg1" => "no_se_encontro_el_objeto", "resMsg2" => "object_not_found"),
                "lat" => array("serv" => "whois.nic.lat", "resMsg1" => "no_se_encontro_el_objeto", "resMsg2" => "object_not_found"),
                "org" => array("serv" => "whois.pir.org", "resMsg1" => "not found", "resMsg2" => null),
                "edu" => array("serv" => "whois.educause.net", "resMsg1" => "No Match", "resMsg2" => null),
                "ggTLDS" => array("serv" => ".whois-servers.net", "resMsg1" => "no match for", "resMsg2" => "notfound\n"));

            if (isset($domArray[$tld])) {
                $serverWhoIs = $domArray[$tld]["serv"];
            } else {
                $serverWhoIs = count($pieces) == 2 ? $pieces[1] : $pieces[1] . "." . $pieces[2];
                $serverWhoIs .= $domArray["ggTLDS"]["serv"];
            }

            // Open a Socket connection to our WHOIS server
            $connection = fsockopen($serverWhoIs, 43, $errno, $errstr, 10);
            if ($connection === false) {
                //echo "$errstr ($errno)<br />\n";
                return false;
            }
            $result = '';
            // Send the data or parameters
            fputs($connection, $params);
            // Listen for data and "append" all the bits of information to our result variable until the data stream is finished. Simple: "give me all the data and tell me when you've reached the end"
            while (!feof($connection)) {
                $result .= fgets($connection, 128);
            }
            fclose($connection);
            $result = str_replace("\r\n", "\n", $result);
            //echo $result; //UTIL PARA SABER QUE REGRESA EL SERVER WHOIS Y MAPEAR LOS DIFERETES TIPOS DE MENSAJES DE ERROR QUE MANDA CUANDO ENCUENTRA DOMINIO QUE ESTA DISPONIBLE
            if (isset($domArray[$tld])) {
                if (isset($domArray[$tld]["resMsg1"]) && isset($domArray[$tld]["resMsg2"])) {
                    return ((stristr(strtolower($result), strtolower($domArray[$tld]["resMsg1"])) !== false) || (stristr(strtolower($result), strtolower($domArray[$tld]["resMsg2"])) !== false)) ? true : false;
                } else {
                    return ((stristr(strtolower($result), strtolower($domArray[$tld]["resMsg1"])) !== false)) ? true : false;
                }
            } else {
                return ((stristr($result, strtolower($domArray["ggTLDS"]["resMsg1"])) !== false) || (strtolower($result) == strtolower($domArray["ggTLDS"]["resMsg2"]))) ? true : false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }
}
