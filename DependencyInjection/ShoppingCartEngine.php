<?php

namespace Jumpersoft\EcommerceBundle\DependencyInjection;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Jumpersoft\EcommerceBundle\Entity\ShoppingCart;
use Jumpersoft\EcommerceBundle\Entity\ShoppingCartItem;
use Jumpersoft\EcommerceBundle\Entity\ShoppingCartPromotion;
use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftUtilExtension;

/**
 * @author Angel
 */
class ShoppingCartEngineResult
{
    public $success = false;
    public $msg = null;
    public $cartJSON = null;
    public $cookieName = 'shopcartId';
    public $createNewCookie = false;
    public $cookieId = "";
    public $cookieExpiration = null;
    public $removeCookie = false;
}

class ShoppingCartEngine
{

    //ACTIONS
    public static $ACTION_ADD = "add";
    public static $ACTION_REMOVE = "remove";
    public static $ACTION_EMPTY = "empty";
    public static $APPLY_COUPON = "applyCoupon";
    public static $REMOVE_COUPON = "removeCoupon";
    public static $MERGE_CARTS = "mergeCarts";
    //GENERAL VARS
    private $cart = [];
    private $itemTotals = [];
    private $logger;
    private $em;
    private $requestStack = null;
    private $result = null;
    private $jsUtil = null;

    public function __construct(LoggerInterface $logger, RequestStack $requestStack = null, EntityManagerInterface $em, JumpersoftUtilExtension $jsUtil)
    {
        $this->logger = $logger;
        $this->result = new ShoppingCartEngineResult();
        $this->requestStack = $requestStack->getCurrentRequest();
        $this->em = $em;
        $this->jsUtil = $jsUtil;
    }

    /**
     * getCart
     * Desc: Información del carrito para manipulación
     * @return object
     */
    private function getCart($for = "view", $customerId = null, $cookieId = null)
    {
        //Get the valid cookie
        $id = $cookieId ? $cookieId : $this->requestStack->cookies->get($this->result->cookieName);
        $cart = $this->em->getRepository('E:Store')->getCart($id, $for, $customerId);
        return $cart ? $cart : ($for == "view" ? ["total" => 0, "subTotal" => 0, "quantity" => 0, "items" => []] : null);
    }

    /**
     * getCartInfo
     * Desc: Resumen de información del carrito para vista html
     * @return array
     */
    public function getCartInfo($customerId = null, $cookieId = null)
    {
        $this->cart = $this->getCart("view", $customerId, $cookieId);
        return $this->cart != null ? $this->cart : [];
    }

    public function isEmpty($customerId = null, $cookieId = null)
    {
        $this->cart = $this->getCart("view", $customerId, $cookieId);
        return $this->cart != null ? count($this->cart["items"]) == 0 : true;
    }

    /**
     * updateCart
     */
    public function updateCart($itemId, $action, $quantity, $promotionCode, $customerId = null)
    {
        try {
            $this->cart = $this->getCart("update", $customerId);

            if ($this->cart == null && ($action == $this::$ACTION_REMOVE || $action == $this::$ACTION_EMPTY)) {
                throw new \Exception();
            }
            //ID FOR NEW CART
            $id = $this->cart == null ? md5(uniqid(rand(), true)) : $this->cart->getId();
            $eventDate = new \DateTime();

            if ($action != $this::$ACTION_EMPTY && $action != $this::$APPLY_COUPON && $action != $this::$REMOVE_COUPON && $action != $this::$MERGE_CARTS &&
                    (!$item = $this->em->getRepository('E:Store')->getItem($itemId, "update"))) {
                throw new \Exception();
            }

            //Actions, add, remove, empt
            switch ($action) {
                case $this::$ACTION_ADD:
                    //Make new cart
                    if ($this->cart == null) {
                        $cart = new ShoppingCart();
                        $expiration = new \DateTime('+30 days');
                        $cart->setId($id);
                        $cart->setStatus($this->em->getReference('E:Status', 'SHC_OPN'));
                        $cart->setStore($item->getStore());
                        $cart->setRegisterDate($eventDate);
                        $cart->setExpirationDate($expiration);
                        $cart->setUser($customerId ? $this->em->getReference('E:User', $customerId) : null);
                        $this->cart = $cart;
                        //Add new shoppingcart cookie
                        $this->result->cookieId = $id;
                        $this->result->createNewCookie = true;
                        $this->result->cookieExpiration = $expiration->getTimestamp();
                        $this->em->persist($this->cart);
                    }

                    if (count($cartItem = current($this->cart->getItems()->filter(fn ($i) => $i->getItem()->getId() === $itemId))) == 0) {
                        //Add item to cart database
                        $cartItem = new ShoppingCartItem();
                        $cartItem->setId(md5(uniqid(rand(), true)));
                        $cartItem->setShoppingCart($this->em->getReference('E:ShoppingCart', $id));
                        $cartItem->setItem($item);
                        $cartItem->setRegisterDate($eventDate);
                        $this->cart->addShoppingCartItem($cartItem);
                        $this->em->persist($cartItem);
                    } else {
                        $cartItem = reset($cartItem); //Get the first element of array
                    }

                    //Set item totals
                    $this->updateItemTotals($action, $quantity, $cartItem);

                    //Update totals
                    $this->updateTotals();
                    break;
                case $this::$ACTION_REMOVE:
                    if (($cartItem = $this->jsUtil->findOneInArray($this->cart->getItems()->toArray(), fn ($i) => $i->getItem()->getId() === $item->getId()))) {
                        if ($quantity <= 0) {
                            $this->em->remove($cartItem);
                        } else {
                            $this->updateItemTotals($action, $quantity, $cartItem);
                        }
                        $this->em->flush();
                        $this->updateTotals();
                        $this->cart->setUpdateDate($eventDate);
                    }
                    break;
                case $this::$ACTION_EMPTY:
                    $this->em->remove($this->cart);
                    $this->result->removeCookie = true;
                    break;
                case $this::$APPLY_COUPON:
                    $promotion = $this->em->getRepository('E:Store')->getPromotionByCode($promotionCode);
                    //Check if is Active and Available
                    if ($promotion && $promotion->getStatus()->getId() == 'PRO_ACT' && new \DateTime() < $promotion->getEndDate()) {
                        //Check if has this promo
                        $promotionsInCart = $this->cart->getPromotions();
                        if (count($promotionsInCart->filter(fn ($p) => $p->getPromotion()->getCode() === $promotionCode)) > 0) {
                            $this->result->msg = "Ya se a aplicado éste código de promoción";
                            throw new \Exception();
                        }
                        // START RESTRICTIONS RULES
                        //
                        // * Check Quantity of promos
                        if ($promotion->getLimitTotalNumberUses() > 0 && $this->em->getRepository('E:Store')->countPromotionsByCodeUsed($promotionCode) >= $promotion->getLimitTotalNumberUses()) {
                            $this->result->msg = "Se ha alcanzado el número limite de cupones permitidos con este código";
                            throw new \Exception();
                        }
                        // * Check Quantity of promos per customer
                        if ($promotion->getLimitNumberUsesPerCustomer() > 0 && $customerId && $this->em->getRepository('E:Store')->countPromotionsByCodeUsed($promotionCode, $customerId) >= $promotion->getLimitTotalNumberUses()) {
                            $this->result->msg = "Se ha alcanzado el número limite de cupones permitidos con este código";
                            throw new \Exception();
                        }

                        // * Check if promo works with other promotions - The coupon in the order or the coupon to apply
                        if (!$promotion->getWorksWithOtherPromotions() && count($promotionsInCart) > 0) {
                            $this->result->msg = "El cupón no aplica con otras promociones";
                            throw new \Exception();
                        }
                        if (count($promotionsInCart->filter(fn ($p) => $p->getPromotion()->getWorksWithOtherPromotions() == false)) > 0) {
                            $this->result->msg = "Tiene un cupón que no aplica con otras promocioness";
                            throw new \Exception();
                        }

                        // Get promo codes IDs
                        $promoItemsIds = array_map(fn ($r) => $r->getItem()->getId(), $promotion->getItems()->toArray());
                        // Get cart items IDs
                        $cartItemsIds = array_map(fn ($r) => $r->getItem()->getId(), $this->cart->getItems()->toArray());
                        // Get Intersect IDs codeItems <-> items
                        $intersectItemsIds = array_intersect($promoItemsIds, $cartItemsIds);

                        // * Check Only products in item list.
                        if ($promoItemsIds && !(bool) $intersectItemsIds) {
                            $this->result->msg = "Este cupón no aplica para los productos en el carrito";
                            throw new \Exception();
                        }

                        // * Check Ammount mininum.
                        $minimumPurchase = $promotion->getMinimumPurchase();
                        $minimumPurchaseFrmt = number_format($minimumPurchase, 2, '.', ',');
                        if ($minimumPurchase > 0) {
                            // * By total ammount
                            if ($promotion->getType()->getId() == 1 && $this->cart->getTotal() < $minimumPurchase) {
                                $this->result->msg = "El monto mínimo para aplicar el cupón es de $" . $minimumPurchaseFrmt;
                                throw new \Exception();
                            }
                            // * By items amount
                            if (in_array($promotion->getType()->getId(), [2, 3])) {
                                $amountItems = array_reduce($this->cart->getItems()->toArray(), function ($i, $r) use ($intersectItemsIds) {
                                    return $i += in_array($r->getItem()->getId(), $intersectItemsIds) ? $r->getTotal() : 0;
                                });
                                if ($amountItems < $minimumPurchase) {
                                    $this->result->msg = "El monto mínimo para aplicar el cupón es de $" . $minimumPurchaseFrmt . " en productos relacionados al cupón";
                                    throw new \Exception();
                                }
                            }
                        }

                        // Add promo
                        $newPromotion = new ShoppingCartPromotion();
                        $newPromotion->setShoppingCart($this->cart);
                        $newPromotion->setPromotion($promotion);
                        $this->cart->addPromotion($newPromotion);
                        $this->em->persist($newPromotion);

                        // Update totals
                        $this->updateItemTotals($action, 0, $this->cart->getItems());
                        $this->updateTotals();

                        $this->result->msg = "Promoción aplicada con éxito";
                    } else {
                        $this->result->msg = "La promoción no es válida";
                        throw new \Exception();
                    }
                    break;
                case $this::$REMOVE_COUPON:
                    if (($promotion = $this->jsUtil->findOneInArray($this->cart->getPromotions()->toArray(), fn ($p) => $p->getPromotion()->getCode() === $promotionCode))) {
                        $this->em->remove($promotion);
                        $this->em->flush();
                        $this->updateItemTotals($action, 0, $this->cart->getItems());
                        $this->updateTotals();
                        $this->result->msg = "Promoción eliminada del carrito";
                    } else {
                        $this->result->msg = "La promoción no es válida";
                        throw new \Exception();
                    }
                    break;
                case $this::$MERGE_CARTS:
                    $opCart = $this->getCart("update"); //Orphan cart
                    $opItems = $opCart ? $opCart->getItems() : null;
                    //Merge to an existig customer cart
                    if ($this->cart && $opCart) {
                        $items = $this->cart->getItems()->toArray();
                        foreach ($opItems as $opItem) {
                            if (($item = $this->jsUtil->findOneInArray($items, fn ($i) => $i->getItem()->getId() === $opItem->getItem()->getId()))) {
                                //Update to mayor ammount
                                if ($opItem->getQuantity() > $item->getQuantity()) {
                                    $this->updateItemTotals($this::$ACTION_ADD, $opItem->getQuantity(), [$item]);
                                    $this->em->flush();
                                }
                            } else {
                                //Add Item
                                $opItem->setShoppingCart($this->em->getReference('E:ShoppingCart', $this->cart->getId()));
                                $this->cart->addShoppingCartItem($opItem);
                                $this->em->flush();
                            }
                        }
                        //Update totals
                        $this->updateTotals();
                        //Remove orphan cart and update customer cookie
                        $this->em->remove($opCart);
                        //Remove cookie from client
                        $this->result->cookieId = $opCart->getId();
                        $this->result->removeCookie = true;
                    } elseif ($opCart) {
                        //Make orphan cart the new customer cart
                        $this->cart = $opCart;
                        $this->cart->setUser($this->em->getReference('E:User', $customerId));
                        //Remove cookie from client
                        $this->result->cookieId = $this->cart->getId();
                        $this->result->removeCookie = true;
                    }
                    break;
            }
            $this->em->flush();
            $this->result->cartJSON = $this->getCartInfo($customerId, $this->result->createNewCookie ? $this->result->cookieId : null);
            $this->result->success = true;
        } catch (\Exception $e) {
            $this->result->success = false;
            $this->result->msg = $this->result->msg ?? $e->getMessage();
            return $this->result;
        }

        return $this->result;
    }

    /**
     * UPDATE MAIN ITEM TOTALS, SET DISCOUNTS, PROMOS AND OTHERS
     */
    private function updateItemTotals($action, $quantity, $cartItems)
    {
        $cartItems = $cartItems instanceof \Doctrine\ORM\PersistentCollection ? $cartItems : [$cartItems];
        foreach ($cartItems as $cartItem) {
            $quantity = $action == $this::$REMOVE_COUPON || $action == $this::$APPLY_COUPON ? $cartItem->getQuantity() : $quantity;
            $item = $cartItem->getItem();
            
            // 0. VALIDATIONS
            if ($quantity <= 0) {
                throw new \Exception("La cantidad tiene que ser mayor a cero");
            }
            if ($quantity > $item->getMaxQtyAllowedInCart()) {
                throw new \Exception("La cantidad es mayor a la permitida");
            }

            $subTotalItem = round($item->getRegularPrice() * $quantity, 2);
            $totalItem = round($item->getSalePrice() * $quantity, 2);

            // 1. DISCOUNT BY CATALOG PRICE
            $discount = round($subTotalItem - $totalItem, 2);

            // 2. DISCOUNTS BY PROMO TYPE TO ONLY ITEMS OR CATEGORIES(TODO: AGREGAR AQUI LA VALIDACIÓN POR CATEGORPIAS CUANDO ESTE LISTA LA FUNCIONALIDAD PARA ASOCIAR CATEGORÍAS A LAS PROMOCIONES)
            if (count($ptis = $this->cart->getPromotions()->filter(fn ($p) => $p->getPromotion()->getType()->getId() == "PRO_ITE")) > 0) {
                if (count($pis = $ptis->filter(fn ($p) => count($p->getPromotion()->getItems()->filter(fn ($i) => $i->getItem()->getId() == $item->getId())) > 0)) > 0) {
                    foreach ($pis as $pi) {
                        $p = $pi->getPromotion();
                        $discountTmp = $p->getDiscountType() == "%" ? $totalItem * ($p->getDiscount() / 100) : $p->getDiscount();
                        $totalItem = $totalItem - $discountTmp;
                        $discount += $discountTmp;
                    }
                }
            }

            // 3. DISCOUNT BY PROMO FOR ALL ITEMS
            if (count($ptis = $this->cart->getPromotions()->filter(function ($p) {
                return $p->getPromotion()->getType()->getId() == 1;
            })) > 0) {
                foreach ($ptis as $pi) {
                    $p = $pi->getPromotion();
                    $discountTmp = $p->getDiscountType() == "%" ? $totalItem * ($p->getDiscount() / 100) : $p->getDiscount();
                    $totalItem = $totalItem - $discountTmp;
                    $discount += $discountTmp;
                }
            }
            //Set totals in item
            $cartItem->setQuantity($quantity);
            $cartItem->setSubTotal($subTotalItem);
            $cartItem->setDiscount($discount);
            $cartItem->setTotal($totalItem);
        }
    }

    /**
     * getItemsCount
     * @return int
     */
    public function getItemsCount()
    {
        $this->cart = $this->getCart("quantity");
        return $this->cart != null ? $this->cart->getQuantity() : 0;
    }

    /**
     * updateTotals
     * Desc: Set cart's totals by sum data
     */
    private function updateTotals()
    {
        $this->cart->setQuantity($this->cart->getItems()->count());
        $this->cart->setSubTotal($this->cart->getItems()->count() > 0 ? array_reduce($this->cart->getItems()->toArray(), fn ($i, $r) => $i += $r->getSubTotal() + 0) : 0);
        $this->cart->setTotal($this->cart->getItems()->count() > 0 ? array_reduce($this->cart->getItems()->toArray(), fn ($i, $r) => $i += $r->getTotal() + 0) : 0);
        $this->cart->setDiscount($this->cart->getItems()->count() > 0 ? array_reduce($this->cart->getItems()->toArray(), fn ($i, $r) => $i += $r->getDiscount() + 0) : 0);
    }
}
