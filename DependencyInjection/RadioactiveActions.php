<?php

namespace Jumpersoft\EcommerceBundle\DependencyInjection;

/**
 * Cálculos diversos
 */
class RadioactiveActions
{
    public static $UT_MINUTE = "m";
    public static $UT_HOUR = "h";
    public static $UT_DAY = "d";
    public static $UT_YEAR = "y";
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function decayCalculation($initialActivity, $timeDecay, $unitTimeDecay, $halfLife, $unitTimeHalfLife)
    {
        $timeDecay = $this->matchUnitTimes($timeDecay, $unitTimeDecay, $unitTimeHalfLife);
        $finalActivity = $initialActivity * exp(-0.693147 * $timeDecay / $halfLife);
        return $finalActivity < 0.000001 ? 0 : $finalActivity;
    }

    public function calcTimeDecay($initialActivity, $finalActivity, $halfLife)
    {
        $t = ($halfLife / -0.693147) * log($finalActivity / $initialActivity);
        return $t <= 0.00001 ? 0 : $t;
    }

    public function matchUnitTimes($timeDecay, $unitTimeDecay, $unitTimeHalfLife)
    {
        $res = 0;

        if ($unitTimeDecay == $UT_MINUTE) {
            $res = ($unitTimeHalfLife == $UT_DAY) ? $timeDecay * (1 / 24.0 / 60.0) :
                    (($unitTimeHalfLife == $UT_YEAR) ? $timeDecay * (1 / 8766.0 / 60.0) :
                    (($unitTimeHalfLife == $UT_HOUR) ? $timeDecay * (1 / 60.0) : $timeDecay));
        } elseif ($unitTimeDecay == $UT_HOUR) {
            $res = ($unitTimeHalfLife == $UT_DAY) ? $timeDecay * (1 / 24.0) :
                    (($unitTimeHalfLife == $UT_YEAR) ? $timeDecay * (1 / 8766.0) : $timeDecay);
        } elseif ($unitTimeDecay == $UT_DAY) {
            $res = ($unitTimeHalfLife == $UT_HOUR) ? $timeDecay * 24 :
                    (($unitTimeHalfLife == $UT_YEAR) ? $timeDecay * (1 / 365.25) : $timeDecay);
        } elseif ($unitTimeDecay == $UT_YEAR) {
            $res = ($unitTimeHalfLife == $UT_HOUR) ? $timeDecay * 8766.0 :
                    (($unitTimeHalfLife == $UT_DAY) ? $timeDecay * 365.25 : $timeDecay);
        }

        return $res;
    }
}
