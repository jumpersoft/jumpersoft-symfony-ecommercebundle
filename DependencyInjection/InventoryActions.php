<?php

namespace Jumpersoft\EcommerceBundle\DependencyInjection;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Jumpersoft\EcommerceBundle\Validators\OrderRecordValidator;
use Jumpersoft\EcommerceBundle\Entity;
use Jumpersoft\EcommerceBundle\DependencyInjection\PaymentGateway;
use Jumpersoft\EcommerceBundle\DependencyInjection;
use Jumpersoft\BaseBundle\DependencyInjection\JumpersoftUtilExtension;

/**
 * @author Angel
 */
class InventoryActions
{
    private $em;
    private $jsUtil;
    private $request = null;
    private $defaultStoreId = null;
    public $result = ["success" => true, "msg" => "", "data" => []];
    private $priceTypeId = '';
    private $finalPrice = null;
    private $msg = [
        "noStock" => "El stock no es suficiente para surtir el producto.",
        "fulFilledCompleteOk" => "Surtido completo",
        "fulFilledPartialOk" => "Surtido parcial exitoso",
        "fulFilledAlready" => "El producto ya está surtido",
        "returnOk" => "Producto regresado al almacen",
        "noInventoryToReturn" => "La partida no tenía inventario que regresar al almacen",
        "errorToFulFill" => "No puede surtirse el pedido",
        "errorToReturn" => "Hubo problemas para regresar el surtido"
    ];

    public function __construct($defaultStoreId, RequestStack $requestStack = null, EntityManagerInterface $em, JumpersoftUtilExtension $jsUtil)
    {
        $this->defaultStoreId = $defaultStoreId;
        $this->request = $requestStack->getCurrentRequest();
        $this->em = $em;
        $this->jsUtil = $jsUtil;
    }

    /**
     * Desc: Pull stock and update based on quantity
     */
    public function pullStock($action, $inventoryCompositeItemId, $inventoryId, $quantity, $ids = null)
    {
        $eventDate = $this->jsUtil->getLocalDateTime();
        if ($action == "fulFill") {
            //FULFILL FROM INVENTORY
            $inventoryCompositeItem = $this->em->getRepository("E:InventoryCompositeItem")->findOneById($inventoryCompositeItemId);
            $inventory = $this->em->getRepository("E:Inventory")->findOneById($inventoryId);
            $item = $inventory->getItem();
            if ($inventory && $inventory->getStock() >= $quantity) { //Stock validations
                //Quantities to fulfill validations
                if ($quantity <= $inventoryCompositeItem->getQuantity() - $inventoryCompositeItem->getQuantityFulfilled()) {
                    //PULL STOCK
                    $inventory->setStock($inventory->getStock() - $quantity);
                    $this->em->persist($inventory);

                    //ADD OR UPDATE INVENTORY COMPOSITE ITEM INVENTORY
                    if (($inventoryCompositeItemInventory = $this->em->getRepository("E:InventoryCompositeItemInventory")->findOneBy(["inventoryCompositeItem" => $inventoryCompositeItem, "inventory" => $inventory]))) {
                        $inventoryCompositeItemInventory->setQuantity($inventoryCompositeItemInventory->getQuantity() + $quantity);
                        $inventoryCompositeItemInventory->setRegisterDate($eventDate);
                    } else {
                        $inventoryCompositeItemInventory = new Entity\InventoryCompositeItemInventory($this->jsUtil->getUid(), $inventoryCompositeItem, $inventory, $quantity, $eventDate);
                    }
                    $this->em->persist($inventoryCompositeItemInventory);
                    $this->em->flush();

                    //UPDATE INVENTORY COMPOSITE ITEM
                    $fulfilledItemCounts = $this->em->getRepository("E:Inventory")->getInventoryCompositeItemInventorySums($inventoryCompositeItem->getInventory()->getId(), $inventoryCompositeItem->getItemCompositeItem()->getId());
                    $inventoryCompositeItem->setQuantityFulFilled($fulfilledItemCounts["quantityFulfilled"]);
                    $inventoryCompositeItem->setStatus($this->em->getReference("E:Status", $inventoryCompositeItem->getQuantity() == $inventoryCompositeItem->getQuantityFulFilled() ? "INV_IN_COM_FUL_SUC" : "INV_IN_COM_FUL_PEN"));
                    $this->em->persist($inventoryCompositeItem);
                    $this->em->flush();

                    //UPDATE INVENTORY OPERATION TYPE TO NEW STATUS
                    $inventoryComposite = $inventoryCompositeItem->getInventory();
                    $fulfilledCounts = $this->em->getRepository("E:Inventory")->getInventoryCompositeItemInventorySums($inventoryComposite->getId());
                    $inventoryComposite->setOperation($this->em->getReference("E:Type", $fulfilledCounts["quantity"] == $fulfilledCounts["quantityFulfilled"] ? "INV_OPE_IN" : "INV_OPE_IN_PEN"));
                    $inventoryComposite->setStock($fulfilledCounts["quantity"] == $fulfilledCounts["quantityFulfilled"] ? $inventoryComposite->getQuantityReceived() : 0);
                    $this->em->persist($inventoryComposite);

                    //ADD INVENTORY OUT BY COMPOSITE ITEM
                    $out = new Entity\Inventory();
                    $this->jsUtil->setValuesToEntity($out, [
                        "id" => $this->jsUtil->getUid(),
                        "registerDate" => $eventDate,
                        "eventDate" => $eventDate,
                        "operation" => "INV_OPE_OUT",
                        "type" => "INV_OUT_COM",
                        "quantityOutput" => $quantity,
                        "item" => $item,
                        "parent" => $inventory,
                        "inventoryCompositeItemInventory" => $inventoryCompositeItemInventory,
                        "serial" => $item->getSerialized() ? $inventory->getSerial() : null,
                        "lot" => $item->getSerialized() ? null : $inventory->getLot()
                    ]);
                    $this->em->persist($out);
                    $this->em->flush();

                    $this->result["inventoryComposite"] = $inventoryComposite;
                    $this->result["msg"] = $inventoryCompositeItem->getStatus()->getId() == "INV_IN_COM_FUL_SUC" ? $this->msg["fulFilledCompleteOk"] : $this->msg["fulFilledPartialOk"];
                } else {
                    $this->result["success"] = false;
                    $this->result["msg"] = $this->msg["errorToFulFill"];
                }
            } else {
                $this->result["success"] = false;
                $this->result["msg"] = $this->msg["noStock"];
            }
        } elseif ($action == "autoFulFill") {
            //AUTOFILL
            if (count($rows = $this->em->getRepository("E:Inventory")->findById($ids)) > 0) {
                foreach ($rows as $inventory) {
                    if ($inventory->getItem()->getType()->getId() == "ITE_COM" && $inventory->getOperation()->getId() == "INV_OPE_IN_PEN") {
                        $inventoryCompositeItems = $inventory->getInventoryCompositeItems();
                        foreach ($inventoryCompositeItems as $inventoryCompositeItem) {
                            if ($inventoryCompositeItem->getQuantity() > $inventoryCompositeItem->getQuantityFulfilled()) {
                                $quantity = $inventoryCompositeItem->getQuantity() - $inventoryCompositeItem->getQuantityFulfilled();
                                $inventoryAvailable = $this->em->getRepository("E:Inventory")->getInventoryItemsForAutoFill($inventoryCompositeItem->getItemCompositeItem()->getItem()->getId(), $quantity);
                                $res = $this->pullStock("fulFill", $inventoryCompositeItem->getId(), $inventoryAvailable["id"], $quantity);
                            }
                        }
                    }
                }
                $this->result["inventoryComposite"] = null;
                $this->result["msg"] = $this->msg["fulFilledCompleteOk"];
            } else {
                $this->result["success"] = false;
                $this->result["msg"] = $this->msg["errorToFulFill"];
            }
        }
        return $this->result;
    }

    /**
     * Desc: Return stock and update based on quantity
     */
    public function returnStock($type, $ids)
    {
        if ($type == "fromInventoryComposite") {
            $inventory = $this->em->getRepository("E:Inventory")->findById($ids);
            if (count($inventory) > 0) {
                foreach ($inventory as $inv) {
                    if ($inv->getItem()->getType()->getId() == "ITE_COM") {
                        $inventoryCompositeItems = $inv->getInventoryCompositeItems();
                        foreach ($inventoryCompositeItems as $inventoryCompositeItem) {
                            if ($inventoryCompositeItem->getQuantityFulfilled() > 0) {
                                $this->returnStock("fromInventoryCompositeItem", $inventoryCompositeItem->getId());
                            }
                        }
                    }
                }
                $this->result["msg"] = $this->msg["returnOk"];
            } else {
                $this->result["success"] = false;
                $this->result["msg"] = $this->msg["noInventoryToReturn"];
            }
        } elseif ($type == "fromInventoryCompositeItem") {
            $inventoryCompositeItem = $this->em->getRepository("E:InventoryCompositeItem")->findOneById($ids);
            if (count($rows = $inventoryCompositeItem->getItemInventory()) > 0) {
                foreach ($rows as $row) {
                    $this->returnStock("fromInventoryCompositeItemInventory", $row->getId());
                }
                $this->result["inventoryComposite"] = $inventoryCompositeItem->getInventory();
                $this->result["msg"] = $this->msg["returnOk"];
            } else {
                $this->result["success"] = false;
                $this->result["msg"] = $this->msg["noInventoryToReturn"];
            }
        } elseif ($type == "fromInventoryCompositeItemInventory") {
            $inventoryCompositeItemInventory = $this->em->getRepository("E:InventoryCompositeItemInventory")->findOneById($ids);
            $inventoryCompositeItem = $inventoryCompositeItemInventory->getInventoryCompositeItem();
            $inventoryComposite = $inventoryCompositeItem->getInventory();
            $inventory = $inventoryCompositeItemInventory->getInventory();
            $quantity = $inventoryCompositeItemInventory->getQuantity();

            if ($inventoryComposite->getOperation()->getId() != "INV_OPE_IN_PEN" && $inventoryComposite->getQuantityReceived() != $inventoryComposite->getStock()) {
                throw new \Exception("El inventario ya está en uso y no puede devolverse el producto");
            }

            //RETURN TO STOCK
            $inventory->setStock($inventory->getStock() + $quantity);

            //REMOVE COMPOSITE ITEM INVENTORY ROW AND OUTS RELATED
            $outs = $inventoryCompositeItemInventory->getInventoryOuts();
            foreach ($outs as $out) {
                $this->em->remove($out);
            }
            $this->em->flush();
            $this->em->remove($inventoryCompositeItemInventory);
            $this->em->flush();

            //UPDATE INVENTORY COMPOSITE ITEM
            $fulfilledItemCounts = $this->em->getRepository("E:Inventory")->getInventoryCompositeItemInventorySums($inventoryCompositeItem->getInventory()->getId(), $inventoryComposite->getId());
            $inventoryCompositeItem->setQuantityFulfilled($inventoryCompositeItem->getQuantityFulfilled() - $quantity);
            $inventoryCompositeItem->setStatus($this->em->getReference("E:Status", $inventoryCompositeItem->getQuantity() == $inventoryCompositeItem->getQuantityFulFilled() ? "INV_IN_COM_FUL_SUC" : "INV_IN_COM_FUL_PEN"));
            $this->em->persist($inventoryCompositeItem);
            $this->em->flush();

            //UPDATE INVENTORY OPERATION TYPE TO NEW STATUS
            $fulfilledCounts = $this->em->getRepository("E:Inventory")->getInventoryCompositeItemInventorySums($inventoryComposite->getId());
            $inventoryComposite->setOperation($this->em->getReference("E:Type", $fulfilledCounts["quantity"] == $fulfilledCounts["quantityFulfilled"] ? "INV_OPE_IN" : "INV_OPE_IN_PEN"));
            $inventoryComposite->setStock($fulfilledCounts["quantity"] == $fulfilledCounts["quantityFulfilled"] ? $inventoryComposite->getQuantityReceived() : 0);
            $this->em->persist($inventoryComposite);

            $this->result["inventoryComposite"] = $inventoryComposite;
            $this->result["inventoryCompositeItem"] = $inventoryCompositeItem;
            $this->result["msg"] = $this->msg["returnOk"];
        }

        $this->em->flush();
        return $this->result;
    }
}
