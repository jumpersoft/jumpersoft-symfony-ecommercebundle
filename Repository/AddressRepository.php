<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;

class AddressRepository extends JumpersoftEntityRepository
{

    /**
     * getAddresses, incluye paginación
     */
    public function getAddresses(&$filters, $userId)
    {
        $address = "CONCAT_WS('', a.streetName, ' ', a.numExt, (CASE WHEN a.numInt is not null AND a.numInt <> '' THEN CONCAT(' Int. ', a.numInt) ELSE '' END), ', Col. ', a.neighborhood, ', ', a.state, ', ', a.country, ', CP. ', a.postalCode) as address";

        $params = ['userId' => $userId];

        $sql = "select u.id as userId,                       
                       a.id, 
                       a.reference,
                       a.receiver,                       
                       a.phone,
                       $address, 
                       a.streetName, 
                       a.numExt, 
                       a.neighborhood, 
                       a.state, 
                       a.country, 
                       a.postalCode, 
                       a.city,                       
                       a.phone, 
                       a.addressType,
                       a.otherReferences,
                       (CASE WHEN IDENTITY(u.defaultShippingAddress) = a.id THEN 1 ELSE 0 END) as defaultShippingAddress,
                       (CASE WHEN IDENTITY(u.defaultBillingAddress) = a.id THEN 1 ELSE 0 END) as defaultBillingAddress,
                       a.coordinates
                from E:Address a
                left join a.user u
               where u.id = :userId
            order by a.registerDate asc ";

        return $this->getResultPaginated($sql, $params, $filters);
    }

    /**
     * getAddress
     */
    public function getAddress($id, $userId = null, $for = "view")
    {
        if ($for == "view") {
            $select = "select u.id as userId,
                              (CASE WHEN et.id = 'CUS_MOR' THEN u.name ELSE CONCAT_WS(' ', u.name, u.lastName, u.mothersLastName) END) as name,
                              a.id, 
                              a.reference,                              
                              a.receiver, 
                              a.phone,                              
                              a.streetName, 
                              a.numExt, 
                              a.numInt, 
                              a.neighborhood, 
                              a.city, 
                              a.otherReferences, 
                              a.town, 
                              a.state, 
                              a.country, 
                              a.postalCode,                              
                              a.addressType,
                              (CASE WHEN IDENTITY(u.defaultShippingAddress) = a.id THEN 1 ELSE 0 END) as defaultShippingAddress,
                              (CASE WHEN IDENTITY(u.defaultBillingAddress) = a.id THEN 1 ELSE 0 END) as defaultBillingAddress
                              ";
        } else {
            //For Update or for Delete
            $select = $for == "update" ? "select a " : "select a ";
        }

        $from = " from E:Address a
                  join a.user u
                  join u.entityType et ";

        $params = ["id" => $id];
        $where = " where a.id = :id ";

        if ($userId) {
            $where .= " and u.id = :userId ";
            $params["userId"] = $userId;
        }

        $query = $this->getEntityManager()->createQuery($select . $from . $where)->setParameters($params);
        return $query->getSingleResult();
    }

    public function getShippingCustomers(&$filters, $q)
    {
        $params = ["q" => '%' . $q . '%'];

        $address = $this->concat(array("addr.streetName", "' '", "addr.numExt", "' '", "addr.numInt", "', '", "addr.neighborhood", "', '", "addr.city", "', '", "addr.state", "', '", "addr.country", "', '", "addr.postalCode"));

        //
        $sql = "select addr.id as value,
                       CONCAT(CASE WHEN et.id = 'CUS_MOR' THEN c.name ELSE CONCAT_WS(' ', c.name, c.lastName, c.mothersLastName) END, ' - ', addr.reference, ' - ', $address) as text,
                       c.name,
                       c.lastName, 
                       c.mothersLastName,
                       addr.id,
                       addr.reference,
                       addr.receiver,
                       addr.phone,
                       addr.streetName,
                       addr.numExt,
                       addr.numInt,
                       addr.neighborhood,
                       addr.city,
                       addr.state,
                       addr.country,
                       addr.postalCode,
                       addr.id defaultShippingAddressId                       
                  from E:User c
                  join c.entityType et
                  join c.defaultShippingAddress addr
                where c.roles LIKE '%ROLE_CUSTOMER%' 
                  and (c.name like :q or c.mothersLastName like :q or c.lastName like :q)  ";
        return $this->getResultPaginated($sql, $params, $filters);
    }

    public function getBillingCustomers(&$filters, $q, $id = null)
    {
        $address = $this->concat(array("addr.streetName", "' '", "addr.numExt", "' '", "addr.numInt", "', '", "addr.neighborhood", "', '", "addr.city", "', '", "addr.state", "', '", "addr.country", "', '", "addr.postalCode"));

        if ($id) {
            $params = ["id" => $id];
            $and = " and c.id = :id ";
        } else {
            $params = ["q" => '%' . $q . '%'];
            $and = " and (c.rfc like :q or c.name like :q or c.mothersLastName like :q or c.lastName like :q) ";
        }

        $sql = "select c.id as value,
                       CONCAT(CASE WHEN et.id = 'CUS_MOR' THEN c.name ELSE CONCAT_WS(' ', c.name, c.lastName, c.mothersLastName) END, ' - ', addr.reference, ' - ', $address) as text,
                       c.name,
                       c.lastName, 
                       c.mothersLastName,
                       c.rfc,
                       c.phone,
                       c.id,
                       addr.reference,
                       addr.receiver,                       
                       addr.streetName,
                       addr.numExt,
                       addr.numInt,
                       addr.neighborhood,
                       addr.city,
                       addr.state,
                       addr.country,
                       addr.postalCode,
                       addr.id defaultBillingAddressId
                 from E:User c
                  join c.entityType et
                  join c.defaultBillingAddress addr            
                where c.roles LIKE '%ROLE_CUSTOMER%'
                  and c.rfc <> '' $and ";

        if ($id) {
            return $this->getOneOrNullResult($sql, $params);
        }
        return $this->getResultPaginated($sql, $params, $filters);
    }
}
