<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class ModuleRepository extends JumpersoftEntityRepository
{
    public function getModules($moduleTypeIds)
    {
        $query = $this->getEntityManager()->createQuery(
            'select m.id, m.name, m.id value, m.name text, m.active, t.id typeId
			 from E:Module m
                         join m.type t                        
                         where t.id in (:ids)
			 ORDER BY m.id DESC'
        )->setParameter('ids', $moduleTypeIds);
        return $query->getArrayResult();
    }
}
