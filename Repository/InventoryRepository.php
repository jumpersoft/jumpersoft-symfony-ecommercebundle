<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class InventoryRepository extends JumpersoftEntityRepository
{
    public function getInventory(&$filters, $storeId, $user, $for = null, $ids = null)
    {
        $select = " select i.id,                            
                            p.id as itemId,
                            p.name,
                            p.sku,                            
                            p.decimals,
                            p.serialized,
                            p.dangerous, 
                            identity(p.type) itemTypeId,
                            i.lot,
                            i.noEco,
                            i.serial,
                            p.radioactive,
                            TRIM(i.quantityReceived) + 0 quantityReceived,                            
                            p.useVolumeToFulFill,
                            TRIM(IFNULL(i.volumeReceived, 0)) + 0 volumeReceived,
                            TRIM(i.stock) + 0 stock,
                            TRIM(i.quantityOutput) + 0 quantityOutput,
                            DATE_FORMAT(i.eventDate, '%Y-%m-%d %H:%i:%s') eventDate,
                            o.name operation,
                            o.id operationId,
                            t.name type,
                            t.id typeId,
                            DATE_FORMAT(i.expirationDate, '%Y-%m-%d %H:%i:%s') expirationDate,
                            um.name as unitMeasure,
                            um.id as unitMeasureId,
                            umbs.name as unitMeasureBulkSale,
                            umbs.code as unitMeasureBulkSaleCode,
                            umbs.id as unitMeasureBulkSaleId,
                            p.quantityByUnitBulkSale,
                            DATE_FORMAT(i.registerDate, '%Y-%m-%d %H:%i:%s') registerDate ";

        $from = "from E:Inventory i
                 join i.item p
                 join i.type t
                 join i.operation o
                 join p.unitMeasure um
            left join p.unitMeasureBulkSale umbs
                 join p.store s 
            left join i.orderRecordItemInventory orii 
            left join orii.orderRecordItem ori ";
        $join = "";
        $where = " where s.id = :storeId ";
        $params = ['storeId' => $storeId];
        if ($user->getRole() != "ROLE_SUPER_ADMIN") {
            $join = " join s.users u ";
            $where .= " and u.id = :userId ";
            $params['userId'] = $user->getId();
        }

        if ($ids == null) {
            //Filter query build
            $this->setConditionalFilters($where, $params, $filters, ["i.lot" => "lot", "i.noEco" => "noEco", "i.serial" => "serial", "p.name" => "name", "p.sku" => "sku", "i.quantityReceived" => "quantityReceived", "i.stock" => "stock",
                "o.id" => "operation", "t.id" => "type", "um.name" => "unitMeasure", "i.eventDate" => "eventDate", "i.expirationDate" => "expirationDate"]);
            //Order filter
            $where .= $this->setOrderFilters($filters, ["name" => "p.name", "sku" => "p.sku", "serial" => "i.serial", "lot" => "i.lot", "noEco" => "i.noEco", "quantityReceived" => "i.quantityReceived", "stock" => "i.stock",
                "eventDate" => "i.eventDate", "operation" => "o.name", "type" => "t.name", "expirationDate" => "i.expirationDate", "updateDate" => "p.updateDate"]);
        } else {
            $where .= " and i.id in (:ids) ";
            $params["ids"] = $ids;
        }

        if ($for == "report") {
            return $this->getResult($select . $from . $join . $where, $params, $filters);
        } elseif ($for == "oneResult") {
            return $this->getOneOrNullResult($select . $from . $join . $where, $params);
        } else {
            return $this->getResultPaginated($select . $from . $join . $where, $params, $filters);
        }
    }

    public function getItem($id, $userId, $storeId, $for = "view")
    {
        if ($for == "view") {
            $select = "select i.id, 
                              i.name, 
                              t.name as type, 
                              t.id as typeId, 
                              i.sku, 
                              i.urlKey, 
                              i.description, 
                              i.shortDescription, 
                              i.specs, 
                              i.comment,                               
                              i.registerDate, 
                              i.updateDate, 
                              i.regularPrice, 
                              i.discount, 
                              i.discountType,
                              DATE_FORMAT(i.discountUntil, '%Y-%m-%d %H:%i:%s') discountUntil,
                              i.salePrice,
                              c.id currency,
                              i.bulk,                              
                              identity(i.trackingType) trackingTypeId,
                              TRIM(i.stock) + 0 stock                              
                              TRIM(i.minQtyAllowedInCart) + 0 minQtyAllowedInCart,
                              TRIM(i.maxQtyAllowedInCart) + 0 maxQtyAllowedInCart, 
                              TRIM(i.notifyForQtyBelow) + 0 notifyForQtyBelow, 
                              TRIM(i.qtyToBecomeOutOfStock) + 0 qtyToBecomeOutOfStock, 
                              i.weight, 
                              um.id unitMeasureId, 
                              um.fractionable,
                              s.id statusId, 
                              DATE_FORMAT(i.newUntil, '%Y-%m-%d %H:%i:%s') newUntil,
                              img.fileName,
                              (CASE WHEN img.thumbSize > 0 THEN " . $this->concat(["i.urlFiles", "'/'", "img.id", "'_'", "img.thumbSize", "'.'", "img.ext"]) . " ELSE '' END) as fileNameThumbSize,
                              i.metaTitle, 
                              i.metaKeywords, 
                              i.metaDescription, 
                              i.socialLinks,
                              (select concat('[',group_concat(concat('\"',icatc.id,'\"')),']')  
                                 from E:Item ic 
                                 join ic.categories icat
                                 join icat.category icatc
                                where ic.id = i.id) as categories,
                              i.handleAttributes,
                              i.attributes,                              
                              IDENTITY(i.parent) parentId,
                              (select concat('[',group_concat(distinct json_object('id', iv.id, 'sequence', iv.sequence, 'name', iv.name, 'sku', iv.sku, 'unitMeasure', vum.name, 'regularPrice', iv.regularPrice, 'discount', iv.discount, 'discountType', iv.discountType, 
                                                                                   'salePrice', iv.salePrice, 'variant', iv.variant, 'variantsGroup', iv.variantsGroup, 'stock', iv.stock) order by iv.sequence),']')
                                from E:Item iv 
                                join iv.unitMeasure vum                                 
                               where IDENTITY(iv.parent) = i.id) as variants                              
                              ";
        } else {
            //For Update or for Delete
            $select = $for == "update" ? "select i " : "select partial i.{id} ";
        }

        $query = $this->getEntityManager()->createQuery($select .
                        " from E:Item i        
                          join i.currency c
                          join i.type t
                     left join i.unitMeasure um
                          join i.status s
                          join i.store st                          
                          join st.users u
                          left join i.defaultImage img
                          where i.id = :id 
                            and u.id = :userId 
                            and st.id = :storeId")->setParameters(["id" => $id, "userId" => $userId, "storeId" => $storeId]);

        $row = $query->getSingleResult();
        return $for == "view" ? $this->jsonDecode($row, ["categories", "variants->variantsGroup"], true, true) : $row;
    }

    public function checkLotOrSerial($serial, $lot, $operationId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select count(i)
			 from E:Inventory i                         
                         where identity(i.operation) = :operationId
                           and (i.lot = :id or i.serial = :serial) '
        )->setParameters(["lot" => $lot, "serial" => $serial, "operationId" => $operationId]);

        return $query->getSingleScalarResult() > 0 ? true : false;
    }

    public function getCatalogItems(&$filters, $q, $storeId)
    {
        $general = "select i.id, i.id as value, " . $this->concat(array("i.name", " (CASE WHEN i.sku IS NULL or i.sku = '' THEN '' ELSE CONCAT(' - ', i.sku) END) ")) . " as text, um.name as unitMeasure";
        $inventory = ", um.fractionable,
                        i.decimals, 
                        identity(i.trackingType) trackingTypeId, 
                        i.serialized, 
                        i.dangerous, 
                        i.perishable,                        
                        umbs.code unitMeasureBulkSale,
                        i.radioactive,
                        i.useVolumeToFulFill, 
                        (select concat('[',group_concat(distinct json_object('id', icoi.id, 'name', ic.name, 'sku', ic.sku, 'unitMeasure', ium.name, 'quantity', icoi.defaultQuantity, 'userDefined', icoi.userDefined) order by icoi.sequence), ']')
                          from E:ItemCompositeOption ico
                          join ico.item item
                          join ico.items icoi
                          join icoi.item ic
                          join ic.unitMeasure ium
                         where item.id = i.id) as compositeItems,
                        first(select concat(i.urlFiles, '/', d.thumbnail) from E:Document d where identity(d.item) = i.id and (identity(d.type) = 'IMG_ITEM_PRIMARY' or d.type is null)) thumbnail
                         ";
        $select = " $general 
                    $inventory                      
                    from E:Item i                     
                          join i.type t
                          join i.unitMeasure um                        
			  join i.store s
                     left join i.unitMeasureBulkSale umbs ";
        $where = " where identity(i.status) = 'ITE_AVA'
                     and s.id = :storeId
                     and ((i.name like :q) OR (i.sku like :q))
                     and t.id in ('ITE_SIM', 'ITE_GRO', 'ITE_COM')                     
                     and identity(i.trackingType) = 'INV_TYP_DET'
                order by i.parent, i.sequence, i.name ";
        $params = ["q" => "%" . $q . "%", "storeId" => $storeId];

        $rows = $this->getResultPaginated($select . $where, $params, $filters);
        return $this->jsonDecode($rows, ["compositeItems"], true);
    }

    public function getItemRelatedDispersion(&$filters, $q, $storeId, $itemId)
    {
        $general = "select i.id, 
                           i.id as value, 
                           " . $this->concat(array("i.name", " (CASE WHEN i.sku IS NULL or i.sku = '' THEN '' ELSE CONCAT(' - ', i.sku) END) ")) . " as text ";
        $inventory = ", i.sku,
                        um.id as unitMeasureId,
                        um.name as unitMeasure,
                        um.fractionable,
                        i.decimals,
                        umbs.name unitMeasureBulkSale,
                        umbs.id unitMeasureBulkSaleId,
                        umbs.fractionable fractionableBulkSale,
                        i.decimalsBulkSale,
                        i.quantityByUnitBulkSale,
                        (SELECT IFNULL(SUM(TRIM(inv.stock) + 0),0) FROM E:Inventory inv where identity(inv.item) = i.id and identity(inv.operation) = 'INV_OPE_IN') stock,
                        identity(i.trackingType) trackingTypeId, 
                        i.serialized, 
                        i.dangerous, 
                        i.perishable,
                        i.radioactive,
                        i.useVolumeToFulFill ";
        $select = " $general $inventory
                    from E:ItemDispersion id
                    join id.itemRelated i
                    join i.type t
                    join i.unitMeasure um                        
                    join i.store s
               left join i.unitMeasureBulkSale umbs ";
        $where = " where identity(id.item) = :itemId
                     and identity(i.status) = 'ITE_AVA'
                     and identity(i.trackingType) = 'INV_TYP_DET'
                     and s.id = :storeId
                     and ((i.name like :q) OR (i.sku like :q))
                     and t.id in ('ITE_SIM', 'ITE_GRO')                     
                     and identity(i.trackingType) = 'INV_TYP_DET'
                order by i.parent, i.sequence, i.name ";
        $params = ["q" => "%" . $q . "%", "storeId" => $storeId, "itemId" => $itemId];
        return $this->getResultPaginated($select . $where, $params, $filters);
    }

    public function getInventoryCompositeItems($inventoryId, $storeId, $user, $inventoryCompositeItemId = null)
    {
        $select = " select  i.id,
                            ici.id inventoryCompositeItemId,
                            i.name,
                            i.sku,
                            i.decimals,
                            i.serialized,
                            i.dangerous,
                            i.serialized,
                            t.name type,
                            t.id typeId,
                            um.name as unitMeasure,
                            um.id as unitMeasureId,
                            umbs.name as unitMeasureBulkSale,
                            umbs.id as unitMeasureBulkSaleId,
                            i.registerDate,
                            ci.userDefined,
                            ci.sequence,
                            trim(ici.quantity) + 0 quantity,
                            trim(ici.quantityFulfilled) + 0 quantityFulfilled,
                            st.id statusId,
                            st.name status ";

        $from = "from E:Inventory inv
                 join inv.inventoryCompositeItems ici
                 join ici.status st
                 join ici.itemCompositeItem ci
                 join ci.item i
                 join i.type t                 
                 join i.unitMeasure um
            left join i.unitMeasureBulkSale umbs
                 join i.store s ";
        $join = "";
        $where = " where inv.id = :id
                     and s.id = :storeId ";
        $params = ["id" => $inventoryId, 'storeId' => $storeId];
        if ($user->getRole() != "ROLE_SUPER_ADMIN") {
            $join = " join s.users u ";
            $where .= " and u.id = :userId ";
            $params['userId'] = $user->getId();
        }
        $order = " order by ci.sequence ";

        if ($inventoryCompositeItemId) {
            $where .= " and ici.id = :inventoryCompositeItemId ";
            $params["inventoryCompositeItemId"] = $inventoryCompositeItemId;
            return $this->getOneOrNullResult($select . $from . $join . $where . $order, $params);
        } else {
            return $this->getResult($select . $from . $join . $where . $order, $params);
        }
    }

    public function getCompositeItem(&$filters, $action, $id)
    {
        if ($action == "fulFilled") {
            $query = "select i.id,
                         TRIM(i.quantity) + 0 quantity,
                         inv.lot,
                         inv.serial,
                         inv.noEco,
                         DATE_FORMAT(i.registerDate, '%Y-%m-%d %H:%i:%s') registerDate
                    from E:InventoryCompositeItemInventory i
                    join i.inventoryCompositeItem ici
                    join i.inventory inv
                   where ici.id = :id ";
            $params = ['id' => $id];
        } elseif ($action == "available") {
            $query = " select inv.id,
                              inv.lot,
                              inv.noEco,
                              inv.serial,
                              TRIM(inv.quantityReceived) + 0 quantityReceived,
                              TRIM(inv.stock) + 0 stock,
                              DATE_FORMAT(inv.eventDate, '%Y-%m-%d %H:%i:%s') eventDate,                            
                              DATE_FORMAT(inv.expirationDate, '%Y-%m-%d %H:%i:%s') expirationDate
                        from E:Inventory inv
                        join inv.item i
                        join i.unitMeasure um
                   left join i.unitMeasureBulkSale umbs
                       where identity(inv.operation) = 'INV_OPE_IN'
                         and inv.quantityReceived > 0
                         and 1 = IFELSE(i.perishable = 1, IFELSE(inv.expirationDate >= CURRENT_DATE(), 1, 0), 1) 
                         and i.id = :id ";
            $params = ['id' => $id];
            $this->setConditionalFilters($query, $params, $filters, ["i.lot" => "lot", "i.serial" => "serial", "inv.quantityReceived" => "quantityReceived", "inv.stock" => "stock"]);
            $query .= $this->setOrderFilters($filters, ["lot" => "i.lot", "serial" => "i.serial", "quantityReceived" => "inv.quantityReceived", "stock" => "inv.stock", "eventDate" => "inv.eventDate", "expirationDate" => "inv.expirationDate"]);
        } elseif ($action == "ordersRelated") {
            $query = "select o.id,
                             o.folio,
                             DATE_FORMAT(o.date, '%Y-%m-%d %H:%i:%s') date,
                             es.name status,
                             CONCAT_WS(' ', cp.name, cp.mothersLastName, cp.lastName) as name,
                             TRIM(i.quantityReceived) + 0 quantityReceived,
                             DATE_FORMAT(i.registerDate, '%Y-%m-%d %H:%i:%s') registerDate
                    from E:OrderRecordInventory i
                    join i.orderRecordItem oi
                    join oi.item it
                    join it.unitMeasure um
               left join it.unitMeasureBulkSale umbs
                    join oi.orderRecord o
                    join o.status es
                    join o.user cp
                   where identity(i.inventory) = :id ";
            $params = ['id' => $id];
//            $this->setConditionalFilters($query, $params, $filters, ["p.folio" => "folio", "es.name" => "status", ["cp.name" => "name", "cp.lastName" => "name", "cp.mothersLastName" => "name"], "s.name" => "storeName"]);
//            $query .= $this->setOrderFilters($filters, ["folio" => "p.folio", "date" => "p.date", "status" => "es.name", "name" => "cp.name", "email" => "cp.email", "total" => "p.total"]);
        }

        return $this->getResultPaginated($query, $params, $filters);
    }

    public function getInventoryCompositeItemInventorySums($inventoryId, $itemCompositeItemId = null)
    {
        $subQuery = " select IFNULL(sum(icii.quantity), 0) 
                        from E:InventoryCompositeItemInventory icii
                        join icii.inventoryCompositeItem ici
                       where ici.inventory = inv.id ";

        if ($itemCompositeItemId) {
            $subQuery .= " and identity(ici.itemCompositeItem) = :itemCompositeItemId ";
        }

        $select = " select IFNULL(sum(i.quantity), 0) quantity, ($subQuery) quantityFulfilled
                      from E:InventoryCompositeItem i
                      join i.inventory inv                 
                     where inv.id = :inventoryId ";
        $params = ["inventoryId" => $inventoryId];

        if ($itemCompositeItemId) {
            $select .= " and identity(i.itemCompositeItem) = :itemCompositeItemId ";
            $params["itemCompositeItemId"] = $itemCompositeItemId;
        }
        return $this->getOneOrNullResult($select, $params);
    }

    public function getInventoryItemsForAutoFill($itemId, $quantity)
    {
        return $this->getOneOrNullResult("select i.id, MIN(i.stock) stock
                                                from E:Inventory i
                                                join i.item p
                                               where identity(i.operation) = 'INV_OPE_IN' 
                                                 and 1 = IFELSE(p.perishable = 1, IFELSE(i.expirationDate >= CURRENT_DATE(), 1, 0), 1)
                                                 and identity(i.item) = :id 
                                                 and i.stock >= :quantity                                            
                                            order by i.stock, i.registerDate ", ["id" => $itemId, "quantity" => $quantity]);
    }
}
