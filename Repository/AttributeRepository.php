<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

//TODO: Falta agrear que solo permita ver atributos propios para usuarios normales y excluir administradores

class AttributeRepository extends JumpersoftEntityRepository
{
    public function getAttributeSets(&$filters, $q, $userId, $storeId, $for = "grid", $itemId = null)
    {
        $params = ["userId" => $userId, "storeId" => $storeId, "q" => "%" . $q . "%"];

        if ($for == "comboItemAttribute") {
            $andNotInItemAttribute = " and a.id not in (select identity(ia.attribute) from E:ItemAttribute ia where identity(ia.item) = :itemId ) ";
            $params["itemId"] = $itemId;
        } else {
            $andNotInItemAttribute = "";
        }

        $select = "select t.id,
                           t.name, 
                           t.description,
                           (select concat('[',group_concat(JSON_OBJECT('id', a.id, 'required', a.required, 'type', at.name, 'typeId', at.id, 'name', a.name, 'description', a.description, 'code', a.code, 'defaultText', a.defaultText, 'defaultValue', a.defaultValue,
                                                                'defaultOption', a.defaultOption, 'defaultValueDecimal', a.defaultValueDecimal, 'internalUse', a.internalUse, 'max', a.max, 'maxDecimal', a.maxDecimal, 'min', a.min, 'minDecimal', a.minDecimal, 
                                                                'useMinCurrentDate', a.useMinCurrentDate, 'useRange', a.useRange,
                                                                'options', (select concat('[',group_concat(json_object('id', ao.id, 'attributeOptionId', ao.id, 'name', ao.name, 'value', ao.value) 
                                                                                              order by ao.sequence asc),']') 
                                                                              from E:AttributeOption ao 
                                                                             where identity(ao.attribute) = a.id)
                                                          ) order by a.sequence asc
                                          ),']')
                            from E:Attribute a
                            join a.type at   
                           where identity(a.attributeSet) = t.id 
                                 $andNotInItemAttribute) attributes ";

        $select .= $for == "comboItemAttribute" ? ", t.id as value, t.name as text " : "";

        $select .= " from E:AttributeSet t
                             join t.store s                
                             join s.users u
                            where u.id = :userId 
                              and s.id = :storeId 
                              and t.name like :q ";

        $this->setConditionalFilters($select, $params, $filters, ["t.name" => "name", "t.description" => "description"]);
        $select .= $this->setOrderFilters($filters, ["name" => "t.name", "description" => "t.description"]);
        $rows = $this->getResultPaginated($select, $params, $filters);
        return $this->jsonDecode($rows, ["attributes->options"], true, false);
    }

    public function getAttributeSet($id, $userId, $storeId, $for = "view")
    {
        if ($for == "view") {
            $select = "select t.id,
                              t.name, 
                              t.description, 
                              (select concat('[',group_concat(JSON_OBJECT('id', a.id, 'required', a.required, 'type', at.name, 'typeId', at.id, 'name', a.name, 'description', a.description, 'code', a.code, 'defaultText', a.defaultText, 'defaultValue', a.defaultValue, 
                                                                'defaultOption', a.defaultOption, 'defaultValueDecimal', a.defaultValueDecimal, 'internalUse', a.internalUse, 'max', a.max, 'maxDecimal', a.maxDecimal, 'min', a.min, 'minDecimal', a.minDecimal, 
                                                                'useMinCurrentDate', a.useMinCurrentDate, 'useRange', a.useRange,
                                                                'options', (select concat('[',group_concat(json_object('id', ao.id, 'name', ao.name, 'value', ao.value) 
                                                                                              order by ao.sequence asc),']') 
                                                                              from E:AttributeOption ao 
                                                                             where identity(ao.attribute) = a.id
                                                                          order by ao.sequence asc)
                                                          ) order by a.sequence asc
                                          ),']')
                               from E:Attribute a
                               join a.type at   
                              where identity(a.attributeSet) = t.id ) attributes";
        } else {
            $select = $for == "update" ? "select t " : "select partial t.{id} ";
        }

        $query = $this->getEntityManager()->createQuery(
            " $select 
                            from E:AttributeSet t                       
                            join t.store s
                            join s.users u
                           where t.id = :id
                             and u.id = :userId
                             and s.id = :storeId"
        )->setParameters(["id" => $id, "userId" => $userId, "storeId" => $storeId]);

        $row = $query->getSingleResult();
        return $for == "view" ? $this->jsonDecode($row, ["attributes->options"], true, true) : $row;
    }

    public function checkSetName($name, $id, $storeId)
    {
        $query = $this->getEntityManager()->createQuery(
            "select count(p)
			from E:AttributeSet p
                        join p.store s
                        where p.name = :name
                          and p.id <> :id
                          and s.id = :storeId
			ORDER BY p.id DESC"
        )->setParameters(["name" => $name, "id" => $id, "storeId" => $storeId]);

        return $query->getSingleScalarResult() > 0 ? true : false;
    }

    public function getAttributeSetsToComboHTML($userId)
    {
        $query = $this->getEntityManager()->createQuery(
            "select t.id as value, CONCAT(CONCAT(t.name, ' - '), t.urlKey) as name
			 from E:AttributeSet t                        
                         join t.store s                         
                         join s.users u
                         where u.id = :id
			 ORDER BY t.id DESC"
        )->setParameter("id", $userId);
        return $query->getResult();
    }
}
