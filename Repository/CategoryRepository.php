<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class CategoryRepository extends JumpersoftEntityRepository
{

    /**
     * getCategoriesTree
     * Obtiene un arbol de los niveles, raíz o nódo que se indique.
     * Notas: "name as label" es usado por que el componente treeselect utiliza este nombre de etiqueta, si se usan los slot para usar un alias no funciona muy bien
     */
    public function getCategoriesTree($filters, $storeId, $node)
    {
        $where = "where s.id = :storeId ";
        $where2 = "";
        $params = ["storeId" => $storeId];

        $where2 = "";
        if (!empty($node)) {
            if ($node == "root") {
                $where .= " and t.parentid is null ";
            //$where2 .= " and level < 5 "; // Can grow
            } else {
                $where .= " and t.parentid = :node ";
                $params["node"] = $node;
                //$where2 .= " and level < 5 "; // Can grow
            }
        }

        //Filter query build
        $this->setConditionalFilters($where, $params, $filters, ["t.name" => "name", "ct.id" => "typeId"]);

        $stmt = $this->getEntityManager()->getConnection()->prepare(
            "with recursive cte  as (
                    select t.id as 'key',
                           '1' nocheck,
                           t.name,
                           t.name as label,
                           t.parentId,
                           t.id,                            
                           t.active as activo,                            
                           t.urlKey, 
                           t.sequence,                            
                           ct.id typeId, 
                           ctr.name as categoryRelatedName, 
                           ctrt.id as categoryRelatedTypeId,
                   (CASE WHEN ct.id = 'ITM' OR ctrt.id = 'ITM' THEN 'fas fa-shopping-cart'
                         WHEN ct.id = 'CMP' OR ctrt.id = 'CMP' THEN 'fas fa-building'
                         WHEN ct.id = 'BLG' OR ctrt.id = 'BLG' THEN 'fas fa-comment-dots'
                         WHEN ct.id = 'URL' OR ctrt.id = 'URL' THEN 'fas fa-link'
                         WHEN ct.id = 'BRN' OR ctrt.id = 'BRN' THEN 'fas fa-tags'
                         WHEN ct.id = 'HLP' OR ctrt.id = 'HLP' THEN 'fas fa-question'
                         WHEN ct.id = 'MNU' THEN 'fas fa-sitemap'
                         ELSE 'fas fa-stop' END) as icon, 
                         1 as level,
                         0 as collapsed
                   from Category t
                   join CategoryType ct on ct.id = t.typeId
                   join Store s on s.id = t.storeid
                   join StoreUser cu on cu.storeid = s.id
                left join Category ctr on ctr.id = t.categoryRelatedId
                left join CategoryType ctrt on ctrt.id = ctr.typeId
                    $where
                    union
                    select f.id as 'key', 
                           '1' nocheck,
                           f.name,
                           f.name as label,
                           f.parentId, 
                           f.id,                            
                           f.active as activo,                            
                           f.urlKey, 
                           f.sequence, 
                           ct.id typeId, 
                           ctr.name as categoryRelatedName, 
                           ctrt.id as categoryRelatedTypeId,
                    (CASE WHEN ct.id = 'ITM' OR ctrt.id = 'ITM' THEN 'fas fa-shopping-cart'
                         WHEN ct.id = 'CMP' OR ctrt.id = 'CMP' THEN 'fas fa-building'
                         WHEN ct.id = 'BLG' OR ctrt.id = 'BLG' THEN 'fas fa-comment-dots'
                         WHEN ct.id = 'URL' OR ctrt.id = 'URL' THEN 'fas fa-link'
                         WHEN ct.id = 'BRN' OR ctrt.id = 'BRN' THEN 'fas fa-tags'
                         WHEN ct.id = 'HLP' OR ctrt.id = 'HLP' THEN 'fas fa-question'
                         WHEN ct.id = 'MNU' THEN 'fas fa-sitemap'
                         ELSE 'fas fa-stop' END) as icon, 
                         level + 1,
                         0 as collapsed
                    from category as f
                    join CategoryType ct on ct.id = f.typeId
                    join Store s on s.id = f.storeid
                    join StoreUser cu on cu.storeId = s.id
               left join Category ctr on ctr.id = f.categoryRelatedId
               left join CategoryType ctrt on ctrt.id = ctr.typeId, cte AS a
                   where f.parentid = a.id $where2
                  ) select * from cte order by cte.parentId, cte.sequence"
        );
        $stmt->execute($params);
        $rows = $stmt->fetchAll();
        $rows = $this->makeTree($rows, "parentId", "id");
        return $rows;
    }

    public function getCategory($id, $userId, $storeId, $for = "view")
    {
        if ($for == "view") {
            $select = " select c.id, 
                               c.name, 
                               c.description, 
                               p.id as parentId, 
                               p.name as parent,
                              (CASE WHEN cr.id is not null THEN cr.title ELSE c.title END) title,
                              (CASE WHEN cr.id is not null THEN cr.subtitle ELSE c.subtitle END) subtitle,
                              (CASE WHEN cr.id is not null THEN cr.label ELSE c.label END) label,
                              (CASE WHEN cr.id is not null THEN cr.cssIcon ELSE c.cssIcon END) cssIcon,
                              (CASE WHEN cr.id is not null THEN cr.text ELSE c.text END) text,
                              (CASE WHEN cr.id is not null THEN cr.urlKey ELSE c.urlKey END) urlKey,
                              (CASE WHEN cr.id is not null THEN cr.externalUrl ELSE c.externalUrl END) externalUrl,
                               c.notShowFilters,
                               c.active,
                               c.metaTitle, 
                               c.metaKeywords, 
                               c.metaDescription, 
                               ct.id as typeId, 
                               ct.name as categoryType, 
                               cr.id categoryRelatedId ";
        } else {
            //For Update or for Delete
            $select = $for == "update" ? " select c " : "select partial c.{id} ";
        }

        $query = $this->getEntityManager()->createQuery($select .
                        " from E:Category c
                          join c.type ct
                     left join c.parent p
                     left join c.categoryRelated cr
                     left join cr.type crt
                          join c.store s                          
                          join s.users u                          
                          where c.id = :id 
                            and u.id = :userId 
                            and s.id = :storeId")->setParameters(["id" => $id, "userId" => $userId, "storeId" => $storeId]);

        return $query->getSingleResult();
    }

    public function getCategoryMenuChildFromCategoryRelated($id, $parentId)
    {
        $query = $this->getEntityManager()->createQuery("select t
                          from E:Category t
                          join t.type ct
                          join t.parent p
                          join t.categoryRelated tr
                          where tr.id = :id 
                            and ct.id = 'MNU'
                            and p.id " . ($parentId == null ? " is null " : " = :parentId "))->setParameters(array_merge(["id" => $id], $parentId == null ? [] : ["parentId" => $parentId]));

        return $query->getOneOrNullResult();
    }

    public function getCategoryOnRootCount($storeId, $typeId)
    {
        return $this->getEntityManager()->createQuery("select count(c.id) "
                        . " from E:Category c "
                        . " join c.type t "
                        . " join c.store s "
                        . "where t.id = :typeId "
                        . "  and s.id = :storeId "
                        . "  and c.parent is null ")->setParameters(["storeId" => $storeId, "typeId" => $typeId])->getSingleScalarResult();
    }

    public function getCategoryTypeToComboHTML()
    {
        $query = $this->getEntityManager()->createQuery(
            "select ct.id, ct.id as value, ct.name, ct.name as text
                   from E:CategoryType ct
                  where ct.showInCatalog = 1
               order by ct.sequence"
        );
        return $query->getResult();
    }

    public function getCategoryByType($storeId, $typeId, $q)
    {
        return $this->getResult("select c.id value, c.name text
                                   from E:Category c
                                  where identity(c.store) = :storeId
                                    and identity(c.type) = :typeId
                                    and c.name like :q
                               order by c.name ", ["storeId" => $storeId, "typeId" => $typeId, "q" => "%" . $q . "%"]);
    }

    public function getCategoryItems(&$filters, $id, $storeId, $action)
    {
        $select = "select i.id, i.name, '0' checked ";
        $from = "   from E:Item i                    
                    join i.store s ";
        $where = " where s.id = :storeId  ";
        $order = "";
        $params = ['storeId' => $storeId, 'id' => $id];

        if ($action == "assigned") {
            $select .= ", ic.id itemCategoryId, ic.sequence ";
            $from .= " join i.categories ic
                       join ic.category ct ";
            $where .= " and ct.id = :id ";
            $order = " order by ic.sequence asc ";
        } else {
            $where .= " and NOT EXISTS(SELECT identity(ic.item) from E:ItemCategory ic where identity(ic.category) = :id and identity(ic.item) = i.id )";
        }

        $this->setConditionalFilters($where, $params, $filters, ["i.name" => "name"]);
        return $this->getResultPaginated($select . $from . $where . $order, $params, $filters);
    }
    
    public function getCategoryItemsCount($id)
    {
        return $this->getSingleScalarResult("select count(ic.id) from E:ItemCategory ic where identity(ic.category) = :id", ["id" => $id]);
    }
}
