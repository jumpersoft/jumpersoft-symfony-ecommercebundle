<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class UnitMeasureRepository extends JumpersoftEntityRepository
{
    public function getUnitsMeasures(&$filters)
    {
        $query = "select um.id, um.name, um.code
                   from E:UnitMeasure um ";

        $params = [];
        $this->setConditionalFilters($query, $params, $filters, ["um.name" => "name", "um.code" => "code"]);
        $query .= $this->setOrderFilters($filters, ["name" => "um.name", "code" => "um.code"]);
        return $this->getResultPaginated($query, $params, $filters);
    }

    public function getUnitMeasure($id, $for = "view")
    {
        if ($for == "view") {
            $select = "select um.id, um.name,  um.code ";
        } else {
            //For update or for delete
            $select = $for == "update" ? "select um " : "select partial um.{id} ";
        }

        $query = $this->getEntityManager()->createQuery($select .
                        " from E:UnitMeasure um                          
                          where um.id = :id ")->setParameters(["id" => $id]);
        return $query->getSingleResult();
    }

    public function checkUnitMeasure($name, $id)
    {
        $query = $this->getEntityManager()->createQuery(
            "select count(p)
			from E:UnitMeasure p                        
                        where p.name = :name
                          and p.id <> :id                          
			ORDER BY p.id DESC"
        )->setParameters(["name" => $name, "id" => $id]);

        return $query->getSingleScalarResult() > 0 ? true : false;
    }

    public function checkId($id)
    {
        $query = $this->getEntityManager()->createQuery(
            "select count(p)
			  from E:UnitMeasure p                        
                         where p.id = :id                                                    
                      order by p.id DESC"
        )->setParameters(["id" => $id]);

        return $query->getSingleScalarResult() > 0 ? true : false;
    }

    public function getUnitsMeasuresToComboHTML()
    {
        return $this->getEntityManager()->createQuery(
            "select um.id, um.id as value, CONCAT(CONCAT(CONCAT(um.name, '('), um.code), ')') as name, CONCAT(CONCAT(CONCAT(um.name, '('), um.code), ')') as text,
                                um.fractionable, um.grouper, um.groupId
			   from E:UnitMeasure um                         
                          where um.active = 1                            
		       order by um.sequence"
        )->getResult();
    }
}
