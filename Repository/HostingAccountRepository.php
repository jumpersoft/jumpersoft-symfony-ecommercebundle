<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;

class HostingAccountRepository extends JumpersoftEntityRepository
{

    /**
     * getAccounts
     */
    public function getAccounts($userId, &$filters)
    {
        $name = $this->concat(array("u.name", "' '", "u.lastName", "' '", "u.mothersLastName", " '('", "u.username", "')'"));
        $sql = " select c.id, p.folio, p.date, ps.name as item, pps.contractDate, pps.expirationDate, s.name as servidor, s.domain, $name as propietario, cp.name as customer
                from E:HostingAccount c                        
                join c.orderRecordItem pps
                join pps.item ps
                join pps.orderRecord p
                join p.customer cp
                join c.user u
                join c.server s
                join c.userAdmin ua
                where ua.id = :id";
        return $this->getResultPaginated($sql, ['id' => $userId], $filters);
    }

    /**
     * getAccount
     */
    public function getAccount($userId, $id, $getEntity = false)
    {
        $folio = $this->concat(array("'Folio: '", "p.folio"));
        $producto = $this->concat(array("ps.name", "' ('", "pps.quantity", "' '", "um.name", "')'"));
        $name = $this->concat(array("u.name", "' '", "u.lastName", "' '", "u.mothersLastName", " '('", "u.username", "')'"));
        $uaname = $this->concat(array("ua.name", "' '", "ua.lastName", "' '", "ua.mothersLastName", " '('", "ua.username", "')'"));

        if ($getEntity == true) {
            $select = "select c ";
        } else {
            $select = "select c.id, s.id as serverId, p.id as orderRecordId, $folio as order, pps.id as orderRecordItemId, $producto as orderRecordItem,
                              u.id as userId, $name  as user, ua.id as userAdminId, $uaname as userAdmin, c.comment";
        }

        return $this->getEntityManager()->createQuery("$select 
                      from E:HostingAccount c                        
                        join c.orderRecordItem pps
                        join pps.item ps
                        join ps.unitMeasure um
                        join pps.orderRecord p
                        join c.user u
                        join c.server s
                        join c.userAdmin ua
                       where ua.id = :userId
                         and c.id = :id ")->setParameter('userId', $userId)->setParameter('id', $id)->getSingleResult($getEntity == false ? \Doctrine\ORM\Query::HYDRATE_ARRAY : null);
    }

    /**
     * getHostingAccountUserOrders
     */
    public function getHostingAccountUserOrders($userId, &$filters, $q)
    {
        $name = $this->concat(array("'Folio: '", "p.folio"));
        $sql = "select p.id, " . $name . " as text
                from E:OrderRecord p
                join E:OrderRecordItem pps with pps.orderRecord = p
                join pps.item ps
                join ps.category pst
                join p.store e
                join e.users u
                where u.id = :userId
                  and p.folio like :q
                  and pst.id in (2,4,11)
                  and not exists (select c.id 
                                 from E:HostingAccount c 
                                 join c.orderRecordItem ppst
                                 where ppst.id = pps.id)";
        return $this->getResultPaginated($sql, ["userId" => $userId, "q" => '%' . $q . '%'], $filters);
    }

    /**
     * getHostingAccountUserOrderRecordItems
     */
    public function getHostingAccountUserOrderRecordItems($userId, $orderRecordId, &$filters, $q)
    {
        $text = $this->concat(array("ps.name", "' ('", "pps.quantity", "' '", "um.name", "')'"));
        $sql = "select pps.id, " . $text . " as text
                from E:OrderRecordItem pps
                join pps.orderRecord p
                join pps.item ps
                join ps.category pst
                join ps.unitMeasure um
                join p.store e                        
                join e.users u
                where p.id = :orderRecordId
                  and u.id = :userId
                  and ps.name like :q
                  and pst.id in (2,4,11) ";
        return $this->getResultPaginated($sql, ["orderRecordId" => $orderRecordId, "userId" => $userId, "q" => '%' . $q . '%'], $filters);
    }

    public function getServersToCombo()
    {
        $query = $this->getEntityManager()->createQuery(
            "select m.id as value, CONCAT(CONCAT(m.name, ' - Domain: '), m.domain) as name "
                . "from E:Server m "
                . "order by m.name"
        );
        return $query->getResult();
    }

    public function getUsersToCombo(&$filters, $q)
    {
        $name = $this->concat(array("u.name", "' '", "u.lastName", "' '", "u.mothersLastName", " '('", "u.username", "')'"));
        $sql = "select u.id as id, $name  as text
                  from E:User u
                 where $name like :q ";
        return $this->getResultPaginated($sql, ["q" => '%' . $q . '%'], $filters);
    }

    /**
     * Next HostingAccount Id
     */
    public function findNextHostingAccountId()
    {
        return md5(uniqid(rand(), true));
    }

    /**
     * Next HostingAccount Id
     */
    public function getUserEventosToFullCallendar($userId)
    {
//        return array(array(
//                'title' => 'Free day',
//                'start' => '2016-01-19T15:53:00',
//                'allDay' => 'true',
//                'color' => '#E0B940'),
//            array(
//                'title' => 'Skype Meeting',
//                'start' => '2016-01-29T15:53:00',
//                'color' => '#5382BB'),
//            array(
//                'title' => 'Evento',
//                'start' => '2016-01-30T15:53:00',
//                'color' => '#49B57E'));
        //Datos del Pedido y Producto
        $orderRecord = " p.folio, p.date, ps.name as item, pps.contractDate, pps.expirationDate, pst.id as categoryId ";
        //Datos de evento
        $evento = ", 'Evento Pendiente' as title , '' as start, '#5382BB' as color ";

        return $this->getEntityManager()->createQuery("select " . $orderRecord . $evento .
                                " from E:OrderRecordItem pps
                        join pps.item ps
                        join ps.category pst
                        join ps.unitMeasure um
                        join pps.orderRecord p
                        join p.store s                        
                        join s.users u                        
                        where u.id = :id
                           and pps.expirationDate is not null")->setParameter('id', $userId)
                        ->getArrayResult();
    }
}
