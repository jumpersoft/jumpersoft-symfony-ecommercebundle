<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class AnnouncementRepository extends JumpersoftEntityRepository
{
    public function getAnnouncements(&$filters, $userId, $storeId)
    {
        $params = ["userId" => $userId, "storeId" => $storeId];
        $sql = "select t.id, 
                       t.description, 
                       DATE_FORMAT(t.startDate, '%Y-%m-%d %H:%i:%s') startDate, 
                       DATE_FORMAT(t.endDate, '%Y-%m-%d %H:%i:%s') endDate, 
                       t.registerDate, 
                       t.updateDate, 
                       es.name status, 
                       es.id statusId,
                       CONCAT('[',GROUP_CONCAT(DISTINCT JSON_OBJECT('id', r.id, 'name', r.name)),']') as roles,
                       CONCAT('[',GROUP_CONCAT(DISTINCT JSON_OBJECT('id', m.id, 'name', m.name)),']') as modules
                  from E:Announcement t
                  join t.store s                  
                  join s.users u
                  join t.status es
                  join t.modules m
                  join t.roles r
                 where u.id = :userId and s.id = :storeId
              group by t.id
              order by t.id DESC";
        $rows = $this->getResultPaginated($sql, $params, $filters);
        return $this->jsonDecode($rows, ["roles", "modules"], true);
    }

    public function getAnnouncement($id, $userId, $storeId, $for = "view")
    {
        if ($for == "view") {
            $select = "select partial t.{id, text}, m, r ";
        } else {
            $select = $for == "update" ? " select t, m, r " : "select partial t.{id} ";
        }

        $query = $this->getEntityManager()->createQuery($select .
                        " from E:Announcement t
                          join t.store s                          
                          join s.users u
                          join t.status es
                          left join t.modules m
                          left join t.roles r
                          where t.id = :id 
                            and u.id = :userId 
                            and s.id = :storeId")->setParameters(["id" => $id, "userId" => $userId, "storeId" => $storeId]);
        return $query->getSingleResult($for == "view" ? \Doctrine\ORM\Query::HYDRATE_ARRAY : null);
    }
    
    public function getAnnouncementsToShow($moduleId, $userRole, $storeId)
    {
        $userRole = $userRole == "ROLE_SUPER_ADMIN" ? "ROLE_ADMIN" : $userRole;

        $params = [
            "now" => new \DateTime('now'),
            "moduleId" => $moduleId,
            "userRole" => '%' . $userRole . '%'];

        $where = " where (t.startDate <= :now and t.endDate >= :now) 
                     and es.id = 'ANC_ACT'
                     and m.id = :moduleId
                     and r.id like :userRole ";

        if ($storeId > 0) {
            $params["storeId"] = $storeId;
            $where .= " and s.id = :storeId ";
        }

        $query = $this->getEntityManager()->createQuery("select t.id, t.description, t.text, t.startDate, t.endDate
                          from E:Announcement t
                          join t.store s                          
                          join t.status es
                          join t.modules m
                          join t.roles r " . $where)->setParameters($params);
        return $query->getResult();
    }
}
