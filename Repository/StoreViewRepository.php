<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

class StoreViewRepository extends JumpersoftEntityRepository
{
    public function getStoreViewMenus($storeId)
    {
        $tmp = $this->getResult("select svt.name, 
                                        concat(svt.id, IFNULL(concat('_', sv.noEco), '')) view, 
                                        svt.multipleChildren,
                                        c.id categoryId,
                                        c.name,
                                        svc.sequence
                                   from E:Store s
                                   join s.viewVersion v
                                   join v.views sv
                                   join sv.type svt                            
                                   join sv.categories svc
                                   join svc.category c                                   
                                  where identity(svt.group) = 'STR_VIEW_GRP_MNU'
                                    and s.id = :storeId ", ["storeId" => $storeId]);
        $menu = [];
        foreach ($tmp as $d) {
            $menu[$d["view"]] = $this->getMenu(['id' => $d["categoryId"], 'storeId' => $storeId]);
        }

        return $menu;
    }

    public function getMenu($conf)
    {
        $where = "where s.id = :storeId
                    and ct.id = 'MNU' ";
        $params = ["storeId" => $conf["storeId"]];

        $where2 = "";
        if (!empty($conf["id"])) {
            $where .= " and t.id = :id";
            $params["id"] = $conf["id"];
            $where2 .= " and level < 4 "; // Can grow
            $where3 = " where cte.level > 1 "; //Get child items from specific root
            // Can grow
        } else {
            $where .= " and t.parentid is null ";
            $where2 .= " and level < 4 ";
            $where3 = " ";
        }

        $stmt = $this->getEntityManager()->getConnection()->prepare(
            "with recursive cte  as (
                   select t.parentId, 
                          t.id, 
                          t.urlKey, 
                          t.name, 
                          t.label, 
                          (CASE WHEN ctr.id IS NOT NULL 
                                THEN IFNULL(ctr.externalUrl, CONCAT(IFNULL(CONCAT('/',ctrp.urlKey), ''),'/',ctr.urlKey,'/',LOWER(ctrt.slug), '-', ctr.id))
                                ELSE IFNULL(t.externalUrl, '')
                            END) as url,
                          (select concat(t.urlFiles, '/', timg.fileName) from Document timg where timg.categoryid = t.id and timg.typeid = 'IMG_ITEM_PRIMARY' limit 1) image,
                          t.description, 
                          t.sequence, 
                          1 as level, 
                          0 as showSubMenu, 
                          ct.id as typeId,
                          ctr.urlKey relatedId
                   from Category t
                   join CategoryType ct on ct.id = t.typeId
                   join Store s on s.id = t.storeid
                   join StoreUser cu on cu.storeId = s.id
                left join Category ctr on ctr.id = t.categoryRelatedId
                left join CategoryType ctrt on ctrt.id = ctr.typeId                
                left join Category ctrp on ctrp.id = ctr.parentId 
                    $where
                    union
                    select f.parentId, 
                           f.id, 
                           f.urlKey, 
                           f.name, 
                           f.label, 
                           (CASE WHEN ctr.id IS NOT NULL 
                                THEN IFNULL(ctr.externalUrl, CONCAT(IFNULL(CONCAT('/',ctrp.urlKey), ''),'/',ctr.urlKey,'/',LOWER(ctrt.slug), '-', ctr.id))
                                ELSE IFNULL(f.externalUrl, '')
                            END) as url,
                           (select concat(f.urlFiles, '/', fimg.fileName) from document fimg where fimg.categoryid = f.id and fimg.typeid = 'IMG_ITEM_PRIMARY' limit 1) image, 
                           f.description, 
                           f.sequence, 
                           level + 1, 
                           0 as showSubMenu, 
                           ct.id as typeId,
                           ctr.urlKey relatedId
                    from category as f
                    join CategoryType ct on ct.id = f.typeId
                    join Store s on s.id = f.storeid
                    join StoreUser cu on cu.storeId = s.id
               left join Category ctr on ctr.id = f.categoryRelatedId
               left join CategoryType ctrt on ctrt.id = ctr.typeId
               left join Category ctrp on ctrp.id = ctr.parentId, cte AS a               
                   where f.parentid = a.id $where2
                  ) select * from cte $where3 order by cte.sequence "
        );
        $stmt->execute($params);
        $rows = $stmt->fetchAll();
        $rows = $this->makeTree($rows, "parentId", "id");
        return $rows;
    }

    public function getStoreViewCategories($storeId = null, $categoryId = null)
    {
        if ($storeId) {
            $parameters = ["storeId" => $storeId];
            $from = " from E:Store s 
                      join s.viewVersion v ";
            $and = " and s.id = :storeId ";
        } else {
            $parameters = ["categoryId" => $categoryId];
            $from = " from E:Category cat 
                      join cat.viewVersion v ";
            $and = " and cat.id = :categoryId ";
        }

        $query = $this->getEntityManager()->createQuery(
            " select c.id,
                                 c.name,
                                 c.description,
                                 c.title,
                                 c.subtitle,
                                 c.label,
                                 c.urlKey,                                  
                                 IFNULL(c.externalUrl, CONCAT(IFNULL(CONCAT('/',cp.urlKey), ''),'/',c.urlKey,'/',LOWER(ct.slug), '-', c.id)) url,
                                 first(select concat(c.urlFiles, '/', img.fileName) from E:Document img where identity(img.category) = c.id and (identity(img.type) = 'IMG_ITEM_PRIMARY' or img.type is null)) image,
                                 concat(svt.id, IFNULL(concat('_', sv.noEco), '')) view
                            $from
                            join v.views sv
                            join sv.type svt                            
                            join sv.categories svc
                            join svc.category c
                       left join c.parent cp
                            join c.type ct
                           where identity(svt.group) = 'STR_VIEW_GRP_CAT'
                             $and
                        order by sv.sequence, svc.sequence "
        )->setParameters($parameters)->setMaxResults(200)->getResult();

        $result = [];
        foreach ($query as $value) {
            $result[$value['view']][] = $value;
        }

        return $result;
    }

    public function getStoreViewCategoriesItems($storeId = null, $categoryId = null)
    {
        if ($storeId) {
            $parameters = ["storeId" => $storeId];
            $from = " from E:Store s 
                      join s.viewVersion v ";
            $and = " and s.id = :storeId ";
        } else {
            $parameters = ["categoryId" => $categoryId];
            $from = " from E:Category cat
                      join cat.viewVersion v ";
            $and = " and cat.id = :categoryId ";
        }
        
        $filters = ['order' => 'asc', 'start' => '0', 'length' => '12'];
        $rows = $this->getResultPaginated(
            " select sg.id,
                        sv.id storeViewId,
                        concat(svt.id, IFNULL(concat('_', sv.noEco), '')) view,
                        sv.sequence,
                        identity(sg.category) categoryId,
                        identity(sg.type) typeId,
                        sg.title,
                        sg.subtitle,
                        sg.sequence subGroupSequence,
                        GROUP_CONCAT(sgf.id) filters,
                        sg.maxItems
                   $from                   
                   join v.views sv
                   join sv.type svt                            
                   join sv.subGroups sg                       
              left join sg.filters sgf
                  where sv.active = 1 and sg.active = 1
                    and identity(svt.group) = 'STR_VIEW_GRP_ITE'
                    $and
               group by sg.id
               order by sv.sequence, sg.sequence ",
            $parameters,
            $filters
        );

        $result = [];

        foreach ($rows as $value) {
            $filtersOnComponent = ['start' => '0', 'length' => $value["maxItems"] ?? 6];
            $params = ["storeId" => $storeId];

            if ($value["typeId"] == "FIL_SALE_BY_CAT") {
                $params["id"] = $value['categoryId'];
                $filtersOnComponent['orderBy']['sequenceInCategory'] = 'asc';
            }

            if (stristr($value["filters"], "FIL_ITE_MOST_VIEWED")) {
                $params["mostViewed"] = true;
            }
            if (stristr($value["filters"], "FIL_ITE_BEST_SELLERS")) {
                $params["bestSellers"] = true;
            }

            $value['items'] = $this->getEntityManager()->getRepository("E:Store")->getItems($filtersOnComponent, $params);
            $result[$value['view']][] = $value;
        }

        return $result;
    }

    public function getMiscellaneous($storeId)
    {
        $filters = ['order' => 'asc', 'start' => '0', 'length' => '3'];
        $filters2 = ['order' => 'asc', 'start' => '0', 'length' => '2'];
        return [
            "testimonials" => $this->getTestiomonials($storeId, $filters),
            "blog" => [
                "latest" => $this->getEntityManager()->getRepository("E:BlogPost")->getLastPosts($storeId, $filters2)
            ]
        ];
    }

    public function getTestiomonials($storeId, $filters)
    {
        return $this->getResultPaginated("select ut.id, CONCAT_WS(' ', u.name, u.lastName) customer, ut.text, ut.subtext
                                            from E:UserTestimonial ut
                                            join ut.user u
                                            join u.stores s
                                           where ut.active = 1 
                                             and s.id = :storeId", ["storeId" => $storeId], $filters);
    }
}
