<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class PriceListRepository extends JumpersoftEntityRepository
{
    public function getPriceLists(&$filters, $storeId)
    {
        $query = "select p.id, 
                         p.name, 
                         p.description, 
                         p.active,
                         DATE_FORMAT(p.registerDate, '%Y-%m-%d %H:%i:%s') registerDate,
                         DATE_FORMAT(p.updateDate, '%Y-%m-%d %H:%i:%s') updateDate,                          
                         p.customerGroup,
                         (select count(c.id) from E:PriceListCustomer c where IDENTITY(c.priceList) = p.id ) customersCount,
                         (select count(i.id) from E:PriceListItem i where IDENTITY(i.priceList) = p.id ) itemsCount
                    from E:PriceList p                    
                    join p.store st
                    where st.id = :storeId
                    ORDER BY p.registerDate ASC";
        return $this->getResultPaginated($query, ["storeId" => $storeId], $filters);
    }

    public function getPriceList($id, $storeId, $for = "view")
    {
        if ($for == "view") {
            $select = "select p.id,
                              p.name,
                              p.description,
                              p.customerGroup,                              
                              p.active";
        } else {
            $select = $for == "update" ? "select p " : "select partial p.{id} ";
        }

        $query = $this->getEntityManager()->createQuery($select .
                        " from E:PriceList p                          
                          join p.store st
                         where p.id = :id                             
                           and st.id = :storeId")->setParameters(["id" => $id, "storeId" => $storeId]);
        return $query->getSingleResult();
    }

    public function getPriceListItems(&$filters, $userId, $storeId, $priceListId)
    {
        $select = "select ics.id,
                          i.name, 
                          i.sku,
                          um.fractionable,
                          i.decimals,
                          i.salePrice as salePriceOriginal,                           
                          curro.id currencyOriginal,                          
                          first(select concat(i.urlFiles, '/', imd.thumbnail) from E:Document imd where identity(imd.item) = i.id and (identity(imd.type) = 'IMG_ITEM_PRIMARY' or imd.type is null)) as thumbnail,
                          ics.discount,
                          ics.discountType,
                          curr.id currency,
                          ics.regularPrice,
                          ics.salePrice,
                          ics.bulk,
                          ics.registerDate
			 from E:PriceListItem ics
                         join ics.priceList ir   
                         join ics.currency curr
                         join ics.item i
                         join i.unitMeasure um
                         join i.currency curro                    
                         join i.store s
                         join s.users u ";
        $where = " where u.id = :userId and s.id = :storeId and ir.id = :priceListId ";
        $params = ['userId' => $userId, 'storeId' => $storeId, 'priceListId' => $priceListId];
        $order = "";
        return $this->getResultPaginated($select . $where . $order, $params, $filters);
    }

    public function getPriceListItemsAvailable(&$filters, $priceListId, $userId, $storeId, $q)
    {
        $select = "select p.id, p.id as value, p.name as text,
                   first(select concat(p.urlFiles, '/', img.thumbnail) from E:Document img where identity(img.item) = p.id and (identity(img.type) = 'IMG_ITEM_PRIMARY' or img.type is null)) thumbnail ";
        $from = "from E:Item p
                 join p.type t
                 join p.store s
                 join s.users u             
            left join p.parent pa ";
        $where = " where identity(p.status) = 'ITE_AVA'
                     and IFELSE(t.id = 'ITE_CON', IFELSE(p.handleGlobalPrice = 1 OR EXISTS(select ch.id from E:Item ch where identity(ch.parent) = p.id and ch.workWithPricesParent = 1), 1, 0), 1) = 1
                     and IFELSE(pa.id IS NOT NULL, IFELSE(pa.handleGlobalPrice <> 1 AND p.workWithPricesParent <> 1, 1, 0), 1) = 1
                     and u.id = :userId
                     and s.id = :storeId 
                     and p.name like :q 
                     and p.id not in (select pii.id 
                                        from E:PriceListItem pi
                                        join pi.item pii
                                        join pi.priceList pip
                                        where pip.id = :priceListId )";
        $order = " order by p.id DESC ";
        $params = ['priceListId' => $priceListId, 'userId' => $userId, 'storeId' => $storeId, 'q' => '%' . $q . '%'];
        return $this->getResultPaginated($select . $from . $where . $order, $params, $filters);
    }

    public function getPriceListItem($priceListId, $id, $userId, $storeId, $for = "update")
    {
        $select = $for == "update" ? "select pi " : "select partial pi.{id} ";
        $params = ["priceListId" => $priceListId, "userId" => $userId, "storeId" => $storeId, "id" => $id];
        $query = $this->getEntityManager()->createQuery($select .
                        " from E:PriceListItem pi
                          join pi.priceList p
                          join pi.item i
                          join i.store s                          
                          join s.users u
                          where p.id = :priceListId 
                            and u.id = :userId 
                            and s.id = :storeId 
                            and pi.id = :id")->setParameters($params);
        return $query->getOneOrNullResult();
    }

    public function getPriceListCustomers(&$filters, $storeId, $priceListId)
    {
        $select = "select ics.id, 
                          CONCAT_WS(' ', c.name, c.mothersLastName, c.lastName) as name,
                          DATE_FORMAT(ics.registerDate, '%Y-%m-%d %H:%i:%s') registerDate
                    from E:PriceListCustomer ics
                    join ics.priceList ir
                    join ics.customer c
                    join ir.store s";
        $where = " where s.id = :storeId and ir.id = :priceListId ";
        $params = ['storeId' => $storeId, 'priceListId' => $priceListId];
        $order = "";
        return $this->getResultPaginated($select . $where . $order, $params, $filters);
    }
    
    public function getPriceListCustomersAvailable(&$filters, $priceListId, $q)
    {
        $select = "select p.id, p.id as value, CONCAT_WS(' ', p.name, p.mothersLastName, p.lastName) as text,
                          IFELSE(EXISTS(select piie.id 
                                         from E:PriceListCustomer pie
                                         join pie.customer piie
                                         where piie.id = p.id), 1, 0) disabled
                     from E:User p                     
                    where p.name like :q
                      and p.roles LIKE '%ROLE_CUSTOMER%'
                      and p.id not in (select pii.id 
                                         from E:PriceListCustomer pi
                                         join pi.customer pii
                                         join pi.priceList pip                                         
                                         where pip.id = :priceListId)
                 order by p.id DESC ";
        $params = ['priceListId' => $priceListId, 'q' => '%' . $q . '%'];
        return $this->getResultPaginated($select, $params, $filters);
    }
}
