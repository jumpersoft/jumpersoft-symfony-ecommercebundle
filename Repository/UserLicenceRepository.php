<?php

namespace Jumpersoft\EcommerceBundle\Repository;

class UserLicenceRepository extends JumpersoftEntityRepository
{
    public function getLicences(&$filters, $userId, $for = "view", $ids = null, $id = null)
    {
        if ($for == "view" || $for == "report") {
            $select = "select l.id,
                              u.id userId,
                              l.folio,                          
                              DATE_FORMAT(l.startDate, '%Y-%m-%d %H:%i:%s') startDate,
                              DATE_FORMAT(l.endDate, '%Y-%m-%d %H:%i:%s') endDate,
                              l.notes,
                              es.id statusId, 
                              es.name status,
                             (select concat('[',group_concat(json_object('id', uli.id, 'isotopeId', iso.id, 'name', iso.name, 'activityByDay', uli.activityByDay, 'unitMeasure', um.name)),']')
                                from E:UserLicenceIsotope uli
                                join uli.isotope iso
                                join uli.unitMeasure um 
                               where IDENTITY(uli.licence) = l.id) as isotopes";
        } else {
            $select = $for == "update" ? "select l " : "select partial l.{id} ";
        }
        $select .= " from E:UserLicence l
                     join l.status es                   
                     join l.user u ";
        $where = " where u.id = :userId ";
        $params = ['userId' => $userId];
        $order = " order by l.registerDate desc ";

        switch ($for) {
            case "report":
                $where .= " and l.id in (:id) ";
                $params["ids"] = $ids;
                $rows = $this->getResult($select . $where . $order, $params, $filters);
                break;
            case "view":
                if ($id) {
                    $where .= " and l.id = :id ";
                    $params["id"] = $id;
                    $rows = $this->getOneOrNullResult($select . $where . $order, $params, $filters);
                } else {
                    //$this->setConditionalFilters($where, $params, $filters, ["l.folio" => "folio"]);
                    //$this->setDateBetweenQuery($where, $params, "p.date", $filters['filters'], ["startDate", "endDate"]);
                    //$where .= $this->setOrderFilters($filters, ["folio" => "s.folio", "date" => "s.date", "status" => "es.name", "name" => "cp.name", "email" => "cp.email", "total" => "p.total"]);
                    $rows = $this->getResultPaginated($select . $where . $order, $params, $filters);
                }
                break;
            case "update":
            case "delete":
                $where .= $ids ? " and l.id in (:ids) " : " and l.id = :id ";
                $params["id"] = $ids ? $ids : $id;
                return $ids ? $this->getResult($select . $where, $params, $filters) : $this->getOneOrNullResult($select . $where, $params);
        }

        return $this->jsonDecode($rows, ["isotopes"], true, false);
    }

    public function checkFolio($folio, $id)
    {
        $query = $this->getEntityManager()->createQuery(
            "select count(p)
			from E:UserLicence p
                        where p.folio = :folio
                          and p.id <> :id "
        )->setParameters(["folio" => $folio, "id" => $id]);

        return $query->getSingleScalarResult() > 0 ? true : false;
    }
}
