<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;

class OrderRecordItemRepository extends JumpersoftEntityRepository
{
    public function getOrderRecordItems(&$filters, $user, $orderRecordId, $storeId)
    {
        $select = "select pps.id,
                          p.id as orderRecordId, 
                          ps.id as itemId,                          
                          ps.name, 
                          TRIM(pps.quantity) + 0 quantity,                          
                          pps.regularPrice,                          
                          pps.discount, 
                          pps.discountType, 
                          pps.subTotal, pps.total, 
                          um.name as unitMeasure,
                          um.fractionable
                     from E:OrderRecordItem pps
                     join pps.orderRecord p
                left join p.seller se
                     join pps.item ps                        
                     join ps.unitMeasure um                        
                     join p.store s ";

        $where = " where p.id = :id 
                     and s.id = :storeId ";
        $params = ['id' => $orderRecordId, 'storeId' => $storeId];

        switch ($user->getRole()) {
            case "ROLE_SELLER":
                $where .= " and se.id = :userId ";
                $params['userId'] = $user->getId();
                break;
            case "ROLE_ADMIN":
            case "ROLE_SELLER_SHARED":
                $select .= "  join s.users u ";
                $where .= " and u.id = :userId ";
                $params['userId'] = $user->getId();
                // no break
            case "ROLE_SUPER_ADMIN":
                break;
            default:
                return [];
        }

        return $this->getResultPaginated($select .  $where, $params, $filters);
    }

    public function getOrderRecordItem($user, $orderRecordId, $id, $storeId, $for = "view")
    {
        if ($for == "view") {
            $select = "select pps.id,
                              pps.sequence,
                              identity(pps.priceType) priceTypeId,
                              p.id as orderRecordId,
                              TRIM(pps.quantity) + 0 quantity,
                              TRIM(pps.quantityFulFilled) + 0 quantityFulFilled,
                              ps.id as itemId,
                              ps.name,
                              ps.stock,                              
                              identity(ps.trackingType) trackingTypeId,
                              ps.minQtyAllowedInCart,
                              ps.maxQtyAllowedInCart,
                              pps.regularPrice, 
                              identity(pps.currency) currencyId,
                              pps.subTotal, 
                              pps.discount, 
                              pps.discountType,
                              pps.total,
                              um.name as unitMeasure, 
                              um.fractionable,
                              cp.id customerId,
                              es.name status,
                              es.id statusId,
                              pps.bulkSale,
                              ps.radioactive,
                              TRIM(ps.quantityByUnitBulkSale) + 0 quantityByUnitBulkSale,
                              umbs.name unitMeasureBulkSale,
                              umbs.fractionable bulkSaleFractionable,
                              ps.decimalsBulkSale,
                              pps.attributeValues,
                              TRIM(pps.volume) + 0 volume,
                              DATE_FORMAT(pps.calibrationDate, '%Y-%m-%d %H:%i:%s') calibrationDate,
                              ps.useContainer, 
                              identity(pps.containerStatus) containerStatusId
                              ";
        } else {
            //For Update or for Delete
            $select = $for == "update" ? "select pps " : "select partial pps.{id} ";
        }

        $from = " from E:OrderRecordItem pps
                        join pps.orderRecord p
                   left join p.seller se
                        join p.store s
                        join p.customer cp
                        join pps.item ps                        
                        join ps.unitMeasure um
                   left join ps.unitMeasureBulkSale umbs
                        join pps.status es ";

        $where = " where pps.id = :id
                     and p.id = :orderRecordId
                     and s.id = :storeId ";

        $params = ['id' => $id, 'orderRecordId' => $orderRecordId, 'storeId' => $storeId];

        $joins = "";
        switch ($user->getRole()) {
            case "ROLE_SELLER":
                $where .= " and se.id = :userId ";
                $params['userId'] = $user->getId();
                break;
            case "ROLE_ADMIN":
            case "ROLE_SELLER_SHARED":
                $from .= "  join s.users u ";
                $where .= " and u.id = :userId ";
                $params['userId'] = $user->getId();
                // no break
            case "ROLE_SUPER_ADMIN":
                break;
            default:
                return [];
        }

        return $this->getEntityManager()->createQuery($select . $from . $where)->setParameters($params)->getSingleResult();
    }

    public function getItemForOrderRecordItem($id, $customerId)
    {
        $general = "select i.id, 
                           i.name, 
                           i.id as value, 
                           " . $this->concat(array("i.name", " (CASE WHEN i.sku IS NULL or i.sku = '' THEN '' ELSE CONCAT(' - ', i.sku) END) ")) . " as text, 
                           i.shortDescription, 
                           um.name as unitMeasure,
                           first(select concat(i.urlFiles, '/', d.thumbnail) from E:Document d where identity(d.item) = i.id and (identity(d.type) = 'IMG_ITEM_PRIMARY' or d.type is null)) as thumbnail, ";
        $price = " i.workWithPricesParent,
                   i.regularPrice,
                   i.salePrice,
                   c.id as currencyId,                   
                   i.discountType, 
                   i.discount,
                   i.bulk,
                   JSON_OBJECT('handleGlobalPrice', ip.handleGlobalPrice, 'regularPrice', ip.regularPrice, 'salePrice', ip.salePrice, 'discountType', ip.discountType, 'discount', ip.discount, 'currencyId', identity(ip.currency), 'bulk', ip.bulk) as parent,
                   (select json_object('id', identity(ppli.item), 'regularPrice', ppli.regularPrice, 'discount', ppli.discount, 'discountType', ppli.discountType,
                                       'salePrice', ppli.salePrice, 'currencyId', identity(ppli.currency), 'bulk', ppli.bulk )
                    from E:PriceListItem ppli                    
                    join ppli.priceList ppl
                    join ppl.customers pplc
                   where identity(ppli.item) = ip.id
                     and IDENTITY(pplc.customer) = :customerId) parentPriceList,
                   (select json_object('id', identity(pli.item), 'regularPrice', pli.regularPrice, 'discount', pli.discount, 'discountType', pli.discountType,
                                       'salePrice', pli.salePrice, 'currencyId', identity(pli.currency), 'bulk', pli.bulk )
                     from E:PriceListItem pli
                     join pli.priceList pl
                     join pl.customers plc
                    where identity(pli.item) = i.id
                      and IDENTITY(plc.customer) = :customerId) priceList, ";
        $inventory = " i.stock,
                       um.fractionable,
                       i.decimals, 
                       i.bulkSale,
                       TRIM(i.quantityByUnitBulkSale) + 0 quantityByUnitBulkSale,
                       umbs.id unitMeasureBulkSaleId,
                       umbs.name unitMeasureBulkSale,
                       umbs.code unitMeasureBulkSaleCode,
                       umbs.fractionable bulkSaleFractionable,
                       i.decimalsBulkSale,
                       i.minQtyAllowedInCart, 
                       i.maxQtyAllowedInCart, 
                       identity(i.trackingType) trackingTypeId,
                       i.serialized, 
                       i.perishable,
                       i.radioactive,
                       i.fixedPrice,
                       i.handleAttributes, 
                       (select concat('[',group_concat(JSON_OBJECT('id', a.id, 'required', a.required, 'type', at.name, 'typeId', at.id, 'name', a.name, 'description', a.description, 'code', a.code, 
                                                                   'defaultText', a.defaultText, 'defaultValue', a.defaultValue, 'defaultValueDecimal', a.defaultValueDecimal, 'defaultOption', ia.defaultOption,  
                                                                   'internalUse', a.internalUse, 'max', a.max, 'maxDecimal', a.maxDecimal, 'min', a.min, 'minDecimal', a.minDecimal, 'useMinCurrentDate', a.useMinCurrentDate, 
                                                                   'useRange', a.useRange, 'itemAttributeId', ia.id, 'isVariant', ia.isVariant,
                                                                   'options', (select concat('[',group_concat(json_object('id', ao.id, 'name', ao.name, 'active', IFELSE(iao.id is not null,1,0), 'value', ao.value) 
                                                                                             order by iao.sequence asc),']') 
                                                                                from E:AttributeOption ao
                                                                           left join ao.itemAttributeOptions iao 
                                                                               where ao.attribute = a
                                                                                 and ( iao.itemAttribute = ia OR iao.id is null))
                                                            ) order by ia.sequence asc
                                      ),']')
                          from E:Attribute a
                          join a.type at
                          join a.item ia
                         where identity(ia.item) = i.id ) as attributes ";
        $select = " $general
                    $price
                    $inventory                    
                    from E:Item i               
                    join i.unitMeasure um
               left join i.unitMeasureBulkSale umbs
                    join i.store s 
                    join i.currency c               
               left join i.parent ip ";
        $where = " where i.id = :id ";
        $params = ["id" => $id, "customerId" => $customerId];
        $row = $this->getOneOrNullResult($select . $where, $params);
        return $this->jsonDecode($row, ["priceList->bulk", "parent->bulk", "parentPriceList->bulk", "attributes->options"], true, true);
    }

    public function getItemsToCombo(&$filters, $q, $storeId, $orderRecordId, $customerId)
    {
        //TODO el campo compositeItems, esta pendiente de usarse, se va a utilizar para poder meter un producto compuesto en el pedido, con esta info se obtienen los productos de los que se compone el bundle.
        $general = "select i.id, i.id as value, " . $this->concat(array("i.name", " (CASE WHEN i.sku IS NULL or i.sku = '' THEN '' ELSE CONCAT(' - ', i.sku) END) ")) . " as text, i.shortDescription, um.name as unitMeasure, ";
        $price = " i.workWithPricesParent,
                   i.regularPrice,
                   i.salePrice,
                   c.id as currencyId,                   
                   i.discountType, 
                   i.discount,
                   i.bulk,                   
                   JSON_OBJECT('handleGlobalPrice', ip.handleGlobalPrice, 'regularPrice', ip.regularPrice, 'salePrice', ip.salePrice, 'discountType', ip.discountType, 'discount', ip.discount, 'currencyId', identity(ip.currency), 'bulk', ip.bulk) as parent,
                   (select json_object('id', identity(ppli.item), 'regularPrice', ppli.regularPrice, 'discount', ppli.discount, 'discountType', ppli.discountType,
                                       'salePrice', ppli.salePrice, 'currencyId', identity(ppli.currency), 'bulk', ppli.bulk )
                    from E:PriceListItem ppli                    
                    join ppli.priceList ppl
                    join ppl.customers pplc
                   where identity(ppli.item) = ip.id
                     and IDENTITY(pplc.customer) = :customerId) parentPriceList,
                   (select json_object('id', identity(pli.item), 'regularPrice', pli.regularPrice, 'discount', pli.discount, 'discountType', pli.discountType,
                                       'salePrice', pli.salePrice, 'currencyId', identity(pli.currency), 'bulk', pli.bulk )
                     from E:PriceListItem pli                     
                     join pli.priceList pl
                     join pl.customers plc
                    where identity(pli.item) = i.id
                      and IDENTITY(plc.customer) = :customerId) priceList, ";
        $inventory = " i.stock,
                       um.fractionable, 
                       i.decimals,
                       i.bulkSale,                              
                       TRIM(i.quantityByUnitBulkSale) + 0 quantityByUnitBulkSale,
                       umbs.id unitMeasureBulkSaleId,
                       umbs.name unitMeasureBulkSale,
                       umbs.code unitMeasureBulkSaleCode,
                       umbs.fractionable bulkSaleFractionable,
                       i.decimalsBulkSale,
                       i.minQtyAllowedInCart, 
                       i.maxQtyAllowedInCart, 
                       identity(i.trackingType) trackingTypeId,
                       i.autoFulFill,                                                
                       i.serialized,
                       i.perishable, 
                       i.radioactive,
                       i.fixedPrice,
                       i.handleAttributes,
                       (select concat('[',group_concat(JSON_OBJECT('id', a.id, 'required', a.required, 'type', at.name, 'typeId', at.id, 'name', a.name, 'description', a.description, 'code', a.code, 
                                                                   'defaultText', a.defaultText, 'defaultValue', a.defaultValue, 'defaultValueDecimal', a.defaultValueDecimal, 'defaultOption', ia.defaultOption,  
                                                                   'internalUse', a.internalUse, 'max', a.max, 'maxDecimal', a.maxDecimal, 'min', a.min, 'minDecimal', a.minDecimal, 'useMinCurrentDate', a.useMinCurrentDate, 
                                                                   'useRange', a.useRange, 'itemAttributeId', ia.id, 'isVariant', ia.isVariant,
                                                                   'options', (select concat('[',group_concat(json_object('id', ao.id, 'name', ao.name, 'active', IFELSE(iao.id is not null,1,0), 'value', ao.value) 
                                                                                             order by iao.sequence asc),']') 
                                                                                from E:AttributeOption ao
                                                                           left join ao.itemAttributeOptions iao 
                                                                               where ao.attribute = a
                                                                                 and ( iao.itemAttribute = ia OR iao.id is null))
                                                            ) order by ia.sequence asc
                                      ),']')
                          from E:Attribute a
                          join a.type at
                          join a.item ia
                         where identity(ia.item) = i.id ) as attributes,
                       (select concat('[',group_concat(distinct json_object('id', icoi.id, 'itemId', ci.id, 'name', ci.name, 'sku', ci.sku, 'fractionable', ciu.fractionable, 'decimals', ci.decimals, 
                                                                            'unitMeasure', ciu.name)),']')
                                from E:ItemCompositeOption ico
                                join ico.items icoi
                                join icoi.item ci
                                join ci.unitMeasure ciu
                               where IDENTITY(ico.item) = i.id) as compositeItems, ";
        $select = " $general
                    $price
                    $inventory
                    first(select concat(i.urlFiles, '/', di.thumbnail) from E:Document di where identity(di.item) = i.id) as image
                    from E:Item i               
                    join i.unitMeasure um                        
                    join i.store s 
                    join i.currency c
               left join i.parent ip 
               left join i.unitMeasureBulkSale umbs ";
        $where = " where identity(i.type) <> 'ITE_CON'
                     and identity(i.status) = 'ITE_AVA'
                     and s.id = :storeId
                     and ((i.name like :q) OR (i.sku like :q))";
        $params = ["q" => "%" . $q . "%", "storeId" => $storeId, "customerId" => $customerId];
//        if (!empty($orderRecordId)) {
//            $where .= " and i.id not in (select it.id from E:OrderRecordItem oi
//                                                      join oi.item it
//                                                      join oi.orderRecord o
//                                                      where o.id = :id)";
//            $params["id"] = $orderRecordId;
//        }
        $rows = $this->getResultPaginated($select . $where, $params, $filters);
        return $this->jsonDecode($rows, ["priceList->bulk", "parent->bulk", "parentPriceList->bulk", "compositeItems", "attributes->options"], true, false);
    }

    public function getOrderRecordItemInventory(&$filters, $action, $id)
    {
        if ($action == "fulFilled") {
            $query = "select i.id,
                         TRIM(i.quantity) + 0 quantity,
                         inv.lot,
                         inv.serial,
                         DATE_FORMAT(i.registerDate, '%Y-%m-%d %H:%i:%s') registerDate
                    from E:OrderRecordItemInventory i
                    join i.orderRecordItem oi
                    join oi.item it
                    join it.unitMeasure um
               left join it.unitMeasureBulkSale umbs
                    join i.inventory inv
                    join inv.item p
                   where oi.id = :id ";
            $params = ['id' => $id];
        } elseif ($action == "available") {
            $query = " select i.id,
                              p.id itemId,
                              p.name,
                              um.name unitMeasure,
                              umbs.name unitMeasureBulkSale,
                              umbs.code unitMeasureBulkSaleCode,
                              p.quantityByUnitBulkSale,
                              i.lot,
                              i.serial,                              
                              TRIM(i.quantityReceived) + 0 quantityReceived,
                              p.radioactive,
                              p.useVolumeToFulFill,
                              TRIM(i.volumeReceived) + 0 volumeReceived,
                              TRIM(i.stock) + 0 stock,
                              DATE_FORMAT(i.eventDate, '%Y-%m-%d %H:%i:%s') eventDate,                            
                              DATE_FORMAT(i.expirationDate, '%Y-%m-%d %H:%i:%s') expirationDate                              
                        from E:Inventory i
                        join i.item p
                        join p.unitMeasure um
                   left join p.unitMeasureBulkSale umbs
                   left join p.itemDispersions id
                       where identity(i.operation) = 'INV_OPE_IN'
                         and i.quantityReceived > 0
                         and 1 = IFELSE(p.perishable = 1, IFELSE(i.expirationDate >= CURRENT_DATE(), 1, 0), 1) 
                         and (p.id = :id or identity(id.itemRelated) = :id) ";
            $params = ['id' => $id];
            $this->setConditionalFilters($query, $params, $filters, ["i.lot" => "lot", "i.serial" => "serial", "i.quantityReceived" => "quantityReceived", "i.stock" => "stock"]);
            $query .= $this->setOrderFilters($filters, ["lot" => "i.lot", "serial" => "i.serial", "quantityReceived" => "i.quantityReceived", "stock" => "i.stock", "eventDate" => "i.eventDate", "expirationDate" => "i.expirationDate"]);
        } elseif ($action == "ordersRelated") {
            $query = "select o.id,
                             o.folio,
                             DATE_FORMAT(o.date, '%Y-%m-%d %H:%i:%s') date,
                             es.name status,
                             CONCAT_WS(' ', cp.name, cp.mothersLastName, cp.lastName) as name,
                             TRIM(i.quantityReceived) + 0 quantityReceived,
                             DATE_FORMAT(i.registerDate, '%Y-%m-%d %H:%i:%s') registerDate
                    from E:OrderRecordItemInventory i
                    join i.orderRecordItem oi
                    join oi.item it
                    join it.unitMeasure um
               left join it.unitMeasureBulkSale umbs
                    join oi.orderRecord o
                    join o.status es
                    join o.customer cp
                   where identity(i.inventory) = :id ";
            $params = ['id' => $id];
//            $this->setConditionalFilters($query, $params, $filters, ["p.folio" => "folio", "es.name" => "status", ["cp.name" => "name", "cp.lastName" => "name", "cp.mothersLastName" => "name"], "s.name" => "storeName"]);
//            $query .= $this->setOrderFilters($filters, ["folio" => "p.folio", "date" => "p.date", "status" => "es.name", "name" => "cp.name", "email" => "cp.email", "total" => "p.total"]);
        }

        return $this->getResultPaginated($query, $params, $filters);
    }

    public function getORItemInventoryQuantitySum($orderRecordItemId)
    {
        return $this->getSingleScalarResult("select sum(i.quantity)
                                               from E:OrderRecordItemInventory i
                                              where identity(i.orderRecordItem) = :orderRecordItemId ", ["orderRecordItemId" => $orderRecordItemId]);
    }

    public function getInventoryItemsForAutoFill($type, $itemId, $quantity)
    {
        if ($type == "grouped") {
            // Obtiene la suma del inventario restante que permitan llenar la cantidad requerida, ej:
            //       Datos ejemplo:
            //          quantity = 3, stock = 1 + 1 + 0.5 + 0.5
            //       Regla de cantidades que debe cumplirse
            //          stock > 0 and stock <= quantity
            $rows = $this->getResult("select i.id, i.stock,
                                            (select (i.stock + sum(n.stock)) 
                                               from E:Inventory n 
                                               join n.item ni
                                          left join ni.itemDispersions nid
                                              where identity(n.operation) = 'INV_OPE_IN' 
                                                and (ni.id = :id or identity(nid.itemRelated) = :id)
                                                and n.stock > 0 
                                                and n.stock <= :quantity 
                                                and n.id <> i.id) as req
                                      from E:Inventory i
                                      join i.item p
                                 left join p.itemDispersions id
                                      where identity(i.operation) = 'INV_OPE_IN' 
                                        and 1 = IFELSE(p.perishable = 1, IFELSE(i.expirationDate >= CURRENT_DATE(), 1, 0), 1)  
                                        and (identity(i.item) = :id or identity(id.itemRelated) = :id)
                                        and i.stock > 0
                                        and  i.stock <= :quantity                                               
                                     having req > 0 AND req >= :quantity
                                      order by i.stock ", ["id" => $itemId, "quantity" => $quantity]);
        } else {
            // Toma solo productos en el inventario que sean mayores o iguales a la cantidad requerida
            $rows = $this->getOneOrNullResult("select i.id, MIN(i.stock) stock
                                                from E:Inventory i
                                                join i.item p
                                           left join p.itemDispersions id
                                               where identity(i.operation) = 'INV_OPE_IN' 
                                                 and 1 = IFELSE(p.perishable = 1, IFELSE(i.expirationDate >= CURRENT_DATE(), 1, 0), 1)                                                 
                                                 and (identity(i.item) = :id or identity(id.itemRelated) = :id)
                                                 and i.stock >= :quantity                                            
                                            order by i.stock, i.registerDate ", ["id" => $itemId, "quantity" => $quantity]);
            $rows = $rows && $rows["id"] != null ? [$rows] : [];
        }
        return $rows;
    }

    public function getOrderRecordItemFulFilledPending($orderRecordId)
    {
        return $this->getResult("select partial ori.{id, quantity, quantityFulFilled, status, bulkSale}
                                   from E:OrderRecordItem ori
                                  where identity(ori.orderRecord) = :id
                                    and identity(ori.status) in ('ORI_FUL_PEN', 'ORI_FUL_PAR')  ", ["id" => $orderRecordId]);
    }

    public function getOrderRecordItemFulFilled($orderRecordId)
    {
        return $this->getResult("select partial ori.{id, quantity, quantityFulFilled, status, bulkSale}
                                   from E:OrderRecordItem ori
                                  where identity(ori.orderRecord) = :id
                                    and identity(ori.status) in ('ORI_FUL_SUC', 'ORI_FUL_PAR')  ", ["id" => $orderRecordId]);
    }

    public function getOrderRecordItemCompositeItems($itemId)
    {
        //TODO
    }
}
