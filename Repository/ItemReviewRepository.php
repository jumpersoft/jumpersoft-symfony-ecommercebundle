<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class ItemReviewRepository extends JumpersoftEntityRepository
{

    /**
     * getReviews
     */
    public function getReviews(&$filters, $userId, $storeId, $itemId)
    {
        $select = "select ir.id, ir.date, ir.rating, ir.title, ir.review, es.name as status, es.id as statusId, ur.username
			 from E:ItemReview ir
                         join ir.item i
                         join ir.status es
                         left join ir.user ur
                         join i.store s                         
                         join s.users u ";
        $where = " where u.id = :userId and s.id = :storeId and i.id = :itemId ";
        $params = ['userId' => $userId, 'storeId' => $storeId, 'itemId' => $itemId];
        //Filter query build
        //$this->setConditionalFilters($where, $params, $filters, ["i.name" => "name", "i.code" => "code", "es.id" => "statusId", 'pst.name' => 'categoryName']);
        $order = " order by ir.date";
        return $this->getResultPaginated($select . $where . $order, $params, $filters);
    }

    /**
     * getReview
     */
    public function getReview($id, $itemId, $userId, $storeId, $for = "view")
    {
        if ($for == "view") {
            $select = " select ir.id, ir.date, ir.rating, ir.title, ir.review, es.name as status, es.id as statusId, ur.username ";
        } else {
            //For Update or for Delete
            $select = $for == "update" ? "select ir " : "select partial ir.{id} ";
        }
        $params = ["id" => $id, "userId" => $userId, "storeId" => $storeId, "itemId" => $itemId];
        $query = $this->getEntityManager()->createQuery($select .
                        " from E:ItemReview ir                          
                          join ir.item i
                          join ir.status es
                          left join ir.user ur
                          join i.store s                          
                          join s.users u
                          where ir.id = :id 
                            and u.id = :userId 
                            and s.id = :storeId 
                            and i.id = :itemId")->setParameters($params);
        return $query->getSingleResult();
    }
}
