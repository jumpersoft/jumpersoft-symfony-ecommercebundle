<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;

class OrderRecordRepository extends JumpersoftEntityRepository
{
    protected $orderRecordItems = "(select concat('[',group_concat(json_object(
                                            'id', ori.id, 'sequence', ori.sequence, 'orderRecordId', o.id, 'itemId', ps.id, 'typeId', t.id, 'name', ps.name, 'trackingTypeId', identity(ps.trackingType), 'serialized', ps.serialized, 'perishable', ps.perishable,
                                            'radioactive', ps.radioactive, 'volume', ori.volume, 'calibrationDate', ori.calibrationDate, 'quantity', ori.quantity, 'quantityFulFilled', ori.quantityFulFilled, 'fractionable', um.fractionable,
                                            'bulkSale', ori.bulkSale, 'unitMeasureBulkSale', umbs.name, 'unitMeasureBulkSaleCode', umbs.code, 'quantityByUnitBulkSale', ps.quantityByUnitBulkSale, 'bulkSaleFractionable', umbs.fractionable, 
                                            'decimalsBulkSale', ps.decimalsBulkSale, 'regularPrice', ori.regularPrice, 'currencyId', c.id, 'discount', ori.discount, 'discountType', ori.discountType, 'subTotal', ori.subTotal, 
                                            'total', ori.total, 'unitMeasure', um.name, 'registerDate', ori.registerDate, 'status', sori.name, 'statusId', sori.id, 'useContainer', ps.useContainer, 'containerStatusId', identity(ori.containerStatus), 
                                            'lot', (select group_concat(inv.lot) from E:OrderRecordItemInventory orinv
                                                     join orinv.inventory inv
                                                    where identity(orinv.orderRecordItem) = ori.id)) order by ori.registerDate asc),']')
                                from E:OrderRecordItem ori                           
                                join ori.status sori
                                join ori.item ps
                                join ps.type t
                                join ps.unitMeasure um 
                           left join ps.unitMeasureBulkSale umbs
                                join ori.currency c                           
                               where IDENTITY(ori.orderRecord) = o.id
                             )";

    public function getOrders(&$filters, $user, $storeId, $for = null, $userIds = null)
    {
        $select = "select o.id, 
                          o.folio, 
                          o.subTotal, 
                          o.discount, 
                          o.total,
                          DATE_FORMAT(o.date, '%Y-%m-%d %H:%i:%s') date, 
                          DATE_FORMAT(o.shipmentDate, '%Y-%m-%d %H:%i:%s') shipmentDate,
                          co.id as customerId,
                          co.phone,
                          co.mobile,
                          co.email,
                          (CASE WHEN et.id = 'CUS_MOR' THEN CONCAT_WS(' ', co.name, co.rfc) ELSE CONCAT_WS(' ', co.name, co.lastName, co.mothersLastName) END) as name,
                          co.rfc,
                          co.rules customerRules,
                          o.cancelation,
                          es.id as statusId, 
                          es.name as status, 
                          s.tradeName,
                          JSON_OBJECT('transactionId', CASE WHEN tr.paynetReferenceId IS NOT NULL THEN tr.paynetReferenceId ELSE tr.gatewayTransactionId END, 
                                      'operationDate', tr.operationDate, 'result', tr.gatewayResult, 'paymentMethod', pm.name, 'paymentForm', pf.name, 'status', es.name) as transaction,
                          pm.id paymentMethodId,                           
                          pm.name as paymentMethod, 
                          pf.id paymentFormId, 
                          pf.name as paymentForm, 
                          o.shippingAddress,
                          o.billingAddress,
                          $this->orderRecordItems as items
                   from E:OrderRecord o                       			
                   join o.store s                   
                   join o.customer co
                   join co.entityType et
                   join o.status es
              left join o.seller se
              left join o.transactions tr
              left join o.paymentMethod pm
              left join o.paymentForm pf ";

        $where = " where s.id = :storeId ";
        $params['storeId'] = $storeId;

        switch ($user->getRole()) {
            case "ROLE_SELLER":
                $where .= " and se.id = :userId ";
                $params['userId'] = $user->getId();
                break;
            case "ROLE_ADMIN":
            case "ROLE_SELLER_SHARED":
                $select .= "  join s.users u ";
                $where .= " and u.id = :userId ";
                $params['userId'] = $user->getId();
                // no break
            case "ROLE_SUPER_ADMIN":
                break;
            default:
                return [];
        }


        if ($userIds == null) {
            $this->setConditionalFilters($where, $params, $filters, ["o.folio" => "folio", "es.id" => "status", ["co.name" => "name", "co.lastName" => "name", "co.mothersLastName" => "name"],
                "co.rfc" => "rfc", "co.email" => "email", "o.total" => "total", "o.date" => "date", "o.shipmentDate" => "shipmentDate"]);
            $where .= $this->setOrderFilters($filters, ["folio" => "o.folio", "date" => "o.date", "shipmentDate" => "o.shipmentDate", "status" => "es.name", "name" => "co.name", "email" => "co.email", "total" => "o.total"]);
        } else {
            $where .= " and o.id in (:userIds) ";
            $params["userIds"] = $userIds;
        }

        $rows = $for == "report" ? $this->getResult($select . $where, $params, $filters) : $this->getResultPaginated($select . $where, $params, $filters);

        return $this->jsonDecode($rows, ["transaction", "items"], true, false);
    }

    public function getOrderRecord($id, $user, $storeId, $for = "view", $forEdit = false)
    {
        if ($for == "view") {
            $saddresses = $this->concat(array("sas.streetName", "' '", "sas.numExt", "' '", "sas.numInt", "', '", "sas.neighborhood", "', '", "sas.city", "', '", "sas.state", "', '", "sas.country", "', '", "sas.postalCode"));

            $select = "select o.id,
                              o.folio,
                              o.subTotal,
                              o.discount,
                              o.total,
                              o.exchangeRate,
                              DATE_FORMAT(o.date, '%Y-%m-%d %H:%i:%s') date,
                              DATE_FORMAT(o.shipmentDate, '%Y-%m-%d %H:%i:%s') shipmentDate,
                              co.phone,
                              co.mobile, 
                              co.email,
                              (CASE WHEN et.id = 'CUS_MOR' THEN co.name ELSE CONCAT_WS(' ', co.name, co.mothersLastName, co.lastName) END) as name,
                              identity(co.defaultShippingAddress) defaultShippingAddressId,
                              identity(co.defaultBillingAddress) defaultBillingAddressId,
                              es.id as statusId, 
                              es.name as status,                             
                              o.comment, 
                              fo.name as paymentForm, 
                              fo.id as paymentFormId, 
                              mo.name as paymentMethod, 
                              mo.description as paymentMethodDescription, 
                              mo.id as paymentMethodId,                             
                             (select CONCAT('[',GROUP_CONCAT(DISTINCT JSON_OBJECT('value', sas.id, 
                                                                                  'text', $saddresses,
                                                                                  'name',  (CASE WHEN et.id = 'CUS_MOR' THEN CONCAT_WS('-', co.name, co.rfc) ELSE CONCAT_WS(' ', co.name, co.mothersLastName, co.lastName) END),                                                                                  
                                                                                  'reference', sas.reference,
                                                                                  'phone', sas.phone, 
                                                                                  'receiver', sas.receiver,
                                                                                  'streetName', sas.streetName,
                                                                                  'numExt', sas.numExt, 
                                                                                  'numInt', sas.numInt,
                                                                                  'neighborhood', sas.neighborhood, 
                                                                                  'city', sas.city,
                                                                                  'state', sas.state,
                                                                                  'country', sas.country,
                                                                                  'postalCode', sas.postalCode
                                 )),']')
                                from E:Address sas
                               where identity(sas.user) = co.id) as shippingAddresses,                             
                             JSON_UNQUOTE(JSON_EXTRACT(o.shippingAddress, '$.id')) shippingAddressId,
                             JSON_UNQUOTE(JSON_EXTRACT(o.billingAddress, '$.id')) billingUserId,
                             o.shippingAddress,
                             o.billingAddress,
                             identity(o.shippingAddressType) shippingAddressTypeId,
                             identity(o.billingAddressType) billingAddressTypeId,
                             $this->orderRecordItems as items
                             
                    ";
        } else {
            $select = $for == "update" ? "select o " : "select partial o.{id} ";
        }

        $from = " from E:OrderRecord o
                  join o.store s
                  join o.customer co
                  join co.entityType et
                  join o.status es
                  join o.paymentForm fo
                  join o.paymentMethod mo
             left join o.seller se ";

        if (is_array($id)) {
            $where = " where o.id in (:id) ";
        } else {
            $where = " where o.id = :id ";
        }

        $where .= " and s.id = :storeId ";
        $where .= ($forEdit == true ? " and es.id not in ('ORD_PAI', 'ORD_INV', 'ORD_CAN_NOR', 'ORD_CAN_RET') " : " ");
        $params = ['id' => $id, 'storeId' => $storeId];

        switch ($user->getRole()) {
            case "ROLE_SELLER":
                $where .= " and se.id = :userId ";
                $params['userId'] = $user->getId();
                break;
            case "ROLE_ADMIN":
            case "ROLE_SELLER_SHARED":
                $from .= "  join s.users u ";
                $where .= " and u.id = :userId ";
                $params['userId'] = $user->getId();
                // no break
            case "ROLE_SUPER_ADMIN":
                break;
            default:
                return [];
        }

        if (is_array($id)) {
            $row = $this->getEntityManager()->createQuery($select . $from . $where)->setParameters($params)->getResult();
            return $for == "view" ? $this->jsonDecode($row, ["shippingAddresses", "billingAddresses", "items", "customer"], true) : $row;
        } else {
            $row = $this->getEntityManager()->createQuery($select . $from . $where)->setParameters($params)->getOneOrNullResult();
            return $for == "view" ? $this->jsonDecode($row, ["shippingAddresses", "billingAddresses", "items", "customer"], true, true) : $row;
        }
    }

    public function getOrderRecordTimeLine($id, $storeId, $user)
    {
        $select = "select o.id,
                              o.folio,
                              o.subTotal,
                              o.discount,
                              o.total,
                              DATE_FORMAT(o.date, '%Y-%m-%d %H:%i:%s') date,
                              JSON_OBJECT('phone', co.phone, 'mobile', co.mobile, 'email', co.email) as customer,
                              (CASE WHEN et.id = 'CUS_MOR' THEN CONCAT_WS('-', co.name, co.rfc) ELSE CONCAT_WS(' ', co.name, co.mothersLastName, co.lastName) END) as name,
                              es.id as statusId,
                              es.name as status,                             
                              o.comment, 
                              fo.name as paymentForm,                               
                              mo.name as paymentMethod, 
                              mo.description as paymentMethodDescription,                                                            
                             o.shippingAddress,
                             o.billingAddress,
                             (select concat('[',group_concat(distinct json_object('id', pc.id, 'stepId', pcs.id, 'name', pcs.name, 'description', pcs.description, 'registerDate', DATE_FORMAT(pc.registerDate, '%Y-%m-%d %H:%i:%s'), 
                                                                                  'data', pc.data, 'user', CONCAT_WS(' ', pcu.name, pcu.mothersLastName, pcu.lastName)) order by pc.registerDate desc),']')
                                from E:ProccessStep pc
                                join pc.step pcs
                                join pc.user pcu
                               where IDENTITY(pc.orderRecord) = o.id) as steps
                    from E:OrderRecord o
               left join o.seller se
                    join o.store s
                    join o.customer co
                    join co.entityType et
                    join o.status es
                    join o.paymentForm fo
                    join o.paymentMethod mo ";

        $params = ['id' => $id, 'storeId' => $storeId];

        $where = " where o.id = :id
                     and s.id = :storeId ";

        switch ($user->getRole()) {
            case "ROLE_SELLER":
                $where .= " and se.id = :userId ";
                $params['userId'] = $user->getId();
                break;
            case "ROLE_ADMIN":
            case "ROLE_SELLER_SHARED":
                $select .= "  join s.users u ";
                $where .= " and u.id = :userId ";
                $params['userId'] = $user->getId();
                // no break
            case "ROLE_SUPER_ADMIN":
                break;
            default:
                return [];
        }


        $row = $this->getEntityManager()->createQuery("$select $where")->setParameters($params)->getOneOrNullResult();
        return $this->jsonDecode($row, ["steps->data", "customer"], true, true);
    }

    public function getStoreOrders(&$filters, $userId, $storeId)
    {
        $select = "select o.id,
                          o.folio,
                          o.total,
                          DATE_FORMAT(o.date, '%Y-%m-%d %H:%i:%s') date,
                          CONCAT_WS(' ', co.name, co.mothersLastName, co.lastName) as name,
                          JSON_OBJECT('phone', co.phone, 'mobile', co.mobile, 'email', co.email) as customer,
                          o.shippingAddress                          
                   from E:OrderRecord o
                   join o.store s
                   join o.customer co
                   join o.status es ";

        $where = " where co.id = :id
                          and s.id = :storeId
                order by o.date desc ";

        $params = ['id' => $userId, 'storeId' => $storeId];

        $this->setConditionalFilters($where, $params, $filters, ["o.folio" => "folio", "es.name" => "status", "o.shippingAddress" => "shippingAddress", "o.total" => "total"]);
        $rows = $this->getResultPaginated($select . $where, $params, $filters);
        return $this->jsonDecode($rows, ["items", "customer"], true);
    }

    public function getStoreOrderRecord($id, $storeId, $userId)
    {
        $select = "select p.id,
                          p.folio,
                          p.subTotal,
                          p.total,
                          p.discount,
                          DATE_FORMAT(p.date, '%Y-%m-%d %H:%i:%s') date,
                          JSON_OBJECT('phone', cp.phone, 'mobile', cp.mobile, 'email', cp.email) as customer,
                          CONCAT_WS(' ', cp.name, cp.mothersLastName, cp.lastName) as name,
                          es.id as statusId, 
                          es.name as status,
                          p.shippingAddress,
                          p.billingAddress,
                          (select CONCAT('[',group_concat(distinct json_object('id', pps.id, 'itemId', ps.id, 'name', ps.name, 'quantity', pps.quantity, 'fractionable', um.fractionable, 'regularPrice', pps.regularPrice, 'salePrice', pps.salePrice,
                                                                               'discount', pps.discount, 'discountType', pps.discountType, 'subTotal', pps.subTotal, 'total', pps.total, 'unitMeasure', um.name, 'urlKey', ps.urlKey,
                                                                               'thumbnail', first(select concat(ps.urlFiles, '/', img.thumbnail) from E:Document img 
                                                                                                   where identity(img.item) = ps.id and (identity(img.type) = 'IMG_ITEM_PRIMARY' or img.type is null)),
                                                                               'thumbnailAlt', first(select concat(ia.urlFiles, '/', da.thumbnail)
                                                                                                       from E:Document da
                                                                                                       join da.itemAttributeOption iao
                                                                                                       join iao.itemAttribute ia
                                                                                                      where ia = par.itemAttribute
                                                                                                        and (identity(da.type) = 'IMG_ITEM_PRIMARY' or da.type is null) 
                                                                                                        and JSON_SEARCH(ps.variantsGroup, 'one', iao.id) is not null ) )), ']')
                             from E:OrderRecordItem pps
                             join pps.item ps
                        left join ps.parent par
                             join ps.unitMeasure um 
                            where IDENTITY(pps.orderRecord) = p.id) as items,
                          pf.name as paymentForm,
                          pm.name as paymentMethod,
                          pm.id as paymentMethodId,
                          JSON_OBJECT('transactionId', CASE WHEN tr.paynetReferenceId IS NOT NULL THEN tr.paynetReferenceId ELSE tr.gatewayTransactionId END, 
                                      'operationDate', tr.operationDate, 
                                      'card', tr.card, 
                                      'paymentMethod', pm.name, 
                                      'paymentForm', pf.name, 
                                      'status', es.name) as transaction
                   from E:OrderRecord p                        			
                   join p.store s
                   join p.customer cp
                   join p.status es
                   join p.paymentForm pf
                   join p.paymentMethod pm
              left join p.transactions tr ";

        $where = " where p.id = :id
                     and cp.id = :userId 
                     and s.id = :storeId ";
        $params = ['id' => $id, 'userId' => $userId, 'storeId' => $storeId];

        $row = $this->getEntityManager()->createQuery($select . $where)->setParameters($params)->getOneOrNullResult();
        return $this->jsonDecode($row, ["items", "transaction", "customer"], true, true);
    }

    public function getOrderFromGatewayNotification($trId)
    {
        $select = "select p ";
        $from = " from E:OrderRecord p
                  join p.status es 
                  join p.transactions t";
        $where = " where t.gatewayTransactionId = :id and es.id in ('ORD_PEN', 'ORD_INS', 'ORD_UPD') ";
        $params = ['id' => $trId];
        return $this->getEntityManager()->createQuery($select . $from . $where)->setParameters($params)->getOneOrNullResult();
    }

    public function getCustomers(&$filters, $q)
    {
        $params = [];
        $and = "";
        if (trim($q)) {
            $params = ["q" => '%' . $q . '%'];
            $and = " and (c.name like :q or c.mothersLastName like :q or c.lastName like :q) ";
        }

        $saddresses = $this->concat(array("sas.streetName", "' '", "sas.numExt", "' '", "sas.numInt", "', '", "sas.neighborhood", "', '", "sas.city", "', '", "sas.state", "', '", "sas.country", "', '", "sas.postalCode"));

        $sql = "select c.id, 
                       identity(c.entityType) entityTypeId,
                       c.id as value,
                       (CASE WHEN et.id = 'CUS_MOR' THEN CONCAT_WS(' ', c.name, c.rfc) ELSE CONCAT_WS(' ', c.name, c.mothersLastName, c.lastName) END) as text,
                       c.rfc,
                       c.phone, 
                       c.mobile, 
                       c.email,
                       CONCAT('[',GROUP_CONCAT(DISTINCT JSON_OBJECT('value', sas.id, 
                                                                    'text', $saddresses,
                                                                    'name', (CASE WHEN et.id = 'CUS_MOR' THEN c.name ELSE CONCAT_WS(' ', c.name, c.mothersLastName, c.lastName) END),
                                                                    'reference', sas.reference,
                                                                    'streetName', sas.streetName, 
                                                                    'numExt', sas.numExt, 
                                                                    'numInt', sas.numInt, 
                                                                    'neighborhood', sas.neighborhood,
                                                                    'city', sas.city, 
                                                                    'town', sas.town, 
                                                                    'state', sas.state, 
                                                                    'country', sas.country, 
                                                                    'postalCode', sas.postalCode, 
                                                                    'phone', sas.phone, 
                                                                    'receiver', sas.receiver)),']') as shippingAddresses,                       
                       IDENTITY(c.defaultShippingAddress) defaultShippingAddressId,
                       IDENTITY(c.defaultBillingAddress) defaultBillingAddressId
                 from E:User c
                 join c.entityType et
            left join c.addresses sas            
                where c.roles LIKE '%ROLE_CUSTOMER%' $and                     
             group by c.id ";
        $rows = $this->getResultPaginated($sql, $params, $filters);
        return $this->jsonDecode($rows, ["shippingAddresses", "billingAddresses"], true, false);
    }

    public function getPaymentMethodsToCombo($showInOrders = 1, $gatewayId = null)
    {
        $select = "select m.id, m.id as value, m.name as name, m.name as text
                   from E:PaymentMethod m ";
        $where = "";
        $params = [];

        if ($showInOrders) {
            $params["showInOrders"] = $showInOrders;
            $where = " where m.showInOrders = :showInOrders ";
        }

//        if ($gatewayId) {
//            $params["gatewayId"] = $gatewayId;
//            $where = (empty($where) ? " where " : " and ") . " g.id = :gatewayId ";
//        }
        $order = " order by m.sequence ";

        $query = $this->getEntityManager()->createQuery($select . $where . $order)->setParameters($params);
        return $query->getResult();
    }

    public function findNextFolio($storeId, $date)
    {
        $nextId = $this->getEntityManager()->createQuery(
            "select CONCAT_WS('-','$date', (COUNT(o.folio) + 1))
                         from E:OrderRecord o
                         join o.store s
                         where s.id = :id 
                           and DATE_FORMAT(o.date, '%Y%m') = '$date' "
        )->setParameter('id', $storeId)->getSingleScalarResult();
        return $nextId;
    }

    public function getOrderRecordItemsSum($orderRecordId)
    {
        $sums = $this->getEntityManager()->createQuery(
            "select SUM(CASE WHEN c.id = 'usd' THEN pps.subTotal * o.exchangeRate ELSE pps.subTotal END) as subTotal,
                                SUM(CASE WHEN c.id = 'usd' THEN pps.total * o.exchangeRate ELSE pps.total END) as total
                        from E:OrderRecordItem pps
                        join pps.orderRecord o
                        join pps.currency c
                        where o.id = :orderRecordId"
        )->setParameter('orderRecordId', $orderRecordId)->getSingleResult();
        return $sums ? $sums : array('subTotal' => 0, 'total' => 0);
    }

    public function getTransactionLog($id, $userId, $isVendorView)
    {
        $select = "select tn.eventDate, tn.type ";
        $where = " where tn.gatewayTransactionId = :id and u.id = :userId ";
        $params = ['id' => $id, 'userId' => $userId];

        $from = " from E:TransactionNotification tn
                  join E:Transaction t with t.gatewayTransactionId = tn.gatewayTransactionId
                  join t.orderRecord o
                  join o.store s
                  join o.customer co";

        if ($isVendorView) {
            $joins = " join s.users u ";
        } else {
            $joins = " join co.users u ";
        }

        $order = " order by tn.registerDate desc";

        return $this->getEntityManager()->createQuery($select . $from . $joins . $where . $order)->setParameters($params)->getResult();
    }

    /**
     * START PRINT RECEIPT, REMISION, LABELS, ETC.
     */
    public function getReceipts($ids, $user, $storeId)
    {
        //TODO AFVR: NO TIENE MANTENIMIENTO AUN
        $select = "select o.id,
                              o.folio,
                              o.subTotal,
                              o.discount,
                              o.total,
                              o.exchangeRate,
                              DATE_FORMAT(o.date, '%Y-%m-%d %H:%i:%s') date,
                              JSON_OBJECT('phone', co.phone, 'mobile', co.mobile, 'email', co.email, 'name', CONCAT_WS(' ', co.name, co.mothersLastName, co.lastName), 
                              'shippingAddress', (select JSON_OBJECT('streetName', sas.streetName, 'numExt', sas.numExt, 'numInt', sas.numInt, 'neighborhood', sas.neighborhood, 
                                                                       'city', sas.city, 'town', sas.town, 'state', sas.state, 'country', sas.country, 'postalCode', sas.postalCode, 'phone', sas.phone, 'receiver', sas.receiver)
                                                      from E:Address sas 
                                                     where sas.id = IDENTITY(co.defaultShippingAddress))) as customer,                           
                              es.id as statusId, 
                              es.name as status,                             
                              o.comment, 
                              fo.name as paymentForm, 
                              fo.id as paymentFormId, 
                              mo.name as paymentMethod, 
                              mo.description as paymentMethodDescription, 
                              mo.id as paymentMethodId,
                             $this->orderRecordItems as items ";

        $from = " from E:OrderRecord o
                  join o.store s
                  join o.customer co
                  join o.status es
                  join o.paymentForm fo
                  join o.paymentMethod mo ";

        $params = ['ids' => $ids, "storeId" => $storeId];
        $where = " where o.id in (:id)
                     and s.id = :storeId ";

        if (!$isAdmin) {
            $where .= " and co.id = :userId ";
            $params['userId'] = $userId;
        }
        $params['storeId'] = $storeId;

        $row = $this->getEntityManager()->createQuery($select . $from . $where)->setParameters($params)->getResult();
        return $this->jsonDecode($row, ["shippingAddresses", "billingAddresses", "items", "customer->shippingAddress,billingAddress"], true);
    }

    public function getRemissions($id, $user, $storeId)
    {
        $select = "select sh.id,
                          sh.folio,
                          DATE_FORMAT(sh.date, '%Y-%m-%d %H:%i:%s') date,
                          JSON_OBJECT('phone', co.phone, 'mobile', co.mobile, 'email', co.email, 'name', CONCAT_WS(' ', co.name, co.mothersLastName, co.lastName)) customer, 
                          sh.shippingAddress,                          
                          (concat('[',group_concat(json_object(
                                            'id', ori.id, 'sequence', ori.sequence, 'folio', o.folio, 'orderRecordId', o.id, 'itemId', ps.id, 'typeId', t.id, 'name', ps.name, 'trackingTypeId', identity(ps.trackingType), 'serialized', ps.serialized, 'perishable', ps.perishable,
                                            'radioactive', ps.radioactive, 'volume', ori.volume, 'calibrationDate', ori.calibrationDate, 'quantity', ori.quantity, 'quantityFulFilled', ori.quantityFulFilled, 'fractionable', um.fractionable,
                                            'bulkSale', ori.bulkSale, 'unitMeasureBulkSale', umbs.name, 'unitMeasureBulkSaleCode', umbs.code, 'quantityByUnitBulkSale', ps.quantityByUnitBulkSale, 'bulkSaleFractionable', umbs.fractionable, 
                                            'decimalsBulkSale', ps.decimalsBulkSale, 'regularPrice', ori.regularPrice, 'currencyId', c.id, 'discount', ori.discount, 'discountType', ori.discountType, 'subTotal', ori.subTotal, 
                                            'total', ori.total, 'unitMeasure', um.name, 'registerDate', ori.registerDate, 'status', sori.name, 'statusId', sori.id, 'attributeValues', ori.attributeValues,
                                            'lot', (select group_concat(inv.lot) from E:OrderRecordItemInventory orinv
                                                     join orinv.inventory inv
                                                    where identity(orinv.orderRecordItem) = ori.id)) order by ori.registerDate asc),']')                                 
                             ) as items,
                          (select json_object('endDate', DATE_FORMAT(MAX(ul.endDate), '%Y-%m-%d %H:%i:%s'), 'folio', ul.folio) from  E:UserLicence ul where identity(ul.user) = co.id) licence,
                          first(select concat(s.urlFiles, '/', d.fileName) from E:Document d where identity(d.store) = s.id and identity(d.type) = 'IMG_PRINT_RECEIPT_LOGO_HEADER') logoHeader
                  from E:Shipment sh
                  join sh.items shi
                  join shi.orderRecordItem ori
                  join ori.orderRecord o
                  join o.status es            
                  join o.store s
                  join ori.status sori
                  join ori.item ps
                  join ps.type t
                  join ps.unitMeasure um 
             left join ps.unitMeasureBulkSale umbs
                  join ori.currency c                                                          
                  join sh.customer co
             left join o.seller se ";

        $params = ['id' => $id, "storeId" => $storeId];

        $where = " where identity(sh.status) in ('SHI_PEN', 'SHI_INP')
                     and o.id in (:id)
                     and s.id = :storeId
                     and es.id in ('ORD_SHI_PEN', 'ORD_SHI_INP')                
                       ";

        switch ($user->getRole()) {
            case "ROLE_SELLER":
                $where .= " and se.id = :userId ";
                $params['userId'] = $user->getId();
                break;
            case "ROLE_ADMIN":
            case "ROLE_SELLER_SHARED":
                $select .= "  join s.users u ";
                $where .= " and u.id = :userId ";
                $params['userId'] = $user->getId();
                // no break
            case "ROLE_SUPER_ADMIN":
                break;
            default:
                return [];
        }

        $row = $this->getResult($select . $where . " group by sh.id ", $params);
        return $this->jsonDecode($row, ["customer", "items->attributeValues", "licence"], true);
    }

    public function getLabels($id, $user, $storeId)
    {
        $select = "select oi.id,
                          oi.sequence,
                          i.name,
                          TRIM(oi.quantity) + 0 quantity,
                          TRIM(oi.subTotal) + 0 subTotal, 
                          TRIM(oi.discount) + 0 discount, 
                          oi.discountType,
                          TRIM(oi.total) + 0 total,
                          um.name as unitMeasure, 
                          um.fractionable,
                          oi.bulkSale,
                          umbs.name unitMeasureBulkSale,
                          umbs.code unitMeasureBulkSaleCode,
                          umbs.fractionable bulkSaleFractionable,
                          i.decimalsBulkSale,
                          i.radioactive,                          
                          TRIM(oi.volume) + 0 volume,
                          DATE_FORMAT(oi.calibrationDate, '%Y-%m-%d %H:%i:%s') calibrationDate,
                          oi.attributeValues,
                          o.id orderRecordId,
                          o.folio,
                          DATE_FORMAT(o.shipmentDate, '%Y-%m-%d %H:%i:%s') shipmentDate,
                          DATE_FORMAT(o.date, '%Y-%m-%d %H:%i:%s') date,
                          JSON_OBJECT('phone', co.phone, 'mobile', co.mobile, 'email', co.email, 'name', CONCAT_WS(' ', co.name, co.mothersLastName, co.lastName),
                                      'shippingAddress', (select JSON_OBJECT('streetName', sas.streetName, 
                                                                             'numExt', sas.numExt, 
                                                                             'numInt', sas.numInt, 
                                                                             'neighborhood', sas.neighborhood,
                                                                             'city', sas.city, 
                                                                             'town', sas.town, 
                                                                             'state', sas.state, 
                                                                             'country', sas.country, 
                                                                             'postalCode', sas.postalCode,
                                                                             'phone', sas.phone, 
                                                                             'receiver', sas.receiver)
                                                           from E:Address sas where sas.id = IDENTITY(co.defaultShippingAddress))) customer,
                          (select group_concat(inv.lot)
                             from E:OrderRecordItemInventory orinv
                             join orinv.inventory inv
                            where identity(orinv.orderRecordItem) = oi.id) lot,
                           (select DATE_FORMAT(max(invexp.expirationDate), '%Y-%m-%d %H:%i:%s')
                             from E:OrderRecordItemInventory orinvexp
                             join orinvexp.inventory invexp
                            where identity(orinvexp.orderRecordItem) = oi.id) expirationDate,
                           first(select concat(s.urlFiles, '/', d.fileName) from E:Document d where identity(d.store) = s.id and identity(d.type) = 'IMG_PRINT_LABEL_LOGO_HEADER') logoHeader
                    from E:OrderRecordItem oi
                    join oi.orderRecord o                  
                    join oi.item i
                    join i.type t
                    join i.unitMeasure um
               left join i.unitMeasureBulkSale umbs
                    join o.store s
                    join o.customer co
               left join o.seller se ";

        $params = ['id' => $id, "storeId" => $storeId];
        $where = " where o.id in (:id)
                     and s.id = :storeId ";
        $order = " order by o.folio, oi.sequence asc ";

        switch ($user->getRole()) {
            case "ROLE_SELLER":
                $where .= " and se.id = :userId ";
                $params['userId'] = $user->getId();
                break;
            case "ROLE_ADMIN":
            case "ROLE_SELLER_SHARED":
                $select .= "  join s.users u ";
                $where .= " and u.id = :userId ";
                $params['userId'] = $user->getId();
                // no break
            case "ROLE_SUPER_ADMIN":
                break;
            default:
                return [];
        }

        $row = $this->getEntityManager()->createQuery($select . $where . $order)->setParameters($params)->getResult();
        return $this->jsonDecode($row, ["billingAddresses", "customer->shippingAddress"], true);
    }

    /**
     *  START STATISTICS QUERYS
     */
    public function getSales($storeId, $user, $type = ["resume", "chartWeek", "chartYear", "latest"])
    {
        //TODO falta agregar las condiciones para que solo se obtengan las estadisticas de ventas de acuerdo a lo que se vendio por vendedor cuando el usuario logeado es un vendedor.
        $sales = [];
        $params = ['storeId' => $storeId, 'statusIds' => ['ORD_PAI', 'ORD_INV', 'ORD_DLV']];
        $select = '';

        if (in_array("resume", $type)) {
            $select = ' SUM((CASE WHEN p.date >= :day THEN 1 ELSE 0 END)) salesDay, 
                        SUM((CASE WHEN p.date >= :day THEN COALESCE(p.total, 0) ELSE 0 END )) salesDayAmount,
                        SUM((CASE WHEN p.date >= :week THEN 1 ELSE 0 END)) salesWeek, 
                        SUM((CASE WHEN p.date >= :week THEN COALESCE(p.total, 0) ELSE 0 END )) salesWeekAmount,
                        SUM((CASE WHEN p.date >= :month THEN 1 ELSE 0 END)) salesMonth, 
                        SUM((CASE WHEN p.date >= :month THEN COALESCE(p.total, 0) ELSE 0 END )) salesMonthAmount,
                        AVG(COALESCE(p.total, 0)) salesAverage ';
            $params['day'] = $this->getLocalDateTimeString()->format('Y-m-d 00:00:00');
            $params['week'] = $this->getLocalDateTimeString()->modify('monday this week')->format('Y-m-d 00:00:00');
            $params['month'] = $this->getLocalDateTimeString()->format('Y-m-01 00:00:00');
        }

        if (in_array("chartWeek", $type)) {
            $select .= (empty($select) ? "" : ",") .
                    " (select CONCAT('[', group_concat(JSON_OBJECT('day', d, 'count', c)),']')
                        from ( select (WEEKDAY(pa.date)+1) d, count(*) c
                                from orderrecord pa
                                join store sa on sa.id = pa.storeid                                                                
                               where sa.id = :storeId                                  
                                 and pa.statusid in (:statusIds)
                                 and pa.date >= :prevWeek  and pa.date < :week
                            group by WEEKDAY(pa.date)
                            order by pa.date) q) as chartSalesCountLastWeek, 
                     (select CONCAT('[', group_concat(JSON_OBJECT('id', id, 'day', d, 'count', c)),']')
                        from ( select (WEEKDAY(pa.date)+1) id, DATE_FORMAT(pa.date,'%a') d, count(*) c
                                from orderrecord pa
                                join store sa on sa.id = pa.storeid
                               where sa.id = :storeId
                                 and pa.statusid in (:statusIds)
                                 and pa.date >= :week
                            group by WEEKDAY(date)
                            order by pa.date) q) as chartSalesCountWeek,
                     SUM((CASE WHEN p.date >= :prevWeek and p.date < :week THEN 1 ELSE 0 END)) salesLastWeek,
                     (SELECT COUNT(*) FROM user where roles LIKE '%ROLE_CUSTOMER%' and registerdate >= :week ) customersWeek,                     
                     (SELECT COUNT(*) FROM itemreview where date >= :week) countWeekRatings ";
            $params["prevWeek"] = $this->getLocalDateTimeString()->modify('-7 day')->modify('monday this week')->format('Y-m-d 00:00:00');
        }

        if (in_array("chartYear", $type)) {
            $select .= (empty($select) ? "" : ",") .
                    " (select CONCAT('[', group_concat(JSON_OBJECT('id', id, 'month', d, 'count', c)),']')
                        from ( select MONTH(pa.date) id, DATE_FORMAT(pa.date, '%b') d, count(*) c
                                from orderrecord pa
                                join store sa on sa.id = pa.storeid
                               where sa.id = :storeId
                                 and pa.statusid in (:statusIds)
                                 and pa.date >= :firstDayOfYear  and pa.date < :lastDayOfYear
                            group by MONTH(pa.date)
                            order by pa.date) q) as chartSalesCountYear ";
            $params["firstDayOfYear"] = $this->getLocalDateTimeString()->format('Y-01-01 00:00:00');
            $params["lastDayOfYear"] = $this->getLocalDateTimeString()->format('Y-12-31 00:00:00');
        }

        if (in_array("latest", $type)) {
            $select .= (empty($select) ? "" : ",") .
                    " SUM((CASE WHEN p.date >= :latest24h THEN 1 ELSE 0 END)) salesLatest24h, 
                      FORMAT(SUM((CASE WHEN p.date >= :latest24h THEN COALESCE(p.total, 0) ELSE 0 END )),2) salesLatest24hAmount,
                      SUM((CASE WHEN p.date >= :latest7d THEN 1 ELSE 0 END)) salesLatest7d, 
                      FORMAT(SUM((CASE WHEN p.date >= :latest7d THEN COALESCE(p.total, 0) ELSE 0 END )),2) salesLatest7dAmount,
                      (select CONCAT('[',GROUP_CONCAT(JSON_OBJECT('name', i.name, 'brand', b.name, 'quantity', oi.quantity, 'time', DATE_FORMAT(pa.date, '%H:%i'), 'total', FORMAT(oi.total,2), 
                                                                  'thumbnail', (select concat(i.urlFiles, '/', img.thumbnail) 
                                                                                  from document img where img.itemId = i.id and img.typeId = 'IMG_ITEM_PRIMARY' limit 1))),']')
                        from orderrecord pa
                        join orderrecorditem oi on oi.orderrecordid = pa.id
                        join item i on i.id = oi.itemid
                   left join category b on b.id = i.brandId
                        join store sa on sa.id = pa.storeid
                       where sa.id = :storeId
                         and pa.statusid in (:statusIds)
                         and pa.date >= :latest24h order by pa.date) salesLatest24List ";
            $params['latest24h'] = $this->getLocalDateTimeString()->modify('-1 day')->format('Y-m-d H:m:s');
            $params['latest7d'] = $this->getLocalDateTimeString()->modify('-7 day')->format('Y-m-d 00:00:00');
        }

        $query = "select $select
                     from orderrecord p                        			
                     join store s on s.id = p.storeId                     
                    where s.id = :storeId
                      and p.statusid in (:statusIds) ";

        $row = $this->getOneResultFromRawSql($query, $params, ['statusIds' => \Doctrine\DBAL\Connection::PARAM_STR_ARRAY]);
        return $this->jsonDecode($row, ["chartSalesCountYear", "chartSalesCountWeek", "salesLatest24List"], true, true);
    }
}
