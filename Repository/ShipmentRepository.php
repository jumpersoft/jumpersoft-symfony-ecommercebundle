<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;

class ShipmentRepository extends JumpersoftEntityRepository
{
    public function getShipments(&$filters, $userId, $storeId, $isVendorView, $for = null, $ids = null)
    {
        $select = "select distinct s.id,
                          s.folio,                          
                          DATE_FORMAT(s.date, '%Y-%m-%d %H:%i:%s') date,
                          es.id as statusId,
                          es.name as status,                          
                          p.subTotal, 
                          p.discount, 
                          p.total,
                          JSON_OBJECT('phone', cp.phone, 'mobile', cp.mobile, 'email', cp.email) as customer, 
                          CONCAT_WS(' ', cp.name, cp.mothersLastName, cp.lastName) as name,
                          pm.id paymentMethodId, 
                          pm.name as paymentMethodName,  
                          pm.name as paymentMethod, 
                          pf.id paymentFormId, 
                          pf.name as paymentForm, 
                          p.shippingAddress,
                          p.billingAddress,
                          (select concat('[',group_concat(distinct json_object('name', ps.name, 'quantity', soi.quantity, 'quantityReal', pps.quantity, 'regularPrice', pps.regularPrice, 'discount', pps.discount, 
                                           'discountType', pps.discountType, 'subTotal', pps.subTotal, 'total', pps.total, 'unitMeasure', um.name) order by pps.registerDate asc),']')
                                from E:ShipmentItem soi
                                join soi.orderRecordItem pps                                
                                join pps.item ps
                                join ps.unitMeasure um 
                               where IDENTITY(soi.shipment) = s.id) as items
                   from E:Shipment s
                   join s.status es
                   join s.items si
                   join si.orderRecordItem oi
                   join oi.orderRecord p
                   join p.store st
                   join p.customer cp                   
              left join p.paymentMethod pm
              left join p.paymentForm pf 
                   ";

        if ($isVendorView) {
            $joins = "  join s.seller u ";
            $where = " where u.id = :id and s.id = :storeId ";
            $params = ['id' => $userId, 'storeId' => $storeId];
        } else {
            $joins = "";
            $where = " where cp.id = :id ";
            $params = ['id' => $userId];
        }
        if ($storeId) {
            $where .= " and s.id = :storeId ";
            $params['storeId'] = $storeId;
        }

        if ($ids == null) {
            $this->setConditionalFilters($where, $params, $filters, ["s.folio" => "folio", "es.name" => "status", "cp.email" => "email", "p.total" => "total", ["cp.name" => "name", "cp.lastName" => "name", "cp.mothersLastName" => "name"]]);
            $this->setDateBetweenQuery($where, $params, "p.date", $filters['filters'], ["startDate", "endDate"]);
            $where .= $this->setOrderFilters($filters, ["folio" => "s.folio", "date" => "s.date", "status" => "es.name", "name" => "cp.name", "email" => "cp.email", "total" => "p.total"]);
        } else {
            $where .= " and p.id in (:ids) ";
            $params["ids"] = $ids;
        }
        $joins = $where = "";
        $params = [];

        $rows = $for == "report" ? $this->getResult($select . $joins . $where, $params, $filters) : $this->getResultPaginated($select . $joins . $where, $params, $filters);

        return $this->jsonDecode($rows, ["customer", "items"], true, false);
    }

    public function getShipment($id, $storeId, $userId, $isVendorView, $for = "view", $forEdit = false)
    {
        if ($for == "view") {
            $saddresses = $this->concat(array("sas.streetName", "' '", "sas.numExt", "' '", "sas.numInt", "', '", "sas.neighborhood", "', '", "sas.city", "', '", "sas.state", "', '", "sas.country", "', '", "sas.postalCode"));
            $baddresses = $this->concat(array("bas.streetName", "' '", "bas.numExt", "' '", "bas.numInt", "', '", "bas.neighborhood", "', '", "bas.city", "', '", "bas.state", "', '", "bas.country", "', '", "bas.postalCode"));

            $select = "select p.id,
                              p.folio,
                              p.subTotal,
                              p.discount,
                              p.total,
                              DATE_FORMAT(p.date, '%Y-%m-%d %H:%i:%s') date,
                              JSON_OBJECT('phone', cp.phone, 'mobile', cp.mobile, 'email', cp.email) as customer,
                              CONCAT_WS(' ', cp.name, cp.mothersLastName, cp.lastName) as name,
                              es.id as statusId, 
                              es.name as status,                             
                              p.comment, 
                              fp.name as paymentForm, 
                              fp.id as paymentFormId, 
                              mp.name as paymentMethod, 
                              mp.description as paymentMethodDescription, 
                              mp.id as paymentMethodId,                             
                             (select CONCAT('[',GROUP_CONCAT(DISTINCT JSON_OBJECT('value', sas.id, 'text', $saddresses, 'phone', sas.phone, 'receiver', sas.receiver)),']')
                                from E:Address sas
                               where identity(sas.user) = cp.id) as shippingAddresses,
                             (select CONCAT('[',GROUP_CONCAT(DISTINCT JSON_OBJECT('value', bas.id, 'text', $baddresses, 'phone', bas.phone, 'rfc', bas.rfc)),']')
                               from E:Address bas
                              where identity(bas.user) = cp.id 
                                and bas.billingAddress = 1) as billingAddresses,
                             IDENTITY(cp.defaultShippingAddress) shippingAddressId,
                             IDENTITY(cp.defaultBillingAddress) billingAddressId,
                             p.shippingAddress,
                             p.billingAddress,
                             (select concat('[',group_concat(distinct json_object('id', pps.id, 'orderRecordId', p.id, 'itemId', ps.id, 'name', ps.name, 'quantity', pps.quantity, 'fractionable', um.fractionable, 'regularPrice', pps.regularPrice, 
                                     'discount', pps.discount, 'discountType', pps.discountType, 'subTotal', pps.subTotal, 'total', pps.total, 'unitMeasure', um.name, 'registerDate', pps.registerDate) order by pps.registerDate asc),']')
                                from E:OrderRecordItem pps
                                join pps.item ps
                                join ps.unitMeasure um 
                               where IDENTITY(pps.orderRecord) = p.id) as items
                             
                    ";
        } else {
            //For Update or for Delete
            $select = $for == "update" ? "select p " : "select partial p.{id} ";
        }
        $params = ['id' => $id, 'userId' => $userId];

        if (is_array($id)) {
            $where = " where p.id in (:id) ";
        } else {
            $where = " where p.id = :id ";
        }

        $where .= ($forEdit == true ? " and es.id not in ('ORD_PAI', 'ORD_INV', 'ORD_CAN_NOR', 'ORD_CAN_RET') " : " ");

        $from = " from E:OrderRecord p
                  join p.store s
                  join p.user cp
                  join p.status es
                  join p.paymentForm fp
                  join p.paymentMethod mp ";

        if ($isVendorView) {
            $where .= " and u.id = :userId and s.id = :storeId ";
            $joins = " join s.users u ";
        } else {
            $joins = "";
            $where .= " and cp.id = :userId ";
        }
        if ($storeId) {
            $where .= " and s.id = :storeId ";
            $params['storeId'] = $storeId;
        }

        if (is_array($id)) {
            $row = $this->getEntityManager()->createQuery($select . $from . $joins . $where)->setParameters($params)->getResult();
            return $for == "view" ? $this->jsonDecode($row, ["shippingAddresses", "billingAddresses", "items", "customer"], true) : $row;
        } else {
            $row = $this->getEntityManager()->createQuery($select . $from . $joins . $where)->setParameters($params)->getOneOrNullResult();
            return $for == "view" ? $this->jsonDecode($row, ["shippingAddresses", "billingAddresses", "items", "customer"], true, true) : $row;
        }
    }

    public function getCurrentShipment($customerId, $date, $shippingAddressId)
    {
        $select = "select s
                     from E:Shipment s
                    where identity(s.customer) = :customerId
                      and s.date = :date
                      and JSON_EXTRACT(s.shippingAddress, '$.id') = :shippingAddressId ";

        $params = ['customerId' => $customerId, 'date' => $date, 'shippingAddressId' => $shippingAddressId];

        return $this->getOneOrNullResult($select, $params);
    }

    public function getSchedule()
    {
        return $this->getResult("select s.id,
                                        s.name, 
                                        s.minute, 
                                        s.hour,
                                        CONCAT_WS('-', LPAD(s.hour,2,'0'), LPAD(IFNULL(s.minute, '00'),2,'0')) as time
                                   from E:ShipmentSchedule s 
                               ORDER BY s.hour asc");
    }

    public function getNextFolio($shipmentDate)
    {
        return $this->getSingleScalarResult("select CONCAT_WS('-', 'E', '{$shipmentDate->format('Ymd')}', (COUNT(s.id)+1), '{$shipmentDate->format('Hi')}' )
                                               from E:Shipment s
                                              where DATE_FORMAT(s.date, '%Y-%m-%d') = :shipmentDate ", ["shipmentDate" => $shipmentDate->format('Y-m-d')]);
    }
}
