<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * StoreRepository
 */
class StoreSocialNetworkRepository extends JumpersoftEntityRepository
{
    public function getSocialNetworks($storeId)
    {
        return $this->getResult("select s.id, s.name, s.url, s.active, DATE_FORMAT(s.registerDate, '%Y-%m-%d')
                                  from E:StoreSocialNetwork s 
                                 where identity(s.store) = :id", ["id" => $storeId]);
    }
}
