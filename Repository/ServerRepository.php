<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class ServerRepository extends EntityRepository
{

    /**
     * Next Server Id
     */
    public function findNextServerId()
    {
        return md5(uniqid(rand(), true));
    }

    /**
     * getServers, incluye paginación
     */
    public function getServers($userId, &$filters)
    {
        $query = $this->getEntityManager()->createQuery(
            'select t.id, t.name, t.description, t.domain, t.emailAdmin, t.registerDate, e.name status
			from E:Server t
                        join t.status e
			ORDER BY t.id DESC'
        )
                        ->setFirstResult($filters['start'])->setMaxResults($filters["length"]);
        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);
        $filters["recordsFiltered"] = $filters["recordsTotal"] = count($paginator);
        return $query->getArrayResult();
    }

    /**
     * Check Name Server
     */
    public function checkServer($name, $id)
    {
        $query = $this->getEntityManager()->createQuery(
            'select count(p)
			from E:Server p
                        where p.name = :name
                          and p.id <> :id
			ORDER BY p.id DESC'
        )
                ->setParameter('name', $name)
                ->setParameter('id', $id);

        return $query->getSingleScalarResult() > 0 ? true : false;
    }
}
