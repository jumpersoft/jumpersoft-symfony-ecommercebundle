<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class DocumentRepository extends JumpersoftEntityRepository
{
    public function getImages($storeId = null, $itemId = null, $itemAttributeId = null, $categoryId = null, $userId = null)
    {
        $select = "select img.id, 
                          img.title,
                          img.alt,
                          img.fileSize, 
                          img.size,                          
                          img.active,
                          img.sequence,
                          img.sequence as order,                          
                          concat(p.urlFiles, '/', img.fileName) fileName,
                          concat(p.urlFiles, '/', img.thumbnail) thumbnail,
                          t.id as typeId,
                          t.name as type,
                          identity(img.item) itemId,
                          identity(img.itemAttributeOption) itemAttributeOptionId ";
        $join = " from E:Document img
             left join img.type t ";

        if ($itemAttributeId) {
            $join .= " join img.itemAttributeOption iao
                       join iao.itemAttribute p
                       join p.item i
                       join i.store s                    
                       join s.users u
                        ";
            $where = " where i.id = :itemId
                         and s.id = :storeId 
                         and u.id = :userId
                         and p.id = :itemAttributeId ";
            $params = ['storeId' => $storeId, 'itemId' => $itemId, 'itemAttributeId' => $itemAttributeId, 'userId' => $userId];
        } elseif ($itemId) {
            $join .= "join img.item p
                     join p.store s                    
                     join s.users u ";
            $where = " where p.id = :itemId and s.id = :storeId and u.id = :userId ";
            $params = ['storeId' => $storeId, 'itemId' => $itemId, 'userId' => $userId];
        } elseif ($categoryId) {
            $join .= "join img.category p
                     join p.store s                    
                     join s.users u ";
            $where = " where p.id = :categoryId and s.id = :storeId and u.id = :userId ";
            $params = ['storeId' => $storeId, 'categoryId' => $categoryId, 'userId' => $userId];
        } else {
            $join .= "join img.store p 
                     join p.users u ";
            $where = "where p.id = :storeId and u.id = :userId ";
            $params = ["storeId" => $storeId, 'userId' => $userId];
        }

        $order = " order by img.sequence ";
        $filters = ["start" => 0, "length" => 50];
        return $this->getResultPaginated($select . $join . $where . $order, $params, $filters);
    }

    public function getImage($id)
    {
        $query = $this->getEntityManager()->createQuery(
            "select im 
                           from E:Document im                           
                          where im.id = :id"
        )->setParameters(["id" => $id]);
        return $query->getSingleResult();
    }
}
