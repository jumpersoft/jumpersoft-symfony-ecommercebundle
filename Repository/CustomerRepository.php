<?php

/* DEPRECADA - IT MUST BE ELIMINATE */

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;

class CustomerRepository extends JumpersoftEntityRepository
{

    /**
     * getCustomersSuppliers, incluye paginación
     */
    public function getCustomersSuppliers(&$filters, $userId, $storeId, $isAdminView)
    {
        $q2 = "";
        $params = [];
        if (!$isAdminView) {
            $q2 = " join E:OrderRecord o with cs = o.customer
                        join o.store s                        
                        left join s.users u
                        where s.id = :storeId and u.id = :userId";
            $params = ['userId' => $userId, 'storeId' => $storeId];
        }

        $sql = "select cs, partial csu.{id, username}
                from E:Customer cs 
                left join cs.users csu " . $q2;
        return $this->getResultPaginated($sql, $params, $filters);
    }

    /**
     * getCustomer
     */
    public function getCustomer($id, $userId, $storeId, $isAdminView, $for = "view")
    {
        if ($for == "view") {
            $select = "select cs.id, cs.rfc, cs.name, cs.email, cs.alternateEmail, cs.emailToSendInvoice, cs.streetName, cs.numExt, cs.numInt, cs.neighborhood, cs.city, cs.reference, "
                    . "cs.town, cs.state, cs.country, cs.postalCode, cs.tradeName, cs.customer, cs.provider, cs.foreigner, cs.customerCounter, cs.physicalPerson, cs.phone, "
                    . "cs.sameFiscalAddressAsContactAddress ";
        } else {
            //For Update or for Delete
            $select = $for == "update" ? "select cs " : "select partial cs.{id} ";
        }

        $q2 = " where cs.id = :id";
        $params = ["id" => $id];

        if (!$isAdminView) {
            $q2 = " join E:OrderRecord o with cs = o.customer
                    join o.store s                    
                    join s.users u
                    where cs.id = :id and s.id = :storeId and u.id = :userId ";
            $params = ["id" => $id, "userId" => $userId, "storeId" => $storeId];
        }

        $query = $this->getEntityManager()->createQuery($select . " from E:Customer cs " . $q2)->setParameters($params);
        return $query->getSingleResult();
    }

    /**
     * getCustomerUsers
     */
    public function getCustomerUsers($id, $userId, $storeId, $isAdminView)
    {
        $q2 = " where cs.id = :id";
        $params = ["id" => $id];
        $name = $this->concat(array("csu.name", "' '", "csu.lastName", "' '", "csu.mothersLastName", " '('", "csu.username", " ')'"));

        if (!$isAdminView) {
            $q2 = "join E:OrderRecord o with cs = o.customer
                   join o.store s                   
                   join s.users u
                   where cs.id = :id and s.id = :storeId and u.id = :userId ";
            $params = ["id" => $id, "userId" => $userId, "storeId" => $storeId];
        }

        $query = $this->getEntityManager()->createQuery("select csu.id, $name as text 
                                                           from E:Customer cs
                                                           join cs.users csu $q2 ")->setParameters($params);
        return $query->getArrayResult();
    }

    /**
     * getCustomerUsersNotAsigned
     * Get users that has not been related to customers
     */
    public function getCustomerUsersNotAsigned(&$filters, $q)
    {
        $params = ['q' => '%' . $q . '%'];
        $where = " where (u.roles LIKE '%ROLE_CUSTOMER%' or u.roles LIKE '%ROLE_ANALYST%')
                     and u.roles NOT LIKE '%ROLE_SELLER%'
                     and u.roles NOT LIKE '%ROLE_ADMIN%' 
                     and u.roles NOT LIKE '%ROLE_SUPER_ADMIN%' 
                     and (u.name like :q or u.lastName like :q or u.mothersLastName like :q)
                     and u.id not in (select csu.id from E:Customer cs join cs.users csu)";
        $name = $this->concat(array("u.name", "' '", "u.lastName", "' '", "u.mothersLastName", " '('", "u.username", " ')'"));
        $sql = "select u.id, $name as text
                  from E:User u
                  $where
                  ORDER BY u.username DESC";
        return $this->getResultPaginated($sql, $params, $filters);
    }

    /**
     * findFirstCustomerArrayResult related to user
     */
    public function findFirstCustomerArrayResult($userId)
    {
        return $this->getEntityManager()->createQuery(
            "select cs
                         from E:Customer cs
                         join cs.users u                        
                         where u.id = :id"
        )->setParameter('id', $userId)->setMaxResults(1)->getSingleResult();
    }
}
