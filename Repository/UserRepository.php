<?php

namespace Jumpersoft\EcommerceBundle\Repository;

class UserRepository extends JumpersoftEntityRepository
{
    public function getUsers(&$filters, $userId, $storeId, $isAdmin, $isSAdmin = false)
    {
        $where = "";
        $params = [];

        $query = "select u.id, 
                         et.id entityTypeId,
                         u.username, 
                         u.email, 
                         u.name, 
                         u.lastName, 
                         u.mothersLastName, 
                         u.rfc,
                         u.tradeName,
                         u.registerDate, 
                         JSON_UNQUOTE(JSON_EXTRACT(u.roles, '$[0]')) roleId
                    from E:User u
                    join u.entityType et ";

        if (!$isAdmin) {
            $query .= " join u.companies e                      
                       join e.users us
                       join e.stores s
                      where u.roles LIKE '%ROLE_SELLER%'
                        and u.roles NOT LIKE '%ROLE_ADMIN%' 
                        and u.roles NOT LIKE '%ROLE_SUPER_ADMIN%' 
                        and us.id = :userId
                        and s.id = :storeId";
            $params = ['userId' => $userId, 'storeId' => $storeId];
        } elseif (!$isSAdmin) {
            $query .= " where u.roles NOT LIKE '%ROLE_SUPER_ADMIN%' ";
        }

        $this->setConditionalFilters($query, $params, $filters, ["u.username" => "username", "u.email" => "email", "u.rfc" => "rfc", ["u.name" => "name", "u.lastName" => "name", "u.mothersLastName" => "name", "u.roles" => "roleId"], "u.registerDate" => "registerDate"]);
        $query .= $this->setOrderFilters($filters, ["username" => "u.username", "email" => "u.email", "name" => "u.name", "name" => "u.name", "roles" => "u.roles", "registerDate" => "u.registerDate"]);

        return $this->getResultPaginated($query, $params, $filters);
    }

    public function getUser($id, $userId, $storeId, $isAdmin, $for = "view")
    {
        if ($for == "view") {
            $select = "select u.id,
                              identity(u.entityType) entityTypeId,  
                              u.username,
                              u.username as confirmEmail, 
                              JSON_UNQUOTE(JSON_EXTRACT(u.roles, '$[0]')) roleId,
                              u.name, 
                              u.mothersLastName, 
                              u.lastName, 
                              u.alternateEmail, 
                              u.mobile, 
                              u.phone, 
                              u.logo, 
                              u.rules,
                              u.rfc,
                              u.tradeName ";
        } else {
            $select = $for == "update" ? "select u " : "select partial u.{id} ";
        }

        if (!$isAdmin) {
            $where = " join u.companies e                      
                       join e.users us
                       join e.stores s
                       where u.id = :id
                         and us.id = :userId
                         and s.id = :storeId";
            $params = ["id" => $id, "userId" => $userId, "storeId" => $storeId];
        } else {
            $where = " where u.id = :id ";
            $params = ["id" => $id];
        }

        return $this->getEntityManager()->createQuery("$select from E:User u $where")->setParameters($params)->getSingleResult();
    }

    public function getUserDetail($id)
    {
        $select = "select u.username, 
                          u.email, 
                          u.email as confirmEmail, 
                          u.roles, 
                          u.name, 
                          u.mothersLastName, 
                          u.lastName, 
                          u.alternateEmail, 
                          u.mobile, 
                          u.phone, 
                          u.logo,
                          u.rfc
                     from E:User u
                    where u.id = :id ";
        $params = ["id" => $id];
        return $this->getEntityManager()->createQuery($select)->setParameters($params)->getOneOrNullResult();
    }

    public function checkUsername($username, $userId)
    {
        $query = $this->getEntityManager()->createQuery(
            "select count(u)
			   from E:User u                        
                          where u.id <> :userId
                            and u.username = :username "
        )->setParameters(["username" => $username, "userId" => $userId]);

        return $query->getSingleScalarResult() > 0 ? true : false;
    }

    public function getRoles($user = null)
    {
        $role = is_object($user) ? $user->getRole() : "ROLE_CUSTOMER";
        $where = "";

        if (!in_array($role, ["ROLE_ADMIN", "ROLE_SUPER_ADMIN"])) {
            $where = " where r.id not in ('ROLE_ADMIN', 'ROLE_SUPER_ADMIN') ";
        } elseif (in_array($role, ["ROLE_ADMIN"])) {
            $where = " where r.id not in ('ROLE_SUPER_ADMIN') ";
        }

        $select = "select r.id, r.name,
                          r.id value, r.name text ";

        $query = $this->getEntityManager()->createQuery(
            $select .
                "from E:Role r
                 $where
              order by r.name"
        );
        return $query->getResult();
    }

    public function getCards(&$filters, $userId)
    {
        $params = ['userId' => $userId];
        $sql = "select c.id, c.name, c.number, c.brand, c.month, c.year
                  from E:UserCard c                
                 where IDENTITY(c.user) = :userId ";
        $rows = $this->getResultPaginated($sql, $params, $filters);
        return $rows;
    }

    public function getCard($userId, $id, $for = "view")
    {
        $params = ['userId' => $userId, 'id' => $id];
        if ($for == "view") {
            $select = "c.id, c.name, c.number, c.brand, c.month, c.year ";
        } else {
            $select = $for == "update" ? " c " : " partial c.{id} ";
        }
        $sql = "select $select
                  from E:UserCard c                
                 where IDENTITY(c.user) = :userId
                   and c.id = :id ";
        return $this->getEntityManager()->createQuery($sql)->setParameters($params)->getSingleResult();
    }
}
