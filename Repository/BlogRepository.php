<?php

namespace Jumpersoft\EcommerceBundle\Repository;

class BlogRepository extends JumpersoftEntityRepository
{
    public function getLastPosts($page, $filters)
    {
        $query = $this->getResultPaginated(
            "select b.id, 
                        b.title, 
                        b.content,
                        b.slug,                                        
                        DATE_FORMAT(b.publishedAt, '%Y-%m-%d %H:%i:%s') publishedAt,
                        CONCAT_WS(' ', a.name, a.lastName) author,
                        (select count(bc.id)
                           from E:BlogComment bc
                           join bc.user bcu
                          where identity(bc.post) = b.id) commentsCount
                   from E:BlogPost b                                   
                   join b.author a 
               order by b.publishedAt desc ",
            [],
            $filters
        );
        return $query;
    }
}
