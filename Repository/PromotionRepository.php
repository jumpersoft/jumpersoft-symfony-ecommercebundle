<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class PromotionRepository extends JumpersoftEntityRepository
{

    /**
     * TODO: Falta incluir la empresa 1 del usuario.
     */
    public function getPromotions(&$filters, $storeId)
    {
        $query = "select p.id, p.code, t.name as type, s.name as status, s.id as statusId, 
                         DATE_FORMAT(p.startDate, '%Y-%m-%d %H:%i:%s') startDate, 
                         DATE_FORMAT(p.endDate, '%Y-%m-%d %H:%i:%s') endDate, 
                         p.worksWithOtherPromotions,
                         p.discountType, FORMAT(COALESCE(p.discount, 0), 2) discount,
                         p.registerDate
                    from E:Promotion p
                    join p.type t
                    join p.store st
                    join p.status s                   
                    where st.id = :storeId
                    ORDER BY p.endDate ASC";
        return $this->getResultPaginated($query, ["storeId" => $storeId], $filters);
    }
    
    public function getPromotion($id, $storeId, $for = "view")
    {
        if ($for == "view") {
            $select = "select p.id, p.code, t.name as type, t.id typeId, s.name as status, s.id as statusId, p.name, p.terms, p.description, p.worksWithOtherPromotions,
                              p.discountType, FORMAT(p.discount, 2) discount ";
        } else {
            //For update or for delete
            $select = $for == "update" ? "select p " : "select partial p.{id} ";
        }

        $query = $this->getEntityManager()->createQuery($select .
                        " from E:Promotion p
                          join p.type t
                          join p.store st
                          join p.status s
                          where p.id = :id                             
                            and st.id = :storeId")->setParameters(["id" => $id, "storeId" => $storeId]);
        return $query->getSingleResult();
    }

    public function checkPromotion($name, $id, $storeId)
    {
        $query = $this->getEntityManager()->createQuery(
            "select count(p)
			from E:Promotion p
                        join p.store s
                        where p.name = :name
                          and p.id <> :id
                          and s.id = :storeId
			ORDER BY p.id DESC"
        )->setParameters(["name" => $name, "id" => $id, "storeId" => $storeId]);

        return $query->getSingleScalarResult() > 0 ? true : false;
    }

    /**
     * TODO: Falta incluir la empresa 1 del usuario.
     */
    public function getPromotionsToComboHTML($userId)
    {
        $query = $this->getEntityManager()->createQuery(
            "select um.id as value, CONCAT(CONCAT(um.name, ' - '), um.code) as name
			 from E:Promotion um                        
                         join um.store s                         
                         join s.users u
                         where u.id = :id
			 ORDER BY um.id DESC"
        )->setParameter("id", $userId);
        return $query->getResult();
    }

    public function getPromotionItems(&$filters, $userId, $storeId, $promotionId)
    {
        $select = "select ics.id, i.name, i.regularPrice, i.salePrice, i.discount, i.discountType,
                          first(select concat(i.urlFiles, '/', d.thumbnail) from E:Document d where identity(d.item) = i.id and (identity(d.type) = 'IMG_ITEM_PRIMARY' or d.type is null)) thumbnail,
                          ics.registerDate
			 from E:PromotionItem ics
                         join ics.promotion ir
                         join ics.item i
                         join i.store s
                         join s.users u ";
        $where = " where u.id = :userId and s.id = :storeId and ir.id = :promotionId ";
        $params = ['userId' => $userId, 'storeId' => $storeId, 'promotionId' => $promotionId];
        $order = "";
        return $this->getResultPaginated($select . $where . $order, $params, $filters);
    }
    
    public function getPromotionItemsAvailable(&$filters, $promotionId, $userId, $storeId, $q)
    {
        $select = "select p.id, p.id as value, p.name as text,
                          first(select concat(p.urlFiles, '/', d.thumbnail) from E:Document d where identity(d.item) = p.id and (identity(d.type) = 'IMG_ITEM_PRIMARY' or d.type is null)) thumbnail ";
        $from = "from E:Item p                         
                 join p.store s                         
                 join s.users u ";
        $where = " where identity(p.status) = 'ITE_AVA'
                     and identity(p.type) <> 'ITE_CON'
                     and u.id = :userId
                     and s.id = :storeId 
                     and p.name like :q 
                     and p.id not in (select pii.id 
                                        from E:PromotionItem pi
                                        join pi.item pii
                                        join pi.promotion pip
                                        where pip.id = :promotionId )";
        $order = " order by p.id DESC ";
        $params = ['promotionId' => $promotionId, 'userId' => $userId, 'storeId' => $storeId, 'q' => '%' . $q . '%'];
        return $this->getResultPaginated($select . $from . $where . $order, $params, $filters);
    }

    public function getPromotionItem($promotionId, $id, $userId, $storeId, $for = "update")
    {
        $select = $for == "update" ? "select pi " : "select partial pi.{id} ";
        $params = ["promotionId" => $promotionId, "userId" => $userId, "storeId" => $storeId, "id" => $id];
        $query = $this->getEntityManager()->createQuery($select .
                        " from E:PromotionItem pi
                          join pi.promotion p
                          join pi.item i
                          join i.store s                          
                          join s.users u
                          where p.id = :promotionId 
                            and u.id = :userId 
                            and s.id = :storeId 
                            and pi.id = :id")->setParameters($params);
        return $query->getOneOrNullResult();
    }
}
