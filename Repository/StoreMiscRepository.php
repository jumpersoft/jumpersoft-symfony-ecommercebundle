<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * StoreMisRepository
 * Desc: Miscellaneous queries
 */
class StoreMiscRepository extends JumpersoftEntityRepository
{
 
    /**
     * getStoreWishlistItems
     */
    public function getStoreWishlistItems(&$filters, $id, $user)
    {
        $select = " i.id,
                    i.name, 
                    i.description, 
                    i.discount, 
                    i.discountType, 
                    i.regularPrice, 
                    i.salePrice, 
                    FORMAT(i.maxQtyAllowedInCart, IFELSE(um.fractionable IS NULL, 0, i.decimals)) maxQtyAllowedInCart,
                    FORMAT(i.minQtyAllowedInCart, IFELSE(um.fractionable IS NULL, 0, i.decimals)) minQtyAllowedInCart,
                    i.urlKey,
                    CONCAT('/store/i/', i.id) as url,
                    first(select concat(i.urlFiles, '/', im.thumbnail) from E:Document im where identity(im.item) = i.id and (identity(im.type) = 'IMG_ITEM_PRIMARY' or im.type is null)) as thumbnail,
                    first(select concat(ia.urlFiles, '/', da.thumbnail) from E:Document da join da.itemAttributeOption iao join iao.itemAttribute ia
                           where ia = par.itemAttribute and (identity(da.type) = 'IMG_ITEM_PRIMARY' or da.type is null) and JSON_SEARCH(i.variantsGroup, 'one', iao.id) is not null ) thumbnailAlt,
                    1 as quantity,
                    IFELSE(t.id = 'ITE_CON', 0, IFELSE(identity(i.trackingType) = 'INV_TYP_SIM', i.stock, IFELSE(identity(i.trackingType) = 'INV_TYP_NAN', 0, (SELECT SUM(inv.stock) FROM E:Inventory inv where identity(inv.item) = i.id and identity(inv.operation) = 'INV_OPE_IN')))) as stock
                    ";

        if (is_object($user)) {
            $where = " where w.registerDate = (SELECT MAX(d.registerDate) FROM E:Wishlist d WHERE IDENTITY(d.user) = :userId) "
                    . " and IDENTITY(w.user) = :userId ";
            $params['userId'] = $user->getId();
        } else {
            $where = " where w.id = :id "
                    . " and IDENTITY(w.user) is null ";
            $params['id'] = $id;
        }

        $query = " select $select
                    from E:Wishlist w
                    join w.items wi
                    join wi.item i
               left join i.parent par
                    join i.type t
                    join i.unitMeasure um
                   $where
                   order by w.registerDate ";

        return $this->getResultPaginated($query, $params, $filters);
    }

    /**
     * getStoreWishlist
     */
    public function getStoreWishlist($id, $user = null)
    {
        if (is_object($user)) {
            $where = " where w.registerDate = (SELECT MAX(d.registerDate) FROM E:Wishlist d WHERE IDENTITY(d.user) = :userId) "
                    . " and IDENTITY(w.user) = :userId ";
            $params['userId'] = $user->getId();
        } else {
            $where = " where w.id = :id "
                    . " and IDENTITY(w.user) is null ";
            $params['id'] = $id;
        }

        $query = $this->getEntityManager()->createQuery(
            " select w
                            from E:Wishlist w
                            join w.items wi
                            join wi.item i
                           $where "
        )->setParameters($params);
        return $query->getOneOrNullResult();
    }

    /**
     * getStoreComparisonItems
     */
    public function getStoreComparisonItems(&$filters, $id, $user)
    {
        $imagesColumnsUrls = $this->getImagesColumnsUrls("i.urlFiles", "im.fileName", "image", "im");
        $url = "CONCAT('/store/i/', i.id) as url ";
        $select = " i.id,
                    i.name, 
                    i.shortDescription, 
                    i.description, 
                    i.specs, 
                    i.discount, 
                    i.discountType, 
                    i.regularPrice, 
                    i.salePrice, 
                    i.stock, 
                    $url, 
                    i.urlKey,
                    $imagesColumnsUrls ";

        if (is_object($user)) {
            $where = " where w.registerDate = (SELECT MAX(d.registerDate) FROM E:Comparison d WHERE IDENTITY(d.user) = :userId) "
                    . " and IDENTITY(w.user) = :userId ";
            $params['userId'] = $user->getId();
        } else {
            $where = " where w.id = :id "
                    . " and IDENTITY(w.user) is null ";
            $params['id'] = $id;
        }

        $query = " select $select
                    from E:Comparison w
                    join w.items wi
                    join wi.item i                            
               left join i.defaultImage im
                   $where
                   order by w.registerDate ";

        return $this->getResultPaginated($query, $params, $filters);
    }

    /**
     * getStoreComparison
     */
    public function getStoreComparison($id, $user = null)
    {
        if (is_object($user)) {
            $where = " where w.registerDate = (SELECT MAX(d.registerDate) FROM E:Comparison d WHERE IDENTITY(d.user) = :userId) "
                    . " and IDENTITY(w.user) = :userId ";
            $params['userId'] = $user->getId();
        } else {
            $where = " where w.id = :id "
                    . " and IDENTITY(w.user) is null ";
            $params['id'] = $id;
        }

        $query = $this->getEntityManager()->createQuery(
            " select w
                            from E:Comparison w
                            join w.items wi
                            join wi.item i
                           $where "
        )->setParameters($params);
        return $query->getOneOrNullResult();
    }
}
