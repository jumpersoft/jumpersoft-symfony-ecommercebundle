<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Jumpersoft\BaseBundle\Repository\BaseEntityRepository;

/**
 * An entiry can extend directly from EntityRepository, but
 * in this way we can extend other util functions.
 *
 * @author Angel
 */
class JumpersoftEntityRepository extends BaseEntityRepository
{

    /**
     * getImagesColumnsUrls
     */
    public function getImagesColumnsUrls($tc_urlFiles, $tc_fileName, $c_image, $t_image, $sizes = "all")
    {
        $c = "";
        if ($sizes == "all" || $sizes == "normal") {
            $c = " (CASE WHEN $tc_fileName <> '' THEN " . $this->concat([$tc_urlFiles, "'/'", $tc_fileName]) . " ELSE '' END) as $c_image ";
        }
        if ($sizes == "all" || $sizes == "thumb") {
            $c .= ($c ? ", " : "") . " (CASE WHEN $t_image.thumbSize > 0 THEN " . $this->concat([$tc_urlFiles, "'/'", "$t_image.id", "'_'", "$t_image.thumbSize", "'.'", "$t_image.ext"]) . " ELSE '' END) as " . $c_image . "ThumbSize ";
        }
        if ($sizes == "all" || $sizes == "lSize") {
            $c .= ($c ? ", " : "") . " (CASE WHEN $t_image.lSize > 0 THEN " . $this->concat([$tc_urlFiles, "'/'", "$t_image.id", "'_'", "$t_image.lSize", "'.'", "$t_image.ext"]) . " ELSE '' END) as " . $c_image . "LSize ";
        }
        if ($sizes == "all" || $sizes == "mSize") {
            $c .= ($c ? ", " : "") . " (CASE WHEN $t_image.mSize > 0 THEN " . $this->concat([$tc_urlFiles, "'/'", "$t_image.id", "'_'", "$t_image.mSize", "'.'", "$t_image.ext"]) . " ELSE '' END) as " . $c_image . "MSize ";
        }
        if ($sizes == "all" || $sizes == "sSize") {
            $c .= ($c ? ", " : "") . " (CASE WHEN $t_image.sSize > 0 THEN " . $this->concat([$tc_urlFiles, "'/'", "$t_image.id", "'_'", "$t_image.sSize", "'.'", "$t_image.ext"]) . " ELSE '' END) as " . $c_image . "SSize ";
        }
        return $c;
    }

    public function makeTree(&$data, $parentKey, $idKey)
    {
        $itemsByReference = array();

        // Build array of item references:
        foreach ($data as $key => &$item) {
            $itemsByReference[$item[$idKey]] = &$item;
            // Children array:
            $itemsByReference[$item[$idKey]]['children'] = array();
        }

        // Set items as children of the relevant parent item.
        foreach ($data as $key => &$item) {
            if ($item[$parentKey] && isset($itemsByReference[$item[$parentKey]]) && $item[$parentKey] <> $item[$idKey]) {
                $itemsByReference[$item[$parentKey]]['children'][] = &$item;
            }
        }

        // Remove items that were added to parents elsewhere:
        foreach ($data as $key => &$item) {
            if ($item[$parentKey] && isset($itemsByReference[$item[$parentKey]]) && $item[$parentKey] <> $item[$idKey]) {
                unset($data[$key]);
            }
        }

        //Reindex array to start with 0, this is important because fancytree has problems with index that start with numbers greater than zero, and this happen since the original query can apply an order by sequence in items.
        $data = array_values($data);
        //Make Breadcrumbs
        return $this->makeBreadcrumbs($data);
    }

    public function makeBreadcrumbs($data, $parent = null)
    {
        foreach ($data as $key => &$item) {
            if (!empty($item["parentId"]) && $parent) {
                $item["slugPath"] = $parent["slugPath"] . "/" . $item["urlKey"];
                $item["slug"] = $parent["slug"];
                $item["slug"][] = ["id" => $item["id"], "urlKey" => $item["urlKey"]];
            } else {
                $item["slugPath"] = "/" . $item["urlKey"];
                $item["slug"][] = ["id" => $item["id"], "urlKey" => $item["urlKey"]];
            }
            if (count($item["children"]) > 0) {
                $this->makeBreadcrumbs($item["children"], $item);
            }
        }
        return $data;
    }
}
