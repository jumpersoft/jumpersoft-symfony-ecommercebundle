<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class TagRepository extends JumpersoftEntityRepository
{

    /**
     * getTags, incluye paginación
     */
    public function getTags(&$filters, $q, $userId, $storeId, $for = "grid")
    {
        if ($for == "select2") {
            $select = "select t.id, t.name as text ";
        } else {
            $select = "select t.id, t.name, t.urlKey, t.description ";
        }
        $query = $select . " from E:Tag t
                join t.store s                
                join s.users u
                where u.id = :userId and s.id = :storeId and t.name like :q ";

        $params = ["userId" => $userId, "storeId" => $storeId, "q" => "%" . $q . "%"];
        $this->setConditionalFilters($query, $params, $filters, ["t.name" => "name", "t.urlKey" => "urlKey", "t.description" => "description"]);
        $query .= $this->setOrderFilters($filters, ["name" => "t.name", "urlKey" => "t.urlKey", "description" => "t.description"]);
        return $this->getResultPaginated($query, $params, $filters);
    }

    /**
     * getTag
     */
    public function getTag($id, $userId, $storeId, $for = "view")
    {
        if ($for == "view") {
            $select = "select t.id, t.name, t.urlKey, t.description ";
        } else {
            //For update or for delete
            $select = $for == "update" ? "select t " : "select partial t.{id} ";
        }

        $query = $this->getEntityManager()->createQuery($select .
                        " from E:Tag t
                          join t.store s                          
                          join s.users u                         
                          where t.id = :id 
                            and u.id = :userId 
                            and s.id = :storeId")->setParameters(["id" => $id, "userId" => $userId, "storeId" => $storeId]);
        return $query->getSingleResult();
    }

    /**
     * Check Tag
     */
    public function checkTag($name, $id, $storeId)
    {
        $query = $this->getEntityManager()->createQuery(
            "select count(p)
			from E:Tag p
                        join p.store s
                        where p.name = :name
                          and p.id <> :id
                          and s.id = :storeId
			ORDER BY p.id DESC"
        )->setParameters(["name" => $name, "id" => $id, "storeId" => $storeId]);

        return $query->getSingleScalarResult() > 0 ? true : false;
    }

    /**
     * checkUrlKeyAction
     */
    public function checkUrlKeyAction($urlKey, $id, $storeId)
    {
        return $this->getEntityManager()->createQuery("select count(t.id) "
                        . " from E:Tag t "
                        . " join t.store s "
                        . "where t.urlKey = :urlKey "
                        . "  and t.id <> :id "
                        . "  and s.id = :storeId ")->setParameters(['id' => $id, 'urlKey' => $urlKey, 'storeId' => $storeId])->getSingleScalarResult() > 0 ? true : false;
    }

    /**
     * getTagsToComboHTML
     */
    public function getTagsToComboHTML($userId)
    {
        $query = $this->getEntityManager()->createQuery(
            "select t.id as value, CONCAT(CONCAT(t.name, ' - '), t.urlKey) as name
			 from E:Tag t                        
                         join t.store s                         
                         join s.users u
                         where u.id = :id
			 ORDER BY t.id DESC"
        )->setParameter("id", $userId);
        return $query->getResult();
    }
}
