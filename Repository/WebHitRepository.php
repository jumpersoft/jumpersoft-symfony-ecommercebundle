<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

class WebHitRepository extends JumpersoftEntityRepository
{

    // Verificamos que no haya un registro ip hasta hace dos meses en el mismo año
    public function getByIp($ipEncoded)
    {
        return $this->getOneOrNullResult("select wh.id 
                                    from E:WebHit wh                               
                                   where wh.ipEncoded = :ip                                 
                                     and year(now()) = year(wh.registerDate) 
                                     and month(wh.registerDate) >= month(now()) - 2 ", ["ip" => $ipEncoded]);
    }

    // Verificamos que no haya un registro de conteo con el mismo ip y producto hasta dos meses en el mismo año
    public function getItemByIp($ipEncoded, $itemId)
    {
        return $this->getOneOrNullResult("select whi.id 
                                    from E:WebHitItem whi
                                    join whi.webHit wb
                                   where wb.ipEncoded = :ip
                                     and identity(whi.item) = :itemId
                                     and year(now()) = year(whi.registerDate) 
                                     and month(whi.registerDate) >= month(now()) - 2 ", ["ip" => $ipEncoded, "itemId" => $itemId]);
    }
}
