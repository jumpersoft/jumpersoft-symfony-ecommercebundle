<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class ItemRepository extends JumpersoftEntityRepository
{

    /**
     * Notes:
     *  stock: only work with typeId <> 'ITE_CON', the query can get simple stock or detailed inventory, in the client(vue)m the grouping record must show the sum of the children's stock
     *  hasBulkPrice: any type of item
     *  hasListPrices: any type of item
     */
    public function getItems(&$filters, $userId, $storeId, $q, $for = "view", $ids = null)
    {
        if ($for == "select2") {
            $select = "select distinct p.id, p.name as text ";
            $where .= " and p.name like :q ";
            $params['q'] = '%' . $q . '%';
        } else {
            $select = " select distinct p.id,
                        p.name, 
                        t.name type,
                        t.id typeId,
                        p.sku, 
                        identity(p.trackingType) trackingTypeId,
                        IFELSE(t.id = 'ITE_CON', 0, IFELSE(identity(p.trackingType) = 'INV_TYP_SIM', p.stock, IFELSE(identity(p.trackingType) = 'INV_TYP_NAN', 0, (SELECT SUM(inv.stock) FROM E:Inventory inv where identity(inv.item) = p.id and identity(inv.operation) = 'INV_OPE_IN')))) as stock,
                        IFELSE(t.id = 'ITE_CON', '', um.name) unitMeasure,
                        um.fractionable,
                        p.decimals,                        
                        p.regularPrice,
                        p.discount,
                        p.discountType,
                        p.salePrice,
                        c.id currency,
                        p.handleGlobalPrice,
                        IFELSE(JSON_LENGTH(p.bulk) > 0, 1, 0) hasBulkPrices,
                        IFELSE(EXISTS(SELECT li.id FROM E:PriceListItem li WHERE identity(li.item) = p.id), 1, 0) hasListPrices,
                        p.registerDate, 
                        p.updateDate,
                        es.id as statusId,
                        es.name as status,
                        'cat' as categormastery,
                        s.tradeName as store,
                        first(select concat(p.urlFiles, '/', dimg.thumbnail) from E:Document dimg where identity(dimg.item) = p.id and (identity(dimg.type) = 'IMG_ITEM_PRIMARY' or dimg.type is null)) thumbnail,
                        first(select concat(imga.urlFiles, '/', dimga.thumbnail) from E:Document dimga  join dimga.itemAttributeOption imgao join imgao.itemAttribute imga
                               where imga = p.itemAttribute and (identity(dimga.type) = 'IMG_ITEM_PRIMARY' or dimga.type is null)) thumbnailAlt,
                        (select concat('[',group_concat(distinct json_object('id', iv.id, 'sequence', iv.sequence, 'name', iv.name, 'sku', iv.sku, 'unitMeasure', vum.name,
                                                                             'regularPrice', IFELSE(p.handleGlobalPrice = 1 OR iv.workWithPricesParent = 1, '', iv.regularPrice),
                                                                             'discount', IFELSE(p.handleGlobalPrice = 1 OR iv.workWithPricesParent = 1, '', iv.discount),
                                                                             'discountType', IFELSE(p.handleGlobalPrice = 1 OR iv.workWithPricesParent = 1, '', iv.discountType),
                                                                             'salePrice', IFELSE(p.handleGlobalPrice = 1 OR iv.workWithPricesParent = 1, '', iv.salePrice),
                                                                             'currency', IFELSE(p.handleGlobalPrice = 1 OR iv.workWithPricesParent = 1, '', ivc.id),
                                                                             'workWithPricesParent', iv.workWithPricesParent,
                                                                             'parentHandleGlobalPrice', p.handleGlobalPrice,
                                                                             'type', tiv.name,
                                                                             'typeId', tiv.id,
                                                                             'hasBulkPrices', IFELSE(tiv.id = 'ITE_CON', IFELSE(iv.handleGlobalPrice = 1 AND JSON_LENGTH(iv.bulk) > 0, 1, 0), IFELSE(JSON_LENGTH(iv.bulk) > 0, 1, 0)),
                                                                             'hasListPrices', IFELSE(EXISTS(SELECT liv.id FROM E:PriceListItem liv WHERE identity(liv.item) = iv.id), 1, 0),
                                                                             'trackingTypeId', identity(iv.trackingType), 'variant', iv.variant, 'variantsGroup', iv.variantsGroup, 'registerDate', iv.registerDate, 'updateDate', iv.updateDate,
                                                                             'statusId', ivs.id, 'status', ivs.name,
                                                                             'stock', IFELSE(identity(iv.trackingType) = 'INV_TYP_SIM', iv.stock, IFELSE(identity(iv.trackingType) = 'INV_TYP_NAN', 0, (SELECT SUM(ivi.stock) FROM E:Inventory ivi where identity(ivi.item) = iv.id and identity(ivi.operation) = 'INV_OPE_IN'))),
                                                                             'thumbnail', first(select concat(iv.urlFiles, '/', vimg.thumbnail) from E:Document vimg where identity(vimg.item) = iv.id and (identity(vimg.type) = 'IMG_ITEM_PRIMARY' or vimg.type is null)),
                                                                             'thumbnailAlt', first(select concat(vimga.urlFiles, '/', dv.thumbnail) 
                                                                                                             from E:Document dv 
                                                                                                             join dv.itemAttributeOption vimgao 
                                                                                                             join vimgao.itemAttribute vimga
                                                                                                            where vimga = p.itemAttribute 
                                                                                                              and (identity(dv.type) = 'IMG_ITEM_PRIMARY' or dv.type is null)
                                                                                                              and JSON_SEARCH(iv.variantsGroup, 'one', vimgao.id) is not null)
                                                                             ) order by iv.sequence),']')
                                from E:Item iv
                                join iv.type tiv
                                join iv.status ivs
                           left join iv.unitMeasure vum                                 
                           left join iv.currency ivc                           
                               where IDENTITY(iv.parent) = p.id) as variants  ";
        }

        $from = "from E:Item p
                         left join p.categories cat
                         join p.type t
                    left join p.currency c
                         join p.unitMeasure um
                         join p.status es                         
                         join p.store s                    
                         join s.users u ";
        $where = " where u.id = :userId
                     and s.id = :storeId
                     and identity(p.parent) is null ";
        $params = ['userId' => $userId, 'storeId' => $storeId];

        if ($ids == null) {
            $this->setConditionalFilters($where, $params, $filters, ["p.name" => "name", "p.sku" => "sku", "p.regularPrice" => "regularPrice", "cat.name" => "categoryName",
                "p.discount" => "discount", "p.salePrice" => "salePrice", "p.stock" => "stock", "um.name" => "unitMeasure", "t.id" => "type", "es.id" => "status"]);
            $where .= $this->setOrderFilters($filters, ["name" => "p.name", "sku" => "p.sku", "regularPrice" => "p.regularPrice", "status" => "es.name", "categoryName" => "cat.name",
                "discount" => "p.discount", "salePrice" => "p.salePrice", "stock" => "p.stock", "unitMeasure" => "um.name", "type" => "t.name", "registerDate" => "p.registerDate", "updateDate" => "p.updateDate"]);
        } else {
            $where .= " and p.id in (:ids) ";
            $params["ids"] = $ids;
        }

        $data = $for == "report" ? $this->getResult($select . $from . $where, $params) : $this->getResultPaginated($select . $from . $where, $params, $filters);
        return $this->jsonDecode($data, ["variants"], true, false);
    }

    public function getItem($id, $userId, $storeId, $for = "view")
    {
        if ($for == "view") {
            $select = "select i.id, 
                              i.name, 
                              t.name as type, 
                              t.id as typeId,
                              b.name brand,
                              b.id brandId,
                              i.sku, i.urlKey, 
                              i.description, 
                              i.shortDescription, 
                              i.specs, 
                              i.comment,                               
                              i.registerDate, 
                              i.updateDate, 
                              i.handleGlobalPrice,
                              i.workWithPricesParent,
                              i.regularPrice, 
                              i.discount, 
                              i.discountType,
                              DATE_FORMAT(i.discountUntil, '%Y-%m-%d %H:%i:%s') discountUntil,
                              i.salePrice,
                              c.id currency,
                              i.bulk,                              
                              i.decimals,
                              i.bulkSale,                              
                              TRIM(i.quantityByUnitBulkSale) + 0 quantityByUnitBulkSale,
                              umbs.id unitMeasureBulkSaleId,
                              umbs.name unitMeasureBulkSale,
                              i.decimalsBulkSale,
                              i.serialized,
                              i.perishable,                              
                              i.autoFulFill,
                              i.useContainer,
                              i.dangerous,
                              i.radioactive,   
                              i.useVolumeToFulFill,
                              iso.id isotopeId,
                              iso.name isotope,                              
                              IDENTITY(i.trackingType) trackingTypeId,
                              TRIM(i.stock) + 0 stock, 
                              TRIM(i.minQtyAllowedInCart) + 0 minQtyAllowedInCart,
                              TRIM(i.maxQtyAllowedInCart) + 0 maxQtyAllowedInCart, 
                              TRIM(i.notifyForQtyBelow) + 0 notifyForQtyBelow, 
                              TRIM(i.qtyToBecomeOutOfStock) + 0 qtyToBecomeOutOfStock, 
                              i.weight,
                              um.id unitMeasureId, 
                              um.fractionable,
                              s.id statusId,
                              first(select concat(i.urlFiles, '/', img.thumbnail) from E:Document img where identity(img.item) = i.id and (identity(img.type) = 'IMG_ITEM_PRIMARY' or img.type is null)) thumbnail,
                              first(select concat(imga.urlFiles, '/', dimga.thumbnail) from E:Document dimga  join dimga.itemAttributeOption imgao join imgao.itemAttribute imga
                                     where imga = i.itemAttribute and (identity(dimga.type) = 'IMG_ITEM_PRIMARY' or dimga.type is null)) thumbnailAlt,
                              i.metaTitle, 
                              i.metaKeywords, 
                              i.metaDescription, 
                              i.socialLinks,
                              (select concat('[',group_concat(concat('\"',icatc.id,'\"')),']')  
                                 from E:Item ic 
                                 join ic.categories icat 
                                 join icat.category icatc
                                where ic.id = i.id and identity(icatc.type) = 'ITM') as categories,
                              (select concat('[',group_concat(concat('\"',ifcatc.id,'\"')),']')  
                                 from E:Item ifc 
                                 join ifc.categories ifcat 
                                 join ifcat.category ifcatc
                                where ifc.id = i.id and identity(ifcatc.type) = 'ITM_FIL') as categoriesFilter,                              
                              identity(i.category) categoryId,
                              i.handleAttributes,
                              (select concat('[',group_concat(JSON_OBJECT('id', a.id, 'required', a.required, 'type', at.name, 'typeId', at.id, 'name', a.name, 'description', a.description, 'code', a.code, 'defaultText', a.defaultText, 'defaultValue', a.defaultValue,
                                                                    'defaultOption', a.defaultOption, 'defaultValueDecimal', a.defaultValueDecimal, 'internalUse', a.internalUse, 'max', a.max, 'maxDecimal', a.maxDecimal, 'min', a.min, 'minDecimal', a.minDecimal, 
                                                                    'useMinCurrentDate', a.useMinCurrentDate, 'useRange', a.useRange, 'itemAttributeId', ia.id, 'isVariant', ia.isVariant, 'sequence', ia.sequence, 'defaultOption', ia.defaultOption,
                                                                    'options', (select concat('[',group_concat(json_object('id', iao.id, 'attributeOptionId', ao.id, 'name', ao.name, 'active', IFELSE(iao.id is not null,1,0), 'value', ao.value, 'sequence', iao.sequence ) 
                                                                                                  order by iao.sequence asc),']') 
                                                                                  from E:AttributeOption ao
                                                                                  join ao.itemAttributeOptions iao 
                                                                                 where ao.attribute = a
                                                                                   and ( iao.itemAttribute = ia OR iao.id is null))
                                                              ) order by ia.sequence asc
                                              ),']')
                                from E:Attribute a
                                join a.type at
                                join a.item ia
                               where identity(ia.item) = i.id ) as attributes,
                              identity(i.itemAttribute) itemAttributeId,
                              (select json_object('id', ip.id, 'handleGlobalPrice', ip.handleGlobalPrice, 'perishable', ip.perishable, 'dangerous', ip.dangerous) 
                                 from E:Item ip where ip.id = IDENTITY(i.parent)) as parent,
                              (select concat('[',group_concat(distinct json_object('id', iv.id, 'sequence', iv.sequence, 'name', iv.name, 'sku', iv.sku, 'unitMeasure', vum.name, 'regularPrice', iv.regularPrice, 'discount', iv.discount, 'discountType', iv.discountType, 
                                                                                   'salePrice', iv.salePrice, 'variant', iv.variant, 'variantsGroup', iv.variantsGroup, 'stock', iv.stock) order by iv.sequence),']')
                                from E:Item iv 
                                join iv.unitMeasure vum                                 
                               where IDENTITY(iv.parent) = i.id) as variants,
                              (select concat('[',group_concat(distinct json_object('id', ico.id, 'name', ico.name, 'sku', ico.sku, 'required', co.required, 'userDefined', coi.userDefined, 'defaultQuantity', coi.defaultQuantity ) order by co.sequence),']')
                                from E:ItemCompositeOption co
                                join co.items coi
                                join coi.item ico
                               where IDENTITY(co.item) = i.id) as compositeItems
                              ";
        } else {
            //For Update or for Delete
            $select = $for == "update" ? "select i " : "select partial i.{id} ";
        }

        $query = $this->getEntityManager()->createQuery($select .
                        " from E:Item i        
                          join i.currency c
                          join i.type t                     
                          join i.unitMeasure um
                          join i.status s
                          join i.store st                          
                          join st.users u                     
                     left join i.brand b
                     left join i.isotope iso                     
                     left join i.unitMeasureBulkSale umbs
                          where i.id = :id 
                            and u.id = :userId 
                            and st.id = :storeId")->setParameters(["id" => $id, "userId" => $userId, "storeId" => $storeId]);

        $row = $query->getSingleResult();
        return $for == "view" ? $this->jsonDecode($row, ["categories", "categoriesFilter", "variants->variantsGroup", "parent", "compositeItems", "attributes->options"], true, true) : $row;
    }

    public function checkName($name, $id, $storeId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select count(p)
			 from E:Item p
                         join p.store s
                         where p.name = :name
                          and p.id <> :id
                          and s.id = :storeId
			 ORDER BY p.id DESC'
        )->setParameters(["id" => $id, "name" => $name, "storeId" => $storeId]);

        return $query->getSingleScalarResult() > 0 ? true : false;
    }

    public function checkUrlKey($urlKey, $id, $storeId)
    {
        return $this->getEntityManager()->createQuery("select count(c.id) "
                        . " from E:Item c "
                        . " join c.store s "
                        . "where c.urlKey = :urlKey "
                        . "  and c.id <> :id "
                        . "  and s.id = :storeId ")->setParameters(['id' => $id, 'urlKey' => $urlKey, 'storeId' => $storeId])->getSingleScalarResult() > 0 ? true : false;
    }

    public function getItemCategories(&$filters, $userId, $storeId, $itemId)
    {
        $select = "select ic.id, ic.name, ct.name as categoryType
			 from E:Category ic
                         join ic.type ct
                         join ic.items i                         
                         join i.store s                         
                         join s.users u ";
        $where = " where u.id = :userId and s.id = :storeId and i.id = :itemId ";
        $params = ['userId' => $userId, 'storeId' => $storeId, 'itemId' => $itemId];

        //Filter query build
//        $this->setConditionalFilters($where, $params, $filters, ["p.name" => "name", "p.sku" => "sku", "es.id" => "statusId"]);
//        $this->setNumberRange($where, $params, "p.regularPrice", $filters['filters'], ["regularPriceA", "regularPriceB"]);

        $order = " order by ic.id";

        return $this->getResultPaginated($select . $where . $order, $params, $filters);
    }

    public function getItemCategoryAvailable(&$filters, $itemId, $userId, $storeId, $q)
    {
        $select = "select t.id, t.name as text ";
        $where = " where u.id = :userId
                     and s.id = :storeId 
                     and t.name like :q 
                     and t.id not in (select cc.id 
                                        from E:Item itm
                                        join itm.categories cc
                                        where itm.id = :itemId)";
        $params = ["itemId" => $itemId, "userId" => $userId, "storeId" => $storeId, "q" => "%" . $q . "%"];
        $select = $select .
                "from E:Category t
                     join t.type ct
                     join t.store s                     
                     join s.users u "
                . $where .
                "ORDER BY t.id DESC";
        return $this->getResultPaginated($select, $params, $filters);
    }

    public function getPrecio($id)
    {
        $query = $this->getEntityManager()->createQuery(
            "select p.regularPrice
			 from E:Item p                         
                         where p.id = :id"
        )->setParameter('id', $id);
        return $query->getSingleScalarResult();
    }

    public function getTags(&$filters, $userId, $storeId, $itemId)
    {
        $select = "select t.id, t.name, t.urlKey
			 from E:Tag t                         
                         join t.items i
                         join i.store s                         
                         join s.users u ";
        $where = " where u.id = :userId and s.id = :storeId and i.id = :itemId ";
        $params = ['userId' => $userId, 'storeId' => $storeId, 'itemId' => $itemId];

        $this->setConditionalFilters($where, $params, $filters, ["t.name" => "name", "t.urlKey" => "urlKey"]);
        $order = $this->setOrderFilters($filters, ["name" => "t.name", "urlKey" => "t.urlKey"]);

        return $this->getResultPaginated($select . $where . $order, $params, $filters);
    }

    public function getItemTagAvailable(&$filters, $itemId, $userId, $storeId, $q)
    {
        $select = "select t.id, t.id as value, t.name, t.name as text, t.urlKey ";
        $where = "  where u.id = :userId
                      and s.id = :storeId 
                      and t.name like :q 
                      and t.id not in (select cc.id 
                                         from E:Item itm
                                         join itm.tags cc
                                        where itm.id = :itemId)";
        $params = ["itemId" => $itemId, "userId" => $userId, "storeId" => $storeId, "q" => "%" . $q . "%"];
        $query = $select .
                "from E:Tag t                         
                join t.store s                
                join s.users u "
                . $where .
                "ORDER BY t.id DESC";
        return $this->getResultPaginated($query, $params, $filters);
    }

    public function getCrossSells(&$filters, $userId, $storeId, $itemId)
    {
        $select = "select ics.id,
                          ics.proximity, 
                          ir.name,
                          first(select concat(ir.urlFiles, '/', d.thumbnail) from E:Document d where identity(d.item) = ir.id) as thumbnail,
                          ir.sku,
                          ir.regularPrice, 
                          ir.discount, 
                          ir.discountType, 
                          ir.salePrice
                     from E:ItemCrossSell ics
                     join ics.itemRelated ir                     
                     join ics.item i
                     join i.store s                     
                     join s.users u ";
        $where = " where u.id = :userId
                     and s.id = :storeId 
                     and i.id = :itemId ";
        $params = ['userId' => $userId, 'storeId' => $storeId, 'itemId' => $itemId];

        $this->setConditionalFilters($where, $params, $filters, ["ir.name" => "name", "ics.proximity" => "proximity", "ir.sku" => "sku"]);
        $order = $this->setOrderFilters($filters, ["name" => "ir.name", "proximity" => "ics.proximity", "sku" => "ir.sku"]);

        return $this->getResultPaginated($select . $where . $order, $params, $filters);
    }

    public function getCrossSell($id, $itemId, $userId, $storeId, $for = "view")
    {
        if ($for == "view") {
            $select = " select ics.id, ics.proximity, ir.id as itemRelatedId, ir.name ";
        } else {
            //For Update or for Delete
            $select = $for == "update" ? "select ics " : "select partial ics.{id} ";
        }

        $params = ["id" => $id, "userId" => $userId, "storeId" => $storeId, "itemId" => $itemId];

        $query = $this->getEntityManager()->createQuery($select .
                        " from E:ItemCrossSell ics
                          join ics.itemRelated ir
                          join ics.item i
                          join i.store s                          
                          join s.users u
                          where ics.id = :id 
                            and u.id = :userId 
                            and s.id = :storeId 
                            and i.id = :itemId")->setParameters($params);
        return $query->getSingleResult();
    }

    public function getItemsCrossSellAvailable(&$filters, $itemId, $userId, $storeId, $q)
    {
        $select = "select p.id,
                          p.id as value,
                          p.name, 
                          p.name as text,
                          first(select concat(p.urlFiles, '/', d.thumbnail) from E:Document d where identity(d.item) = p.id and (identity(d.type) = 'IMG_ITEM_PRIMARY' or d.type is null)) thumbnail,
                          p.sku,
                          p.regularPrice, 
                          p.discount, 
                          p.discountType, 
                          p.salePrice ";
        $from = "from E:Item p
                    left join p.unitMeasure um
                         join p.status es                         
                         join p.store s                         
                         join s.users u ";
        $where = " where identity(p.type) <> 'ITE_CON'
                     and identity(p.status) = 'ITE_AVA'
                     and u.id = :userId
                     and s.id = :storeId 
                     and p.name like :q 
                     and p.id not in (select itmr.id 
                                        from E:ItemCrossSell pp 
                                        join pp.item itm
                                        join pp.itemRelated itmr
                                        where itm.id = :itemId)
                     and p.id <> :itemId ";
        $order = " order by p.id DESC ";
        $params = ['itemId' => $itemId, 'userId' => $userId, 'storeId' => $storeId, 'q' => '%' . $q . '%'];
        return $this->getResultPaginated($select . $from . $where . $order, $params, $filters);
    }

    public function getUpSells(&$filters, $userId, $storeId, $itemId)
    {
        $select = "select ics.id,
                          ics.proximity, 
                          ir.name,
                          first(select concat(ir.urlFiles, '/', d.thumbnail) from E:Document d where identity(d.item) = ir.id and (identity(d.type) = 'IMG_ITEM_PRIMARY' or d.type is null)) thumbnail,
                          ir.sku,
                          ir.regularPrice, 
                          ir.discount, 
                          ir.discountType, 
                          ir.salePrice
                    from E:ItemUpSell ics
                    join ics.itemRelated ir
                    join ics.item i
                    join i.store s                    
                    join s.users u ";
        $where = " where u.id = :userId and s.id = :storeId and i.id = :itemId ";
        $params = ['userId' => $userId, 'storeId' => $storeId, 'itemId' => $itemId];

        $this->setConditionalFilters($where, $params, $filters, ["ir.name" => "name", "ics.proximity" => "proximity", "ir.sku" => "sku"]);
        $order = $this->setOrderFilters($filters, ["name" => "ir.name", "proximity" => "ics.proximity", "sku" => "ir.sku"]);

        return $this->getResultPaginated($select . $where . $order, $params, $filters);
    }

    public function getUpSell($id, $itemId, $userId, $storeId, $for = "view")
    {
        if ($for == "view") {
            $select = " select ics.id, ics.proximity, ir.id as itemRelatedId, ir.name ";
        } else {
            //For Update or for Delete
            $select = $for == "update" ? "select ics " : "select partial ics.{id} ";
        }

        $params = ["id" => $id, "userId" => $userId, "storeId" => $storeId, "itemId" => $itemId];

        $query = $this->getEntityManager()->createQuery($select .
                        " from E:ItemUpSell ics
                          join ics.itemRelated ir
                          join ics.item i
                          join i.store s                          
                          join s.users u
                          where ics.id = :id 
                            and u.id = :userId 
                            and s.id = :storeId 
                            and i.id = :itemId")->setParameters($params);
        return $query->getSingleResult();
    }

    public function getItemsUpSellAvailable(&$filters, $itemId, $userId, $storeId, $q)
    {
        $select = "select p.id, 
                          p.id as value,
                          p.name,
                          p.name as text,
                          p.sku,
                          p.regularPrice, 
                          p.discount, 
                          p.discountType, 
                          p.salePrice,
                          first(select concat(p.urlFiles, '/', d.thumbnail) from E:Document d where identity(d.item) = p.id and (identity(d.type) = 'IMG_ITEM_PRIMARY' or d.type is null)) thumbnail ";
        $from = "from E:Item p
                    left join p.unitMeasure um
                         join p.status es                         
                         join p.store s                         
                         join s.users u ";
        $where = " where identity(p.type) <> 'ITE_CON'
                     and identity(p.status) = 'ITE_AVA'
                     and u.id = :userId
                     and s.id = :storeId 
                     and p.name like :q 
                     and p.id not in (select itmr.id 
                                        from E:ItemUpSell pp 
                                        join pp.item itm
                                        join pp.itemRelated itmr
                                        where itm.id = :itemId )
                     and p.id <> :itemId ";
        $order = " order by p.id DESC ";
        $params = ['itemId' => $itemId, 'userId' => $userId, 'storeId' => $storeId, 'q' => '%' . $q . '%'];
        return $this->getResultPaginated($select . $from . $where . $order, $params, $filters);
    }

    public function getAttributes($id, $userId, $storeId)
    {
        $sql = "select i.attributes 
                  from E:Item i
                  join i.store s                
                  join s.users u
                 where u.id = :userId 
                   and s.id = :storeId
                   and i.id = :id ";

        $params = ["userId" => $userId, "storeId" => $storeId, "id" => $id];
        $res = $this->getOneOrNullResult($sql, $params);
        return $res["attributes"] ? $res["attributes"] : [];
    }

    public function checkAttributeName($name, $itemId, $storeId)
    {
        $query = $this->getEntityManager()->createQuery(
            "select count(i)
			from E:Item i
                        join i.store s
                        where JSON_EXTRACT(i.attributes, '$.name') = :name
                          and i.id <> :itemId
                          and s.id = :storeId
			ORDER BY i.id DESC"
        )->setParameters(["name" => $name, "itemId" => $itemId, "storeId" => $storeId]);

        return $query->getSingleScalarResult() > 0 ? true : false;
    }

    /* TODO TODAVIA NO SE USA ?? NO TIENE CONTROLADOR*/
    public function getDispersions($userId, $storeId, $itemId)
    {
        $select = "select ics.id,
                          TRIM(ics.quantityToDisperse) + 0 quantityToDisperse,
                          ir.id itemRelatedId,
                          ir.name,
                          i.radioactive,
                          um.id unitMeasureId,
                          umb.id unitMeasureBulkSaleId,
                          um.name unitMeasure,
                          umb.name unitMeasureBulkSale,
                          (CASE WHEN imd.thumbSize > 0 THEN " . $this->concat(["ir.urlFiles", "'/'", "imd.id", "'_'", "imd.thumbSize", "'.'", "imd.ext"]) . " ELSE '' END) as thumbnail                          
                     from E:ItemDispersion ics
                     join ics.itemRelated ir
                     join ir.unitMeasure um
                left join ir.unitMeasureBulkSale umb
                     left join E:ItemImage imd with ir.defaultImage = imd
                     join ics.item i                     
                     join i.store s                     
                     join s.users u ";
        $where = " where u.id = :userId
                     and s.id = :storeId 
                     and i.id = :itemId ";
        $params = ['userId' => $userId, 'storeId' => $storeId, 'itemId' => $itemId];

        return $this->getResult($select . $where, $params);
    }

    public function getItemsDispersionAvailable(&$filters, $item, $userId, $storeId, $q)
    {
        $thumbUrl = $this->getImagesColumnsUrls("i.urlFiles", "im.fileName", "image", "im", "thumbnail");
        $select = "select i.id,
                          i.id as value,
                          i.name, 
                          i.name as text,
                          $thumbUrl,
                          i.sku,
                          i.regularPrice, 
                          i.discount, 
                          i.discountType, 
                          i.salePrice,
                          i.radioactive,
                          um.id unitMeasureId,
                          umb.id unitMeasureBulkSaleId,
                          um.name unitMeasure,
                          umb.name unitMeasureBulkSale,
                          (CASE WHEN umb.id = :unitMeasureBulkSaleId THEN TRIM(i.quantityByUnitBulkSale)+0 ELSE 1 END) quantityByUnitBulkSale ";
        $from = "from E:Item i
                    left join i.defaultImage im
                         join i.unitMeasure um
                    left join i.unitMeasureBulkSale umb
                         join i.status es                         
                         join i.store s                         
                         join s.users u ";
        $where = " where identity(i.type) = 'ITE_SIM'
                     and identity(i.status) = 'ITE_AVA'
                     and identity(i.trackingType) = 'INV_TYP_DET'
                     and (umb.id = :unitMeasureBulkSaleId OR um.id = :unitMeasureBulkSaleId)
                     and (CASE WHEN umb.id = :unitMeasureBulkSaleId THEN i.quantityByUnitBulkSale ELSE 1 END) <= :quantityByUnitBulkSale
                     and u.id = :userId
                     and s.id = :storeId 
                     and i.name like :q 
                     and i.id not in (select itmr.id 
                                        from E:ItemDispersion pi
                                        join pi.item itm
                                        join pi.itemRelated itmr
                                        where itm.id = :itemId)
                     and i.id <> :itemId ";
        $order = " order by i.name DESC ";
        $params = ['itemId' => $item->getId(), 'unitMeasureBulkSaleId' => $item->getUnitMeasureBulkSale()->getId(), 'quantityByUnitBulkSale' => $item->getQuantityByUnitBulkSale(), 'userId' => $userId, 'storeId' => $storeId, 'q' => '%' . $q . '%'];
        return $this->getResultPaginated($select . $from . $where . $order, $params, $filters);
    }
}
