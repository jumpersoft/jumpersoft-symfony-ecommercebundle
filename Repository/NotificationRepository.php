<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class NotificationRepository extends JumpersoftEntityRepository
{

    /**
     * getNotifications, incluye paginación
     */
    public function getNotifications(&$filters, $userId)
    {
        $name = $this->concat(array("p.name", "' '", "p.lastName", "' '", "p.mothersLastName", " '('", "p.username","')'"));
        $select = "select t.id, t.subject, t.fromEmail, " . $name . " as to, p.id as toId, t.sendDate, nt.id typeId, nt.name as type, ps.name orderRecordItem, ps.id orderRecordItemId
                    from E:Notification t                    
                    join t.to p
                    join t.type nt
               left join t.orderRecordItem upps
               left join upps.item ps
               left join t.user u
                    where u.id = :userId or p.id = :userId
                    ORDER BY t.id DESC";
        return $this->getResultPaginated($select, ["userId" => $userId], $filters);
    }

    /**
     * getNotification
     */
    public function getNotification($id, $userId, $isVendor, $for = "view")
    {
        if ($for == "view") {
            $select = "select t.id, t.subject, t.fromEmail, t.message, t.sendDate, nt.id as typeId, ps.id as orderRecordItemId , ps.name as orderRecordItem ";
        } else {
            //for Update or Delete
            $select = $for == "update" ? "select t " : " select partial t.{id} ";
        }

        if ($isVendor) {
            $and = " and u.id = :userId ";
        } else {
            $and = " and p.id = :userId ";
        }

        return $this->getEntityManager()->createQuery("$select 
                        from E:Notification t
                        join t.user u
                        join t.type nt
                        join t.to p
                        left join t.orderRecordItem upps
                        left join upps.item ps
                        where t.id = :id $and ")->setParameters(['userId' => $userId, 'id' => $id])->getSingleResult();
    }

    /**
     * getUsers
     */
    public function getUsers(&$filters, $q, $userId, $isAdmin)
    {
        $name = $this->concat(array("u.name", "' '", "u.lastName", "' '", "u.mothersLastName", " '('", "u.email", "')'"));
        $where = " where $name like :q ";
        if (!$isAdmin) {
            $where .= " and u.roles like '%ROLE_CUSTOMER%' "; //For Vendors we show only customer users
        }
        $query = "select u.id, u.id as value, $name  as text
                    from E:User u $where ";
        return $this->getResultPaginated($query, ["q" => '%' . $q . '%'], $filters);
    }

    /**
     * getNotificationUserOrderRecordItem, incluye paginación
     */
    public function getNotificationUserOrders(&$filters, $userId, $storeId, $q, $typeId)
    {
        $query = "select pps.id as value, CONCAT(ps.name, CONCAT(' - Pedido folio: ', p.folio)) as text, p.id as orderRecordId, pps.registerDate
                    from E:OrderRecordItem pps
                    join pps.item ps
                    join pps.orderRecord p
                    join p.store s                         
                    join p.user u
                    where u.id = :userId
                      and s.id = :storeId
                      and (ps.name like :q or p.folio like :q)
                      and pps.id not in (select COALESCE(IDENTITY(n.orderRecordItem), 0)
                                           from E:Notification n
                                          where IDENTITY(n.type) = :typeId)";
        return $this->getResultPaginated($query, ["userId" => $userId, "storeId" => $storeId, "q" => '%' . $q . '%', 'typeId' => $typeId], $filters);
    }
}
