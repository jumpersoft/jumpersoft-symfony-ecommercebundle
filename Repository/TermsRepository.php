<?php

namespace Jumpersoft\EcommerceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class TermsRepository extends JumpersoftEntityRepository
{

    /**
     * getTerms
     */
    public function getTerms(&$filters, $userId, $storeId)
    {
        $query = 'select t.id, t.description, SUBSTRING(t.text, 1, 20) as text, t.active, t.paymentDays, t.registerDate, t.updateDate, tt.name as type, tt.id as typeId
                    from E:Terms t
                    join t.type tt
                    join t.store s                    
                    join s.users u
                    where u.id = :userId and s.id = :storeId
                    ORDER BY t.id DESC';
        return $this->getResultPaginated($query, ["userId" => $userId, "storeId" => $storeId], $filters);
    }

    /**
     * getTerm
     */
    public function getTerm($id, $userId, $storeId, $for = "view")
    {
        if ($for == "view") {
            $select = "select t.id, t.paymentDays, t.description, t.text, t.active, tt.name as type, tt.id as typeId ";
        } else {
            $select = $for == "update" ? "select t " : "select  partial t.{id} ";
        }

        $query = $this->getEntityManager()->createQuery($select .
                        " from E:Terms t
                          join t.type tt
                          join t.store s                          
                          join s.users u                         
                          where t.id = :id 
                            and u.id = :userId 
                            and s.id = :storeId")->setParameters(["id" => $id, "userId" => $userId, "storeId" => $storeId]);
        return $query->getSingleResult();
    }
}
